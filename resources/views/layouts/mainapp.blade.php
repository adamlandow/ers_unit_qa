<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @yield('title')
    </title>
    @include('layouts.includes')

</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"
         @if(config('app.env')=='dev')
         style="background-color: darkred;"
            @endif
    >
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">{{config('app.name')}}</a>
        </div>
        <ul class="nav navbar-right top-nav">
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">


                @if (!Auth::guest())
                    @can('is_admin')
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i>&nbsp;System
                            Administration <b class="caret"></b></a>
                        <ul class="dropdown-menu alert-dropdown">
                            <li>
                                <a href="{{ url('/user') }}"><i class="fa fa-users"></i>&nbsp;System Users setup</a>
                            </li>
                            <li>
                                <a href="{{ url('/setup/roles') }}"><i class="fa fa-eye-slash"></i>&nbsp;System Roles
                                    setup</a>
                            </li>

                            <li>
                                <a href="{{ url('unit') }}"><i class="fa fa-list-ul"></i>
                                    Manage units
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('course') }}"><i class="fa fa-list-ul"></i>
                                    Manage courses
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/setup/lookups') }}"><i class="fa fa-list-ul"></i>&nbsp;Lookups</a>
                            </li>

                            </li>

                        </ul>
                    </li>
                    @endcan
                    {{--<li><a href="{{ url('/auth/login') }}">Login</a></li>--}}
                    {{----}}
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Logged in as: <b>{{ Auth::user()->name }} </b><span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @if (session()->has('sudosu.has_sudoed'))
                                {{--{{dd(session('sudosu.has_sudoed'))}}--}}
                                <li>

                                    <form action="{{ route('sudosu.return') }}" method="post">
                                        {!! csrf_field() !!}
                                        <button class="btn" type="submit">
                                            <i class="fa fa-btn fa-undo"></i>&nbsp; Return
                                            to {{\App\User::findOrFail(session('sudosu.original_id'))->name}} </button></button>
                                    </form>
                                </li>

                            @endif
                            <li>
                                <form action="{{ url('/logout') }}">
                                    <button type="submit" class="btn" style="width: 100%" value="Logout">Logout<i
                                                class="fa fa-btn fa-sign-out"></i></button>
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endif
                    </li>
            </ul>

        </ul>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav side-nav">
                @yield('menu')
            </ul>
        </div>
    </nav>

    <div id="page-wrapper">

        @yield('content')
                <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>


</body>
</html>
