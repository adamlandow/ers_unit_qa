

<!-- Styles -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/bootstrap.min.css')}}">

<!-- Optional theme -->
{{--
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/bootstrap-theme.min.css')}}">
--}}
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/jquery-ui.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/sb-admin.css')}}">
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/bootstrap-datepicker3.min.css')}}">
{{--Select2 dropdowns--}}
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/select2.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/select2-bootstrap.css')}}">
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/sortertheme.css')}}">

<link rel="stylesheet" href="{{URL::asset('resources/assets/css/metisMenu.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/timeline.css')}}">
{{--Fancy font and icon support- see http://fontawesome.io/--}}
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/font-awesome.min.css')}}">
{{--Drag and drop re-odering- see https://github.com/bevacqua/dragula--}}
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/dragula.min.css')}}">
 {{--TouchSpin numeric spinner. http://www.virtuosoft.eu/code/bootstrap-touchspin/--}}
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/jquery.bootstrap-touchspin.min.css')}}">
{{--Checkbox enhancements. https://github.com/flatlogic/awesome-bootstrap-checkbox--}}
<link rel="stylesheet" href="{{URL::asset('resources/assets/css/awesome-bootstrap-checkbox.css')}}">

<!-- JavaScripts -->
<script src="{{ URL::asset('resources/assets/js/jquery-2.2.3.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/js/jquery-ui.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/js/validator.js') }}"></script>
<script src="{{ URL::asset('resources/assets/js/waiting.js') }}"></script>
<script src="{{ URL::asset('resources/assets/js/values.js') }}"></script>
{{--Select2 dropdowns--}}
<script src="{{ URL::asset('resources/assets/js/select2.full.min.js') }}"></script>
<script src="{{ URL::asset('resources/assets/js/metisMenu.min.js') }}"></script>
{{--Drag and drop re-odering- see https://github.com/bevacqua/dragula--}}
<script src="{{ URL::asset('resources/assets/js/dragula.min.js') }}"></script>
{{--TouchSpin numeric spinner. http://www.virtuosoft.eu/code/bootstrap-touchspin/--}}
<script src="{{ URL::asset('resources/assets/js/jquery.bootstrap-touchspin.min.js') }}"></script>