<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->


<li class="active">
    <a href="{{ url('/unitsetup') }}"><i class="fa fa-fw fa-cog"></i> Offerings Setup</a>
</li>
<li>
    <a href="{{ url('/unitreviews') }}"><i class="fa fa-fw fa-cog"></i> Unit Reviews</a>
</li>
{{--<li>--}}
    {{--<a href="{{ url('/unitarchive') }}"><i class="fa fa-fw fa-archive"></i> Unit Setup Archive</a>--}}
{{--</li>--}}
{{--<li>--}}
    {{--<a href="{{ url('/reviewarchive') }}"><i class="fa fa-fw fa-archive"></i> Review Archive</a>--}}
{{--</li>--}}


<!-- /.navbar-collapse -->