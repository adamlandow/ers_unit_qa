<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.includes')
    <title>
        @yield('title')
    </title>

    <!-- Styles -->


    <!-- Scripts -->
    <script>
        window.Laravel = '<?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>'
    </script>
</head>
<body style="height:auto; min-height: 100%;">
<div id="wrapper" style="height:auto;">
    {{--Swap out for dev--}}
    {{--<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color: orangered">--}}
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"
@if(config('app.env')=='dev')
style="background-color: darkred;"
@endif
>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            {{--<a class="navbar-brand" href="{{ url('/') }}">ERS QA 2020</a>--}}
            <a class="navbar-brand" href="{{ url('/') }}">{{config('app.name')}}</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">


                @if (!Auth::guest())
                    @can('is_admin')
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i>&nbsp;System
                                Administration <b class="caret"></b></a>
                            <ul class="dropdown-menu alert-dropdown">
                                <li>
                                    <a href="{{ url('/user') }}"><i class="fa fa-users"></i>&nbsp;System Users setup</a>
                                </li>
                                <li>
                                    <a href="{{ url('/setup/roles') }}"><i class="fa fa-eye-slash"></i>&nbsp;System
                                        Roles
                                        setup</a>
                                </li>

                                <li>
                                    <a href="{{ url('unit') }}"><i class="fa fa-list-ul"></i>
                                        Manage units
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('course') }}"><i class="fa fa-list-ul"></i>
                                        Manage courses
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/setup/lookups') }}"><i class="fa fa-list-ul"></i>&nbsp;Lookups</a>
                                </li>

                                </li>

                            </ul>
                        </li>
                    @endcan
                    {{--<li><a href="{{ url('/auth/login') }}">Login</a></li>--}}
                    {{----}}
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Logged in as: <b>{{ Auth::user()->name }} </b><span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">

                            @if (session()->has('sudosu.has_sudoed'))
                                {{--{{dd(session('sudosu.has_sudoed'))}}--}}
                                <li>

                                    <form action="{{ route('sudosu.return') }}" method="post">
                                        {!! csrf_field() !!}
                                        <button class="btn" type="submit">
                                            <i class="fa fa-btn fa-undo"></i>&nbsp; Return
                                            to {{\App\User::findOrFail(session('sudosu.original_id'))->name}} </button></button>
                                    </form>
                                </li>

                            @endif
                            <li>
                                <form action="{{ url('/logout') }}">
                                    <button type="submit" class="btn" style="width: 100%" value="Logout"><i
                                                class="fa fa-btn fa-sign-out"></i>&nbsp; Logout</button>
                                </form>
                            </li>
                        </ul>
                    </li>
                    @endif
                    </li>
            </ul>
            @yield('menu')
        </ul>
    </nav>
</div>
<div id="page-wrapper">
    <div class="container-fluid">
        @yield('content')
    </div>
</div>
</body>
</html>
