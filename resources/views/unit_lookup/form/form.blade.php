{{--This is the main form for the Patient class. It handles creation and update functionality--}}
{{--TODO format this properly- it'll end up as an injected jQuery dialog I reckon...--}}

        <!-- Temp bodge -->
{{--{!! Form::hidden('user_id', Auth::user()->id) !!}--}}

<div class="form-group row">
    {!! Form::label('type', 'Login Type', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <select class="select2 form-control" name="type" style="width: 300px" onchange='changevalidation("{{$formid}}")'>
            <option value='ad'>Institution Login</option>
            <option value='manual'>Manual</option>
        </select>
        {{--{!! Form::select('type', array('ad' => 'Institution Login', 'manual' => 'Manual'), null, ['class'=>'form-control' , 'required', 'onchange'=>'changevalidation(this.form)']) !!}--}}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('username', 'User Login', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="text" name="username" class='form-control' required>
    </div>
</div>
<div class="form-group row">
    {!! Form::label('name', 'User Full Name', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="text" name="name" class='form-control' required>
    </div>
</div>
<div class="form-group row">
    {!! Form::label('role_id', 'Role', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <select class="select2 form-control" name="role_id" style="width: 300px">
        <option value="-1" >None</option>
        @foreach ($roles as $role)
            <option value='{{$role->id}}'>{{$role->text}}</option>
        @endforeach
    </select>
</div>

<div class="form-group row">
    {!! Form::label('password1', 'Password', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="password" class='form-control'  onchange="changevalidation('{{$formid}}')" onblur="changevalidation('{{$formid}}')" )">

    </div>
</div>
<div class="form-group row">
    {!! Form::label('password2', 'Confirm', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="password" class='form-control' name="password"  data-match='#{{$formid}} :password:first' onchange="changevalidation('{{$formid}}')" onblur="changevalidation('{{$formid}}')"
               data-match-error="Whoops, these don't match" placeholder="Confirm">
        <div class="help-block with-errors"></div>
    </div>

</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>