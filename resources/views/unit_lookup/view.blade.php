@extends('layouts.app')

@section('content')
    <script>
        var currenteditingcontactid = 0;
        var currenteditingconditionid = 0;
        var currentmediaid = 0;

        $(document).ready(function () {

            $('select').select2();



        });


    </script>

    <!-- Tabs -->
    {!! Breadcrumbs::render('user.show', $user) !!}
    <ul class="nav nav-tabs" id="tabslabels">
        <li class="active"><a data-toggle="tab" href='#detailstab'>Patient Details</a></li>
        <li><a data-toggle="tab" href='#contacttab'>Patient Contact History</a></li>
        <li><a data-toggle="tab" href='#conditionstab'>Patient Conditions</a></li>
        <li><a data-toggle="tab" href='#mediatab'>Associated Media</a></li>
    </ul>
    <div class="tab-content">
        <div id="detailstab" class="tab-pane active">
            <fieldset style="width: 90%">
                <legend>Patient details...
                    <button id="edit_patient_but" class="btn btn-info btn-lg" data-toggle="modal"
                            data-target="#editpatientdialog">Edit
                    </button>
                </legend>
                <div style="display: table; float: left; width: 40%">
                    <div style="display: table-row" class="row" class="labelcell">
                        <div style="display: table-cell">
                            <strong>Name:</strong>
                        </div>
                        <div style="display: table-cell">
                            {{ $patient->title}} {{ $patient->fname}} {{$patient->lname}}
                        </div>
                    </div>
                    <div style="display: table-row" class="row" class="labelcell">
                        <div style="display: table-cell">
                            <strong>Gender:</strong>
                        </div>
                        <div style="display: table-cell">
                            {{ $patient->gender}}
                        </div>
                    </div>
                    <div style="display: table-row" class="row" class="labelcell">
                        <div style="display: table-cell">
                            <strong>Birthdate:</strong>
                        </div>
                        <div style="display: table-cell">
                            {{ $patient->dob}}
                        </div>
                    </div>
                    <div style="display: table-row" class="row" class="labelcell">
                        <div style="display: table-cell">
                            <strong>Doctor:</strong>
                        </div>
                        <div style="display: table-cell">
                            {{$patient->doctor->name}}
                        </div>
                    </div>
                    <div style="display: table-row" class="row" class="labelcell">
                        <div style="display: table-cell">
                            <strong>Primary Phone:</strong>
                        </div>
                        <div style="display: table-cell">
                            {{$patient->phone}}
                        </div>
                    </div>
                    <div style="display: table-row" class="row">
                        <div style="display: table-cell" class="labelcell">
                            <strong>Alternate Phone:</strong>
                        </div>
                        <div style="display: table-cell">
                            {{$patient->altphone}}
                        </div>
                    </div>
                    <div style="display: table-row" class="row">
                        <div style="display: table-cell" class="labelcell">
                            <strong>Email:</strong>
                        </div>
                        <div style="display: table-cell">
                            {{$patient->email}}
                        </div>
                    </div>
                    <div style="display: table-row" class="row">
                        <div style="display: table-cell" class="labelcell">
                            <strong>Address:</strong>
                        </div>
                        <div style="display: table-cell">
                            {{$patient->address}}: {{$patient->pcode}}
                        </div>
                    </div>
                    <div style="display: table-row" class="row">
                        <div style="display: table-cell">
                            <strong>Last used:</strong>
                        </div>
                        <div style="display: table-cell">
                            @if(count($patient->contact_history)>0)
                                {{$patient->contact_history->last()->created_at->format('j/m/Y')}}
                            @else
                                Never used
                            @endif
                        </div>
                    </div>

                </div>
                <div style="display: table; float: left; width: 59%">
                    <div style="display: table-row" class="row">
                        <div style="display: table-cell" class="labelcell">
                            <strong>Status:</strong>
                        </div>
                        <div style="display: table-cell;  <?php print("background-color:" . ($patient->status == 'OK' ? '#C8FFC8' : ($patient->status == 'Temporarily Unavailable' ? '#FFB733' : '#FF9999'))); ?>">
                            {{$patient->status}}
                        </div>
                    </div>
                    <div style="display: table-row" class="row">
                        <div style="display: table-cell" class="labelcell">
                            <strong>Level:</strong>
                        </div>
                        <div style="display: table-cell;">
                            {{$patient->level}}
                        </div>
                    </div>
                    <div style="display: table-row" class="row">
                        <div style="display: table-cell" class="labelcell">
                            <strong>Notes:</strong>
                        </div>
                        <div style="display: table-cell;">
                            {{$patient->notes}}
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>

        <div id="contacttab" class="tab-pane">
            <fieldset style="width: 90%">
                <legend>Contact history/Experience
                    <button id="new_contact_but" class="btn btn-info btn-lg" data-toggle="modal"
                            data-target="#newcontactdialog">Add
                    </button>
                </legend>
                <table class="table table-striped">
                    <thead class="thead-inverse">
                    <th>
                        Contacted By
                    </th>
                    <th>
                        Role
                    </th>
                    <th>
                        Sim or real
                    </th>
                    <th>
                        Unit
                    </th>
                    <th>
                        Date
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                        Delete
                    </th>
                    </thead>
                    @foreach($patient->contact_history as $contact )
                        <tr>
                            <td>
                                {{$contact->contacted_by['name']}}
                            </td>
                            <td>
                                {{$contact->patient_role['text']}}
                            </td>
                            <td>
                                @if($contact->sim=='true')
                                    Sim
                                @else
                                    Real
                                @endif
                            </td>
                            <td>
                                {{$contact->unit['text']}}
                            </td>
                            <td>
                                {{$contact->created_at}}
                            </td>
                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$contact->id}}"
                                   data-target="#editcontactdialog">
                                    Edit
                                </a>
                            </td>
                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$contact->id}}"
                                   data-target="#deletecontactdialog">
                                    Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </table>

            </fieldset>
        </div>

        <div id="conditionstab" class="tab-pane">
            <fieldset style="width: 90%">
                <legend>Contact history/Experience
                    <button id="new_condition_but" class="btn btn-info btn-lg" data-toggle="modal"
                            data-target="#newconditiondialog">Add
                    </button>
                </legend>

                <table class="table table-striped">
                    <thead class="thead-inverse">
                    <th>
                        Condition
                    </th>
                    <th>
                        Clinical Notes
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                        Delete
                    </th>
                    </thead>
                    @foreach($patient->conditions as $condition )
                        <tr>
                            <td>
                                {{$condition->text}}
                            </td>
                            <td>
                                {{$condition->pivot->notes}}
                            </td>

                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$condition->pivot->id}}"
                                   data-target="#editconditiondialog">
                                    Edit
                                </a>
                            </td>
                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$condition->pivot->id}}"
                                   data-target="#deleteconditiondialog">
                                    Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </table>

            </fieldset>
        </div>


        <div id="mediatab" class="tab-pane">
            <fieldset style="width: 90%">
                <legend>
                    Associated Media
                    <button id="new_file_but" class="btn btn-info btn-lg" data-toggle="modal"
                            data-target="#newmediadialog">Add
                    </button>
                </legend>
                <table class="table table-striped">
                    <thead class="thead-inverse">
                    <th>
                        Thumb
                    </th>
                    <th>
                        File name (click to download)
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                        Edit
                    </th>
                    <th>
                        Delete
                    </th>
                    </thead>
                    @foreach($patient->media as $media)
                        <tr>
                            <td>
                                <img src="{{URL::asset('media/thumb/'.$media->id)}}">
                            </td>
                            <td>
                                <a href="{{URL::asset('media/download/'.$media->id)}}">{{$media->name}}</a>
                            </td>
                            <td>
                                {{$media->comments}}
                            </td>
                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$media->id}}"
                                   data-target="#editmediadialog">
                                    Edit
                                </a>
                            </td>
                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$media->id}}"
                                   data-target="#deletemediadialog">
                                    Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </table>
            </fieldset>
        </div>

    </div>
    {{--
    If the user can update patients
    --}}
    @can('update-patients')
    {{--
        Dialogs for various things
        --}}

    {{--  Dialog for editing the patient details  --}}
    <div id="editpatientdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Patient Details</h4>
                </div>
                <div class="modal-body">
                    {!! Form::model($patient, ['class'=>'form-horizontal'])!!}

                    @include('patients.form.form', ['submitButtonText'=>'Update Patient', 'doctors'=>$doctors])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    {{--  Adding a contact event  --}}
    <div id="newcontactdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add contact event</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('patients.form.patientcontactentry', ['submitButtonText'=>'Add Contact Event', 'patientid'=>$patient->id])
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>

    {{--  Editing a contact event  --}}
    <div id="editcontactdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit contact event</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('patients.form.patientcontactentry', ['submitButtonText'=>'Update Contact Event', 'patientid'=>$patient->id])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Deleting a contact event  --}}
    <div id="deletecontactdialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete contact event?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('patients.form.patientcontactdelete', [ 'patientid'=>$patient->id])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Insert a new patient condition  --}}
    <div id="newconditiondialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Patient Condition</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('patients.form.patientconditionentry', ['submitButtonText'=>'Add Patient Condition', 'patientid'=>$patient->id])
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>

    {{--Edit a condition--}}
    <div id="editconditiondialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit patient condition</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('patients.form.patientconditionentry', ['submitButtonText'=>'Update Patient Condition', 'patientid'=>$patient->id])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Deleting a condition  --}}
    <div id="deleteconditiondialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete patient conditon?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('patients.form.patientconditiondelete', [ 'patientid'=>$patient->id])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    {{--Add a file to this record--}}
    <div id="newmediadialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add File</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('patients.form.patientfileupload', ['submitButtonText'=>'Add File', 'patientid'=>$patient->id, 'required'=>'required', 'fileuploadcontrolid'=>'upload_file', 'commentscontrolid'=>'filenotes'])
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>

    {{--Update a file--}}
    <div id="editmediadialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update File</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('patients.form.patientfileupload', ['submitButtonText'=>'Update File', 'patientid'=>$patient->id, 'required'=>'', 'fileuploadcontrolid'=>'altupload_file', 'commentscontrolid'=>'altfilenotes'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Deleting a media  --}}
    <div id="deletemediadialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete file?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('patients.form.patientfiledelete', [ 'patientid'=>$patient->id])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    @endcan

    @if(count($errors) > 0)
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    @endif



@stop
 