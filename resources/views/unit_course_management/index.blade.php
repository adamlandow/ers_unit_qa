@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{URL::asset('resources/assets/css/bootstrap-colorpicker.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('resources/assets/css/jquery.bootstrap-touchspin.min.css')}}">
    <script src="{{ URL::asset('resources/assets/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ URL::asset('resources/assets/js/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script>
        var currenteditingid = 0;

        var currentdeletingid = 0;
        var currentdeleteroute = "";

        $(document).ready(function () {

            $('select').select2();
            $('.colorpicker-component').colorpicker();
            $(".touchspin").TouchSpin({
                verticalbuttons: true,
                min: 0,
                max: 1000000000,
                boostat: 5,
                maxboostedstep: 100,
            });

            if (location.hash.substr(0, 2) == "#!") {
                $("a[href='#" + location.hash.substr(2) + "']").tab("show");
            }

            $("a[data-toggle='tab']").on("shown.bs.tab", function (e) {
                var hash = $(e.target).attr("href");
                if (hash.substr(0, 1) == "#") {
                    location.replace("#!" + hash.substr(1));
                }
            });

            $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
                currenteditingid = $(this).data('id');
                switch ($(this).data('target')) {

                    case '#editunitdialog':
                        getDetails(currenteditingid, 'unit', 'editunitdialog');
                        break;
                    case '#editcoursedialog':
                        getDetails(currenteditingid, 'course_lookup', 'editcoursedialog');
                        break;
                    case '#deletedialog':
                        currentdeleteroute = $(this).data('route');
                        console.log($(this).data('route'));
                        currentdeletingid = $(this).data('id');
                        break;
                    default:
                        currenteditingid = -1;
                        currentdeletingid = -1;
                        break;
                }

            });

            $('.modalform').on('shown.bs.modal', function () {
                $(this).find("form").validator();
            });

            $('.newdialog').submit(function (event) {
                event.preventDefault();
                $(this).modal('hide');
                console.log($(this).attr('model'));
                var vars = $(this).find("form").serializeArray();
                waitingDialog.show();
                submitNewForm(vars, $(this).attr('model'));
            });

            $('.editdialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();
                vars.push({name: 'id', value: currenteditingid});
                vars.push({name: '_method', value: 'PATCH'});

                waitingDialog.show();
                submitUpdateForm(vars, $(this).attr('model'));
            });

            // all deletes are the same
            $("#deletedialog").submit(function (event) {
                $(this).modal('hide');
                // cancels the form submission
                event.preventDefault();
                waitingDialog.show();
                deleteItem(currentdeletingid, currentdeleteroute);
            });

            // intercept upload event dialogue form submission
            $('#mapcoursedialog').submit(function (event) {
                console.log('submitting CSV')
                $('#mapcoursedialog').modal('hide');
                if ($('#upload_file')[0].files.length > 0) {

                    var data = new FormData();
                    // file
                    jQuery.each($('#upload_file')[0].files, function (i, file) {
                        data.append('csvfile', file);
                    });
                    //

                    data.append('_token', '{{ csrf_token() }}');
                    // console.log(data);
                    // cancels the form submission
                    event.preventDefault();
                    //   waitingDialog.show();
                    submitCSVForm(data);
                } else {
                    alert('Need to pick a file')
                }
            });

        });

        function getDetails(id, route, formid) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/' + id,
                type: 'GET',
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (data) {
                    $("[name='course_ids[]']").select2('val', [1, 3]).trigger('change');
                    populate($("#" + formid).find("form"), data);
                    waitingDialog.hide();
                }
            });
        }

        function submitNewForm(vars, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/create',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (Number(data) > 0) {
                        location.reload();

                    } else {
                        alert('something went wrong with the insertion: ' + data.toString());
                    }
                }
            });
        }

        // Edit condition
        function submitUpdateForm(vars, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/update',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    //   waitingDialog.hide();
                    if (data.status.toString() == "1") {
                       location.reload();

                    } else {
                        waitingDialog.hide();
                        alert('something went wrong with the update');
                    }
                }
            });
        }

        // Delete contact event
        function deleteItem(id, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/destroy',
                type: 'post',
                data: ([{name: 'id', value: id}, {name: '_method', value: 'DELETE'}, {
                    name: '_token',
                    value: '{{csrf_token()}}'
                }]),
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data == "1") {
                        location.reload();
                    } else {
                        alert('something went wrong with the delete');
                    }
                }
            });

        }

        function submitCSVForm(vars) {
            console.log('submitCSVForm');
            waitingDialog.show('Uploading...');
            $.ajax({
                url: '{{URL::to('setup/coursemapcsvupload')}}',
                type: 'post',
                data: vars,
                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status > 0) {
                        location.reload();
                    }else{
                        alert(data);
                    }
                }
            });
        }


        /*
         *
         * Helper functions
         *
         */

        // populates a form with data returned from Laravel
        function populate(frm, data) {

            $.each(data, function (key, value) {
                var $ctrl = $('[name="' + key + '"]', frm);
                if ($ctrl.is("select")) {
                    if ($ctrl.attr('multiple')) {
                        $ctrl.select2('val', value.split(',')).trigger('change');
                    } else {
                        $ctrl.select2('val', value);
                    }
                } else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":


                            $ctrl.val(value);
                            // special case if a colorpicker
                            if ($ctrl.parent().hasClass('colorpicker-component')) {
                                $ctrl.parent().colorpicker('setValue', value);
                            }
                            break;
                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).attr("checked", value);
                                }
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }
            });
        }

    </script>

    {{--{!! Breadcrumbs::render('systemlookups.index') !!}--}}

            <!-- Tabs -->

    <ul class="nav nav-tabs" id="tabslabels">
        <li class="active"><a data-toggle="tab" href='#unitstab'>Units</a></li>
        <li><a data-toggle="tab" href='#coursestab'>Courses/Majors</a></li>

    </ul>

    <div class="tab-content">
        <div id="unitstab" class="tab-pane active">
            <fieldset style="width: 90%">
                <legend>
                    Units
                    <button id="new_file_but" class="btn btn-info btn-lg" data-toggle="modal"
                                     data-target="#newunitdialog">Add
                    </button>

                </legend>
                <table class="table table-striped">
                    <thead class="thead-inverse">
                    <th>
                        Description
                    </th>

                    <th>
                        Last reviewed
                    </th>
                    <th>
                        Included in courses
                    </th>
                    <th>
                    </th>
                    </thead>
                    @foreach ($units as $unit)
                        <tr>
                            <td>  <a href="{{URL::to('unitcoursemanagement/unit/'.$unit->id)}}">{{$unit->unit_code}} ({{$unit->description}})</a></td>

                            <td>{{isset($unit->reviews->first()->end_date)?$unit->reviews->first()->end_date:''}}</td>
                            <td>
                                <ul>@foreach($unit->courses as $course)
                                        <li>{{$course->text}}</li>
                                    @endforeach</ul>
                            </td>

                            <td><a href="#" data-toggle="modal" data-id="{{$unit->id}}" data-route="unit"
                                   data-target="#deletedialog">Delete</a></td>
                        </tr>
                    @endforeach
                </table>
            </fieldset>
        </div>


        <div id="coursestab" class="tab-pane">
            <fieldset style="width: 90%">
                <legend>
                    Courses/Majors
                    <button id="new_file_but" class="btn btn-info btn-lg" data-toggle="modal"
                            data-target="#newcoursedialog">Add
                    </button>
                    <button  class="btn btn-info btn-lg" data-toggle="modal"
                            data-target="#mapcoursedialog">Map using CSV
                    </button>
                </legend>
                <table class="table table-striped">
                    <thead class="thead-inverse">
                    <th>
                        Description
                    </th>
                    <th>
                        TRIM reference(s)
                    </th>

                    <th>

                    </th>
                    </thead>
                    @foreach ($courses as $course)
                        <tr>
                            <td><a href="#" data-toggle="modal" data-id="{{$course->id}}"
                                   data-route="courselookup"
                                   data-target="#editcoursedialog">{{$course->text}}</a></td>
                            <td>{{$course->trim_reference}}</td>
                            <td><a href="#" data-toggle="modal" data-id="{{$course->id}}" data-route="course_lookup"
                                   data-target="#deletedialog">Delete</a></td>
                        </tr>
                    @endforeach
                </table>
            </fieldset>
        </div>

    </div>




    {{--
        Dialogs for various things
        --}}


    <div id="newunitdialog" class="modal fade modalform newdialog" role="dialog" model="unit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Unit Lookup</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('setup.lookups.form.unitlookupentry', ['submitButtonText'=>'Add Unit'])
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>

    <div id="editunitdialog" class="modal fade modalform editdialog" role="dialog" model="unit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit unit</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('setup.lookups.form.unitlookupentry', ['submitButtonText'=>'Update', 'users'=>$users])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div id="newcoursedialog" class="modal fade modalform newdialog" role="dialog" model="course_lookup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Course/Major lookup</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('unit_course_management.form.courselookupentry', ['submitButtonText'=>'Fieldofstudy'])
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>

    <div id="editcoursedialog" class="modal fade modalform editdialog" role="dialog" model="course_lookup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit course</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('unit_course_management.form.courselookupentry', ['submitButtonText'=>'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>



    {{--  Deleting an item  --}}
    <div id="deletedialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('form_common.deletedialog')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div id="mapcoursedialog" class="modal fade  modalform" role="dialog"
         >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Map using CSV file</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['class'=>'form-horizontal', 'role'=>'form'])!!}
                    @include('unit_course_management.form.coursemapcsvupload')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop
 