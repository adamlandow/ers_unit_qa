@extends('layouts.app')

@section('content')
    <style>
        table.note,
        table.note tr,
        table.note td {
            border: 1px solid black;
        }

        table.note {
            border-collapse: collapse;
        }

        table.table-striped td {
            border-left: solid 1px gray;

        }

        @if(($review->ld['id']==Auth::user()->id))
        table.table-striped tr td:nth-child(2) {
            width: 30%;
        }

        div.handle {
            width: 1%;
            white-space: nowrap;
            cursor: grab;
        }

        div.handle:active {
            width: 1%;
            white-space: nowrap;
            cursor: grabbing;
        }

        @else
        table.table-striped tr td:first-child {
            min-width: 10%;
            width: 30%;

        }

        @endif
    </style>
    <script src="{{ URL::asset('resources/assets/js/tinymce/tinymce.min.js') }}"></script>
    <script>

        //tinymce Bootstrap fix
        // https://www.tinymce.com/docs/integrations/bootstrap/
        $(document).on('focusin', function (e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });

        // init document
        $(document).ready(function () {

            // set up select2 dropdowns
            $('select').select2();

            // init timyMCE
            tinymce.init({
                selector: '.tinymce',
                height: 500,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                content_css: '//www.tinymce.com/css/codepen.min.css'
            });

            // Set up tab persistence across reloads
            if (location.hash.substr(0, 2) == "#!") {
                $("a[href='#" + location.hash.substr(2) + "']").tab("show");
            }

            $("a[data-toggle='tab']").on("shown.bs.tab", function (e) {
                var hash = $(e.target).attr("href");
                if (hash.substr(0, 1) == "#") {
                    location.replace("#!" + hash.substr(1));
                }
            });



        });



        /*
         *
         * Helper functions
         *
         */



        // populates a form with data returned from Laravel
        function populate(frm, data) {
            $.each(data, function (key, value) {
                var $ctrl = $('[name=' + key + ']', frm);
                if ($ctrl.is("select")) {
                    $ctrl.select2('val', value);
                } else if ($ctrl.is("textarea")) {
                    if ($ctrl.hasClass('tinymce')) {
                        // fix for this weird bug where tinymce chucks a wobbly if the content is set to a null value
                        tinyMCE.get($ctrl.attr('id')).setContent((value === null) ? '' : value);
                    } else {
                        $ctrl.val((value === null) ? '' : value);
                    }
                }
                else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":
                            $ctrl.val(value);
                            $ctrl.trigger('change');
                            break;

                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).attr("checked", value);
                                }
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }

            });
        }

        function getDetails(id, route, formid) {
            //  console.log('getDetails(' + id + "," + route + "," + formid + ")");
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/' + id,
                type: 'GET',
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (data) {
                    //      console.log(data);
                    populate($("#" + formid).find("form"), data);
                    waitingDialog.hide();
                }
            });
        }

        // Edit
        function submitUpdateForm(vars, route) {
            // console.log(route);
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/update',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    //   waitingDialog.hide();
                    if (data.status.toString() == "1") {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert('something went wrong with the update');
                    }
                }
            });
        }


        function submitNewForm(vars, route) {
            vars.push({name: 'review_instances_id', value: '{{$review->id}}'})

            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/create',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (Number(data.id) > 0) {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the insertion');
                    }
                }
            });
        }


        // Delete contact event
        function deleteItem(id, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/destroy',
                type: 'post',
                data: ([{name: 'id', value: id}, {name: '_method', value: 'DELETE'}, {
                    name: '_token',
                    value: '{{csrf_token()}}'
                }]),
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status == "1") {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the delete');
                    }
                }
            });
        }




    </script>

    <!-- Tabs -->
    {{--{!! Breadcrumbs::render('unitreviewstemplates.show', $review) !!}--}}
    <div class="col-md-12 page-header" style="margin-top: 0px">

        <div class="box box-element">
            <div class="view">
                <div class="row">
                    <div class="container col-md-4">

                        <h3 style="margin-top: 0px">
                            {{$unit->description}} ({{$unit->code}})
                            <a href="#" data-toggle="modal" data-target="#editunitdialog">(Update)</a>

                        </h3>


                    </div>


                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div id="coursetab" class="tab-pane active">
            <fieldset style="width: 100%">
                <legend>Core courses...
                    @can('update_review_instance')
                    <button class="btn btn-info btn-lg" data-toggle="modal"
                            data-target="#addcoursetounitdialog">New item
                    </button>
                    @endcan
                </legend>
                <table class="table table-striped" style="overflow-y: auto; overflow-x: hidden">
                    <thead class="thead-inverse">


                    <th>
                        Course(click to update)

                    </th>


                    <th>
                    </th>

                    </thead>
                    <tbody id="corecourselisttablebody">
                    @foreach($unit->courses as $course)

                            <tr class='sortablerow' entryid='{{$course->id}}'>

                                <td>

                                    <a href="#" data-toggle="modal"
                                       data-id="{{$course->id}}"
                                       data-route=""
                                       data-target="">
                                        {!! $course->text!!}
                                    </a>

                                </td>

                                <td>
                                    <a href="#" data-toggle="modal" data-id="{{$course->id}}"
                                       data-route=""
                                       data-target="#deletedialog">
                                        <i class="fa fa-2x fa-times-circle" style='color: #ff6600'></i>
                                    </a>
                                </td>

                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </fieldset>
        </div>


    </div>

    {{--
    Dialogs for various things
    --}}

    <div id="editunitdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit template label</h4>
                </div>
                <div class="modal-body">

                    {!! Form::model($review, ['class'=>'form-horizontal'])!!}

                    {{--@include('reviews.templates.form.uniteditform', ['submitButtonText'=>'Update template label'])--}}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>




    {{--  Adding a contact event  --}}
    <div id="newchecklistitemdialog" class="modal fade newdialog" role="dialog"
         data-route="reviewinstancechecklistitem">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add checklist item</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    {{--@include('reviews.templates.form.checklistitemform')--}}
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>

    {{--  Deleting a media  --}}
    <div id="deletedialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('form_common.deletedialog', [ 'id'=>$review->id])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    {{--  Editing a list itemt  --}}
    <div id="editchecklistitemdialog" class="modal fade editdialog" role="dialog"
         data-route="reviewinstancechecklistitem">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit checklist item</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    {{--@include('reviews.templates.form.checklistitemupdateform', ['submitButtonText'=>'Update'])--}}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    @if(count($errors) > 0)
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    @endif



@stop
