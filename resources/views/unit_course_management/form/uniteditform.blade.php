<div class="form-group">

    {!! Form::label('unit_code', 'Unit Code') !!}
    {!! Form::text('unit_code', null, ['class'=>'form-control', 'required',]) !!}
</div>
<div class="form-group">

    {!! Form::label('description', 'Description') !!}
    {!! Form::text('description', null, ['class'=>'form-control', 'required',]) !!}
</div>

<div class="form-group">
    {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>



