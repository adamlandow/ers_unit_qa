<div class="form-group">

    {!! Form::label('text', 'Description') !!}
    {!! Form::text('text', null, ['class'=>'form-control', 'required',]) !!}
</div>
<div class="form-group">

    {!! Form::label('trim_reference', 'TRIM reference(s)') !!}
    {!! Form::text('trim_reference', null, ['class'=>'form-control']) !!}
</div>


<div class="form-group">
    {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>



