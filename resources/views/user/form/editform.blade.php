
<div class="form-group row">
    {!! Form::label('edit_type', 'Login Type', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <select class="select2 form-control" onchange="changevalidation(this.form)" name="type" style="width: 300px" required>
            <option value='ad'>Institution Login</option>
            <option value='manual'>Manual</option>
        </select>
        {{--{!! Form::select('type', array('ad' => 'Institution Login', 'manual' => 'Manual'), null, ['class'=>'form-control select2' , 'required', 'onchange'=>'changevalidation(this.form)', 'id'=>'type']) !!}--}}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('username', 'User Login', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::text('username', null, ['class'=>'form-control' , 'required']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('title', 'Title', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="text" name="title" id="title" class='form-control' >

    </div>
</div>
<div class="form-group row">
    {!! Form::label('name', 'User Full Name', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::text('name', null, ['class'=>'form-control' , 'required']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('role_id', 'Role(s)', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <select class="select2 form-control" name="role_id[]" style="width: 300px" required multiple="multiple">
        <option >None</option>
        @foreach ($roles as $role)
            <option value='{{$role->id}}'>{{$role->text}}</option>
        @endforeach
    </select>
</div>
<div class="form-group row">
    {!! Form::label('role_id', 'School(s)', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <select class="select2 form-control" name="school_id[]" style="width: 300px" required multiple="multiple">
        <option >None</option>
        @foreach ($schools as $school)
            <option value='{{$school->id}}'>{{$school->description}}</option>
        @endforeach
    </select>
</div>
<div class="form-group row">
    {!! Form::label('url', 'Home page URL', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="url" name="url" class='form-control'>
    </div>
</div>
<div class="form-group row">
    {!! Form::label('password1', 'Password', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="password" id="password1" class='form-control' data-minlength="6" required>
        <div class="help-block">Minimum of 6 characters</div>
    </div>
</div>
<div class="form-group row">
    {!! Form::label('password2', 'Confirm', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="password" class='form-control' id="password2" required data-match='#password1' data-match-error= "Whoops, these don't match" placeholder="Confirm">
        <div class="help-block with-errors"></div>
    </div>

</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>