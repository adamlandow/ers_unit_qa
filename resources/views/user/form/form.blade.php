<div class="form-group row">
    {!! Form::label('type', 'Login Type', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <select class="select2 form-control" name="type" style="width: 300px" onchange='changevalidation("{{$formid}}")'>
            <option value='ad'>Institution Login</option>
            <option value='manual'>Manual</option>
        </select>
        {{--{!! Form::select('type', array('ad' => 'Institution Login', 'manual' => 'Manual'), null, ['class'=>'form-control' , 'required', 'onchange'=>'changevalidation(this.form)']) !!}--}}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('username', 'User Login', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="text" name="username" class='form-control' required>
    </div>
</div>

<div class="form-group row">
    {!! Form::label('name', 'User Full Name', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="text" name="name" class='form-control' required>
    </div>
</div>
<div class="form-group row">
    {!! Form::label('title', 'Title', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="text" name="title" id="title" class='form-control' >
    </div>
</div>
<div class="form-group row">
    {!! Form::label('role_id[]', 'Role(s)', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <select class="select2 form-control" name="role_id[]" style="width: 300px" multiple="multiple">

        @foreach ($roles as $role)
            <option value='{{$role->id}}'>{{$role->text}}</option>
        @endforeach
    </select>
</div>
<div class="form-group row">
    {!! Form::label('role_id', 'School(s)', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <select class="select2 form-control" name="school_id[]" style="width: 300px" required multiple="multiple">
        <option >None</option>
        @foreach ($schools as $school)
            <option value='{{$school->id}}'>{{$school->description}}</option>
        @endforeach
    </select>
</div>
<div class="form-group row">
    {!! Form::label('url', 'Home page URL', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="url" name="url" class='form-control' >
    </div>
</div>
<div class="form-group row">
    {!! Form::label('password1', 'Password', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="password" class='form-control'  onchange="changevalidation('{{$formid}}')" onblur="changevalidation('{{$formid}}')" )">

    </div>
</div>
<div class="form-group row">
    {!! Form::label('password2', 'Confirm', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <input type="password" class='form-control' name="password"  data-match='#{{$formid}} :password:first' onchange="changevalidation('{{$formid}}')" onblur="changevalidation('{{$formid}}')"
               data-match-error="Whoops, these don't match" placeholder="Confirm">
        <div class="help-block with-errors"></div>
    </div>

</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>