@extends('layouts.app')
@section('title')System Users @endsection
@section('content')
    <script>

        var currentid = -1;

        $(document).ready(function () {

            $('select').select2({
                // placeholder: 'Select an option'
                placeholderOption: 'first'
            });


            $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
                if ((typeof $(this).data('id')) !== 'undefined') {
                    currentid = $(this).data('id');
                    if ($(this).data('target') == "#editdialog") {
                        getUserDetails(currentid);
                    }
                }
            });


            $('#newuserdialog').on('shown.bs.modal', function () {
                //$("#newuserform").validator();
                changevalidation('newuserdialog');
            });


            // New patient event dialogue
            $('#newuserdialog').submit(function (event) {
                console.log('submitting n')
                $('#newuserdialog').modal('hide');

                var vars = $("#newuserform").serializeArray();
                // vars.push({name: 'roles', value: $("#newuserform :input[name='role_id']").val()});
                console.log(vars);
                // cancels the form submission
                event.preventDefault();
                waitingDialog.show();
                submitNewUserForm(vars);
            });

            $('#editdialog').on('shown.bs.modal', function () {
                $("#edituserform").validator();
            });


            // Update patient dialogue
            $('#editdialog').submit(function (event) {
                $('#editdialog').modal('hide');
                var vars = $("#edituserform").serializeArray();
                // little hack to get the multiple values in from the role input dialog box
                // vars.push({name: 'roles', value: $("#edituserform :input[name='role_id']").val()});
                vars.push({name: 'id', value: currentid});
                // cancels the form submission so that we can submit it using AJAX
                event.preventDefault();
                waitingDialog.show();
                submitUpdateUserForm(vars);
            });

            $('#deletedialog').submit(function (event) {
                $('#deletedialog').modal('hide');
                var vars = $("#deletedialog").find("form").serializeArray();
                vars.push({name: 'id', value: currentid});
                //vars.push({name: '_method', value: 'DELETE'});
                // cancels the form submission
                event.preventDefault();
                waitingDialog.show();
                deleteUser(vars);
            });

        });


        /*
         *
         * New user AJAX
         *
         */
        function submitNewUserForm(vars) {
            $.ajax({
                url: '{{URL::to('user/ajaxstore')}}',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    console.log(data.status)
                    if (data.status > 0) {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert(data.statusText);
                    }
                }
            });
        }

        // update user AJAX
        // Get condition
        function getUserDetails(id) {
            console.log('getUserDetails:' + id);
            $.ajax({
                url: '{!! URL::to('user')!!}/' + id,
                type: 'GET',
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (data) {
                    //  console.log(data);
                    //$("#editcontactdialog").find("form").values(data);
                    populate($("#edituserform"), data);
                    changevalidation('edituserform');
                    waitingDialog.hide();
                }
            });
        }

        function submitUpdateUserForm(vars) {
            $.ajax({
                url: '{{URL::to('user/ajaxupdate')}}',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    if (data.status > 0) {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert(data.statusText);
                    }
                }
            });
        }

        function changevalidation(frm) {
            // change the validation scheme
            $("#" + frm).validator('destroy');

            //$("#" + frm).validator('destroy')
            if ($('#' + frm + ' [name="type"]').select2('val') == 'ad') {
                $('#' + frm + ' :password').removeAttr('required').prop('disabled', true);
                $('#' + frm + ' :password').val('');
                $('#' + frm + ' [name="name"]').removeAttr('required').prop('disabled', true);
            } else {
                $('#' + frm + ' :password').attr('required', true).prop('disabled', false);
                $('#' + frm + ' [name="name"]').attr('required', true).prop('disabled', false);
                if (frm == 'edituserform') {
                    if ($('#edituserform :password:first').val().length > 0) {
                        $('#edituserform :password').attr('required', true);
                        //$('#edituserform :password:first').next().attr('required', true);
                    } else {
                        console.log('empty password')

                        $('#edituserform :password').removeAttr('required');
                        $('#edituserform :password').val('');
                    }
                }

            }
            $("#" + frm).validator({'disable': true});
        }


        function deleteUser(vars) {
            $.ajax({
                url: '{{URL::to('user/ajaxdestroy')}}',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data == "1") {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the delete');
                    }
                }
            });
        }


        /*
         *
         * Helper functions
         *
         */

        // populates a form with data returned from Laravel
        function populate(frm, data) {
            console.log('Populating')
            $.each(data, function (key, value) {
                var $ctrl = $('[name="' + key + '"]', frm);
                if ($ctrl.is("select")) {
                    if ($ctrl.attr('multiple')) {
                        console.log(key, value)
                        $ctrl.val(value.split(',')).trigger('change');
                    } else {
                        console.log(key, value)
                        $ctrl.val(value).trigger('change');
                    }
                } else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":


                            $ctrl.val(value);
                            // special case if a colorpicker
                            if ($ctrl.parent().hasClass('colorpicker-component')) {
                                $ctrl.parent().colorpicker('setValue', value);
                            }
                            break;
                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).attr("checked", value);
                                }
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }
            });
        }

    </script>
    {!! Breadcrumbs::render('user.index') !!}
    <fieldset style="padding-left: 15px; padding-right: 15px; margin-top: 0">
        <div class="col-md-12" style="padding-left: 0px">
            <div class="col-md-10 text-left" style="padding-left: 0px">
        <div class="form-group row " style="margin: 0">

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newuserdialog">New User
                <i class="fa fa-user-plus"></i></button>
        </div>
            </div>
            <div class="col-md-2" style="padding-left: 0px">
                {!! Form::open( ['class'=>'form-inline', 'role'=>'form', 'url'=>Request::path(), 'method' => 'get'])!!}
                <input type="text" class="form-control" name="search"
                       value="{{isset(Request::all()['search'])?Request::all()['search']:''}}">
                <button type="submit" class="btn btn-primary" style="vertical-align: bottom">Search <i
                            class="fa fa-search" aria-hidden="true"></i></button>{!! Form::close()!!}
            </div>
        </div>

        &nbsp;
        <table class="table table-striped">
            <thead class="thead-inverse">
            <tr>
                <th class="headerSortable header">User Login</th>
                <th class="headerSortable header">Login Type</th>
                <th class="headerSortable header">Title</th>
                <th class="headerSortable header">Name</th>
                <th class="headerSortable header">User Role(s)</th>
                <th class="headerSortable header">School(s)</th>
                <th class="header">URL</th>
                <th></th>
                <th></th>
            </tr>
            </thead>

            @foreach ($users as $user)
                <tr>
                    <td>
                        <a href="#" data-toggle="modal" data-id="{{$user->id}}"
                           data-target="#editdialog">
                            {{$user->username}}
                        </a>
                    </td>
                    <td>
                        {{$user->type}}
                    </td>
                    <td>
                        {{$user->title}}
                    </td>
                    <td>
                        {{$user->name}}
                    </td>

                    <td>
                        <ul>
                            @foreach($user->roles as $role)
                                <li>{{$role['text']}}</li>
                            @endforeach
                        </ul>
                    </td>
                    <td>
                        <ul>
                            @foreach($user->schools as $school)
                                <li>{{$school['description']}}</li>
                            @endforeach
                        </ul>
                    </td>
                    <td>
                        @if(isset($user->url))
                        <a href="{{$user->url}}" target="_blank"> {{$user->url}}</a>
                        @endif
                    </td>
                    <td>
                        @if( Auth::user()->can('is_admin'))
                            @if (!session()->has('sudosu.has_sudoed'))
                                @if($user->id!=\Illuminate\Support\Facades\Auth::user()->id )
                                <form action="{{ route('sudosu.login_as_user') }}" method="post">
                                    <input type="hidden" name="userId" value="{{ $user->id }}">
                                    <input type="hidden" name="originalUserId" value="{{ \Illuminate\Support\Facades\Auth::user()->id ?? null }}">

                                    {!! csrf_field() !!}
                                    <button class="btn btn-sm" type="submit"> <i class="fa fa-btn fa-exclamation-triangle"></i> Log in as this user</button>
                                </form>
                            @endif
                                @endif
                        @endif
                    </td>
                    <td>
                        <a href="#" data-toggle="modal" data-id="{{$user->id}}"
                           data-target="#deletedialog">
                            <i style="color: firebrick" class="fa fa-btn fa-2x fa-times-circle"></i>
                        </a>
                    </td>

            @endforeach

        </table>
        {{ $users->links() }}
    </fieldset>

    <div id="newuserdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New User</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open( ['class'=>'form-horizontal', 'role'=>'form', 'id'=>'newuserform', 'data-toggle'=>'validator'])!!}

                    @include('user.form.form', ['submitButtonText'=>'Add User', 'roles'=>$roles,  'formid'=>'newuserform'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div id="editdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update User</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open( ['class'=>'form-horizontal', 'role'=>'form', 'id'=>'edituserform',  'data-toggle'=>'validator'])!!}

                    @include('user.form.form', ['submitButtonText'=>'Update User', 'roles'=>$roles, 'formid'=>'edituserform'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    {{--  Deleting a contact event  --}}
    <div id="deletedialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete user?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('form_common.deletedialog')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@stop
