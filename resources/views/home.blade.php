@extends('layouts.app')


@section('content')
    <script src="{{ URL::asset('resources/assets/js/Chart.min.js') }}"></script>
    <script src="https://unpkg.com/popper.js@1"></script>
    <script src="https://unpkg.com/tippy.js@5"></script>
    <div class="page-header">
        <h3>Hi {{\Illuminate\Support\Facades\Auth::user()->name}}</h3>
        <p>Choose your task below...</p>
    </div>

    <div class="container" style="width: 100%">
        {{--@can('update_unit_instance')--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="font-weight: bolder; font-size: 1.2em"><a
                                    href="{{ url('/unitsetup') }}">Offerings
                                Setup:</a><br/>

                        </div>
                        <div class="panel-body">

                            <div class="col-md-12 row">

                                <div class="col-md-3">

                                    <form id="teachingperiodselectform">
                                        <span
                                                style="margin-top: 0px;  font-size: 1.2em">Teaching periods:</span><br/>
                                        <select multiple
                                                class="form-control" name="teachingperiod[]"
                                                id="teaching_period_id_selector"
                                                style="width: 200px">

                                            @foreach ($teachingperiods as $teachingperiod)

                                                {{--@if(in_array($teachingperiod->id, explode(',',session('teachingperiod'))))--}}
                                                    <option value='{{$teachingperiod->id}}'
                                                    {{in_array($teachingperiod->id, session("teachingperiod") ?: []) ? "selected": ""}}
                                       >

                                                        {{$teachingperiod->teaching_period_lookup->teaching_period}} {{$teachingperiod->year}}</option>
                                                    @endforeach
                                        </select>
                                        <button class="btn btn-primary" data-tippy-content="Show offering statistics for the selected teaching periods" id="filterbut"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Show stats


                                        </button>

                                    </form>
                                    <p/>
                                    <p
                                            style="margin-top: 0px;  font-size: 1.2em; font-weight: bold">Status:</p>
                                    <p style="font-size: 1.2em">Your offerings: {{$myofferings->count()}}</p>
                                    <p style="font-size: 1.2em">In progress: {{$in_progress}}</p>
                                    <p p style="font-size: 1.2em">Complete: {{$complete}}</p>
                                    <p class="btn btn-default" style="font-weight: bolder; font-size: 1.2em"><a
                                                href="{{ url('/unitsetup') }}">Go
                                            to list <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                        </a><br/>

                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <div class="chart-container">
                                        <canvas id="chart-area"></canvas>
                                    </div>
                                </div>
                            </div>
                            {{--<table class="table table-striped">--}}
                            {{--<thead class="thead-inverse">--}}
                            {{--<tr>--}}
                            {{--<th class="headerSortable header">Unit</th>--}}
                            {{--<th class="headerSortable header">Combined units</th>--}}
                            {{--<th class="headerSortable header">Teaching period</th>--}}

                            {{--<th class="headerSortable header">Completion</th>--}}

                            {{--</tr>--}}
                            {{--</thead>--}}

                            {{--@if(isset($myofferings))--}}
                            {{--@foreach ($myofferings as $instance)--}}
                            {{--<tr--}}
                            {{--{!! ($instance->follow_up=='true'?'style="background-color: #ffb991"':"") !!}>--}}
                            {{--<td>--}}

                            {{--<a href="{{action('OfferingsController@show', $instance->id)}}">--}}
                            {{--{{$instance->unit->unit_code}}--}}
                            {{--</a>--}}

                            {{--</td>--}}
                            {{--<td>--}}
                            {{--@foreach($instance->combined_units as $combined_unit)--}}
                            {{--{{$combined_unit->unit['unit_code']}}{{($combined_unit != $instance->combined_units->last())?",":""}}--}}
                            {{--@endforeach--}}
                            {{--</td>--}}
                            {{--<td>--}}
                            {{--{{$instance->teaching_period->teaching_period_lookup->teaching_period}} {{$instance->teaching_period['year']}}--}}

                            {{--</td>--}}


                            {{--<td>--}}
                            {{--<div class="progress">--}}

                            {{--@if(($instance->checklist()->get()->where('heading', '<>', '1')->count())>0)--}}

                            {{--<div class="progress-bar" role="progressbar"--}}
                            {{--aria-valuenow="{{($instance->status=='complete')?100:round(($instance->checklistcomplete/($instance->checklist()->get()->where('heading', '<>', '1')->count()))*100)}}"--}}
                            {{--aria-valuemin="0" aria-valuemax="100"--}}
                            {{--style="min-width: 2em; width:{{($instance->status=='complete')?100:round(($instance->checklistcomplete/($instance->checklist()->get()->where('heading', '<>', '1')->count()))*100)}}%;--}}
                            {{--@if($instance->status=='complete')--}}
                            {{--background-color:#28a745--}}
                            {{--@endif--}}
                            {{--">--}}
                            {{--@if($instance->status=='complete')--}}
                            {{--Complete--}}
                            {{--@else--}}
                            {{--{{round(($instance->checklistcomplete/($instance->checklist()->get()->where('heading', '<>', '1')->count()))*100)}}--}}
                            {{--%--}}
                            {{--@endif--}}
                            {{--</div>--}}

                            {{--@endif--}}
                            {{--</div>--}}

                            {{--</td>--}}


                            {{--@endforeach--}}

                            {{--@endif--}}
                            {{--</table>--}}
                            {{--@if($myofferingscount>10)--}}
                            {{--<a href="{{ url('/unitsetup') }}"><strong>More...</strong></a>--}}
                            {{--@endif--}}
                        </div>
                    </div>
                </div>
            </div>
        {{--@endcan--}}
        @can('view_review_instance')
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="font-weight: bolder; font-size: 1.2em">Unit reviews:</div>
                        <div class="panel-body">
                            <a class="btn btn-default" style="font-weight: bolder; font-size: 1.2em"
                               href="{{ url('/unitreviews') }}">See all <i class="fa fa-arrow-right"
                                                                           aria-hidden="true"></i>
                            </a>
                            <div style="font-weight: bolder; text-decoration: underline">Unit reviews in which I am
                                involved:
                            </div>

                            @if($myunitreviewsascoordinator->count() > 0)
                                <table class="table table-striped">
                                    <thead class="thead-inverse">
                                    <tr>
                                        <th class="headerSortable header">Unit</th>
                                        <th class="headerSortable header">Reviewed offering</th>
                                        <th class="headerSortable header">Review Date</th>
                                        <th class="headerSortable header">Reviewed by</th>
                                    </tr>
                                    </thead>
                                    @foreach($myunitreviewsascoordinator as $review)
                                        @if(isset($review->reviewed_offering->unit))
                                            <tr>
                                                <td>
                                                    <a href="{{action('ReviewInstancesController@show', $review->id)}}">
                                                        {{--//{{dd($review->reviewed_offering)}}--}}
                                                        {{$review->reviewed_offering->unit->unit_code}}
                                                    </a>

                                                </td>
                                                <td>
                                                    {{isset($review->reviewed_offering)? $review->reviewed_offering->teaching_period->teaching_period_lookup->teaching_period:''}} {{isset($review->reviewed_offering)?$review->reviewed_offering->teaching_period['year']:''}}
                                                </td>

                                                <td>
                                                    {{isset($review->end_date)?$review->end_date:'no date set'}}
                                                </td>

                                                <td>

                                                    <strong>Co-ordinator:</strong>
                                                    @if(isset($review->override_coordinator))
                                                        {{$review->override_coordinator['name']}} (Taught
                                                        by {{$review->reviewed_offering->coordinator['name']}})
                                                    @else
                                                        {{$review->reviewed_offering->coordinator['name']}}
                                                    @endif
                                                    <br/>
                                                    <strong>Peer reviewer:</strong> {{$review->peer_reviewer['name']}}
                                                    <br/>
                                                    <strong>Learning Designer:</strong> {{$review->ld['name']}}
                                                </td>
                                            </tr>
                                        @endif

                                    @endforeach

                                </table>
                                <a class="btn btn-default" style="font-weight: bolder; font-size: 1.2em"
                                   href="{{ url('/unitreviews') }}">See all <i class="fa fa-arrow-right"
                                                                               aria-hidden="true"></i>
                                </a>
                            @endif


                        </div>
                    </div>
                </div>
            </div>
        @endcan
    </div>

    <script>


        $(document).ready(function () {

            $('select').select2();


            $("#teachingperiodselectform").submit(function (event) {
                {{--event.preventDefault();--}}
                {{--if($("#teaching_period_id_selector").val()){--}}
                    {{--var teachingperiods = $("#teaching_period_id_selector").select2('val').join();--}}
                    {{--location.replace("{{ url('/') }}?teachingperiod="+teachingperiods);--}}
                {{--}else{--}}
                    {{--location.replace("{{ url('/') }}?teachingperiod=-1");--}}
                {{--}--}}
            });


            tippy('button');

            var ctx = document.getElementById('chart-area').getContext('2d');
            var my_DoughnutChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [{{ $myofferingscount-($in_progress+$complete)}}, {{ $in_progress}}, {{$complete}}],
                        backgroundColor: ['#FA8072', '#99CCFF', '#28a745']
                    }],

                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: [
                        'Not started',
                        'In progress',
                        'Complete'
                    ]
                },
                // backgroundColor:['#FA8072','#48D1CC','#FF6600'],

                options: {

                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: false,
                        text: ''
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });
        });


    </script>
@endsection
