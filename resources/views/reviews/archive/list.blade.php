@extends('layouts.app')
@section('title')Unit reviews archive
@endsection

<style>
    .pagination {
        display: inline-block;
        /*padding-left: 0;*/
        /*margin: 20px 0;*/
        border-radius: 4px;
    }

</style>

@section('content')

    {!! Breadcrumbs::render('unitreviews.archive') !!}
    <div class="col-md-12 page-header" style="margin-top: 0px">

        <div class="box box-element">
            <div class="view">
                <fieldset style="padding-left: 15px; padding-right: 15px; margin-top: 0">
                    <div class="form-group row " style="margin: 0">
                        {{ $reviews->appends(\Illuminate\Support\Facades\Input::except('page'))->links() }}
                    </div>
                    <div class="form-group row " style="margin: 0">
                        {!! Form::open( ['class'=>'form-inline', 'role'=>'form', 'url'=>Request::path(), 'method' => 'get'])!!}
                        <input type="text" class="form-control" name="search"
                               value="{{isset(Request::all()['search'])?Request::all()['search']:''}}">
                        <button type="submit" class="btn btn-primary" style="vertical-align: bottom">Search (unit or
                            reviewer)<i
                                    class="fa fa-search" aria-hidden="true"></i></button>{!! Form::close()!!}

                    </div>

                    <table class="table table-striped">
                        <thead class="thead-inverse">
                        <tr>
                            <th class="headerSortable header">@sortablelink('unit_code', 'Unit')</th>
                            <th class="headerSortable header">Reviewed offering</th>
                            <th class="headerSortable header">@sortablelink('start_date', 'Start date')</th>
                            <th class="headerSortable header">@sortablelink('start_date', 'End date')</th>
                            <th class="headerSortable header">Reviewed by</th>
                            <th class="headerSortable header">@sortablelink('ratio', 'Overall completion')</th>
                            @can('update_review_instance')

                                <th></th>@endcan
                        </tr>
                        </thead>
                        @if(isset($reviews))
                            @foreach ($reviews as $review)
                                <tr>
                                    <td>
                                        @if(($review->live=='true')||(Auth::user()->can('update_review_instance')))
                                            <a href="{{ url('/unitreviewsarchive/'.$review->id) }}">@endif

                                                {{$review->unit_code}}
                                                @if($review->live!='true')
                                                    (Not live yet)
                                                @endif
                                                @if(($review->live=='true')||(Auth::user()->can('update_review_instance')))   </a>@endif
                                            </a>
                                    </td>
                                    <td>
                                        {{isset($review->reviewed_offering)?$review->reviewed_offering->teaching_period->teaching_period_lookup->teaching_period:''}} {{isset($review->reviewed_offering)?$review->reviewed_offering->teaching_period['year']:''}}
                                    </td>

                                    <td>
                                        {{isset($review->start_date)?$review->start_date:'no date set'}}
                                    </td>
                                    <td>
                                        {{isset($review->end_date)?$review->end_date:'no date set'}}
                                    </td>
                                    <td>


                                        <strong>Co-ordinator:</strong>
                                        @if(isset($review->override_coordinator))
                                            {{$review->override_coordinator['name']}} (Taught
                                            by {{$review->reviewed_offering->coordinator['name']}})
                                        @else
                                            {{$review->reviewed_offering->coordinator['name']}}
                                        @endif

                                        <span style="font-weight: bold; color: {{(($review->coordinator_ratio>0)?($review->coordinator_ratio==1?'#7ab800':'#f0ad4e'):'#d9534f')}}"> :({{round($review->coordinator_ratio*100)}}
                                            %)</span>

                                        <br/>
                                        <strong>Peer reviewer:</strong> {{$review->peer_reviewer['name']}}
                                        <span style="font-weight: bold; color: {{(($review->peer_reviewer_ratio>0)?($review->peer_reviewer_ratio==1?'#7ab800':'#f0ad4e'):'#d9534f')}}"> :({{round($review->peer_reviewer_ratio*100)}}
                                            %)</span>
                                        <br/>
                                        <strong>Learning Designer:</strong> {{$review->ld['name']}}
                                        <span style="font-weight: bold; color: {{(($review->ld_ratio>0)?($review->ld_ratio==1?'#7ab800':'#f0ad4e'):'#d9534f')}}"> :({{round($review->ld_ratio*100)}}
                                            %)</span>
                                    </td>
                                    <td>
                                        <div class="progress">
                                            @if($review->live!='true')
                                                Not live yet
                                            @else
                                                @if($review->ratio>0)
                                                    <div class="progress-bar" role="progressbar"
                                                         aria-valuenow="{{round($review->ratio*100)}}"
                                                         aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;
                                                            width:{{round($review->ratio*100)}}%">
                                                        {{round($review->ratio*100)}}%
                                                    </div>

                                                @endif
                                            @endif
                                        </div>

                                    </td>
                                    @can('update_review_instance')
                                        <td>
                                            <a href="#" data-toggle="modal" data-id="{{$review->id}}"
                                               data-target="#copydialog">
                                                <i class="fa fa-2x fa-copy" style='color: #0066ff'></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-id="{{$review->id}}"
                                               data-target="#deletedialog">
                                                <i class="fa fa-2x fa-times-circle" style='color: #ff6600'></i>
                                            </a>
                                        </td>
                                @endcan
                            @endforeach
                        @endif
                    </table>

                    {{ $reviews->appends(\Illuminate\Support\Facades\Input::except('page'))->links() }}

                </fieldset>
            </div>
        </div>
    </div>
    @can('update_review_instance')


        {{--  Deleting a unit review event  --}}
        <div id="deletedialog" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 300px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Really delete?</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('form_common.deletedialog')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    @endcan

@stop
