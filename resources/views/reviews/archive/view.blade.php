@extends('layouts.app')
@section('title')Review for {{$review->reviewed_offering->unit['unit_code']}}@endsection
@section('content')
    {{--Some extra libraries for inline editing--}}
    <link rel="stylesheet" href="{{URL::asset('resources/assets/css/bootstrap-editable.css')}}">
    <script src="{{ URL::asset('resources/assets/js/bootstrap-editable.min.js') }}"></script>
    <style>
        @media screen {
            table.note,
            table.note tr,
            table.note td {
                border: 1px solid black;
            }

            table.note {
                border-collapse: collapse;
            }

            table.table-striped td {
                border-left: solid 1px gray;

            }

            .assessmentabletheader {
                font-weight: bold;
                background-color: #d2d2d2;
            }

            .printable {
                display: none;
            }

            /*!* Icon when the collapsible content is shown *!*/
            /*#collapsiblelink:before {*/
            /*font-family: "Glyphicons Halflings";*/
            /*content: "\e114";*/
            /*float: right;*/
            /*margin-left: 15px;*/
            /*}*/
            /*!* Icon when the collapsible content is hidden *!*/
            /*#collapsiblelink.collapsed:before {*/
            /*content: "\e080";*/
            /*}*/
            @if(($review->ld['id']==Auth::user()->id))
        table.table-striped tr td:nth-child(2) {
                /*width: 30%;*/
            }

            div.handle {
                width: 1%;
                white-space: nowrap;
                cursor: grab;
            }

            div.handle:active {
                width: 1%;
                white-space: nowrap;
                cursor: grabbing;
            }

            @else
        table.table-striped tr td:first-child {
                /*min-width: 10%;*/
                /*width: 30%;*/

            }

            @endif





.modal.modal-wide .modal-dialog {
                width: 90%;
            }

            .modal-wide .modal-body {
                overflow-y: auto;
            }

            /* irrelevant styling */
            /*body {*/
            /*!text-align: center;*! **/
            /*}*/
            /*body p {*/
            /*max-width: 400px;*/
            /*margin: 20px auto;*/
            /*}*/
            #tallModal .modal-body p {
                margin-bottom: 900px
            }

        }

        @media print {

            .printable {
                width: 100%;
            }

            .page-header {
                display: none;
            }

            .nav-tabs {
                display: none;
            }

            .tab-content {
                display: none;
            }

        }
    </style>

    <script>

    </script>

    <!-- Tabs -->
    {!! Breadcrumbs::render('unitreviews.archive.show', $review) !!}

    {{--End Print view--}}
    <div class="col-md-12 page-header" style="margin-top: 0px">

        <div class="box box-element">
            <div class="view">
                <div class="row">
                    <div class="container col-md-4">

                        <h3 style="margin-top: 0px">Review
                            for
                            <a href="{{$review->reviewed_offering->moodle_url}}"> {{$review->reviewed_offering->unit['unit_code']}} {{$review->reviewed_offering->teaching_period['teaching_period']}} {{$review->reviewed_offering->teaching_period['year']}}</a>

                        </h3>


                        <strong>Co-ordinator:</strong>
                        @if(isset($review->override_coordinator))
                            {{$review->override_coordinator['name']}}  {{($review->override_coordinator['id']==Auth::user()->id)?'(you)':''}}
                            (Unit was taught by {{$review->reviewed_offering->coordinator['name']}})
                        @else

                            {{$review->reviewed_offering->coordinator['name']}} {{($review->reviewed_offering->coordinator['id']==Auth::user()->id)?'(you)':''}}
                        @endif

                        <br/>
                        <strong>Peer
                            Reviewer:</strong> {{$review->peer_reviewer['name']}} {{($review->peer_reviewer['id']==Auth::user()->id)?'(you)':''}}
                        {{--@if(!isset($review->archived_at))--}}
                        {{--@can('update_review_instance')--}}
                        {{--<a href="#" data-toggle="modal" data-target="#editpeerreviewerdialog">(Update)</a>--}}
                        {{--@endcan--}}
                        {{--@endif--}}

                        <br/>
                        <strong>Learning
                            Designer:</strong> {{$review->ld['name']}} {{($review->ld['id']==Auth::user()->id)?'(you)':''}}
                        {{--@if(!isset($review->archived_at))--}}
                        {{--@can('update_review_instance')<a href="#" data-toggle="modal"--}}
                        {{--data-target="#editlddialog">(Update)</a>--}}
                        {{--@endcan--}}
                        {{--@endif--}}
                        <br/>
                        <strong>Start date: </strong>{{$review->start_date}}
                        <br/>
                        <strong>End date: </strong>{{$review->end_date}}
                        @if(Carbon\Carbon::createFromFormat('j/m/Y', $review->end_date)->isFuture())
                            (expect completion
                            in {{\Carbon\Carbon::now()->diffInDays(Carbon\Carbon::createFromFormat('j/m/Y', $review->end_date))}}
                            days)
                        @else
                            (Overdue by {{\Carbon\Carbon::now()->diffInDays(Carbon\Carbon::createFromFormat('j/m/Y', $review->end_date))}} days)
                        @endif
                        <br/>


                    </div>
                    <div class="container col-md-6">

                        <div class="row">
                            <div class="col-md-2"><strong>Complete:</strong></div>
                            @if($review->checklist()->get()->where('heading', '<>', '1')->count()>0)
                                <div class="progress">
                                    <div class="progress-bar" id="progress_bar" role="progressbar"
                                         aria-valuenow="{{round(($review->checklistcomplete/($review->checklist()->get()->where('heading', '<>', '1')->count()*3))*100)}}"
                                         aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;
                                            width: {{round(($review->checklistcomplete/($review->checklist()->get()->where('heading', '<>', '1')->count()*3))*100)}}%">
                                        {{round(($review->checklistcomplete/($review->checklist()->get()->where('heading', '<>', '1')->count()*3))*100)}}
                                        %
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="font-weight: bold; text-decoration: underline"><a
                                        data-toggle="collapse" id="collapsiblelink" href="#collapsiblecoursepanel">
                                    Courses/Majors in which the unit is Core, Prescribed or
                                    Listed:<span id="collapseindicator">&nbsp;&nbsp;&nbsp;&nbsp;(Show)<i
                                                class="fa fa-chevron-right" aria-hidden="true"></i></span> </a></div>
                            <div class="col-md-12 collapse" id="collapsiblecoursepanel">
                                <table class="table table-condensed ">
                                    <thead class="thead-inverse">
                                    <tr>
                                        <th>
                                            Course
                                        </th>
                                        <th>
                                            Trim container
                                        </th>
                                        <th>
                                            Trim reference(s)
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($review->reviewed_offering->unit->courses as $course)
                                        <tr>
                                            <td>{{$course->label}} ({{$course->course_code}})

                                                @if(isset($course->pivot->major_id))
                                                    <p/>
                                                    Major: {{\App\Fieldofstudy::find($course->pivot->major_id)->label}}
                                                @endif
                                            </td>
                                            <td>
                                                @if (isset($course->trim_container))
                                                    Course: {{$course->trim_container}}

                                                @endif
                                                @if(isset($course->pivot->major_id))
                                                    <p/>

                                                    @if (isset(\App\Fieldofstudy::find($course->pivot->major_id)->trim_container))
                                                        Major:  {{\App\Fieldofstudy::find($course->pivot->major_id)->trim_container}}


                                                    @endif
                                                @endif</td>
                                            <td>
                                                @if (isset($course->trim_reference))
                                                    Course: {{$course->trim_reference}}
                                                @endif
                                                @if(isset($course->pivot->major_id))
                                                    <p/>
                                                    @if (isset(\App\Fieldofstudy::find($course->pivot->major_id)->trim_reference))
                                                        Major: {{\App\Fieldofstudy::find($course->pivot->major_id)->trim_reference}}
                                                    @endif
                                                @endif</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                    <div class="container col-md-2">


                    </div>
                </div>
            </div>
        </div>
    </div>

    <ul class="nav nav-tabs" id="tabslabels">
        <li class="active"><a data-toggle="tab" href='#checklisttab'>Review Checklist Items</a></li>
        <li class=""><a data-toggle="tab" href='#notestab'>Review Notes/log ({{$review->log->count()}})</a></li>
        <li class=""><a data-toggle="tab" href='#mediatab'>Associated Media ({{$review->media->count()}})</a></li>
        {{--<li class=""><a data-toggle="tab" href='#emailstab'>Messages ({{$review->emaillog->count()}})</a></li>--}}
        <li class=""><a data-toggle="tab" href='#outcomestab'>Unit Amendments Outcome/synthesis</a></li>
    </ul>
    <div class="tab-content">
        <div id="checklisttab" class="tab-pane active printable">
            <fieldset style="width: 100%">
                <legend>Review Checklist...
                    @can('update_review_instance')
                        <button class="btn btn-info btn-lg" data-toggle="modal"
                                data-target="#newchecklistitemdialog">New item
                        </button>
                    @endcan
                </legend>

                {{--CHecklist item table--}}

                <table class="table table-striped">
                    <thead class="thead-inverse">


                    <th>
                        Item
                    </th>


                    <th>
                        Co-ordinator status
                    </th>
                    <th>
                        Peer Reviewer status
                    </th>
                    <th>
                        LD status
                    </th>


                    <th>
                        notes/recomendations
                    </th>

                    </thead>

                    <tbody id="checklisttablebody">

                    @foreach($review->checklist as $item)
                        <?php
                        switch($item->heading){
                        case 1:
                        ?>
                        <tr style="background-color: #7ab800" class='sortablerow' entryid='{{$item->id}}'>
                            <td colspan="100%">
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#editchecklistitemdialog">


                                    <h4> {{$item->description}}</h4>
                                </a>
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#deletedialog">
                                    <i class="fa fa-times-circle" style='color: #ff6600'></i></a>

                            </td>
                        </tr>
                        <?php
                        break;
                        case 2:
                        ?>
                        <tr style="background-color:rgba(122, 184, 0, 0.3)" class='sortablerow' entryid='{{$item->id}}'>

                            <td colspan="100%">
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#editchecklistitemdialog">


                                    <span style="font-weight: bold; padding-left: 20px"> {{$item->description}}</span>
                                </a><br/>
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#deletedialog">
                                    <i class="fa fa-times-circle" style='color: #ff6600'></i></a>

                            </td>
                        </tr>
                        <?php
                        break;
                        default:
                        ?>
                        <tr class='sortablerow' entryid='{{$item->id}}'>

                            <td>

                                {{$item->description}}

                            </td>

                            <td>
                                <?php
                                switch ($item['coordinator_status']) {
                                    case 'Does not meet criterion':
                                        print("<i class='fa fa-2x fa-close' style='color: red'></i>");
                                        break;
                                    case 'Meets criterion':
                                        print("<i class='fa fa-2x fa-check' style='color: green'></i>");
                                        break;
                                    case 'N/A':
                                        print("<span style='color: orange; font-weight: bolder'>N/A</span>");
                                        break;
                                    default:
                                        break;
                                }

                                ?>
                            </td>

                            <td>
                                @php
                                    switch ($item->peer_reviewer_status) {
                                        case 'Does not meet criterion':
                                            print("<i class='fa fa-2x fa-close' style='color: red'></i>");
                                            break;
                                        case 'Meets criterion':
                                            print("<i class='fa fa-2x fa-check' style='color: green'></i>");
                                            break;
                                        case 'N/A':
                                            print("<span style='color: orange; font-weight: bolder'>N/A</span>");
                                            break;
                                        default:
                                            break;

                                    }
                                @endphp
                            </td>

                            <td>

                                @php

                                    switch ($item->ld_status) {
                                        case 'Does not meet criterion':
                                            print("<i class='fa fa-2x fa-close' style='color: red'></i>");
                                            break;
                                        case 'Meets criterion':
                                            print("<i class='fa fa-2x fa-check' style='color: green'></i>");
                                            break;
                                        case 'N/A':
                                            print("<span style='color: orange; font-weight: bolder'>N/A</span>");
                                            break;
                                        default:
                                            break;

                                    }


                                @endphp


                            </td>


                            <td>

                                <ul>
                                    @if(isset($item['coordinator_text']))
                                        <li>Co-ordinator:<br/>{!! $item['coordinator_text']!!}</li>
                                    @endif
                                    @if(isset($item['peer_reviewer_text']))
                                        <li>Peer Reviewer:<br/>{!! $item['peer_reviewer_text'] !!}</li>
                                    @endif
                                    @if(isset($item['ld_text']))
                                        <li>Learning Designer:<br/>{!! $item['ld_text'] !!}</li>
                                    @endif

                                </ul>

                            </td>


                            <td>

                            </td>

                        </tr>
<?php } ?>
                    @endforeach
                    </tbody>
                </table>
            </fieldset>
        </div>

        <div id="notestab" class="tab-pane ">
            <fieldset style="width: 90%">
                <legend>Review notes


                </legend>
                <table class="table table-striped">
                    <thead class="thead-inverse">
                    <th>
                        Description
                        (click to update)

                    </th>
                    <th>
                        Text
                    </th>
                    <th>
                        Entered at
                    </th>
                    <th>
                        Entered by
                    </th>
                    </thead>

                    @foreach($review->log as $item)
                        <tr>
                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$item->id}}"
                                   data-route="reviewnotesitem"
                                   data-target="#editnoteitemdialog">


                                    {{$item->description}}

                                </a>

                            </td>
                            <td>
                                {{$item->text}}
                            </td>
                            <td>
                                {{$item->created_at}}
                            </td>
                            <td>
                                {{$item->created_by['name']}}

                            </td>

                            <td>


                            </td>
                        </tr>
                    @endforeach
                </table>
            </fieldset>
        </div>

        <div id="mediatab" class="tab-pane">
            <fieldset style="width: 90%">
                <legend>
                    Associated Media

                </legend>
                <table class="table table-striped">
                    <thead class="thead-inverse">
                    <th>
                        Thumb
                    </th>
                    <th>
                        File name (click to download)
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                    </th>
                    <th>
                    </th>
                    </thead>
                    @foreach($review->media as $media)
                        <tr>
                            <td>
                                <img src="{{URL::asset('reviewmedia/thumb/'.$media->id)}}">
                            </td>
                            <td>
                                <a href="{{URL::asset('reviewmedia/download/'.$media->id)}}">{{$media->name}}</a>
                            </td>
                            <td>
                                {{$media->description}}
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                        </tr>
                    @endforeach

                </table>
            </fieldset>
        </div>

        <div id="outcomestab" class="tab-pane ">
            <fieldset style="width: 100%">
                <legend>Unit Amendments Outcome
                    <a href="{!! URL::to('')!!}/unitreviews/{!! $review->id !!}/getoutcomesasword"
                       class="btn btn-primary"><i class="fa fa-file-word-o" aria-hidden="true"></i> Export</a>

                </legend>


                <table class="table table-striped">
                    <colgroup>
                        <col style="width: 10%;!important;">
                        <col style="width: 45%;">
                        <col style="width: 40%;">
                        <col style="width: 5%;">
                    </colgroup>
                    <thead class="thead-inverse">

                    <th>
                        Item
                    </th>
                    <th>
                        Current Version
                    </th>
                    <th>
                        Proposed Version
                    </th>
                    <th>

                    </th>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="font-weight: bold">
                            Unit code
                        </td>
                        <td id="unit_code">
                            {{$review->review_instances_outcomes->unit_code}}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10" id="new_unit_code">
                                    {!! $review->review_instances_outcomes->new_unit_code !!}
                                </div>
                                <div class="col-md-2">

                                </div>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold">
                            Unit title
                        </td>
                        <td id="unit_title">
                            {{ $review->review_instances_outcomes->unit_title}}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_unit_title">
                                    {!! $review->review_instances_outcomes->new_unit_title !!}
                                </div>

                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Combined Unit(s)
                        </td>
                        <td id="combined_units">
                            {!! $review->review_instances_outcomes->combined_units !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_combined_units">
                                    {!! $review->review_instances_outcomes->new_combined_units !!}
                                </div>
                                <div class="col-md-2">

                                </div>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Credit Point Value
                        </td>
                        <td id="credit_points">
                            {!! $review->review_instances_outcomes->credit_points !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_credit_points">
                                    {!! $review->review_instances_outcomes->new_credit_points !!}
                                </div>
                                <div class="col-md-2">

                                </div>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Unit Description
                        </td>
                        <td id="description">
                            {!! $review->review_instances_outcomes->description !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_description">
                                    {!! $review->review_instances_outcomes->new_description !!}
                                </div>
                                <div class="col-md-2">

                                </div>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Unit Prerequisites
                        </td>
                        <td id="prerequisites">
                            {!! $review->review_instances_outcomes->prerequisites !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_prerequisites">
                                    {!! $review->review_instances_outcomes->new_prerequisites !!}
                                </div>
                                <div class="col-md-2">

                                </div>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Unit Corequisites
                        </td>
                        <td id="corequisites">
                            {!! $review->review_instances_outcomes->corequisites !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_corequisites">
                                    {!! $review->review_instances_outcomes->new_corequisites !!}
                                </div>
                                <div class="col-md-2">

                                </div>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Unit restrictions
                        </td>
                        <td id="restrictions">
                            {!! $review->review_instances_outcomes->restrictions !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_restrictions">
                                    {!! $review->review_instances_outcomes->new_restrictions !!}
                                </div>
                                <div class="col-md-2">

                                </div>
                            </div>
                        </td>
                        <td>

                        </td>
                    <tr>
                        <td style="font-weight: bold">
                            Offerings
                        </td>
                        <td id="offerings">
                            {!! $review->review_instances_outcomes->offerings !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10" id="new_offerings">
                                    {!! $review->review_instances_outcomes->new_offerings !!}
                                </div>
                                <div class="col-md-2">

                                </div>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Learning Outcomes
                        </td>
                        <td id="outcomes_learning_outcomes_src">

                            <ol>
                                @if(isset($review->review_instances_outcomes->learning_outcomes))
                                    @foreach(json_decode($review->review_instances_outcomes->learning_outcomes) as $learningOutcome)
                                        <li>{{$learningOutcome->description}}</li>
                                    @endforeach
                                @endif
                            </ol>

                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="learning_outcomes">
                                    <ol id="lo_list">
                                        {{--{{dd($review->review_instances_outcomes->review_instances_outcomes_learning_outcomes)}}--}}
                                        @foreach($review->review_instances_outcomes->review_instances_outcomes_learning_outcomes as $learningOutcome)
                                            <li>{{$learningOutcome->text}}</li>
                                        @endforeach
                                    </ol>
                                </div>
                                <div class="col-md-2">

                                </div>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Assessment tasks
                        </td>
                        <td id="outcome_assessment_tasks_src">
                            <table>
                                <tr class='assessmentabletheader'>
                                    <th><strong>Must Complete</strong></th>
                                    <th><strong>Component</strong></th>
                                    <th><strong>Exam length</strong></th>
                                    <th><strong>Weight</strong></th>
                                    <th><strong>Mode</strong></th>
                                    <th><strong>No. words</strong></th>
                                </tr>
                                @if(isset($review->review_instances_outcomes->assessment_tasks))
                                    @foreach(json_decode($review->review_instances_outcomes->assessment_tasks) as $assessment)
                                        <tr style='background-color: #D2D2D2'>
                                            <td style='background-color: #EEEEEE'>{{($assessment->compulsory ? 'Yes' : 'No') }}</td>

                                            <td style='background-color: #EEEEEE'>{{$assessment->title}}</td>
                                            <td style='background-color: #EEEEEE'>{{$assessment->formattedExamLength}}</td>
                                            <td style='background-color: #EEEEEE'>{{$assessment->weightString}}</td>
                                            <td style='background-color: #EEEEEE'>{{$assessment->studyMode}}</td>
                                            <td style='background-color: #EEEEEE'>@if(strlen($assessment->wordCount)>0)
                                                    Word count: {{$assessment->wordCount}} words
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan='6'><p><span
                                                            style='font-style: italic'> Assessment notes:</span><br/>
                                                    {{$assessment->notes}}
                                                </p>
                                                <p>
                                                    <span style='font-style: italic'> Relates to Learning Outcomes (LO):</span><br/>
                                                    {{$assessment->notesOutcomesAttributes}}

                                                </p>
                                        </tr>
                                    @endforeach
                                @endif
                            </table>
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="outcome_assessment_tasks">
                                    <table>
                                        <tr class='assessmentabletheader'>
                                            <th><strong>Must Complete</strong></th>
                                            <th><strong>Component</strong></th>
                                            <th><strong>Exam length</strong></th>
                                            <th><strong>Weight</strong></th>
                                            <th><strong>Mode</strong></th>
                                            <th><strong>No. words</strong></th>
                                        </tr>
                                        @foreach($review->review_instances_outcomes->review_instances_outcomes_assessment_tasks as $assessment)
                                            <tr style='background-color: #D2D2D2'>
                                                <td style='background-color: #EEEEEE'>{{$assessment->must_complete }}</td>

                                                <td style='background-color: #EEEEEE'>{{$assessment->component}}</td>
                                                <td style='background-color: #EEEEEE'>{{$assessment->exam_length}}</td>
                                                <td style='background-color: #EEEEEE'>{{$assessment->weight}}</td>
                                                <td style='background-color: #EEEEEE'>{{$assessment->mode}}</td>
                                                <td style='background-color: #EEEEEE'>@if(strlen($assessment->word_count)>0)
                                                        Word count: {{$assessment->word_count}} words
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='6'><p><span
                                                                style='font-style: italic'> Assessment notes:</span><br/>
                                                        {{$assessment->notes}}
                                                    </p>
                                                    <p>
                                                        <span style='font-style: italic'> Relates to Learning Outcomes (LO):</span><br/>
                                                        {{$assessment->relates_to_lo}}

                                                    </p>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <div class="col-md-2">

                                </div>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Coordinators
                        </td>
                        <td id="coordinators">
                            {!! $review->review_instances_outcomes->coordinators !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_coordinators">{!! $review->review_instances_outcomes->new_coordinators !!}</div>
                                <div class="col-md-2">

                                </div>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tbody>
                </table>
            </fieldset>
        </div>
    </div>







@stop
