@extends('layouts.mainapp')
@section('title')Unit reviews @endsection
@section('menu')

    <li>

        @can('update_review_instance')
            <button type="button" class="btn btn-primary" style="width: 220px" data-toggle="modal"
                    data-target="#newdialog">
                New Review <i
                        class="fa fa-plus"></i>
            </button>
        @endcan
        <p/>
    </li>

    <li class="active">
        <a href="{{ url('/unitreviewstemplates') }}"><i class="fa fa-fw fa-cog"></i>Templates</a>

    </li>
    <li class="active">
        <a href="{{ url('/reviewemails') }}"><i class="fa fa-fw fa-cog"></i>Email templates</a>

    </li>
    <li>
        <a href="{{ url('/unitreviewsarchive') }}"><i class="fa fa-fw fa-archive"></i>Archive</a>

    </li>
@endsection
<style>
    .pagination {
        display: inline-block;
        /*padding-left: 0;*/
        /*margin: 20px 0;*/
        border-radius: 4px;
    }
</style>
@section('content')
    <script>

        var currentid = -1;

        $(document).ready(function () {
            @can('update_review_instance')
            $('#reviewed_offering_id').select2({
                placeholder: 'Search by unit code',
                minimumInputLength: 2,
                ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                    url: "{!! URL::to('')!!}/searchofferingforselect2",

                    dataType: 'json',
                    quietMillis: 250,
                    data: function (term) {
                        return {
                            q: term, // search term
                        };
                    },

                    cache: true
                },
            });

            $('.userselect').select2({
                placeholder: 'Search by user name',
                minimumInputLength: 2,
                ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                    url: "{!! URL::to('')!!}/searchuserforselect2",

                    dataType: 'json',
                    quietMillis: 250,
                    data: function (term) {
                        return {
                            q: term, // search term
                        };
                    },

                    cache: true
                },
            });
            // $('select').select2();

            // make all date class elements into datepicker
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
            });

            $('.modalform').on('shown.bs.modal', function () {
                console.log('hi');
                $(this).find("form").validator();
            });

            $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
                currenteditingid = $(this).data('id');
                switch ($(this).data('target')) {
                    case '#deletedialog':
                        currentid = $(this).data('id');
                        break;
                    case '#copydialog':
                        currentid = $(this).data('id');
                        break;
                    default:
                        currentid = -1;
                        break;
                }

            });

            $('.newdialog').submit(function (event) {
                event.preventDefault();
                $(this).modal('hide');
                var vars = $(this).find("form").serializeArray();
                waitingDialog.show();
                submitNewForm(vars, $(this).data('route'));
            });

            $("#copydialog").submit(function (event) {
                $(this).modal('hide');
                // cancels the form submission
                var vars = $(this).find("form").serializeArray();
                vars.push({name: 'id', value: currentid});
                event.preventDefault();
                waitingDialog.show();
                submitCopyForm(vars);
            });

            // all deletes are the same
            $("#deletedialog").submit(function (event) {
                $(this).modal('hide');
                // cancels the form submission
                event.preventDefault();
                waitingDialog.show();
                deleteItem(currentid, 'unitreviews');
            });
            @endcan
        });

        @can('update_review_instance')
        function submitNewForm(vars, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/create',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();

                    if (Number(data.id) > 0) {
                        location.reload(true);
                    }
                    else if ((data.error).length > 0) {
                        alert(data.error);
                    }
                    else {
                        alert('something went wrong with the insertion');
                    }
                }
            });
        }

        function submitCopyForm(vars) {
            $.ajax({
                url: '{!! URL::to('')!!}/unitreviews/clone',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();

                    if (Number(data.id) > 0) {
                        // take me to the newly created instance
                        location.replace('{!! URL::to('')!!}/unitreviews/' + data.id);
                    }
                    else if ((data.error).length > 0) {
                        alert(data.error);
                    }
                    else {
                        alert('something went wrong with the operation');
                    }
                }
            });
        }

        function deleteItem(id, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/destroy',
                type: 'post',
                data: ([{name: 'id', value: id}, {name: '_method', value: 'DELETE'}, {
                    name: '_token',
                    value: '{{csrf_token()}}'
                }]),
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status == "1") {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the delete');
                    }
                }
            });
        }

        /*
         *
         * Helper functions
         *
         */

        // populates a form with data returned from Laravel
        function populate(frm, data) {
            $.each(data, function (key, value) {
                var $ctrl = $('[name=' + key + ']', frm);
                if ($ctrl.is("select")) {
                    $ctrl.select2('val', value);
                } else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":
                            $ctrl.val(value);
                            break;
                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).attr("checked", value);
                                }
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }
            });
        }
        @endcan
    </script>
    {!! Breadcrumbs::render('unitreviews.index') !!}
    <fieldset style="padding-left: 15px; padding-right: 15px; margin-top: 0">
        <div class="form-group row " style="margin: 0">
            {{ $reviews->appends(\Illuminate\Support\Facades\Input::except('page'))->links() }}
        </div>
        <div class="form-group row " style="margin: 0">
            {!! Form::open( ['class'=>'form-inline', 'role'=>'form', 'url'=>Request::path(), 'method' => 'get'])!!}
            <input type="text" class="form-control" name="search"
                   value="{{isset(Request::all()['search'])?Request::all()['search']:''}}">
            <button type="submit" class="btn btn-primary" style="vertical-align: bottom">Search (unit or reviewer)<i
                        class="fa fa-search" aria-hidden="true"></i></button>{!! Form::close()!!}

        </div>

        <table class="table table-striped">
            <thead class="thead-inverse">
            <tr>
                <th class="headerSortable header">@sortablelink('unit_code', 'Unit')</th>
                <th class="headerSortable header">Reviewed offering</th>
                <th class="headerSortable header">@sortablelink('start_date', 'Start date')</th>
                <th class="headerSortable header">@sortablelink('start_date', 'End date')</th>
                <th class="headerSortable header">Reviewed by</th>
                <th class="headerSortable header">@sortablelink('ratio', 'Overall completion')</th>
                @can('update_review_instance')

                    <th></th>@endcan
            </tr>
            </thead>
            @if(isset($reviews))
                @foreach ($reviews as $review)
                    <tr>
                        <td>
                            @if(($review->live=='true')||(Auth::user()->can('update_review_instance')))
                                <a href="{{action('ReviewInstancesController@show', $review->id)}}">@endif

                                    {{$review->unit_code}}
                                    @if($review->live!='true')
                                        (Not live yet)
                                    @endif
                                    @if(($review->live=='true')||(Auth::user()->can('update_review_instance')))   </a>@endif
                                </a>
                        </td>
                        <td>
                            {{isset($review->reviewed_offering)?$review->reviewed_offering->teaching_period->teaching_period_lookup->teaching_period:''}} {{isset($review->reviewed_offering)?$review->reviewed_offering->teaching_period['year']:''}}
                        </td>

                        <td>
                            {{isset($review->start_date)?$review->start_date:'no date set'}}
                        </td>
                        <td>
                            {{isset($review->end_date)?$review->end_date:'no date set'}}
                        </td>
                        <td>


                            <strong>Co-ordinator:</strong>
                            @if(isset($review->override_coordinator))
                                {{$review->override_coordinator_name}} (Taught
                                by {{$review->reviewed_offering->coordinator->name}})
                            @else
                                {{$review->reviewed_offering->coordinator->name}}
                            @endif

                                <span style="font-weight: bold; color: {{(($review->coordinator_ratio>0)?($review->coordinator_ratio==1?'#7ab800':'#f0ad4e'):'#d9534f')}}"> :({{round($review->coordinator_ratio*100)}}%)</span>

                            <br/>
                            <strong>Peer reviewer:</strong> {{$review->peer_reviewer_name}}
                            <span style="font-weight: bold; color: {{(($review->peer_reviewer_ratio>0)?($review->peer_reviewer_ratio==1?'#7ab800':'#f0ad4e'):'#d9534f')}}"> :({{round($review->peer_reviewer_ratio*100)}}%)</span>
                            <br/>
                            <strong>Learning Designer:</strong> {{$review->ld_name}}
                            <span style="font-weight: bold; color: {{(($review->ld_ratio>0)?($review->ld_ratio==1?'#7ab800':'#f0ad4e'):'#d9534f')}}"> :({{round($review->ld_ratio*100)}}%)</span>
                        </td>
                        <td>
                            <div class="progress">
                                @if($review->live!='true')
                                    Not live yet
                                @else
                                    @if($review->ratio>0)
                                        <div class="progress-bar" role="progressbar"
                                             aria-valuenow="{{round($review->ratio*100)}}"
                                             aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;
                                                width:{{round($review->ratio*100)}}%">
                                            {{round($review->ratio*100)}}%
                                        </div>

                                    @endif
                                @endif
                            </div>

                        </td>
                        @can('update_review_instance')

                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$review->id}}"
                                   data-target="#deletedialog">
                                    <i class="fa fa-2x fa-times-circle" style='color: #ff6600'></i>
                                </a>
                            </td>
                    @endcan
                @endforeach
            @endif
        </table>

        {{ $reviews->appends(\Illuminate\Support\Facades\Input::except('page'))->links() }}

    </fieldset>
    @can('update_review_instance')
        <div id="newdialog" class="modal fade newdialog modalform" role="dialog"
             data-route="unitreviews">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">New Review</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open( ['class'=>'form-horizontal', 'role'=>'form'])!!}

                        @include('reviews.form.newreviewform', ['submitButtonText'=>'Add review'])

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        {{--  Copying a unit review event  --}}
        {{--<div id="copydialog" class="modal fade" role="dialog">--}}
            {{--<div class="modal-dialog">--}}
                {{--<div class="modal-content">--}}
                    {{--<div class="modal-header">--}}
                        {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                        {{--<h4 class="modal-title">Copy a review</h4>--}}
                    {{--</div>--}}
                    {{--<div class="modal-body">--}}
                        {{--{!! Form::open(['class'=>'form-horizontal', 'role'=>'form'])!!}--}}
                        {{--@include('reviews.form.copyreviewform', ['submitButtonText'=>'Copy review'])--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--  Deleting a unit review event  --}}
        <div id="deletedialog" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 300px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Really delete?</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('form_common.deletedialog')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    @endcan

@stop
