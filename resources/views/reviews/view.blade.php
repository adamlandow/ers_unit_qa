@extends('layouts.app')
@section('title')Review for {{$review->reviewed_offering->unit['unit_code']}}@endsection
@section('content')
    {{--Some extra libraries for inline editing--}}
    <link rel="stylesheet" href="{{URL::asset('resources/assets/css/bootstrap-editable.css')}}">
    <script src="{{ URL::asset('resources/assets/js/bootstrap-editable.min.js') }}"></script>
    <style>

        table.note,
        table.note tr,
        table.note td {
            border: 1px solid black;
        }

        table.note {
            border-collapse: collapse;
        }

        table.table-striped td {
            border-left: solid 1px gray;

        }

        .assessmentabletheader {
            font-weight: bold;
            background-color: #d2d2d2;
        }

        @media screen {

            .printable {
                display: none;
            }

            /*!* Icon when the collapsible content is shown *!*/
            /*#collapsiblelink:before {*/
            /*font-family: "Glyphicons Halflings";*/
            /*content: "\e114";*/
            /*float: right;*/
            /*margin-left: 15px;*/
            /*}*/
            /*!* Icon when the collapsible content is hidden *!*/
            /*#collapsiblelink.collapsed:before {*/
            /*content: "\e080";*/
            /*}*/
            @if(($review->ld['id']==Auth::user()->id))
        table.table-striped tr td:nth-child(2) {
                /*width: 30%;*/
            }

            div.handle {
                width: 1%;
                white-space: nowrap;
                cursor: grab;
            }

            div.handle:active {
                width: 1%;
                white-space: nowrap;
                cursor: grabbing;
            }

            @else
        table.table-striped tr td:first-child {
                /*min-width: 10%;*/
                /*width: 30%;*/

            }

            @endif





.modal.modal-wide .modal-dialog {
                width: 90%;
            }

            .modal-wide .modal-body {
                overflow-y: auto;
            }

            /* irrelevant styling */
            /*body {*/
            /*!text-align: center;*! **/
            /*}*/
            /*body p {*/
            /*max-width: 400px;*/
            /*margin: 20px auto;*/
            /*}*/
            #tallModal .modal-body p {
                margin-bottom: 900px
            }

        }

        @media print {
            body {-webkit-print-color-adjust: exact;}

            /*Hide everything non-printable*/
            .noprint {
                display: none;
            }

            /*Hide dialogs*/
            [role="dialog"] {
                display: none;
            }

            table{
                border: 1px solid black;
            }

            .heading1{
                font-weight: bolder;
                background-color: #d2d2d2 !important;
                border: 2px solid black;
            }

            .heading2{
                font-weight: bold;
                background-color: #f0f0f0 !important;
                border: 1px solid black;
            }

        }
    </style>
    <script src="{{ URL::asset('resources/assets/js/tinymce/tinymce.min.js') }}"></script>
    <script>
        // set inline editable mode to inline
        $.fn.editable.defaults.mode = 'inline';

        //tinymce Bootstrap fix
        // https://www.tinymce.com/docs/integrations/bootstrap/
        $(document).on('focusin', function (e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });

        $(".modal-wide").on("show.bs.modal", function () {
            var height = $(window).height() - 200;
            $(this).find(".modal-body").css("max-height", height);
        });

        var currentoutcometargetid = '';
        var currentoutcometargetfield = "";

        // init document
        $(document).ready(function () {

                $("#collapsiblecoursepanel").on("hide.bs.collapse", function () {
                    $("#collapseindicator").html('    (Show)<i class="fa fa-chevron-right" aria-hidden="true"></i>');
                });
                $("#collapsiblecoursepanel").on("show.bs.collapse", function () {
                    $("#collapseindicator").html('    (Hide)<i class="fa fa-chevron-down" aria-hidden="true"></i>');
                });

                // set up select2 dropdowns
                $('select').select2();

                // $(':input[name=heading]').on("change", function (e) {
                //     if ($(this).val().toString() != '0') {
                //         $(".notlabel").hide()
                //     } else {
                //         $(".notlabel").show()
                //     }
                // });

                $('#template_id').on('change', function (e) {
                    loademailtemplate()
                });

                // make all date class elements into datepicker
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true,
                });

                // init timyMCE
                tinymce.init({
                    selector: '.tinymce',
                    height: 500,
                    plugins: [
                        'advlist autolink lists link image charmap print preview anchor @can('update_review_instance') template @endcan',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table contextmenu paste code'
                    ],

                    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | template',
                    @can('update_review_instance')
                    templates: [
                        {
                            title: 'Unit description',
                            description: 'Unit description from CAUC',
                            content: '{cauc_description}'
                        },
                        {
                            title: 'Unit description table',
                            description: 'Unit description from CAUC in a handy table for comparison with Moodle content',
                            content: '{cauc_description_table}'
                        },
                        {
                            title: 'Learning outcomes table',
                            description: 'Learning outcomes from CAUC in a handy table for comparison with Moodle content',
                            content: '{cauc_learning_outcomes_comparison_table}'
                        },
                        {
                            title: 'Assessments table',
                            description: 'Assessments from CAUC in a handy table for comparison with Moodle content',
                            content: '{cauc_assessments_table}'
                        },
                        {
                            title: 'Pre/corequisites and restrictions',
                            description: 'Table to review where the unit sits within the degree using CAUC table',
                            content: '{cauc_unit_relationship_table}'
                        },
                    ],
                    @endcan
                    content_css: '//www.tinymce.com/css/codepen.min.css'
                });

                // Set up tab persistence across reloads
                if (location.hash.substr(0, 2) == "#!") {
                    $("a[href='#" + location.hash.substr(2) + "']").tab("show");
                }

                $("a[data-toggle='tab']").on("shown.bs.tab", function (e) {
                    var hash = $(e.target).attr("href");
                    if (hash.substr(0, 1) == "#") {
                        location.replace("#!" + hash.substr(1));
                    }
                });


                    @if(!isset($review->archived_at))

                    $('.editdialog').submit(function (event) {
                        // cancels the form submission
                        event.preventDefault();
                        $(this).modal('hide');
                        //  var vars = $("#newmediadialog").find("form").serializeArray();
                        var vars = $(this).find("form").serializeArray();
                        vars.push({name: 'id', value: currenteditingid});
                        vars.push({name: '_method', value: 'PATCH'});

//console.log(vars)
                        waitingDialog.show();
                        submitUpdateForm(vars, $(this).data('route'));
                    });



                    @can('update_review_instance')
                // drag and drop reordering
                dragula([document.getElementById('checklisttablebody')], {
                    moves: function (el, container, handle) {
                        console.log(handle)
                        return handle.classList.contains('handle');
                    }
                }).on('drop', function (el, target, source, sibling) {
                    // send an array of elements to backend to reorder
                    //    waitingDialog.show();
                    console.log(sibling);

                    var orderArray = ([]);
                    var orderint = 0;
                    $(".sortablerow").each(function () {
                        orderArray.push({id: $(this).attr('entryid'), order: orderint})
                        orderint++;
                        console.log($(this).attr('entryid'));
                    });
                    // for some reason, the last element of this is always the element being moved.
                    orderArray.pop();

                    var dataObj = ([{name: '_token', value: '{{csrf_token()}}'}, {
                        name: 'order',
                        value: JSON.stringify(orderArray)
                    }]);

                    $.ajax({
                        url: '{!! URL::to('')!!}/reviewinstancechecklistitem/reorder',
                        type: 'post',
                        data: dataObj,
                        error: function (jqXHR, textStatus, errorThrown) {
                            waitingDialog.hide();
                            alert(errorThrown);
                        },
                        success: function (data) {
                            //   waitingDialog.hide();
                            if (data.status.toString() == "0") {
                                location.reload();
                            } else {
                                waitingDialog.hide();
                                alert('something went wrong with the update');
                            }
                        }
                    });

                });

                    @endcan

                    $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
                        currenteditingid = $(this).data('id');
                        $(".emailtemplate").hide();
                        switch ($(this).data('target')) {
                            case '#editchecklistitemdialog':
                                currenteditingid = $(this).data('id');
                                console.log(currenteditingid)
                                getDetails(currenteditingid, 'reviewinstancechecklistitem', 'editchecklistitemdialog');
                                break;
                            case '#updateitemtextdialog':
                                currenteditingid = $(this).data('id');
                                console.log(currenteditingid)
                                getDetails(currenteditingid, 'reviewinstancechecklistitem', 'updateitemtextdialog');
                                break;
                            case '#editnoteitemdialog':
                                currenteditingid = $(this).data('id');
                                console.log(currenteditingid)
                                getDetails(currenteditingid, 'reviewnotesitem', 'editnoteitemdialog');
                                break;
                            case '#editmediadialog':
                                currenteditingid = $(this).data('id');
                                console.log(currenteditingid)
                                getDetails(currenteditingid, 'reviewmedia', 'editmediadialog');
                                break;

                            case '#deletedialog':
                                currentdeleteroute = $(this).data('route');
                                console.log($(this).data('route'));
                                currentdeletingid = $(this).data('id');
                                break;
// email handling
//                             case "#newemaildialog":
//                                 loademailtemplate();
//                                 break;

                            // dummy dialog for overview demo
                            case "#updateoutcomestextitemdialog":
                                currentoutcometargetid = $(this).data('id');
                                currentoutcometargetfield = $(this).data('field');
                                console.log('setting text from new_' + currentoutcometargetfield)
                                if ($("#new_" + currentoutcometargetfield).html().trim().length == 0) {
                                    tinyMCE.get("updateoutcomestextcontrol").setContent($("#" + currentoutcometargetfield).html());
                                } else {
                                    tinyMCE.get("updateoutcomestextcontrol").setContent($("#new_" + currentoutcometargetfield).html());
                                }

                                break;


                            // Learning outcomes
                            case "#editlearningoutcomedialog":
                                $("#loitemstbl tbody tr").remove();
                                @if($review->review_instances_outcomes->review_instances_outcomes_learning_outcomes->count()>0)
                                @foreach($review->review_instances_outcomes->review_instances_outcomes_learning_outcomes as $learningOutcome)
                                $("#loitemstbl tbody").append('<tr class="updateitemitemrow">' +
                                    '<td><a href="#" class="updateinlineeditabletextarea" data-name="description" data-type="textarea" data-pk="{{$learningOutcome->id}}" data-url="" data-title="Description">{{preg_replace('~[\r\n]+~', '', trim($learningOutcome->text))}}</a></td>' +
                                    '<td><a href="#" onclick="$(this).closest(\'tr\').remove()"><i class="fa fa-times" style="font-size: 2em; color: red"aria-hidden="true"></i></a></td>' +
                                    '</tr> ');

                            @endforeach
                            @else
                                //   if ($("#lo_list").children().length == 0) {
                                @if(isset($review->review_instances_outcomes->learning_outcomes))
                                @foreach(json_decode($review->review_instances_outcomes->learning_outcomes) as $learningOutcome)
                                $("#loitemstbl tbody").append('<tr class="updateitemitemrow">' +
                                    '<td><a href="#" class="updateinlineeditabletextarea" data-name="description" data-type="textarea" data-pk="" data-url="" data-title="Description">{{ preg_replace('~[\r\n]+~', '', trim($learningOutcome->description))}}</a></td>' +
                                    '<td><a href="#" onclick="$(this).closest(\'tr\').remove()"><i class="fa fa-times" style="font-size: 2em; color: red"aria-hidden="true"></i></a></td>' +
                                    '</tr> ');

                        @endforeach
                                @endif

                                @endif

                            // assessments
                            case "#editassessmentsoutcomedialog":
                                $("#assessmentitemstbl tbody tr").remove();
                                @if($review->review_instances_outcomes->review_instances_outcomes_assessment_tasks->count()>0)
                                @foreach($review->review_instances_outcomes->review_instances_outcomes_assessment_tasks as $assessment)
                                $("#assessmentitemstbl tbody").append("<tr style='background-color: #D2D2D2' class='updateassessmentitemrow'>" +
                                    "<td style='background-color: #EEEEEE'>" +
                                    "<a href='#' class='updatemustcomplete' data-name='must_complete' data-type='select' data-pk='' data-url='' data-title='Compulsory'>{{($assessment->must_complete ? 'Yes' : 'No') }}</a></td>" +
                                    "<td style='background-color: #EEEEEE'>" +
                                    "<a href='#' class='updatecomponent' data-name='component' data-type='text' data-pk='' data-url='' data-title='title'>{{preg_replace('~[\r\n]+~', '',$assessment->component)}}</a></td>" +
                                    "<td style='background-color: #EEEEEE'><a href='#' class='updateexam_length' data-name='exam_length' data-type='text' data-pk='' data-url='' data-title='formattedExamLength'>{{$assessment->exam_length}}</a></td>" +
                                    "<td style='background-color: #EEEEEE'><a href='#' class='updateweight' data-name='weight' data-type='text' data-pk='' data-url='' data-title='weightString'>{{$assessment->weight}}</a></td>" +
                                    "<td style='background-color: #EEEEEE'><a href='#' class='updatemode' data-name='mode' data-type='text' data-pk='' data-url='' data-title='studyMode'>{{$assessment->mode}}</a></td>" +
                                    "<td style='background-color: #EEEEEE'>" +

                                    "Word count: <a href='#' class='updateword_count' data-name='word_count' data-type='text' data-pk='' data-url='' data-title='wordCount'>{{$assessment->word_count}}</a> words" +

                                    "</td><td style='background-color: #FFF'>" + '<a href="#" onclick="$(this).closest(\'tr\').add($(this).closest(\'tr\').next()).remove()"><i class="fa fa-times" style="font-size: 2em; color: red" aria-hidden="true"></i></a></td>' +
                                    "</tr>" +
                                    "<tr >" +
                                    "<td colspan='6'><p><span style='font-style: italic'> Assessment notes:</span><br/><a href='#' class='updatenotes' data-name='notes' data-type='textarea' data-pk='' data-url='' data-title='notes'>{!! preg_replace('~[\r\n]+~', '', trim($assessment->notes))!!}</a></p>" +
                                    "<p><span style='font-style: italic'> Relates to Learning Outcomes (LO):</span><br/><a href='#' class='updaterelates_to_lo' data-name='relates_to_lo' data-type='text' data-pk='' data-url='' data-title='notesOutcomesAttributes'>{{preg_replace('~[\r\n]+~', '', trim($assessment->relates_to_lo))}}</a></p></tr>");


                                @endforeach

                                @else
                                @if(isset($review->review_instances_outcomes->assessment_tasks))
                                @foreach(json_decode($review->review_instances_outcomes->assessment_tasks) as $assessment)
                                $("#assessmentitemstbl tbody").append("<tr style='background-color: #D2D2D2' class='updateassessmentitemrow'>" +
                                    "<td style='background-color: #EEEEEE'>" +
                                    "<a href='#' class='updatemustcomplete' data-name='must_complete' data-type='select' data-pk='' data-url='' data-title='Compulsory'>{{($assessment->compulsory ? 'Yes' : 'No') }}</a></td>" +
                                    "<td style='background-color: #EEEEEE'>" +
                                    "<a href='#' class='updatecomponent' data-name='component' data-type='text' data-pk='' data-url='' data-title='title'>{{$assessment->title}}</a></td>" +
                                    "<td style='background-color: #EEEEEE'><a href='#' class='updateexam_length' data-name='exam_length' data-type='text' data-pk='' data-url='' data-title='formattedExamLength'>{{$assessment->formattedExamLength}}</a></td>" +
                                    "<td style='background-color: #EEEEEE'><a href='#' class='updateweight' data-name='weight' data-type='text' data-pk='' data-url='' data-title='weightString'>{{$assessment->weightString}}</a></td>" +
                                    "<td style='background-color: #EEEEEE'><a href='#' class='updatemode' data-name='mode' data-type='text' data-pk='' data-url='' data-title='studyMode'>{{$assessment->studyMode}}</a></td>" +
                                    "<td style='background-color: #EEEEEE'>" +

                                    "Word count: <a href='#' class='updateword_count' data-name='word_count' data-type='text' data-pk='' data-url='' data-title='wordCount'>{{$assessment->wordCount}}</a> words" +

                                    "</td><td style='background-color: #FFF'>" + '<a href="#" onclick="$(this).closest(\'tr\').add($(this).closest(\'tr\').next()).remove()"><i class="fa fa-times" style="font-size: 2em; color: red" aria-hidden="true"></i></a></td>' +
                                    "</tr>" +
                                    "<tr >" +
                                    "<td colspan='6'><p><span style='font-style: italic'> Assessment notes:</span><br/><a href='#' class='updatenotes' data-name='notes' data-type='textarea' data-pk='' data-url='' data-title='notes'>{!! preg_replace('~[\r\n]+~', '', trim($assessment->notes))!!}</a></p>" +
                                    "<p><span style='font-style: italic'> Relates to Learning Outcomes (LO):</span><br/><a href='#' class='updaterelates_to_lo' data-name='relates_to_lo' data-type='text' data-pk='' data-url='' data-title='notesOutcomesAttributes'>{{preg_replace('~[\r\n]+~', '', trim($assessment->notesOutcomesAttributes))}}</a></p></tr>");

                                @endforeach
                                @endif
                                @endif
                                $('.updatemustcomplete').editable({
                                    source: [
                                        {value: 'Yes', text: 'Yes'},
                                        {value: 'No', text: 'No'}
                                    ],
                                    send: 'never',
                                    url: '',
                                    onblur: 'submit'
                                });

                                $('.updatecomponent, .updateexam_length, .updateweight, .updatemode, .updateword_count, .updatenotes, .updaterelates_to_lo').editable({
                                    url: '',
                                    send: 'never',
                                    emptytext: 'Click to edit',
                                    onblur: 'submit'
                                });

                                break;
                            default:
                                currenteditingid = -1;
                                currentdeletingid = -1;
                                break;
                        }

                        $('.updateinlineeditabletextarea').editable({
                            url: '',
                            send: 'never',
                            emptytext: 'Click to edit',
                            onblur: 'submit'
                        });

                        $('.updateinlineeditabletextinput').editable({
                            url: '',
                            send: 'never',
                            emptytext: 'Click to edit',
                            onblur: 'submit'
                        });


                    })
                    ;

                $('.modalform').on('shown.bs.modal', function () {
                    $(this).find("form").validator();
                });

                    @can('update_review_instance')
                    $('#editunitdialog').submit(function (event) {
                        // cancels the form submission
                        event.preventDefault();
                        $(this).modal('hide');
                        //  var vars = $("#newmediadialog").find("form").serializeArray();
                        var vars = $(this).find("form").serializeArray();

                        vars.push({name: 'id', value: '{{$review->id}}'});
                        waitingDialog.show();
                        submitUpdateForm(vars, 'unitreviews/{{$review->id}}');
                    });

                $('#editlddialog').submit(function (event) {
                    // cancels the form submission
                    event.preventDefault();
                    $(this).modal('hide');
                    //  var vars = $("#newmediadialog").find("form").serializeArray();
                    var vars = $(this).find("form").serializeArray();
                    vars.push({name: 'id', value: '{{$review->id}}'});
                    waitingDialog.show();
                    submitUpdateForm(vars, 'unitreviews/{{$review->id}}');
                });

                $('#editpeerreviewerdialog').submit(function (event) {
                    // cancels the form submission
                    event.preventDefault();
                    $(this).modal('hide');
                    //  var vars = $("#newmediadialog").find("form").serializeArray();
                    var vars = $(this).find("form").serializeArray();
                    vars.push({name: 'id', value: '{{$review->id}}'});
                    waitingDialog.show();
                    submitUpdateForm(vars, 'unitreviews/{{$review->id}}');
                });

                    @endcan

                // update outcomes text-only dialogues
                $('#updateoutcomestextitemdialog').submit(function (event) {
                    console.log('updateoutcomestextitemdialog submitted: setting text at ' + currentoutcometargetid);
                    // cancels the form submission
                    event.preventDefault();
                    $(this).modal('hide');

                    var data = new FormData();
                    data.append('new_' + currentoutcometargetfield, tinyMCE.get('updateoutcomestextcontrol').getContent());
                    data.append('id', currentoutcometargetid);
                    data.append('_token', '{{ csrf_token() }}');
                    submitUpdateFormWithFormData(data, 'reviewoutcome');
                });

                // update learning outcomes
                $('#editlearningoutcomedialog').submit(function (event) {
                    // cancels the form submission
                    console.log('editlearningoutcomedialog submitted');
                    event.preventDefault();
                    //$(this).modal('hide');
                    var vars = $(this).find("form").serializeArray();
                    console.log(vars);
                    var i = 0;
                    var items = '{"items":[';
                    $(".updateitemitemrow").each(function () {
                        i++;
                        items += '{"text":"' + $(this).find('.updateinlineeditabletextarea').editable('getValue')['description'] + '", "id":"' + $(this).find('.updateinlineeditabletextarea').data('pk') + '"},';
                    })
                    items = items.slice(0, -1);
                    items += ']}';
                    console.log(items);

                    //  var vars = $("#newmediadialog").find("form").serializeArray();
                    if ($(".updateitemitemrow").length > 0) {
                        vars.push({name: 'items', value: items});
                    }
                    vars.push({name: 'id', value: '{{$review->review_instances_outcomes->id}}'});

                    waitingDialog.show();
                    submitUpdateLOForm(vars);
                });

                // update learning outcomes
                $('#editassessmentsoutcomedialog').submit(function (event) {
                    // cancels the form submission
                    console.log('editassessmentsoutcomedialog submitted');
                    event.preventDefault();
                    //$(this).modal('hide');
                    var vars = $(this).find("form").serializeArray();
                    //   console.log(vars);
                    var i = 0;
                    var items = '{"items":[';
                    $(".updateassessmentitemrow").each(function () {
                        console.log($(this).find('.updatemustcomplete').text());
                        i++;
                        items += '{"must_complete":"' + (($(this).find('.updatemustcomplete').editable('getValue')['must_complete'] === undefined) ? $(this).find('.updatemustcomplete').text() : $(this).find('.updatemustcomplete').editable('getValue')['must_complete']) +
                            '", "component":"' + $(this).find('.updatecomponent').editable('getValue')['component'] +
                            '", "exam_length":"' + $(this).find('.updateexam_length').editable('getValue')['exam_length'] +
                            '", "weight":"' + $(this).find('.updateweight').editable('getValue')['weight'] +
                            '", "mode":"' + $(this).find('.updatemode').editable('getValue')['mode'] +
                            '", "word_count":"' + $(this).find('.updateword_count').editable('getValue')['word_count'] +
                            '", "notes":"' + $(this).next('tr').find('.updatenotes').editable('getValue')['notes'] +
                            '", "relates_to_lo":"' + $(this).next('tr').find('.updaterelates_to_lo').editable('getValue')['relates_to_lo'] +
                            '", "id":"' + $(this).next('tr').find('.updatemustcomplete').data('pk') +
                            '"},';
                    })
                    items = items.slice(0, -1);
                    items += ']}';
                    console.log(items);

                    //  var vars = $("#newmediadialog").find("form").serializeArray();
                    if ($(".updateassessmentitemrow").length > 0) {
                        vars.push({name: 'items', value: items});
                    }
                    vars.push({name: 'id', value: '{{$review->review_instances_outcomes->id}}'});

                    // waitingDialog.show();
                    submitUpdateassessmentForm(vars);
                });


                $('.newdialog').submit(function (event) {
                    event.preventDefault();
                    $(this).modal('hide');
                    var vars = $(this).find("form").serializeArray();
                    waitingDialog.show();
                    submitNewForm(vars, $(this).data('route'));
                });


                // all deletes are the same
                $("#deletedialog").submit(function (event) {
                    $(this).modal('hide');
                    // cancels the form submission
                    event.preventDefault();
                    waitingDialog.show();
                    deleteItem(currentdeletingid, currentdeleteroute);
                });

                // files
                /**
                 *
                 *  media AJAX form support
                 *
                 * */

                // intercept new condition event dialogue form submission
                $('#newmediadialog').find("form").submit(function (event) {
                    $('#newmediadialog').modal('hide');
                    if ($('#upload_file')[0].files.length > 0) {
                        //  var vars = $("#newmediadialog").find("form").serializeArray();
                        var data = new FormData();
                        // file

                        jQuery.each($('#upload_file')[0].files, function (i, file) {
                            data.append('userfile', file);
                        });
                        data.append('review_id', '{{$review->id}}');
                        // data.append('description', $descriptionctrl.val());
                        var $descriptionctrl = $('[name=description]', $("#newmediadialog").find("form"));
                        //console.log($descriptionctrl.val());
                        data.append('description', $descriptionctrl.val());
                        data.append('_token', '{{ csrf_token() }}');
                        // console.log(data);
                        // cancels the form submission
                        event.preventDefault();
                        //   waitingDialog.show();
                        submitNewMediaForm(data);
                    } else {
                        alert('Need to pick a file')
                    }
                });

                // intercept edit media event dialogue form submission
                $('#editmediadialog').submit(function (event) {
                    $('#editmediadialog').modal('hide');
                    //  var vars = $("#newmediadialog").find("form").serializeArray();
                    console.log('editmediadialog submission');
                    var data = new FormData();
                    // file
                    jQuery.each($('#altupload_file')[0].files, function (i, file) {
                        data.append('userfile', file);
                    });
                    var $descriptionctrl = $('[name=description]', $("#editmediadialog").find("form"));

                    data.append('description', $descriptionctrl.val());
                    data.append('id', currenteditingid);
                    data.append('_method', 'PATCH');
                    data.append('_token', '{{ csrf_token() }}');

                    // cancels the form submission
                    event.preventDefault();
                    waitingDialog.show();
                    submitEditMediaForm(data);
                });

                    {{--@can('update_review_instance')--}}
                    $('#makelivedialog').submit(function (event) {
                        // cancels the form submission
                        event.preventDefault();
                        $(this).modal('hide');
                        //  var vars = $("#newmediadialog").find("form").serializeArray();
                        var vars = $(this).find("form").serializeArray();

                        waitingDialog.show();
                        $.ajax({
                            url: '{!! URL::to('')!!}/unitreviews/{{$review->id}}/makelive',
                            type: 'post',
                            data: vars,
                            error: function (jqXHR, textStatus, errorThrown) {
                                waitingDialog.hide();
                                alert(errorThrown);
                            },
                            success: function (data) {
                                //   waitingDialog.hide();
                                if (data.status.toString() == "1") {
                                    location.reload();
                                } else {
                                    waitingDialog.hide();
                                    alert('something went wrong with the update');
                                }
                            }
                        });
                    });

                $('#archivedialog').submit(function (event) {
                    // cancels the form submission
                    event.preventDefault();
                    $(this).modal('hide');
                    //  var vars = $("#newmediadialog").find("form").serializeArray();
                    var vars = $(this).find("form").serializeArray();

                    waitingDialog.show();
                    $.ajax({
                        url: '{!! URL::to('')!!}/unitreviews/{{$review->id}}/archive',
                        type: 'post',
                        data: vars,
                        error: function (jqXHR, textStatus, errorThrown) {
                            waitingDialog.hide();
                            alert(errorThrown);
                        },
                        success: function (data) {
                            //   waitingDialog.hide();
                            if (data.status.toString() == "1") {
                                location.replace('{!! URL::to('')!!}/unitreviews');
                            } else {
                                waitingDialog.hide();
                                alert('something went wrong with the update');
                            }
                        }
                    });
                });

                    {{--@endcan--}}
                    @endif

                // email support
                $('#newemaildialog').submit(function (event) {
                    // cancels the form submission
                    event.preventDefault();
                    $(this).modal('hide');
                    //  var vars = $("#newmediadialog").find("form").serializeArray();
                    var vars = $(this).find("form").serializeArray();
                    // hack to get combined units as comma separated values


                    waitingDialog.show();
                    submitMailForm(vars);
                });

            }
        )
        ;

        @if(!isset($review->archived_at))

        /*
         *
         * Helper functions
         *
         */


        // populates a form with data returned from Laravel
        function populate(frm, data) {
            $.each(data, function (key, value) {
                var $ctrl = $('[name=' + key + ']', frm);
                if ($ctrl.is("select")) {
                    if ($ctrl.attr('multiple')) {
                        console.log(key, value)
                        $ctrl.val(value.split(',')).trigger('change');
                    } else {
                        console.log(key, value)
                        $ctrl.val(value).trigger('change');
                    }
                } else if ($ctrl.is("textarea")) {
                    if ($ctrl.hasClass('tinymce')) {
                        // fix for this weird bug where tinymce chucks a wobbly if the content is set to a null value
                        tinyMCE.get($ctrl.attr('id')).setContent((value === null) ? '' : value);
                    } else {
                        $ctrl.val((value === null) ? '' : value);
                    }
                }
                else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":
                            $ctrl.val(value);
                            $ctrl.trigger('change');
                            break;

                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).prop("checked", true);
                                } else {
                                    $(this).prop("checked", false);
                                }
                                $(this).trigger('change');
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }

            });
        }

        function getDetails(id, route, formid) {
            //  console.log('getDetails(' + id + "," + route + "," + formid + ")");
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/' + id,
                type: 'GET',
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (data) {
                    //      console.log(data);
                    populate($("#" + formid).find("form"), data);
                    waitingDialog.hide();
                }
            });
        }

        // Edit
        function submitUpdateForm(vars, route) {
            console.log(route);
            console.log(vars);
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/update',
                type: 'POST',
                data: vars,
                //  processData: false,
                // contentType: false,

                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    //   waitingDialog.hide();
                    if (data.status.toString() == "1") {
                        location.reload();
                    } else {
                        waitingDialog.hide();
                        alert('something went wrong with the update');
                    }
                }
            });
        }

        function submitUpdateFormWithFormData(vars, route) {
            console.log(route);
            console.log(vars);
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/update',
                type: 'POST',
                data: vars,
                processData: false,
                contentType: false,

                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    //   waitingDialog.hide();
                    if (data.status.toString() == "1") {
                        location.reload();
                    } else {
                        waitingDialog.hide();
                        alert('something went wrong with the update');
                    }
                }
            });
        }

        function submitNewForm(vars, route) {
            vars.push({name: 'review_instances_id', value: '{{$review->id}}'})

            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/create',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (Number(data.id) > 0) {
                        location.reload();
                    } else {
                        alert('something went wrong with the insertion');
                    }
                }
            });
        }

        // Delete contact event
        function deleteItem(id, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/destroy',
                type: 'post',
                data: ([{name: 'id', value: id}, {name: '_method', value: 'DELETE'}, {
                    name: '_token',
                    value: '{{csrf_token()}}'
                }]),
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status == "1") {
                        location.reload();
                    } else {
                        alert('something went wrong with the delete');
                    }
                }
            });
        }

        @if(($review->reviewed_offering->coordinator['id']==Auth::user()->id)||($review->override_coordinator_id==Auth::user()->id))
        function updateCoordinatorStatus(id, value) {
            $("#waiting_cog_" + id).show();
            var data = new FormData();
            data.append('id', id);
            data.append('coordinator_status', value);
            data.append('_token', '{{csrf_token()}}');
            $.ajax({
                url: '{{URL::to('reviewinstancechecklistitem/update')}}',
                type: 'post',
                data: data,
                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#waiting_cog_" + id).hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    $("#waiting_cog_" + id).hide();
                    if (data < 1) {
                        alert('There was a problem with the update');
                    }
                }
            });
        }

        @endif

        @if(($review->peer_reviewer['id']==Auth::user()->id))
        function updatePeerReviewerStatus(id, value) {
            $("#waiting_cog_" + id).show();
            var data = new FormData();
            data.append('id', id);
            data.append('peer_reviewer_status', value);
            data.append('_token', '{{csrf_token()}}');
            $.ajax({
                url: '{{URL::to('reviewinstancechecklistitem/update')}}',
                type: 'post',
                data: data,
                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#waiting_cog_" + id).hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    $("#waiting_cog_" + id).hide();
                    if (data < 1) {
                        alert('There was a problem with the update');
                    }
                }
            });
        }

        @endif

        @if(($review->ld['id']==Auth::user()->id))
        function updateLDStatus(id, value) {
            $("#waiting_cog_" + id).show();
            var data = new FormData();
            data.append('id', id);
            data.append('ld_status', value);
            data.append('_token', '{{csrf_token()}}');
            $.ajax({
                url: '{{URL::to('reviewinstancechecklistitem/update')}}',
                type: 'post',
                data: data,
                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#waiting_cog_" + id).hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    $("#waiting_cog_" + id).hide();
                    if (data < 1) {
                        alert('There was a problem with the update');
                    }
                }
            });
        }
        @endif
        // Media-specific functions
        // New media
        function submitNewMediaForm(vars) {
            $.ajax({
                url: '{{URL::to('reviewmedia/create')}}',
                type: 'post',
                data: vars,
                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data > 0) {
                        location.reload();
                    }
                }
            });
        }

        // Edit media
        function submitEditMediaForm(vars) {
            $.ajax({
                url: '{{URL::to('reviewmedia/update')}}',
                type: 'post',
                data: vars,
                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {

                    if (data.status == "1") {
                        location.reload();
                    } else {
                        waitingDialog.hide();
                        alert('something went wrong with the update');
                    }
                }
            });
        }

        @endif

        function loademailtemplate() {

            $.ajax({
                url: '{{URL::to('reviewemails/')}}/' + $('#template_id').select2('val'),
                type: 'get',

                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {

                    populate($("#newemaildialog").find("form"), data);
                }
            });
        }

        // submits the mail form, triggers the sending of an email
        function submitMailForm(vars) {

            $.ajax({
                url: '{!! URL::to('')!!}/unitreviews/{{$review->id}}/sendemail',
                type: 'get',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (Number(data.status) == 0) {
                        location.reload();
                    } else {
                        alert('something went wrong with the insertion');
                    }
                }
            });
        }

        // loading teh CAUC data into this unit reire

        function updateCaucData() {
            waitingDialog.show();
            $.ajax({
                url: '{!! URL::to('')!!}/unitreviews/{{$review->id}}/updatecaucdata',
                type: 'get',
                data: ([{name: '_token', value: '{{csrf_token()}}'}]),
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();

                    if (data > 0) {
                        location.reload();
                    } else {
                        alert('something went wrong with the update');
                    }
                }
            });
        }

        function submitUpdateOutcomesForm(vars, route) {
            // console.log(route);
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/update',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    //   waitingDialog.hide();
                    if (data.status.toString() == "1") {
                        location.reload();
                    } else {
                        waitingDialog.hide();
                        alert('something went wrong with the update');
                    }
                }
            });
        }

        // adds a line to the learning outcomes list
        function addLOItem() {

            $("#loitemstbl tbody").append('<tr class="updateitemitemrow">' +
                '<td><a href="#" class="updateinlineeditabletextinput" data-name="description" data-type="textarea" data-pk="" data-url="" data-title="Description">New learning outcome</a></td>' +
                '<td><a href="#" onclick="$(this).closest(\'tr\').remove()"><i class="fa fa-times" style="font-size: 2em; color: red"aria-hidden="true"></i></a></td>' +
                '</tr> ');


            $('.updateinlineeditabletextinput').editable({
                url: '',
                send: 'never',
                emptytext: 'Click to edit',
                onblur: 'submit'
            });

        }

        function addAssessmentItem() {

            $("#assessmentitemstbl tbody").append("<tr style='background-color: #D2D2D2' class='updateassessmentitemrow'>" +
                "<td style='background-color: #EEEEEE'>" +
                "<a href='#' class='updatemustcomplete' data-name='must_complete' data-type='select' data-pk='' data-url='' data-title='Compulsory'></a></td>" +
                "<td style='background-color: #EEEEEE'>" +
                "<a href='#' class='updatecomponent' data-name='component' data-type='text' data-pk='' data-url='' data-title='title'></a></td>" +
                "<td style='background-color: #EEEEEE'><a href='#' class='updateexam_length' data-name='exam_length' data-type='text' data-pk='' data-url='' data-title='formattedExamLength'></a></td>" +
                "<td style='background-color: #EEEEEE'><a href='#' class='updateweight' data-name='weight' data-type='text' data-pk='' data-url='' data-title='weightString'></a></td>" +
                "<td style='background-color: #EEEEEE'><a href='#' class='updatemode' data-name='mode' data-type='text' data-pk='' data-url='' data-title='studyMode'></a></td>" +
                "<td style='background-color: #EEEEEE'>" +

                "Word count: <a href='#' class='updateword_count' data-name='word_count' data-type='text' data-pk='' data-url='' data-title='wordCount'></a> words" +

                "</td><td style='background-color: #FFF'>" + '<a href="#" onclick="$(this).closest(\'tr\').add($(this).closest(\'tr\').next()).remove()"><i class="fa fa-times" style="font-size: 2em; color: red" aria-hidden="true"></i></a></td>' +
                "</tr>" +
                "<tr >" +
                "<td colspan='6'><p><span style='font-style: italic'> Assessment notes:</span><br/><a href='#' class='updatenotes' data-name='notes' data-type='textarea' data-pk='' data-url='' data-title='notes'></a></p>" +
                "<p><span style='font-style: italic'> Relates to Learning Outcomes (LO):</span><br/><a href='#' class='updaterelates_to_lo' data-name='relates_to_lo' data-type='text' data-pk='' data-url='' data-title='relates_to_lo'></a></p></tr>");


            $('.updatemustcomplete').editable({
                source: [
                    {value: 'true', text: 'Yes'},
                    {value: 'false', text: 'No'}
                ],
                send: 'never',
                url: '',
                onblur: 'submit'
            });

            $('.updatecomponent, .updateexam_length, .updateweight, .updatemode, .updateword_count, .updatenotes, .updaterelates_to_lo').editable({
                url: '',
                send: 'never',
                emptytext: 'Click to edit',
                onblur: 'submit'
            });


        }

        function submitUpdateLOForm(vars) {
            $.ajax({
                url: '{!! URL::to('')!!}/reviewlooutcome/update',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status == 0) {
                        location.reload();
                    } else {
                        waitingDialog.hide();
                        alert('something went wrong with the update');
                    }
                }
            });
        }

        function submitUpdateassessmentForm(vars) {
            $.ajax({
                url: '{!! URL::to('')!!}/reviewassessmenttasksoutcome/update',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status == 0) {
                        location.reload();
                    } else {
                        waitingDialog.hide();
                        alert('something went wrong with the update');
                    }
                }
            });
        }

        /**
         * @TODO implement this: gets the chenge request as a Word document
         */
        function getWordDocument() {

        }
    </script>

    <div class="col-md-12 row noprint" style="margin: 0px; padding: 0px; ">
        <div class="col-md-10"
             style="margin: 0px; padding: 0px">{!! Breadcrumbs::render('unitreviews.show', $review) !!}</div>
        <div class="col-md-2 text-right" style="margin: 0px; padding: 0px;  "><a
                    href="https://sorm-une-dev.une.edu.au/wiki/doku.php?id=the_unit_review_process" target="_blank"
                    style=" font-size: 1.1em;">Need help?&nbsp;<i class="fa fa-question-circle" style="font-size: 2em;"
                                                                  aria-hidden="true"></i></a>
        </div>
    </div>

    <div class="col-md-12 page-header noprint" style="margin: 0px; padding: 0px">

        <div class="box box-element">
            <div class="view">
                <div class="row">
                    <div class="container col-md-4">

                        <h3 style="margin-top: 0px">Review
                            for
                            <a href="{{$review->reviewed_offering->moodle_url}}"> {{$review->reviewed_offering->unit['unit_code']}} {{$review->reviewed_offering->teaching_period->teaching_period_lookup['teaching_period']}} {{$review->reviewed_offering->teaching_period['year']}}</a>

                            @if(!isset($review->archived_at))
                                @can('update_review_instance')
                                    <a href="#" data-toggle="modal" data-target="#editunitdialog">(Update)</a>
                                @endif
                            @endcan
                        </h3>


                        <strong>Co-ordinator:</strong>
                        @if(isset($review->override_coordinator))
                            {{$review->override_coordinator['name']}}  {{($review->override_coordinator['id']==Auth::user()->id)?'(you)':''}}
                            (Unit was taught by {{$review->reviewed_offering->coordinator['name']}})
                        @else

                            {{$review->reviewed_offering->coordinator['name']}} {{($review->reviewed_offering->coordinator['id']==Auth::user()->id)?'(you)':''}}
                        @endif

                        <br/>
                        <strong>Peer
                            Reviewer:</strong> {{$review->peer_reviewer['name']}} {{($review->peer_reviewer['id']==Auth::user()->id)?'(you)':''}}
                        {{--@if(!isset($review->archived_at))--}}
                        {{--@can('update_review_instance')--}}
                        {{--<a href="#" data-toggle="modal" data-target="#editpeerreviewerdialog">(Update)</a>--}}
                        {{--@endcan--}}
                        {{--@endif--}}

                        <br/>
                        <strong>Learning
                            Designer:</strong> {{$review->ld['name']}} {{($review->ld['id']==Auth::user()->id)?'(you)':''}}
                        {{--@if(!isset($review->archived_at))--}}
                        {{--@can('update_review_instance')<a href="#" data-toggle="modal"--}}
                        {{--data-target="#editlddialog">(Update)</a>--}}
                        {{--@endcan--}}
                        {{--@endif--}}
                        <br/>
                        <strong>Start date: </strong>{{$review->start_date}}
                        <br/>
                        <strong>End date: </strong>{{$review->end_date}}
                        @if(Carbon\Carbon::createFromFormat('j/m/Y', $review->end_date)->isFuture())
                            (expect completion
                            in {{\Carbon\Carbon::now()->diffInDays(Carbon\Carbon::createFromFormat('j/m/Y', $review->end_date))}}
                            days)
                        @else
                            (Overdue by {{\Carbon\Carbon::now()->diffInDays(Carbon\Carbon::createFromFormat('j/m/Y', $review->end_date))}} days)
                        @endif
                        <br/>


                    </div>
                    <div class="container col-md-6">

                        <div class="row">
                            <div class="col-md-2"><strong>Complete:</strong></div>
                            @if($review->checklist()->get()->where('heading', '<>', '1')->count()>0)
                                <div class="progress">
                                    <div class="progress-bar" id="progress_bar" role="progressbar"
                                         aria-valuenow="{{round(($review->checklistcomplete/($review->checklist()->get()->where('heading', '<>', '1')->count()*3))*100)}}"
                                         aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;
                                            width: {{round(($review->checklistcomplete/($review->checklist()->get()->where('heading', '<>', '1')->count()*3))*100)}}%">
                                        {{round(($review->checklistcomplete/($review->checklist()->get()->where('heading', '<>', '1')->count()*3))*100)}}
                                        %
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="font-weight: bold; text-decoration: underline"><a
                                        data-toggle="collapse" id="collapsiblelink" href="#collapsiblecoursepanel">
                                    Courses/Majors in which the unit is Core, Prescribed or
                                    Listed:<span id="collapseindicator">&nbsp;&nbsp;&nbsp;&nbsp;(Show)<i
                                                class="fa fa-chevron-right" aria-hidden="true"></i></span> </a></div>
                            <div class="col-md-12 collapse" id="collapsiblecoursepanel">
                                <table class="table table-condensed ">
                                    <thead class="thead-inverse">
                                    <tr>
                                        <th>
                                            Course
                                        </th>
                                        <th>
                                            Trim container
                                        </th>
                                        <th>
                                            Trim reference(s)
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($review->reviewed_offering->unit->courses as $course)
                                        <tr>
                                            <td>
                                                @can('is_admin')
                                                    <a href="{{action('CourseController@show', $course->id)}}">
                                                        @endcan
                                                        {{$course->label}} ({{$course->course_code}})
                                                        @can('is_admin')</a>
                                                @endcan

                                                @if(isset($course->pivot->field_of_study_id))
                                                    <p/>
                                                    {{ucwords(\App\Field_of_study::find($course->pivot->field_of_study_id)->major_or_minor)}}
                                                    : {{\App\Field_of_study::find($course->pivot->field_of_study_id)->label}}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($course->trim->last()!==null)
                                                    Course: {{$course->trim->last()->reference}}
                                            @endif

                                            <td>
                                                @if(isset($course->pivot->field_of_study_id))
                                                    <p/>
                                                    @if (\App\Field_of_study::find($course->pivot->field_of_study_id)->trim->last()!==null)
                                                        Field Of
                                                        Study:  {{\App\Field_of_study::find($course->pivot->field_of_study_id)->trim->last()->reference}}
                                                    @endif
                                                @endif</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                    <div class="container col-md-2">

                        @if(!isset($review->archived_at))
                            @can('update_review_instance')
                                @if($review->live!='true')
                                    <button id="edit_reivew_but" class="btn btn-info btn-lg" style="width: 100%"
                                            data-toggle="modal"
                                            data-target="#makelivedialog">Make review live
                                    </button>
                                @else
                                    <button id="edit_reivew_but" class="btn btn-info btn-lg" style="width: 100%"
                                            data-toggle="modal"
                                            data-target="#archivedialog">Archive review
                                    </button>
                                @endif
                            @endif
                        @endcan


                    </div>
                </div>
            </div>
        </div>
    </div>

    <ul class="nav nav-tabs noprint" id="tabslabels">
        <li class="active"><a data-toggle="tab" href='#checklisttab'>Review Checklist Items</a></li>
        <li class=""><a data-toggle="tab" href='#notestab'>Review Notes/log ({{$review->log->count()}})</a></li>
        <li class=""><a data-toggle="tab" href='#mediatab'>Associated Media ({{$review->media->count()}})</a></li>
        {{--<li class=""><a data-toggle="tab" href='#emailstab'>Messages ({{$review->emaillog->count()}})</a></li>--}}
        <li class=""><a data-toggle="tab" href='#outcomestab'>Unit Amendments Outcome/synthesis</a></li>
    </ul>
    <div class="tab-content noprint">
        <div id="checklisttab" class="tab-pane active printable">
            <fieldset style="width: 100%">
                <legend>
                    <div class="col-md-12 row" style="margin: 0px; padding: 0px; ">
                        <div class="col-md-10 text-left">
                            Review Checklist...
                            @can('update_review_instance')
                                <button class="btn btn-info btn-lg" data-toggle="modal"
                                        data-target="#newchecklistitemdialog">New item
                                </button>

                            @endcan
                        </div>
                        <div class="col-md-2 text-right" style="font-size: 16px">
                            @can('update_review_instance')
                                <a href="{!! URL::to('')!!}/unitreviews/{{$review->id}}/getaspdf" target="_blank"
                                >(View as PDF)
                                </a>
                            @endcan
                        </div>
                    </div>
                </legend>


                <table class="table table-striped">
                    <thead class="thead-inverse">
                    @can('update_review_instance')
                        <th>
                            Move
                        </th>
                    @endcan
                    <th>
                        Item
                    </th>


                    @if(($review->ld['id']==Auth::user()->id)||(Auth::user()->can('is_admin')=='true'))
                        <th>
                            Co-ordinator status
                        </th>
                        <th>
                            Peer Reviewer status
                        </th>
                        <th>
                            LD status
                        </th>
                    @elseif(($review->peer_reviewer['id']==Auth::user()->id)||($review->reviewed_offering->coordinator['id']==Auth::user()->id)||($review->override_coordinator_id==Auth::user()->id))
                        <th>
                            Status
                        </th>
                    @endif
                    <th>
                        notes/recomendations
                    </th>
                    @can('update_review_instance')
                        <th>
                        </th>
                    @endcan
                    </thead>

                    <tbody id="checklisttablebody">

                    @foreach($review->checklist as $item)
                        @php
                            switch($item->heading){
                            case 1:
                        @endphp
                        <tr style="background-color: #7ab800" class='sortablerow' entryid='{{$item->id}}'>
                            @can('update_review_instance')
                                <td style="width: 20px">
                                    <div class="handle" style="width: 20px"><i class="fa fa-2x fa-arrows-v handle"
                                                                               style='color: #bf800c'></i></div>
                                </td>
                            @endcan
                            <td colspan="100%">
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#editchecklistitemdialog">


                                    <h4> {{$item->description}}</h4>
                                </a>
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#deletedialog">
                                    <i class="fa fa-times-circle" style='color: #ff6600'></i></a>

                            </td>
                        </tr>
                        @php
                            break;
                            case 2:
                        @endphp
                        <tr style="background-color:rgba(122, 184, 0, 0.3)" class='sortablerow' entryid='{{$item->id}}'>
                            @can('update_review_instance')
                                <td style="width: 20px">
                                    <div class="handle" style="width: 20px"><i class="fa fa-2x fa-arrows-v handle"
                                                                               style='color: #bf800c'></i></div>
                                </td>
                            @endcan
                            <td colspan="100%">
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#editchecklistitemdialog">


                                    <span style="font-weight: bold; padding-left: 20px"> {{$item->description}}</span>
                                </a><br/>
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#deletedialog">
                                    <i class="fa fa-times-circle" style='color: #ff6600'></i></a>

                            </td>
                        </tr>
                        @php
                            break;
                            default:
                        @endphp
                        <tr class='sortablerow' entryid='{{$item->id}}'>
                            @can('update_review_instance')
                                <td>
                                    <div class="handle" style="width: 20px"><i class="fa fa-2x fa-arrows-v handle"
                                                                               style='color: #bf800c'></i></div>
                                </td>
                            @endcan
                            <td>
                                @if(($review->ld['id'] == Auth::user()->id)||Auth::user()->can('is_admin'))
                                    <a href="#" data-toggle="modal" data-id="{{$item->id}}"
                                       data-route="reviewinstancechecklistitem"
                                       data-target="#editchecklistitemdialog">
                                        @endif
                                        {{$item->description}}
                                        @if(($review->ld['id'] == Auth::user()->id)||Auth::user()->can('is_admin'))
                                    </a>
                                    @if($item->trigger_email==1)
                                        <br/><i class="fa fa-exclamation-triangle"
                                                style='color: #d9534f;'></i><strong>Will send email with template
                                            "{{$item->trigger_email_template->label}}" to Learning Designer when
                                            checked</strong>
                                    @endif
                                @endif
                            </td>


                            @if(($review->ld['id']==Auth::user()->id)||($review->reviewed_offering->coordinator['id']==Auth::user()->id)||($review->override_coordinator_id==Auth::user()->id)||Auth::user()->can('is_admin'))
                                <td>
                                    @if(($review->reviewed_offering->coordinator['id']==Auth::user()->id)||($review->override_coordinator_id==Auth::user()->id))
                                        <div class="radio radio-success">
                                            <input type="radio" name="coordinator_status_{{$item->id}}"
                                                   id="coordinator_status_{{$item->id}}_meets"
                                                   value="Meets criterion"
                                                   {{($item->coordinator_status=="Meets criterion")?'checked':''}}
                                                   {{($item->status == '1')?'checked':''}} data-id="{{$item->id}}"
                                                   onclick="updateCoordinatorStatus($(this).data('id'), $('#coordinator_status_{{$item->id}}_meets').val())">
                                            <label for="coordinator_status_{{$item->id}}_meets">Meets
                                                criterion</label>
                                        </div>
                                        <div class="radio radio-danger">
                                            <input type="radio" name="coordinator_status_{{$item->id}}"
                                                   id="coordinator_status_{{$item->id}}_doesnt"
                                                   value="Does not meet criterion"
                                                   {{($item->coordinator_status=="Does not meet criterion")?'checked':''}}
                                                   {{($item->status == '1')?'checked':''}} data-id="{{$item->id}}"
                                                   onclick="updateCoordinatorStatus($(this).data('id'), $('#coordinator_status_{{$item->id}}_doesnt').val())">
                                            <label for="coordinator_status_{{$item->id}}_doesnt">Does not meet
                                                criterion</label>

                                        </div>
                                        <div class="radio radio-warning">
                                            <input type="radio" name="coordinator_status_{{$item->id}}"
                                                   id="coordinator_status_{{$item->id}}_na" value="N/A"
                                                   {{($item->status == '1')?'checked':''}} data-id="{{$item->id}}"
                                                   {{($item->coordinator_status=="N/A")?'checked':''}}
                                                   onclick="updateCoordinatorStatus($(this).data('id'), $('#coordinator_status_{{$item->id}}_na').val())">
                                            <label for="coordinator_status_{{$item->id}}_na">N/A</label>

                                        </div>
                                        <i id="waiting_cog_{{$item->id}}" style="display: none"
                                           class="fa fa-cog fa-spin a-fw"></i>
                                    @endif

                                    <?php if ((isset($item['coordinator_status']) && ($review->reviewed_offering->coordinator['id'] != Auth::user()->id)) || ($review->override_coordinator_id == Auth::user()->id) || (Auth::user()->can('is_admin') == 'true')) {

                                        switch ($item['coordinator_status']) {
                                            case 'Does not meet criterion':
                                                print("<i class='fa fa-2x fa-close' style='color: red'></i>");
                                                break;
                                            case 'Meets criterion':
                                                print("<i class='fa fa-2x fa-check' style='color: green'></i>");
                                                break;
                                            case 'N/A':
                                                print("<span style='color: orange; font-weight: bolder'>N/A</span>");
                                                break;
                                            default:
                                                break;

                                        }
                                    }

                                    ?>
                                </td>
                            @endif
                            @if(($review->ld['id']==Auth::user()->id)||($review->peer_reviewer_id==Auth::user()->id)||(Auth::user()->can('is_admin')=='true'))
                                <td>
                                    @if(($review->peer_reviewer_id==Auth::user()->id))
                                        <div class="radio radio-success">
                                            <input type="radio" name="peer_reviewer_status_{{$item->id}}"
                                                   id="peer_reviewer_status_{{$item->id}}_meets"
                                                   value="Meets criterion"
                                                   {{($item->peer_reviewer_status=="Meets criterion")?'checked':''}}
                                                   {{($item->status == '1')?'checked':''}} data-id="{{$item->id}}"
                                                   onclick="updatePeerReviewerStatus($(this).data('id'), $('#peer_reviewer_status_{{$item->id}}_meets').val())">
                                            <label for="peer_reviewer_status_{{$item->id}}_meets">Meets
                                                criterion</label>
                                        </div>
                                        <div class="radio radio-danger">
                                            <input type="radio" name="peer_reviewer_status_{{$item->id}}"
                                                   id="peer_reviewer_status_{{$item->id}}_doesnt"
                                                   value="Does not meet criterion"
                                                   {{($item->peer_reviewer_status=="Does not meet criterion")?'checked':''}}
                                                   {{($item->status == '1')?'checked':''}} data-id="{{$item->id}}"
                                                   onclick="updatePeerReviewerStatus($(this).data('id'), $('#peer_reviewer_status_{{$item->id}}_doesnt').val())">
                                            <label for="peer_reviewer_status_{{$item->id}}_doesnt">Does not meet
                                                criterion</label>

                                        </div>
                                        <div class="radio radio-warning">
                                            <input type="radio" name="peer_reviewer_status_{{$item->id}}"
                                                   id="peer_reviewer_status_{{$item->id}}_na" value="N/A"
                                                   {{($item->status == '1')?'checked':''}} data-id="{{$item->id}}"
                                                   {{($item->peer_reviewer_status=="N/A")?'checked':''}}
                                                   onclick="updatePeerReviewerStatus($(this).data('id'), $('#peer_reviewer_status_{{$item->id}}_na').val())">
                                            <label for="peer_reviewer_status_{{$item->id}}_na">N/A</label>

                                        </div>
                                        <i id="waiting_cog_{{$item->id}}" style="display: none"
                                           class="fa fa-cog fa-spin a-fw"></i>
                                    @endif
                                    <?php if ((isset($item->peer_reviewer_status) && ($review->peer_reviewer_id != Auth::user()->id)) || (Auth::user()->can('is_admin') == 'true')) {

                                        switch ($item->peer_reviewer_status) {
                                            case 'Does not meet criterion':
                                                print("<i class='fa fa-2x fa-close' style='color: red'></i>");
                                                break;
                                            case 'Meets criterion':
                                                print("<i class='fa fa-2x fa-check' style='color: green'></i>");
                                                break;
                                            case 'N/A':
                                                print("<span style='color: orange; font-weight: bolder'>N/A</span>");
                                                break;
                                            default:
                                                break;

                                        }
                                    }

                                    ?>
                                </td>
                            @endif

                            @if(($review->ld['id']==Auth::user()->id)||(Auth::user()->can('is_admin')=='true'))

                                <td>
                                    @if(($review->ld['id']==Auth::user()->id))
                                        <div class="radio radio-success">
                                            <input type="radio" name="ld_status_{{$item->id}}"
                                                   id="ld_status_{{$item->id}}_meets"
                                                   value="Meets criterion"
                                                   {{($item->ld_status=="Meets criterion")?'checked':''}}
                                                   data-id="{{$item->id}}"
                                                   onclick="updateLDStatus($(this).data('id'), $('#ld_status_{{$item->id}}_meets').val())">
                                            <label for="ld_status_{{$item->id}}_meets">Meets
                                                criterion</label>
                                        </div>
                                        <div class="radio radio-danger">
                                            <input type="radio" name="peer_reviewer_status_{{$item->id}}"
                                                   id="peer_reviewer_status_{{$item->id}}_doesnt"
                                                   value="Does not meet criterion"
                                                   {{($item->ld_status=="Does not meet criterion")?'checked':''}}
                                                   data-id="{{$item->id}}"
                                                   onclick="updateLDStatus($(this).data('id'), $('#ld_status_{{$item->id}}_doesnt').val())">
                                            <label for="ld_status_{{$item->id}}_doesnt">Does not meet
                                                criterion</label>

                                        </div>
                                        <div class="radio radio-warning">
                                            <input type="radio" name="peer_reviewer_status_{{$item->id}}"
                                                   id="ld_status_{{$item->id}}_na" value="N/A"
                                                   data-id="{{$item->id}}"
                                                   {{($item->ld_status=="N/A")?'checked':''}}
                                                   onclick="updateLDStatus($(this).data('id'), $('#ld_status_{{$item->id}}_na').val())">
                                            <label for="ld_status_{{$item->id}}_na">N/A</label>

                                        </div>
                                        <i id="waiting_cog_{{$item->id}}" style="display: none"
                                           class="fa fa-cog fa-spin a-fw"></i>
                                    @endif

                                    {{--if the user isn't an LD but is an admin --}}
                                    <?php if ((isset($item->ld_status) && ($review->ld['id'] != Auth::user()->id)) || (Auth::user()->can('is_admin') == 'true')) {

                                        switch ($item->ld_status) {
                                            case 'Does not meet criterion':
                                                print("<i class='fa fa-2x fa-close' style='color: red'></i>");
                                                break;
                                            case 'Meets criterion':
                                                print("<i class='fa fa-2x fa-check' style='color: green'></i>");
                                                break;
                                            case 'N/A':
                                                print("<span style='color: orange; font-weight: bolder'>N/A</span>");
                                                break;
                                            default:
                                                break;

                                        }
                                    }

                                    ?>


                                </td>
                            @endif

                            <td>
                                @if(($review->reviewed_offering->coordinator['id']==Auth::user()->id)||($review->override_coordinator_id==Auth::user()->id)||($review->ld['id'] == Auth::user()->id)||($review->peer_reviewer['id'] == Auth::user()->id)||Auth::user()->can('view_review_instance'))
                                    <div style="width:100%; text-align: right">
                                        <button class="btn btn-info btn-sm" data-toggle="modal"
                                                data-id="{{$item->id}}"
                                                data-target="#updateitemtextdialog">Update
                                        </button>
                                    </div>
                                @endif
                                @if(($review->reviewed_offering->coordinator['id']==Auth::user()->id)||($review->override_coordinator_id==Auth::user()->id))
                                    @if(isset($item['coordinator_text']))
                                        {!! $item['coordinator_text']!!}
                                    @endif
                                @endif
                                @if(($review->peer_reviewer['id']==Auth::user()->id))
                                    @if(isset($item['peer_reviewer_text']))
                                        {!! $item['peer_reviewer_text'] !!}
                                    @endif
                                @endif
                                @if(($review->ld['id']==Auth::user()->id)||(Auth::user()->can('is_admin')=='true'))
                                    <ul>
                                        @if(isset($item['coordinator_text']))
                                            <li>Co-ordinator:<br/>{!! $item['coordinator_text']!!}</li>
                                        @endif
                                        @if(isset($item['peer_reviewer_text']))
                                            <li>Peer Reviewer:<br/>{!! $item['peer_reviewer_text'] !!}</li>
                                        @endif
                                        @if(isset($item['ld_text']))
                                            <li>Learning Designer:<br/>{!! $item['ld_text'] !!}</li>
                                        @endif

                                    </ul>
                                @endif
                            </td>

                            @can('update_review_instance')
                                <td>
                                    <a href="#" data-toggle="modal" data-id="{{$item->id}}"
                                       data-route="reviewinstancechecklistitem"
                                       data-target="#deletedialog">
                                        <i class="fa fa-2x fa-times-circle" style='color: #ff6600'></i>
                                    </a>
                                </td>
                            @endcan
                        </tr>
                        @php
                            }
                        @endphp
                        {{--@endif--}}
                    @endforeach
                    </tbody>
                </table>
            </fieldset>
        </div>

        <div id="notestab" class="tab-pane ">
            <fieldset style="width: 90%">
                <legend>Review notes

                    <button class="btn btn-info btn-lg" data-toggle="modal"
                            data-target="#newnoteitemdialog">New note
                    </button>

                </legend>
                <table class="table table-striped">
                    <thead class="thead-inverse">
                    <th>
                        Description
                        (click to update)

                    </th>
                    <th>
                        Text
                    </th>
                    <th>
                        Entered at
                    </th>
                    <th>
                        Entered by
                    </th>
                    </thead>

                    @foreach($review->log as $item)
                        <tr>
                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$item->id}}"
                                   data-route="reviewnotesitem"
                                   data-target="#editnoteitemdialog">


                                    {{$item->description}}

                                </a>

                            </td>
                            <td>
                                {{$item->text}}
                            </td>
                            <td>
                                {{$item->created_at}}
                            </td>
                            <td>
                                {{$item->created_by['name']}}

                            </td>

                            <td>

                                <a href="#" data-toggle="modal" data-id="{{$item->id}}"
                                   data-route="reviewnotesitem"
                                   data-target="#deletedialog">
                                    <i class="fa fa-2x fa-times-circle" style='color: red'></i>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                </table>
            </fieldset>
        </div>

        <div id="mediatab" class="tab-pane">
            <fieldset style="width: 90%">
                <legend>
                    Associated Media
                    <button id="new_file_but" class="btn btn-info btn-lg" data-toggle="modal"
                            data-target="#newmediadialog">Add
                    </button>
                </legend>
                <table class="table table-striped">
                    <thead class="thead-inverse">
                    <th>
                        Thumb
                    </th>
                    <th>
                        File name (click to download)
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                    </th>
                    <th>
                    </th>
                    </thead>
                    @foreach($review->media as $media)
                        <tr>
                            <td>
                                <img src="{{URL::asset('reviewmedia/thumb/'.$media->id)}}">
                            </td>
                            <td>
                                <a href="{{URL::asset('reviewmedia/download/'.$media->id)}}">{{$media->name}}</a>
                            </td>
                            <td>
                                {{$media->description}}
                            </td>
                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$media->id}}"
                                   data-target="#editmediadialog">
                                    Update
                                </a>
                            </td>
                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$media->id}}"
                                   data-target="#deletedialog"
                                   data-route="reviewmedia">
                                    <i class="fa fa-2x fa-times-circle" style='color: #ff6600'></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </table>
            </fieldset>
        </div>

        <div id="outcomestab" class="tab-pane ">
            <fieldset style="width: 100%">
                <legend>Unit Amendments Outcome
                    <a href="{!! URL::to('')!!}/unitreviews/{!! $review->id !!}/getoutcomesasword"
                       class="btn btn-primary"><i class="fa fa-file-word-o" aria-hidden="true"></i> Export</a>


                    <button class="btn btn-info" style="float: right" onclick="updateCaucData()">Update CAUC data
                    </button>
                </legend>
                (Please note- HTML tables don't export to Word yet)

                <table class="table table-striped">
                    <colgroup>
                        <col style="width: 10%;!important;">
                        <col style="width: 45%;">
                        <col style="width: 40%;">
                        <col style="width: 5%;">
                    </colgroup>
                    <thead class="thead-inverse">

                    <th>
                        Item
                    </th>
                    <th>
                        Current Version
                    </th>
                    <th>
                        Proposed Version
                    </th>
                    <th>
                        Confirmed
                    </th>
                    </thead>
                    <tbody>
                    <tr>
                        <td style="font-weight: bold">
                            Unit code
                        </td>
                        <td id="unit_code">
                            {{$review->review_instances_outcomes->unit_code}}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10" id="new_unit_code">
                                    {!! $review->review_instances_outcomes->new_unit_code !!}
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info btn-sm" data-field="unit_code"
                                            data-id="{{$review->review_instances_outcomes->id}}" data-toggle="modal"
                                            data-target="#updateoutcomestextitemdialog"
                                            style="float: right"><i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1"
                                       onclick="confirmoutcome('outcomes_unit_code')">
                                <label></label>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight: bold">
                            Unit title
                        </td>
                        <td id="unit_title">
                            {{ $review->review_instances_outcomes->unit_title}}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_unit_title">
                                    {!! $review->review_instances_outcomes->new_unit_title !!}
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info btn-sm"
                                            data-id="{{$review->review_instances_outcomes->id}}" data-field="unit_title"
                                            data-toggle="modal" data-target="#updateoutcomestextitemdialog"
                                            style="float: right"><i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1"
                                       onclick="confirmoutcome('outcomes_unit_title')">
                                <label></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Combined Unit(s)
                        </td>
                        <td id="combined_units">
                            {!! $review->review_instances_outcomes->combined_units !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_combined_units">
                                    {!! $review->review_instances_outcomes->new_combined_units !!}
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info btn-sm"
                                            data-id="{{$review->review_instances_outcomes->id}}"
                                            data-field="combined_units"
                                            data-toggle="modal" data-target="#updateoutcomestextitemdialog"
                                            style="float: right"><i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1"
                                       onclick="confirmoutcome('outcomes_combined_units')">
                                <label></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Credit Point Value
                        </td>
                        <td id="credit_points">
                            {!! $review->review_instances_outcomes->credit_points !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_credit_points">
                                    {!! $review->review_instances_outcomes->new_credit_points !!}
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info btn-sm" data-field="credit_points"
                                            data-id="{{$review->review_instances_outcomes->id}}"
                                            data-toggle="modal"
                                            data-target="#updateoutcomestextitemdialog"
                                            style="float: right"><i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1"
                                       onclick="confirmoutcome('outcomes_credit_points')">
                                <label></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Unit Description
                        </td>
                        <td id="description">
                            {!! $review->review_instances_outcomes->description !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_description">
                                    {!! $review->review_instances_outcomes->new_description !!}
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info btn-sm" data-field="description"
                                            data-toggle="modal" data-target="#updateoutcomestextitemdialog"
                                            data-id="{{$review->review_instances_outcomes->id}}"
                                            style="float: right"><i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1"
                                       onclick="confirmoutcome('outcomes_description')">
                                <label></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Unit Prerequisites
                        </td>
                        <td id="prerequisites">
                            {!! $review->review_instances_outcomes->prerequisites !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_prerequisites">
                                    {!! $review->review_instances_outcomes->new_prerequisites !!}
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info btn-sm" data-field="prerequisites"
                                            data-id="{{$review->review_instances_outcomes->id}}"
                                            data-toggle="modal" data-target="#updateoutcomestextitemdialog"
                                            style="float: right"><i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1"
                                       onclick="confirmoutcome('outcomes_prerequisites')">
                                <label></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Unit Corequisites
                        </td>
                        <td id="corequisites">
                            {!! $review->review_instances_outcomes->corequisites !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_corequisites">
                                    {!! $review->review_instances_outcomes->new_corequisites !!}
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info btn-sm" data-field="corequisites"
                                            data-id="{{$review->review_instances_outcomes->id}}"
                                            data-toggle="modal" data-target="#updateoutcomestextitemdialog"
                                            style="float: right"><i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1"
                                       onclick="confirmoutcome('outcomes_corequisites')">
                                <label></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Unit restrictions
                        </td>
                        <td id="restrictions">
                            {!! $review->review_instances_outcomes->restrictions !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_restrictions">
                                    {!! $review->review_instances_outcomes->new_restrictions !!}
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info btn-sm" data-field="restrictions"
                                            data-id="{{$review->review_instances_outcomes->id}}"
                                            data-toggle="modal" data-target="#updateoutcomestextitemdialog"
                                            style="float: right"><i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1"
                                       onclick="confirmoutcome('outcomes_restrictions')">
                                <label></label>
                            </div>
                        </td>
                    <tr>
                        <td style="font-weight: bold">
                            Offerings
                        </td>
                        <td id="offerings">
                            {!! $review->review_instances_outcomes->offerings !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10" id="new_offerings">
                                    {!! $review->review_instances_outcomes->new_offerings !!}
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info btn-sm" data-field="offerings"
                                            data-id="{{$review->review_instances_outcomes->id}}"
                                            data-toggle="modal" data-target="#updateoutcomestextitemdialog"
                                            style="float: right"><i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1"
                                       onclick="confirmoutcome('outcomes_offerings')">
                                <label></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Learning Outcomes
                        </td>
                        <td id="outcomes_learning_outcomes_src">

                            <ol>
                                @if(isset($review->review_instances_outcomes->learning_outcomes))
                                    @foreach(json_decode($review->review_instances_outcomes->learning_outcomes) as $learningOutcome)
                                        <li>{{$learningOutcome->description}}</li>
                                    @endforeach
                                @endif
                            </ol>

                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="learning_outcomes">
                                    <ol id="lo_list">
                                        {{--{{dd($review->review_instances_outcomes->review_instances_outcomes_learning_outcomes)}}--}}
                                        @foreach($review->review_instances_outcomes->review_instances_outcomes_learning_outcomes as $learningOutcome)
                                            <li>{{$learningOutcome->text}}</li>
                                        @endforeach
                                    </ol>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-success btn-sm"
                                            data-toggle="modal" data-target="#editlearningoutcomedialog"
                                            style="float: right"><i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1"
                                       onclick="confirmoutcome('outcomes_learning_outcomes')">
                                <label></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Assessment tasks
                        </td>
                        <td id="outcome_assessment_tasks_src">
                            <table>
                                <tr class='assessmentabletheader'>
                                    <th><strong>Must Complete</strong></th>
                                    <th><strong>Component</strong></th>
                                    <th><strong>Exam length</strong></th>
                                    <th><strong>Weight</strong></th>
                                    <th><strong>Mode</strong></th>
                                    <th><strong>No. words</strong></th>
                                </tr>
                                @if(isset($review->review_instances_outcomes->assessment_tasks))
                                    @foreach(json_decode($review->review_instances_outcomes->assessment_tasks) as $assessment)
                                        <tr style='background-color: #D2D2D2'>
                                            <td style='background-color: #EEEEEE'>{{($assessment->compulsory ? 'Yes' : 'No') }}</td>

                                            <td style='background-color: #EEEEEE'>{{$assessment->title}}</td>
                                            <td style='background-color: #EEEEEE'>{{$assessment->formattedExamLength}}</td>
                                            <td style='background-color: #EEEEEE'>{{$assessment->weightString}}</td>
                                            <td style='background-color: #EEEEEE'>{{$assessment->studyMode}}</td>
                                            <td style='background-color: #EEEEEE'>@if(strlen($assessment->wordCount)>0)
                                                    Word count: {{$assessment->wordCount}} words
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan='6'><p><span
                                                            style='font-style: italic'> Assessment notes:</span><br/>
                                                    {{$assessment->notes}}
                                                </p>
                                                <p>
                                                    <span style='font-style: italic'> Relates to Learning Outcomes (LO):</span><br/>
                                                    {{$assessment->notesOutcomesAttributes}}

                                                </p>
                                        </tr>
                                    @endforeach
                                @endif
                            </table>
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="outcome_assessment_tasks">
                                    <table>
                                        <tr class='assessmentabletheader'>
                                            <th><strong>Must Complete</strong></th>
                                            <th><strong>Component</strong></th>
                                            <th><strong>Exam length</strong></th>
                                            <th><strong>Weight</strong></th>
                                            <th><strong>Mode</strong></th>
                                            <th><strong>No. words</strong></th>
                                        </tr>
                                        @foreach($review->review_instances_outcomes->review_instances_outcomes_assessment_tasks as $assessment)
                                            <tr style='background-color: #D2D2D2'>
                                                <td style='background-color: #EEEEEE'>{{$assessment->must_complete }}</td>

                                                <td style='background-color: #EEEEEE'>{{$assessment->component}}</td>
                                                <td style='background-color: #EEEEEE'>{{$assessment->exam_length}}</td>
                                                <td style='background-color: #EEEEEE'>{{$assessment->weight}}</td>
                                                <td style='background-color: #EEEEEE'>{{$assessment->mode}}</td>
                                                <td style='background-color: #EEEEEE'>@if(strlen($assessment->word_count)>0)
                                                        Word count: {{$assessment->word_count}} words
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='6'><p><span
                                                                style='font-style: italic'> Assessment notes:</span><br/>
                                                        {{$assessment->notes}}
                                                    </p>
                                                    <p>
                                                        <span style='font-style: italic'> Relates to Learning Outcomes (LO):</span><br/>
                                                        {{$assessment->relates_to_lo}}

                                                    </p>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-success btn-sm" data-id="outcome_assessment_tasks"
                                            data-toggle="modal" data-target="#editassessmentsoutcomedialog"
                                            style="float: right"><i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1"
                                       onclick="confirmoutcome('outcome_assessment_tasks')">
                                <label></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">
                            Coordinators
                        </td>
                        <td id="coordinators">
                            {!! $review->review_instances_outcomes->coordinators !!}
                        </td>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-10"
                                     id="new_coordinators">{!! $review->review_instances_outcomes->new_coordinators !!}</div>
                                <div class="col-md-2">
                                    <button class="btn btn-info btn-sm" data-field="coordinators"
                                            data-id="{{$review->review_instances_outcomes->id}}"
                                            data-toggle="modal" data-target="#updateoutcomestextitemdialog"
                                            style="float: right"><i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1"
                                       onclick="confirmoutcome('outcomes_coordinators')">
                                <label></label>
                            </div>
                        </td>

                    </tbody>
                </table>

            </fieldset>
        </div>

    </div>

    {{--
    Dialogs for various things
    --}}
    @can('update_review_instance')
        <div id="editunitdialog" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Review</h4>
                    </div>
                    <div class="modal-body">

                        {!! Form::model($review, ['class'=>'form-horizontal'])!!}

                        @include('reviews.form.uniteditform', ['submitButtonText'=>'Update Review'])

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        {{--  Adding a new item event  --}}
        <div id="newchecklistitemdialog" class="modal fade newdialog " role="dialog"
             data-route="reviewinstancechecklistitem">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add checklist item</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('reviews.form.checklistitemform', ['submitButtonText'=>'Add Checklist Item'])
                        {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>


        {{--  Deleting a media  --}}
        <div id="deletedialog" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 300px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Really delete?</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('form_common.deletedialog', [ 'id'=>$review->id])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div id="makelivedialog" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 300px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Really make live?</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('form_common.confirmdialog', [ 'id'=>$review->id])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div id="archivedialog" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 300px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Really archive?</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('form_common.confirmdialog', [ 'id'=>$review->id])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        {{--  Editing a list itemt  --}}
        <div id="editchecklistitemdialog" class="modal fade editdialog" role="dialog"
             data-route="reviewinstancechecklistitem">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit checklist item</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('reviews.form.checklistitemupdateform', ['submitButtonText'=>'Update'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>


    @endcan


    {{--  Adding a note --}}
    <div id="newnoteitemdialog" class="modal fade newdialog" role="dialog"
         data-route="reviewnotesitem">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Note</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('reviews.form.noteitemform', ['submitButtonText'=>'Add Note'])
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>

    {{--  Editing a note --}}
    <div id="editnoteitemdialog" class="modal fade  editdialog modal-wide" role="dialog"
         data-route="reviewnotesitem">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Notes item</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('reviews.form.noteitemform', ['submitButtonText'=>'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    {{--Add a file to this record--}}
    <div id="newmediadialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add File</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('reviews.form.fileupload', ['submitButtonText'=>'Add File',  'reviewid'=>$review->id, 'required'=>'required', 'fileuploadcontrolid'=>'upload_file', 'commentscontrolid'=>'description'])
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>

    {{--Update a file--}}
    <div id="editmediadialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update File</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('reviews.form.fileupload', ['submitButtonText'=>'Update File', 'reviewid'=>$review->id, 'required'=>'', 'fileuploadcontrolid'=>'altupload_file', 'commentscontrolid'=>'description'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Editing the checklist item notes --}}
    <div id="updateitemtextdialog" class="modal fade editdialog modal-wide" role="dialog"
         data-route="reviewinstancechecklistitem">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit notes/recommendations</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('reviews.form.checklistitemnotesupdateform', ['submitButtonText'=>'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Editing a note --}}
    <div id="updateoutcomestextitemdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit item</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    <div class="col-sm-12">
                        {!! Form::textarea('text', null, ['class'=>'form-control tinymce', 'id'=>'updateoutcomestextcontrol']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::submit('Update', ['class'=>'btn btn-primary form-control']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    {{--Learning outcomes edit dialog--}}
    <div id="editlearningoutcomedialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Learning Outcomes</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('reviews.form.learningoutcomesupdateform', ['submitButtonText'=>'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--Assessment tasks edit dialog --}}
    <div id="editassessmentsoutcomedialog" class="modal modal-wide fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Assessment tasks</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('reviews.form.assessmenttasksupdateform', ['submitButtonText'=>'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--Printable view--}}
    <div class="printable">

        <div>

            <h3 style="margin-top: 0px">Review
                for {{$review->reviewed_offering->unit['unit_code']}} {{$review->reviewed_offering->teaching_period->teaching_period_lookup['teaching_period']}} {{$review->reviewed_offering->teaching_period['year']}}
            </h3>


            <strong>Co-ordinator:</strong>
            @if(isset($review->override_coordinator))
                {{$review->override_coordinator['name']}}
                (Unit was taught by {{$review->reviewed_offering->coordinator['name']}})
            @else
                {{$review->reviewed_offering->coordinator['name']}}
            @endif

            <br/>
            <strong>Peer
                Reviewer:</strong> {{$review->peer_reviewer['name']}}

            <br/>
            <strong>Learning
                Designer:</strong> {{$review->ld['name']}}
            <br/>
            <strong>Start date: </strong>{{$review->start_date}}
            <br/>
            <strong>End date: </strong>{{$review->end_date}}
            @if(Carbon\Carbon::createFromFormat('j/m/Y', $review->end_date)->isFuture())
                (expect completion
                in {{\Carbon\Carbon::now()->diffInDays(Carbon\Carbon::createFromFormat('j/m/Y', $review->end_date))}}
                days)
            @else
                (Overdue by {{\Carbon\Carbon::now()->diffInDays(Carbon\Carbon::createFromFormat('j/m/Y', $review->end_date))}} days)
            @endif
            <br/>


        </div>


        <div class="printable">

                <table class="table table-striped">
                    <thead class="thead-inverse">
                    <tr>
                        <th style="border: 1px solid gray;">
                            Item
                        </th>
                        <th style="border: 1px solid gray;">
                            Co-ordinator status
                        </th>
                        <th style="border: 1px solid gray;">
                            Peer Reviewer status
                        </th>
                        <th style="border-left: 1px solid gray; border-top: 1px solid gray; border-bottom: 1px solid gray;">
                            notes/recomendations
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($review->checklist as $item)
                        @php
                            switch($item->heading){
                            case 1:
                        @endphp
                        <tr class="heading1">
                            <td colspan="4">
                                <h4>{{$item->description}}</h4>
                            </td>
                        </tr>
                        @php
                            break;
                            case 2:
                        @endphp
                        <tr class="heading2">
                            <td colspan="4">
                                <h5> {{$item->description}}</h5>
                            </td>
                        </tr>
                        @php
                            break;
                            default:
                        @endphp

                        <tr>
                            <td>
                                {{$item->description}}
                            </td>

                            <td>
                                <?php

                                switch ($item['coordinator_status']) {
                                    case 'Does not meet criterion':
                                        print("<span style='color: red; font-weight: bolder'>Does not meet criterion</span>");
                                        break;
                                    case 'Meets criterion':
                                        print("<span style='color: green; font-weight: bolder'>Meets criterion</span>");
                                        break;
                                    case 'N/A':
                                        print("<span style='color: orange; font-weight: bolder'>N/A</span>");
                                        break;
                                    default:
                                        break;
                                }
                                //
                                //                            ?>
                            </td>


                            <td>
                                <!--                            --><?php
                                //
                                switch ($item->peer_reviewer_status) {
                                    case 'Does not meet criterion':
                                        print("<span style='color: red; font-weight: bolder'>Does not meet criterion</span>");
                                        break;
                                    case 'Meets criterion':
                                        print("<span style='color: green; font-weight: bolder'>Meets criterion</span>");
                                        break;
                                    case 'N/A':
                                        print("<span style='color: orange; font-weight: bolder'>N/A</span>");
                                        break;
                                    default:
                                        break;
                                    //
                                }
                                //
                                //                            ?>
                            </td>


                            <td>

                                <ul>
                                    @if(isset($item['coordinator_text']))
                                        <li>
                                            Co-ordinator:<br/>{!!    preg_replace('/(<[^>]+) style=".*?"/i', '$1', $item['coordinator_text']);!!}
                                        </li>
                                    @endif
                                    @if(isset($item['peer_reviewer_text']))
                                        <li>Peer
                                            Reviewer:<br/>{!!  preg_replace('/(<[^>]+) style=".*?"/i', '$1', $item['peer_reviewer_text']) !!}
                                        </li>
                                    @endif


                                </ul>
                            </td>


                            {{--</td>--}}
                        </tr>
                        @php
                            }
                        @endphp

                    @endforeach
                    </tbody>
                </table>

        </div>


    </div>


@stop
