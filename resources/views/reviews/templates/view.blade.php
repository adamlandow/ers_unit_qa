@extends('layouts.app')

@section('content')
    <style>
        table.note,
        table.note tr,
        table.note td {
            border: 1px solid black;
        }

        table.note {
            border-collapse: collapse;
        }

        table.table-striped td {
            border-left: solid 1px gray;

        }

        @if(($review->ld['id']==Auth::user()->id))
        table.table-striped tr td:nth-child(2) {
            width: 30%;
        }

        div.handle {
            width: 1%;
            white-space: nowrap;
            cursor: grab;
        }

        div.handle:active {
            width: 1%;
            white-space: nowrap;
            cursor: grabbing;
        }

        @else
        table.table-striped tr td:first-child {
            min-width: 10%;
            width: 30%;

        }

        @endif
    </style>
    <script src="{{ URL::asset('resources/assets/js/tinymce/tinymce.min.js') }}"></script>
    <script>

        //tinymce Bootstrap fix
        // https://www.tinymce.com/docs/integrations/bootstrap/
        $(document).on('focusin', function (e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });

        // init document
        $(document).ready(function () {

            // set up select2 dropdowns
            $('select').select2();

            $(':input[name=heading]').on("change", function (e) {
                if ($(this).val().toString() != '0') {
                    $(".notlabel").hide()
                } else {
                    $(".notlabel").show()
                }
            });

            tinymce.init({
                selector: '.tinymce',
                height: 500,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor @can('update_review_instance') template @endcan',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code'
                ],

                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | template',
                @can('update_review_instance')
                templates: [
                    {title: 'Unit description', description: 'Unit description from CAUC', content: '{cauc_description}'},
                    {title: 'Unit description table', description: 'Unit description from CAUC in a handy table for comparison with Moodle content', content: '{cauc_description_table}'},
                    {title: 'Learning outcomes table', description: 'Learning outcomes from CAUC in a handy table for comparison with Moodle content', content: '{cauc_learning_outcomes_comparison_table}'},
                    {title: 'Assessments table', description: 'Assessments from CAUC in a handy table for comparison with Moodle content', content: '{cauc_assessments_table}'},
                ],
                @endcan
                content_css: '//www.tinymce.com/css/codepen.min.css'
            });

            // Set up tab persistence across reloads
            if (location.hash.substr(0, 2) == "#!") {
                $("a[href='#" + location.hash.substr(2) + "']").tab("show");
            }

            $("a[data-toggle='tab']").on("shown.bs.tab", function (e) {
                var hash = $(e.target).attr("href");
                if (hash.substr(0, 1) == "#") {
                    location.replace("#!" + hash.substr(1));
                }
            });


            @if(!isset($review->archived_at))

            $('.editdialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();
                vars.push({name: 'id', value: currenteditingid});
                vars.push({name: '_method', value: 'PATCH'});

                waitingDialog.show();
                submitUpdateForm(vars, $(this).data('route'));
            });



            @can('update_review_instance')
            // drag and drop reordering
            dragula([document.getElementById('checklisttablebody')], {
                moves: function (el, container, handle) {
                    console.log(handle)
                    return handle.classList.contains('handle');
                }
            }).on('drop', function (el, target, source, sibling) {
                // send an array of elements to backend to reorder
                //    waitingDialog.show();
                console.log(sibling);

                var orderArray = ([]);
                var orderint = 0;
                $(".sortablerow").each(function () {
                    orderArray.push({id: $(this).attr('entryid'), order: orderint})
                    orderint++;
                    console.log($(this).attr('entryid'));
                });
                // for some reason, the last element of this is always the element being moved.
                orderArray.pop();

                var dataObj = ([{name: '_token', value: '{{csrf_token()}}'}, {
                    name: 'order',
                    value: JSON.stringify(orderArray)
                }]);

                $.ajax({
                    url: '{!! URL::to('')!!}/reviewinstancechecklistitem/reorder',
                    type: 'post',
                    data: dataObj,
                    error: function (jqXHR, textStatus, errorThrown) {
                        waitingDialog.hide();
                        alert(errorThrown);
                    },
                    success: function (data) {
                        //   waitingDialog.hide();
                        if (data.status.toString() == "0") {
                            location.reload(true);
                        } else {
                            waitingDialog.hide();
                            alert('something went wrong with the update');
                        }
                    }
                });

            });

            @endcan

         $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
                currenteditingid = $(this).data('id');
                switch ($(this).data('target')) {

                    case '#editchecklistitemdialog':
                        currenteditingid = $(this).data('id');
                        console.log(currenteditingid)
                        getDetails(currenteditingid, 'reviewinstancechecklistitem', 'editchecklistitemdialog');
                        break;


                    case '#deletedialog':
                        currentdeleteroute = $(this).data('route');
                        console.log($(this).data('route'));
                        currentdeletingid = $(this).data('id');
                        break;

                    default:
                        currenteditingid = -1;
                        currentdeletingid = -1;
                        break;
                }

            });

            $('.modalform').on('shown.bs.modal', function () {
                $(this).find("form").validator();
            });

            @can('update_review_instance')
            $('#editunitdialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();
                vars.push({name: 'id', value: '{{$review->id}}'});
                waitingDialog.show();
                submitUpdateForm(vars, 'unitreviews/{{$review->id}}');
            });


            @endcan

                        $('.newdialog').submit(function (event) {
                event.preventDefault();
                $(this).modal('hide');
                var vars = $(this).find("form").serializeArray();
                waitingDialog.show();
                submitNewForm(vars, $(this).data('route'));
            });


            // all deletes are the same
            $("#deletedialog").submit(function (event) {
                $(this).modal('hide');
                // cancels the form submission
                event.preventDefault();
                waitingDialog.show();
                deleteItem(currentdeletingid, currentdeleteroute);
            });


            @can('update_review_instance')


            $('#archivedialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();

                waitingDialog.show();
                $.ajax({
                    url: '{!! URL::to('')!!}/unitreviews/{{$review->id}}/archive',
                    type: 'post',
                    data: vars,
                    error: function (jqXHR, textStatus, errorThrown) {
                        waitingDialog.hide();
                        alert(errorThrown);
                    },
                    success: function (data) {
                        //   waitingDialog.hide();
                        if (data.status.toString() == "1") {
                            location.replace('{!! URL::to('')!!}/unitreviews');
                        } else {
                            waitingDialog.hide();
                            alert('something went wrong with the update');
                        }
                    }
                });
            });

            @endcan
            @endif

        });

        @if(!isset($review->archived_at))

        /*
         *
         * Helper functions
         *
         */



        // populates a form with data returned from Laravel
        function populate(frm, data) {
            console.log(('populating'))
            $.each(data, function (key, value) {
                var $ctrl = $('[name=' + key + ']', frm);
                if ($ctrl.is("select")) {
                    if ($ctrl.attr('multiple')) {
                        console.log(key, value)
                        $ctrl.val(value.split(',')).trigger('change');
                    } else {
                        console.log(key, value)
                        $ctrl.val(value).trigger('change');
                    }
                } else if ($ctrl.is("textarea")) {
                    if ($ctrl.hasClass('tinymce')) {
                        // fix for this weird bug where tinymce chucks a wobbly if the content is set to a null value
                        tinyMCE.get($ctrl.attr('id')).setContent((value === null) ? '' : value);
                    } else {
                        $ctrl.val((value === null) ? '' : value);
                    }
                }
                else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":
                            $ctrl.val(value);
                            $ctrl.trigger('change');
                            break;

                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).prop("checked", true);
                                }else{
                                    $(this).prop("checked", false);
                                }
                                $(this).trigger('change');
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }

            });
        }

        function getDetails(id, route, formid) {
            //  console.log('getDetails(' + id + "," + route + "," + formid + ")");
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/' + id,
                type: 'GET',
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (data) {
                    //      console.log(data);
                    populate($("#" + formid).find("form"), data);
                    waitingDialog.hide();
                }
            });
        }

        // Edit
        function submitUpdateForm(vars, route) {
            // console.log(route);
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/update',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    //   waitingDialog.hide();
                    if (data.status.toString() == "1") {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert('something went wrong with the update');
                    }
                }
            });
        }


        function submitNewForm(vars, route) {
            vars.push({name: 'review_instances_id', value: '{{$review->id}}'})

            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/create',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (Number(data.id) > 0) {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the insertion');
                    }
                }
            });
        }


        // Delete contact event
        function deleteItem(id, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/destroy',
                type: 'post',
                data: ([{name: 'id', value: id}, {name: '_method', value: 'DELETE'}, {
                    name: '_token',
                    value: '{{csrf_token()}}'
                }]),
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status == "1") {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the delete');
                    }
                }
            });
        }


        @endif

    </script>

    <!-- Tabs -->
    {!! Breadcrumbs::render('unitreviewstemplates.show', $review) !!}
    <div class="col-md-12 page-header" style="margin-top: 0px">

        <div class="box box-element">
            <div class="view">
                <div class="row">
                    <div class="container col-md-4">

                        <h3 style="margin-top: 0px">Template:
                            {{$review->label}}
                            <a href="#" data-toggle="modal" data-target="#editunitdialog">(Update)</a>

                        </h3>


                    </div>


                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div id="checklisttab" class="tab-pane active">
            <fieldset style="width: 100%">
                <legend>Review Checklist...
                    @can('update_review_instance')
                    <button class="btn btn-info btn-lg" data-toggle="modal"
                            data-target="#newchecklistitemdialog">New item
                    </button>
                    @endcan
                </legend>
                <table class="table table-striped" style="overflow-y: auto; overflow-x: hidden">
                    <thead class="thead-inverse">

                    <th style="width: 20px">
                        Move
                    </th>

                    <th>
                        Item (click to update)

                    </th>

                    <th>
                        notes/recomendations
                    </th>

                    <th>
                    </th>

                    </thead>
                    <tbody id="checklisttablebody">
                    @foreach($review->checklist as $item)

                        {{--@if(($item->heading)==1)--}}
                        @php
                        switch($item->heading){
                        case 1:
                        @endphp
                        <tr style="background-color: #7ab800" class='sortablerow' entryid='{{$item->id}}'>

                            <td style="width: 20px">
                                <div class="handle" style="width: 20px"><i class="fa fa-2x fa-arrows-v handle"
                                                                           style='color: #bf800c'></i></div>
                            </td>

                            <td colspan="100%">
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#editchecklistitemdialog">


                                    <h4> {{$item->description}}</h4>
                                </a>
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#deletedialog">
                                    <i class="fa fa-times-circle" style='color: #ff6600'></i></a>

                            </td>
                        </tr>
                        @php
                        break;
                        case 2:
                        @endphp
                        <tr style="background-color:rgba(122, 184, 0, 0.3)" class='sortablerow' entryid='{{$item->id}}'>

                            <td style="width: 20px">
                                <div class="handle" style="width: 20px"><i class="fa fa-2x fa-arrows-v handle"
                                                                           style='color: #bf800c'></i></div>
                            </td>

                            <td colspan="100%">
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#editchecklistitemdialog">


                                    <span style="font-weight: bold; padding-left: 20px"> {{$item->description}}</span>
                                </a><br/>
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#deletedialog">
                                    <i class="fa fa-times-circle" style='color: #ff6600'></i></a>

                            </td>
                        </tr>
                        @php
                        break;
                        default:
                        @endphp

                        <tr class='sortablerow' entryid='{{$item->id}}'>
                            <td style="width: 20px">
                                <div class="handle" style="width: 20px"><i class="fa fa-2x fa-arrows-v handle"
                                                                           style='color: #bf800c'></i></div>
                            </td>
                            <td>
                                <a href="#" data-toggle="modal"
                                   data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#editchecklistitemdialog">
                                    {!! $item['description']!!}
                                </a>
                                @if($item->trigger_email==1)
                                    <br/><i class="fa fa-exclamation-triangle"
                                            style='color: #d9534f;'></i><strong>Will send email with template
                                        "{{$item->trigger_email_template->label}}" to Learning Designer when
                                        checked</strong>
                                @endif
                            </td>
                            <td>


                                {!! $item['template_text']!!}

                            </td>

                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$item->id}}"
                                   data-route="reviewinstancechecklistitem"
                                   data-target="#deletedialog">
                                    <i class="fa fa-2x fa-times-circle" style='color: #ff6600'></i>
                                </a>
                            </td>

                        </tr>
                        @php
                        }
                        @endphp
                    @endforeach
                    </tbody>
                </table>
            </fieldset>
        </div>


    </div>

    {{--
    Dialogs for various things
    --}}

    <div id="editunitdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit template label</h4>
                </div>
                <div class="modal-body">

                    {!! Form::model($review, ['class'=>'form-horizontal'])!!}

                    @include('reviews.templates.form.uniteditform', ['submitButtonText'=>'Update template label'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>




    {{--  Adding a contact event  --}}
    <div id="newchecklistitemdialog" class="modal fade newdialog" role="dialog"
         data-route="reviewinstancechecklistitem">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add checklist item</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('reviews.templates.form.checklistitemform')
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>

    {{--  Deleting a media  --}}
    <div id="deletedialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('form_common.deletedialog', [ 'id'=>$review->id])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    {{--  Editing a list itemt  --}}
    <div id="editchecklistitemdialog" class="modal fade editdialog" role="dialog"
         data-route="reviewinstancechecklistitem">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit checklist item</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('reviews.templates.form.checklistitemupdateform', ['submitButtonText'=>'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    @if(count($errors) > 0)
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    @endif



@stop
