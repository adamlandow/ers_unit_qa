@extends('layouts.app')

@section('content')
    <script>

        var currentid = -1;

        $(document).ready(function () {
            @can('update_review_instance')
                        $('select').select2();

            // make all date class elements into datepicker
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
            });

            $('.modalform').on('shown.bs.modal', function () {
                $(this).find("form").validator();
            });

            $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
                currenteditingid = $(this).data('id');
                switch ($(this).data('target')) {
                    case '#deletedialog':
                        currentid = $(this).data('id');
                        break;
                    case '#copydialog':
                        currentid = $(this).data('id');
                        break;
                    default:
                        currentid = -1;
                        break;
                }

            });

            $('.newdialog').submit(function (event) {
                event.preventDefault();
                $(this).modal('hide');
                var vars = $(this).find("form").serializeArray();
                waitingDialog.show();
                submitNewForm(vars, $(this).data('route'));
            });

            $('#copydialog').submit(function (event) {
                event.preventDefault();
                $(this).modal('hide');
                var vars = $(this).find("form").serializeArray();
                vars.push({name: 'id', value: currentid});
                waitingDialog.show();
                submitCopyForm(vars, $(this).data('route'));
            });

            // all deletes are the same
            $("#deletedialog").submit(function (event) {
                $(this).modal('hide');
                // cancels the form submission
                event.preventDefault();
                waitingDialog.show();
                deleteItem(currentid, 'unitreviews');
            });
            @endcan
        });

        @can('update_review_instance')
        function submitNewForm(vars, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/create',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();

                    if (Number(data.id) > 0) {
                        location.reload(true);
                    }
                    else if ((data.error).length > 0) {
                        alert(data.error);
                    }
                    else {
                        alert('something went wrong with the insertion');
                    }
                }
            });
        }

        function submitCopyForm(vars) {
            $.ajax({
                url: '{!! URL::to('')!!}/unitreviewstemplates/clone',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();

                    if (Number(data.id) > 0) {
                        // take me to the newly created instance
                        location.replace('{!! URL::to('')!!}/unitreviewstemplates/' + data.id);
                    }
                    else if ((data.error).length > 0) {
                        alert(data.error);
                    }
                    else {
                        alert('something went wrong with the operation');
                    }
                }
            });
        }

        function deleteItem(id, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/destroy',
                type: 'post',
                data: ([{name: 'id', value: id}, {name: '_method', value: 'DELETE'}, {
                    name: '_token',
                    value: '{{csrf_token()}}'
                }]),
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status == "1") {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the delete');
                    }
                }
            });
        }

        /*
         *
         * Helper functions
         *
         */

        // populates a form with data returned from Laravel
        function populate(frm, data) {
            $.each(data, function (key, value) {
                var $ctrl = $('[name=' + key + ']', frm);
                if ($ctrl.is("select")) {
                    $ctrl.select2('val', value);
                } else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":
                            $ctrl.val(value);
                            break;
                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).attr("checked", value);
                                }
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }
            });
        }
        @endcan
    </script>
    {!! Breadcrumbs::render('unitreviewstemplates.index') !!}
    <fieldset style="padding-left: 15px; padding-right: 15px; margin-top: 0">
        @can('update_review_instance')
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newdialog">New Review  Template<i
                    class="fa fa-plus"></i>
        </button>
        @endcan
        &nbsp;
        <table class="table table-striped">
            <thead class="thead-inverse">
            <tr>
                <th class="headerSortable header">Template Name</th>

                <th>Copy</th>
                <th>Delete</th>
            </tr>
            </thead>
            @if(isset($instances))
                @foreach ($instances as $instance)
                    <tr>
                        <td>
                            <a href="{{action('ReviewInstancesController@templateshow', $instance->id)}}">
                                {{$instance->label}}
                            </a>

                        </td>
                        <td>
                            <a href="#" data-toggle="modal" data-id="{{$instance->id}}"
                               data-target="#copydialog">
                                <i class="fa fa-2x fa-copy" style='color: #0066ff'></i>
                            </a>
                        </td>
                        <td>
                            <a href="#" data-toggle="modal" data-id="{{$instance->id}}"
                               data-target="#deletedialog">
                                <i class="fa fa-2x fa-times-circle" style='color: #ff6600'></i>
                            </a>
                        </td>
                @endforeach
            @endif
        </table>
    </fieldset>
    @can('update_review_instance')
    <div id="newdialog" class="modal fade newdialog modalform" role="dialog"
         data-route="unitreviewstemplates">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Review</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open( ['class'=>'form-horizontal', 'role'=>'form'])!!}

                    @include('reviews.templates.form.newreviewform', ['submitButtonText'=>'Add review'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Copying a unit review event  --}}
    <div id="copydialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Copy a template</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['class'=>'form-horizontal', 'role'=>'form'])!!}
                    @include('reviews.templates.form.copyreviewform', ['submitButtonText'=>'Copy template'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Deleting a unit review event  --}}
    <div id="deletedialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('form_common.deletedialog')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @endcan

@stop
