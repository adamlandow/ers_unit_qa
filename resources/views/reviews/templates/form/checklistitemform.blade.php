<div class="form-group row">
    {!! Form::label('description', 'Description', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::text('description', null, ['class'=>'form-control' , 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('text', 'Heading Level', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <select class="select2 form-control" name="heading" style="width: 300px">
            <option value='0'>Not a heading</option>
            <option value='1'>Level 1</option>
            <option value='2'>Level 2 (subheading)</option>
        </select>
    </div>
</div>
<div class="form-group row text">
    {!! Form::label('template_id', 'Notify LD when checked', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-4">
        <div class="checkbox checkbox-success" >
            {!! Form::checkbox('trigger_email', 1, false, ['class'=>'styled', 'onchange' => 'if($(this).prop("checked")==true){$(".emailtemplate").show()}else{$(".emailtemplate").hide()}']) !!}
            <label>Check to send email to Learning Designer when this item is checked</label>
        </div>
    </div>
    <div class="col-sm-6 emailtemplate" >
        <div class="col-sm-12">{!! Form::label('template_id', 'Use template', ['class'=>'control-label  text-left']) !!}</div>

        <div class="col-sm-12">
            {!! Form::select('trigger_email_id', $emailtemplates->pluck('label', 'id'), null, ['style'=>"width: 300px", 'class'=>'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group row  template_text">
    {!! Form::label('text', 'Notes (gets added to all user\'s notes)', ['class'=>'control-label  col-sm-5 text-left',]) !!}
</div>
<div class="form-group row  template_text" >
    <div class="col-sm-12">
        {!! Form::textarea('template_text', null, ['class'=>'form-control tinymce' ]) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::submit('New checklist item', ['class'=>'btn btn-primary form-control']) !!}
</div>