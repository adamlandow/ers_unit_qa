<div class="form-group row">
    <div class="col-sm-12">    {!! Form::label('label', 'Template Name', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">
        {!! Form::text('label',null, ['style'=>"width: 300px", 'class'=>'form-control', 'required']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>