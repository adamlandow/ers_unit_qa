<div class="form-group row">
    {!! Form::label('description', 'Description', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::text('description', null, ['class'=>'form-control' , 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('text', 'This is a Heading', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::checkbox('heading', 1, false, ['id'=>'heading_cb', 'onclick' => 'if($("#heading_cb").prop("checked")==true){$(".template_text").hide()}else{$(".template_text").show()}']) !!}
    </div>
</div>
<div class="form-group row text">
    {!! Form::label('template_id', 'Notify LD when checked', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-4">
        <div class="checkbox checkbox-success" >
            {!! Form::checkbox('trigger_email', 1, false, ['class'=>'styled', 'onchange' => 'if($(this).prop("checked")==true){$(".emailtemplate").show()}else{$(".emailtemplate").hide()}']) !!}
            <label>Check to send email to Learning Designer when this item is checked</label>
        </div>
    </div>
    <div class="col-sm-6 emailtemplate" >
        <div class="col-sm-12">{!! Form::label('template_id', 'Use template', ['class'=>'control-label  text-left']) !!}</div>

        <div class="col-sm-12">
            {!! Form::select('trigger_email_id', $emailtemplates->pluck('label', 'id'), null, ['style'=>"width: 300px", 'class'=>'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group row  template_text">
    {!! Form::label('text', 'Notes (gets added to all user\'s notes)', ['class'=>'control-label  col-sm-5 text-left',]) !!}
</div>
<div class="form-group row  template_text" >

    <div class="col-sm-12">

        {!! Form::textarea('template_text', null, ['class'=>'form-control tinymce' ]) !!}

    </div>
</div>
<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>