
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('template_id', 'Use template', ['class'=>'control-label  text-left']) !!}</div>

    <div class="col-sm-12">
        {{--{!! Form::select('template_id', $emailtemplates->pluck('label', 'id'), null, ['style'=>"width: 300px", 'id'=>'template_id', 'class'=>'form-control', 'required']) !!}--}}
    </div>

</div>
<div class="form-group row">
    <div class="col-sm-12">
        {!! Form::label('subject', 'Subject- use {unit} for unit, {coordinator} for coordinator name', ['class'=>'control-label text-left']) !!}
    </div>
    <div class="col-sm-12">
        {!! Form::text('subject',null, [ 'class'=>'form-control', 'required']) !!}
    </div>
</div>
<div class="form-group row ">
    <div class="col-sm-12">
        {!! Form::label('text', 'Email text- use {unit} for unit, {coordinator} for coordinator name', ['class'=>'control-label  text-left']) !!}
    </div>
    <div class="col-sm-12">
        {!! Form::textarea('text', null, ['class'=>'form-control tinymce']) !!}
    </div>
</div>
<div class="form-group">

    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>