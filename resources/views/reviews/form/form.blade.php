<div class="form-group row">
    {!! Form::label('type', 'Peer Reviewer', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <select class="select2 form-control" name="peer_reviewer_id" style="width: 300px">
            @foreach ($users as $user)
                @if($user->id!=$review->reviewed_unit_instance->coordinator['id'])
                <option value='{{$user->id}}'>{{$user->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    {!! Form::label('type', 'Learning Designer', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <select class="select2 form-control" name="ld_id" style="width: 300px">
            @foreach ($users as $user)
                @if($user->id!=$review->reviewed_unit_instance->coordinator['id'])
                    <option value='{{$user->id}}'>{{$user->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>