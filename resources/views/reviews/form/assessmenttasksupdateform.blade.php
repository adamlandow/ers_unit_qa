<div class="form-group row edit_template_text" style="padding-left: 10px">
    <fieldset style="width: 100%">
        <legend style="">Assessment tasks
            <button id="additemitembut" type="button" class="btn btn-info btn-sm" onclick="addAssessmentItem()"><i
                        class="fa fa-plus" aria-hidden="true"></i></button>
        </legend>

        <table class="table table-striped table-condensed" id="assessmentitemstbl">
            <thead>
            <tr>
                <th>Must Complete</th>
                <th>Component</th>
                <th>Exam length</th>
                <th>Weight</th>
                <th>Mode</th>
                <th>No. words</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </fieldset>
</div>
<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>