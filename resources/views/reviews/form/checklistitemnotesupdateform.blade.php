<div class="form-group row notlabel">
    {!! Form::label(null, 'Notes/Recommendations', ['class'=>'control-label  col-sm-2 text-left']) !!}
</div>
<div class="form-group row ">

    <div class="col-sm-12 notlabel">
        @if(Auth::user()->can('is_admin')=='true')
            <strong>Coordinator:</strong><br/>
            {!! Form::textarea('coordinator_text', null, ['class'=>'form-control tinymce', 'id'=>'coordinator_text']) !!}
            <strong>Peer Reviewer:</strong><br/>
            {!! Form::textarea('peer_reviewer_text', null, ['class'=>'form-control tinymce' , 'id'=>'peer_reviewer_text']) !!}
            <strong>Learning Designer:</strong><br/>
            {!! Form::textarea('ld_text', null, ['class'=>'form-control tinymce' , 'id'=>'ld_text']) !!}
        @else
            <?php switch (Auth::user()->id){
            case $review->reviewed_offering->coordinator['id']:
            case $review->override_coordinator_id == Auth::user()->id:
            ?>
            {!! Form::textarea('coordinator_text', null, ['class'=>'form-control tinymce', 'id'=>'coordinator_text']) !!}
            <?php
            break;

            case $review->peer_reviewer['id']:
            ?>
            {!! Form::textarea('peer_reviewer_text', null, ['class'=>'form-control tinymce' , 'id'=>'peer_reviewer_text']) !!}
            <?php
            break;
            case $review->ld['id']:
            ?>
            {!! Form::textarea('ld_text', null, ['class'=>'form-control tinymce' , 'id'=>'ld_text']) !!}
            <?php
            break;
            }
            ?>
        @endif
    </div>


    <div class="form-group">
        {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
    </div>
</div>