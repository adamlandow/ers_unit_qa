<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('type', 'Reviewed Unit Instance', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">
        <select class="select2 form-control" name="reviewed_offering_id" style="width: 300px">
            @foreach ($unit_instances as $instance)
                <option value='{{$instance->id}}' @if($instance->id ==$review->reviewed_offering->id)
                selected
                        @endif>{{$instance->unit['unit_code']}} {{$instance->teaching_period['teaching_period']}} {{$instance->teaching_period['year']}}
                    (Co-ordinator:{{$instance->coordinator['name']}})
                </option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('coordinator_id', 'Coordinator', ['class'=>'control-label   text-left']) !!}</div>
    <div class="col-sm-12">
        <select class="select2 form-control" name="coordinator_id" style="width: 300px">
            @foreach ($users as $user)
                {{--@if(($user->id!=$review->reviewed_unit_instance->coordinator['id'])||($user->id!=$review->peer_reviewer['id']))--}}
                <option value='{{$user->id}}'
                        @if(isset($review->override_coordinator))
                        @if($user->id ==$review->override_coordinator->id)
                        selected
                        @endif
                        @elseif($user->id ==$review->reviewed_offering->coordinator['id'])
                        selected
                        @endif>{{$user->name}}</option>
                {{--@endif--}}
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('type', 'Peer Reviewer (needs to be different to coordinator and Learning Designer)', ['class'=>'control-label   text-left']) !!}</div>
    <div class="col-sm-12">
        <select class="select2 form-control" name="peer_reviewer_id" style="width: 300px">
            @foreach ($users as $user)
                {{--@if(($user->id!=$review->reviewed_unit_instance->coordinator['id'])||($user->id!=$review->peer_reviewer['id']))--}}
                <option value='{{$user->id}}'
                        @if($user->id ==$review->peer_reviewer['id'])
                        selected
                        @endif>{{$user->name}}</option>
                {{--@endif--}}
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('type', 'Learning Designer (needs to be different to coordinator and Peer Reviewer)', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">
        <select class="select2 form-control" name="ld_id" style="width: 300px">
            @foreach ($users as $user)
                {{--@if(($user->id!=$review->reviewed_unit_instance->coordinator['id'])||($user->id!=$review->ld['id']))--}}
                <option value='{{$user->id}}'
                        @if($user->id ==$review->ld['id'])
                        selected
                        @endif>{{$user->name}}</option>
                {{--@endif--}}
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12"> {!! Form::label('start_date', 'Start date', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">

        {!! Form::input('text', 'start_date', null, ['class'=>'form-control datepicker' , 'id'=>'start_date', 'required']) !!}

    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12"> {!! Form::label('end_date', 'End date', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">

        {!! Form::input('text', 'end_date', null, ['class'=>'form-control datepicker' , 'id'=>'end_date', 'required']) !!}

    </div>
</div>
<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>