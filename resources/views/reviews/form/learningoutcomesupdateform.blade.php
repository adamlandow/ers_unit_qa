<div class="form-group row edit_template_text" style="padding-left: 10px">
    <fieldset style="width: 100%">
        <legend style="">Learning Outcomes
            <button id="additemitembut" type="button" class="btn btn-info btn-sm" onclick="addLOItem()"><i
                        class="fa fa-plus" aria-hidden="true"></i></button>
        </legend>

        <table class="table table-striped table-condensed" id="loitemstbl">
            <thead>
            <tr>
                <th>Description</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </fieldset>
</div>
<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>