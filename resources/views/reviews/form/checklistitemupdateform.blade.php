{!! Form::hidden('heading', '', ['id'=>'hiddenheading', 'onchange' => 'if($("#hiddenheading").val()=="1"){$(".notlabel").hide()}else{$(".notlabel").show()}']) !!}
<div class="form-group row">
    {!! Form::label('description', 'Description', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::text('description', null, ['class'=>'form-control' , 'required', (Auth::user()->can('update_review_instance'))=='true'?'':'disabled']) !!}
    </div>
</div>


@if(($review->ld['id']==Auth::user()->id)||(Auth::user()->can('is_admin')=='true'))

    <div class="form-group row">
        {!! Form::label('text', 'Heading Level', ['class'=>'control-label  col-sm-2 text-left']) !!}
        <div class="col-sm-10">
            <select class="select2 form-control" name="heading" style="width: 300px">
                <option value='0'>Not a heading</option>
                <option value='1'>Level 1</option>
                <option value='2'>Level 2 (subheading)</option>
            </select>
        </div>
    </div>
    <div class="form-group row text">
        {!! Form::label('template_id', 'Notify LD', ['class'=>'control-label  col-sm-2 text-left']) !!}
        <div class="col-sm-4">
            <div class="checkbox checkbox-success" >
                {!! Form::checkbox('trigger_email', 1, false, ['class'=>'styled', 'onchange' => 'if($(this).prop("checked")==true){$(".emailtemplate").show()}else{$(".emailtemplate").hide()}']) !!}
                <label>Check to send email to Learning Designer when this item is checked</label>
            </div>
        </div>
        <div class="col-sm-6 emailtemplate" >
            <div class="col-sm-12">{!! Form::label('template_id', 'Use template', ['class'=>'control-label  text-left']) !!}</div>

            <div class="col-sm-12">
                {!! Form::select('trigger_email_id', $emailtemplates->pluck('label', 'id'), null, ['style'=>"width: 300px", 'class'=>'form-control']) !!}
            </div>
        </div>
    </div>
@endif
<div class="form-group row notlabel">
    {!! Form::label('text', 'Notes/Recommendations', ['class'=>'control-label  col-sm-2 text-left']) !!}
</div>
<div class="form-group row ">

    <div class="col-sm-12 notlabel">
        <?php switch (Auth::user()->id){
        case $review->reviewed_offering->coordinator['id']:
        ?>
        {!! Form::textarea('coordinator_text', null, ['class'=>'form-control tinymce']) !!}
        <?php
        break;
        case $review->peer_reviewer['id']:
        ?>
        {!! Form::textarea('peer_reviewer_text', null, ['class'=>'form-control tinymce' ]) !!}
        <?php
        break;
        case $review->ld['id']:
        ?>
        <div class="form-group row">

            <div class="col-sm-12">
                {!! Form::label('text', 'Co-ordinator notes', ['class'=>'control-label  col-sm-4 text-left']) !!}
            </div>
            <div class="col-sm-12">
                {!! Form::textarea('coordinator_text', null, ['class'=>'form-control tinymce']) !!}
            </div>
            <br/>
            <div class="col-sm-12" style="padding-top: 10px">
                {!! Form::label('text', 'Peer Reviewer notes', ['class'=>'control-label  col-sm-4 text-left']) !!}
            </div>
            <div class="col-sm-12">
                {!! Form::textarea('peer_reviewer_text', null, ['class'=>'form-control tinymce' ]) !!}
            </div>
            <br/>
            <div class="col-sm-12" style="padding-top: 10px">
                {!! Form::label('text', 'Learning Designer notes', ['class'=>'control-label  col-sm-4 text-left']) !!}
            </div>
            <div class="col-sm-12">
                {!! Form::textarea('ld_text', null, ['class'=>'form-control tinymce' ]) !!}
            </div>

            <?php
            break;
            }
            ?>
        </div>
    </div>


    <div class="form-group">
        {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
    </div>
</div>