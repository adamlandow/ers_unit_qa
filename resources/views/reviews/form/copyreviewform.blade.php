<div class="form-group row">
    <div class="well"> <i class="fa fa-2x fa-info-circle" style='color: #0066ff'></i>Copying a review makes a new review, with all the checklist items from this review copied across</div>
    <div class="col-sm-12">    {!! Form::label('type', 'Reviewed Unit Instance', ['class'=>'control-label  text-left']) !!}</div>

    <div class="col-sm-12">
        <select class="select2 form-control" name="reviewed_unit_instance_id" style="width: 100%" required>
            @foreach ($unit_instances as $instance)

                <option value='{{$instance->id}}'>{{$instance->unit['unit_code']}} {{$instance->teaching_period['teaching_period']}} {{$instance->teaching_period['year']}} (Co-ordinator:{{$instance->coordinator['name']}}) </option>

            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('peer_reviewer_id', 'Peer Reviewer (needs to be different to coordinator and Learning Designer)', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">
        <select class="select2 form-control" name="peer_reviewer_id" style="width: 100%" required>
            @foreach ($users as $user)
                    <option value='{{$user->id}}'>{{$user->name}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12"> {!! Form::label('ld_id', 'Learning Designer (needs to be different to coordinator and Peer Reviewer)', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">
        <select class="select2 form-control" name="ld_id" style="width: 100%">
            @foreach ($users as $user)
                {{--@if(($user->id!=$review->reviewed_unit_instance->coordinator['id'])||($user->id!=$review->ld['id']))--}}
                    <option value='{{$user->id}}'>{{$user->name}}</option>
                {{--@endif--}}
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-12"> {!! Form::label('start_date', 'Start date', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">

            {!! Form::input('date', 'start_date', null, ['class'=>'form-control datepicker' , 'id'=>'review_date', 'required']) !!}

    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12"> {!! Form::label('end_date', 'End date', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">

        {!! Form::input('date', 'end_date', null, ['class'=>'form-control datepicker' , 'id'=>'review_date', 'required']) !!}

    </div>
</div>
<div class="form-group">

    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>