<div class="form-group row">
    {!! Form::label('description', 'Description', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::text('description', null, ['class'=>'form-control' , 'required']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('text', 'Text', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <br/><div class="col-sm-10">
        {!! Form::textarea('text', null, ['class'=>'form-control' ]) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>