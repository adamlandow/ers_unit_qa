@extends('layouts.app')

@section('content')
    <script>

        var currentid = -1;

        $(document).ready(function () {
            {{--@can('update-review-instance')--}}


            $('.modalform').on('shown.bs.modal', function () {
                $(this).find("form").validator();
            });

            $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
                currenteditingid = $(this).data('id');
                switch ($(this).data('target')) {
                    case '#deletedialog':
                        currentid = $(this).data('id');
                        break;
                    case '#copydialog':
                        currentid = $(this).data('id');
                        $("#copy_label").val('Copy of ' +$(this).data('label'))
                        break;
                    default:
                        currentid = -1;
                        break;
                }

            });

            $('.newdialog').submit(function (event) {
                event.preventDefault();
                $(this).modal('hide');
                var vars = $(this).find("form").serializeArray();
                vars.push({name: 'is_template', value: 'true'});
                console.log(vars);
                waitingDialog.show();
                submitNewForm(vars, $(this).data('route'));
            });

            $("#copydialog").submit(function (event) {
                $(this).modal('hide');
                // cancels the form submission
                var vars = $(this).find("form").serializeArray();
                vars.push({name: 'id', value: currentid});
                event.preventDefault();
                waitingDialog.show();
                submitCopyForm(vars);
            });


            // all deletes are the same
            $("#deletedialog").submit(function (event) {
                $(this).modal('hide');
                // cancels the form submission
                event.preventDefault();
                waitingDialog.show();
                deleteItem(currentid, 'unitsetup');
            });
            {{--@endcan--}}
        });

        {{--@can('update-review-instance')--}}
        function submitNewForm(vars, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/create',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();

                    if (Number(data.id) > 0) {
                        location.reload(true);
                    }
                    else if ((data.error).length > 0) {
                        alert(data.error);
                    }
                    else {
                        alert('something went wrong with the insertion');
                    }
                }
            });
        }

        function submitCopyForm(vars) {
            $.ajax({
                url: '{!! URL::to('')!!}/unitsetup/clone',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();

                    if (Number(data.id) > 0) {
                        // take me to the newly created instance
                        location.replace('{!! URL::to('')!!}/unitsetuptemplates/' + data.id);
                    }
                    else if ((data.error).length > 0) {
                        alert(data.error);
                    }
                    else {
                        alert('something went wrong with the operation');
                    }
                }
            });
        }

        function deleteItem(id, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/destroy',
                type: 'post',
                data: ([{name: 'id', value: id}, {
                    name: '_token',
                    value: '{{csrf_token()}}'
                }]),
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status == "1") {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the delete');
                    }
                }
            });
        }

        /*
         *
         * Helper functions
         *
         */

        // populates a form with data returned from Laravel
        function populate(frm, data) {
            $.each(data, function (key, value) {
                var $ctrl = $('[name=' + key + ']', frm);
                if ($ctrl.is("select")) {
                    if ($ctrl.attr('multiple')) {
                        $ctrl.select2('val', value.split(',')).trigger('change');
                    } else {
                        $ctrl.select2('val', value);
                    }

                } else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":
                            $ctrl.val(value);
                            break;
                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).attr("checked", value);
                                }
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }
            });
        }


        {{--@endcan--}}


    </script>
    {!! Breadcrumbs::render('unitsetuptemplates.index') !!}
    <fieldset style="padding-left: 15px; padding-right: 15px; margin-top: 0">
        {{--@can('update-review-instance')--}}

        <div class="col-md-12" style="padding-left: 0px">
            <div class="col-md-10 text-left" style="padding-left: 0px">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newdialog">New template
                    Instance<i
                            class="fa fa-plus"></i>
                </button>
            </div>


        </div>
        {{--@endcan--}}

            <table class="table table-striped">
                <thead class="thead-inverse">
                <tr>
                    <th class="headerSortable header">Template</th>
                    <th>Copy</th>
                    <th></th>
                    {{--@endcan--}}
                </tr>
                </thead>
                @if(isset($instances))
                    @foreach ($instances as $instance)
                        <tr>
                            <td>
                                <a href="{{action('OfferingsController@templateshow', $instance->id)}}">
                                    {{$instance->label}}
                                </a>
                            </td>

                            {{--@can('update-review-instance')--}}
                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$instance->id}}" data-label="{{$instance->label}}"
                                   data-target="#copydialog">
                                    <i class="fa fa-2x fa-copy" style='color: #0066ff'></i>
                                </a>
                            </td>
                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$instance->id}}"
                                   data-target="#deletedialog">
                                    <i class="fa fa-2x fa-times-circle" style='color: #ff6600'></i>
                                </a>
                            </td>
                        {{--@endcan--}}
                    @endforeach
                @endif
            </table>

    </fieldset>
    {{--@can('update-review-instance')--}}
    <div id="newdialog" class="modal fade newdialog modalform" role="dialog"
         data-route="unitsetuptemplates">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Template Instance</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open( ['class'=>'form-horizontal', 'role'=>'form'])!!}

                    @include('unit_setup.templates.form.newinstanceform', ['submitButtonText'=>'Add template'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div id="newcsvdialog" class="modal fade newdialog modalform" role="dialog"
         data-route="unitsetup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add using CSV</h4>
                </div>
                <div class="modal-body">
                    Coming soon!
                    {{--{!! Form::open( ['class'=>'form-horizontal', 'role'=>'form'])!!}--}}

                    {{--@include('unit_setup.form.newinstanceform', ['submitButtonText'=>'Add instance'])--}}

                    {{--{!! Form::close() !!}--}}
                </div>
            </div>
        </div>
    </div>

    {{--  Copying a unit review event  --}}
    <div id="copydialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Copy a template</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['class'=>'form-horizontal', 'role'=>'form'])!!}
                    @include('unit_setup.form.copytemplateform', ['submitButtonText'=>'Copy template'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Deleting a unit review event  --}}
    <div id="deletedialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('form_common.deletedialog')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    {{--@endcan--}}

@stop

