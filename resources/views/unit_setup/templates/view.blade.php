@extends('layouts.app')
@section('content')

    <style>
        table.note,
        table.note tr,
        table.note td {
            border: 1px solid black;
        }

        table.note {
            border-collapse: collapse;
        }

        table.table-striped td {
            border-left: solid 1px gray;

        }


        .fill {
            min-height: 100%;
            height: 100%;
            width: 100%;
            box-sizing: border-box;
        }

        /*
         * Main content
         */

        .main {
            padding: 20px;
        }

        @can('update_unit_instance')
        table.table-striped tr td:nth-child(2) {
            width: 30%;
        }

        div.handle {
            width: 1%;
            white-space: nowrap;
            cursor: grab;
        }

        div.handle:active {
            width: 1%;
            white-space: nowrap;
            cursor: grabbing;
        }

        @else
        table.table-striped tr td:first-child {
            min-width: 10%;
            width: 30%;

        }
        @endcan

    </style>
    <script src="{{ URL::asset('resources/assets/js/tinymce/tinymce.min.js') }}"></script>

    <script>

        //tinymce Bootstrap fix
        // https://www.tinymce.com/docs/integrations/bootstrap/
        $(document).on('focusin', function (e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });

        // init document
        $(document).ready(function () {

            // set up select2 dropdowns
            $('select').select2();


            // init timyMCE
            tinymce.init({
                selector: '.tinymce',
                height: 500,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor @can('update_review_instance') template @endcan',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code'
                ],

                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ',

                content_css: '//www.tinymce.com/css/codepen.min.css'
            });



            @if(!isset($instance->archived_at))

            $('.editdialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();
                vars.push({name: 'id', value: currenteditingid});
                vars.push({name: '_method', value: 'PATCH'});

                waitingDialog.show();
                submitUpdateForm(vars, $(this).data('route'));
            });



            {{--@can('update-review-instance')--}}
            // drag and drop reordering
            dragula([document.getElementById('checklisttablebody')], {
                moves: function (el, container, handle) {

                    return handle.classList.contains('handle');
                }
            }).on('drop', function (el, target, source, sibling) {
                // send an array of elements to backend to reorder
                //    waitingDialog.show();
                console.log(sibling);

                var orderArray = ([]);
                var orderint = 0;
                $(".sortablerow").each(function () {
                    orderArray.push({id: $(this).attr('entryid'), order: orderint})
                    orderint++;
                    console.log($(this).attr('entryid'));
                });
                // for some reason, the last element of this is always the element being moved.
                orderArray.pop();

                var dataObj = ([{name: '_token', value: '{{csrf_token()}}'}, {
                    name: 'order',
                    value: JSON.stringify(orderArray)
                }]);

                $.ajax({
                    url: '{!! URL::to('')!!}/unitsetupinstancechecklistitem/reorder',
                    type: 'post',
                    data: dataObj,
                    error: function (jqXHR, textStatus, errorThrown) {
                        waitingDialog.hide();
                        alert(errorThrown);
                    },
                    success: function (data) {
                        //   waitingDialog.hide();
                        if (data.status.toString() == "0") {
                            location.reload(true);
                        } else {
                            waitingDialog.hide();
                            alert('something went wrong with the update');
                        }
                    }
                });

            });

            {{--@endcan--}}

         $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
                currenteditingid = $(this).data('id');
                console.log($(this).data('target'))
                $(".emailtemplate").hide()
                switch ($(this).data('target')) {
                    case '#editchecklistitemdialog':
                        currenteditingid = $(this).data('id');
                        console.log(currenteditingid)
                        getDetails(currenteditingid, 'unitsetupinstancechecklistitem', 'editchecklistitemdialog');
                        break;


                    case '#deletedialog':
                        currentdeleteroute = $(this).data('route');
                        console.log($(this).data('route'));
                        currentdeletingid = $(this).data('id');
                        break;

                    default:
                        currenteditingid = -1;
                        currentdeletingid = -1;
                        break;
                }

            });

            $('.modalform').on('shown.bs.modal', function () {
                $(this).find("form").validator();

            });

            {{--@can('update-review-instance')--}}

            $('#editdetailsdialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();
                // hack to get combined units as comma separated values
                vars.push({name: 'id', value: '{{$instance->id}}'});
                waitingDialog.show();
                submitUpdateForm(vars, 'unitsetuptemplates/{{$instance->id}}');
            });


            {{--@endcan--}}

            $('.newdialog').submit(function (event) {

                event.preventDefault();
                $(this).modal('hide');
                var vars = $(this).find("form").serializeArray();
                waitingDialog.show();
                submitNewForm(vars, $(this).data('route'));
            });


            // all deletes are the same
            $("#deletedialog").submit(function (event) {
                $(this).modal('hide');
                // cancels the form submission
                event.preventDefault();
                waitingDialog.show();
                deleteItem(currentdeletingid, currentdeleteroute);
            });



            {{--@endcan--}}
            @endif

        });





        @if(!isset($instance->archived_at))

        /*
         *
         * Helper functions
         *
         */



        // populates a form with data returned from Laravel
        function populate(frm, data) {
            $.each(data, function (key, value) {
                var $ctrl = $('[name=' + key + ']', frm);
                if ($ctrl.is("select")) {
                    if ($ctrl.attr('multiple')) {
                        console.log(key, value)
                        $ctrl.val(value.split(',')).trigger('change');
                    } else {
                        console.log(key, value)
                        $ctrl.val(value).trigger('change');
                    }
                } else if ($ctrl.is("textarea")) {
                    if ($ctrl.hasClass('tinymce')) {
                        // fix for this weird bug where tinymce chucks a wobbly if the content is set to a null value
                        tinyMCE.get($ctrl.attr('id')).setContent((value === null) ? '' : value);
                    } else {
                        $ctrl.val((value === null) ? '' : value);
                    }
                }
                else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":
                            $ctrl.val(value);
                            $ctrl.trigger('change');
                            break;

                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).prop("checked", true);
                                }else{
                                    $(this).prop("checked", false);
                                }
                                $(this).trigger('change');
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }

            });
        }

        function getDetails(id, route, formid) {
            //  console.log('getDetails(' + id + "," + route + "," + formid + ")");
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/' + id,
                type: 'GET',
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (data) {
                    //      console.log(data);
                    populate($("#" + formid).find("form"), data);
                    waitingDialog.hide();
                }
            });
        }

        // Edit
        function submitUpdateForm(vars, route) {
            // console.log(route);
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/update',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    //   waitingDialog.hide();
                    if (data.status.toString() == "1") {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert('something went wrong with the update');
                    }
                }
            });
        }


        function submitNewForm(vars, route) {
            vars.push({name: 'offering_id', value: '{{$instance->id}}'})

            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/create',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (Number(data.id) > 0) {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the insertion');
                    }
                }
            });
        }


        // Delete contact event
        function deleteItem(id, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/destroy',
                type: 'post',
                data: ([{name: 'id', value: id}, {name: '_method', value: 'DELETE'}, {
                    name: '_token',
                    value: '{{csrf_token()}}'
                }]),
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status == "1") {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the delete');
                    }
                }
            });
        }



        @endif

    </script>

    <!-- Tabs -->
    {!! Breadcrumbs::render('unitsetuptemplates.show', $instance) !!}
    {{--{{dd($instance)}}--}}
    <div class="col-md-12 page-header" style="margin-top: 0px">

        <div class="box box-element">
            <div class="view">
                <div class="row">
                    <div class="container col-md-4">

                        <h3 style="margin-top: 0px">Template:
                            {{$instance->label}}
                            @if(!isset($instance->archived_at))
                                <a href="#" data-toggle="modal" data-target="#editdetailsdialog">(Update)</a>
                            @endif
                        </h3>


                    </div>
                </div>
            </div>
        </div>
    </div>






    <div id="checklisttab">


        <button class="btn btn-info btn-lg" data-toggle="modal"
                data-target="#newchecklistitemdialog">New item
        </button>


        <table class="table table-striped">
            <thead class="thead-inverse">
            @can('update_unit_instance')
            <th>
                Move
            </th>
            @endcan
            <th>
                Item @can('update_unit_instance')(click to update)@endcan

            </th>
            {{--@if($instance->coordinator['id']==Auth::user()->id||)--}}
            <th>
                Notes
            </th>
            {{--@endif--}}

            @can('update_unit_instance')
            <th>
            </th>
            @endcan
            </thead>
            <tbody id="checklisttablebody">
            @foreach($instance->checklist as $item)
                @if(($item->heading)==1)
                    <tr style="background-color: #7ab800" class='sortablerow' entryid='{{$item->id}}'>
                        @can('update_unit_instance')
                        <td>
                            <div class="handle" style="width: 20px"><i class="fa fa-2x fa-arrows-v handle"
                                                                       style='color: #bf800c'></i></div>
                        </td>
                        @endcan
                        <td colspan="100%">
                            @can('update_unit_instance')
                            <a href="#" data-toggle="modal"
                               data-id="{{$item->id}}"
                               data-route="unitsetupinstancechecklistitem"
                               data-target="#editchecklistitemdialog">
                                @endcan
                                <h4> {{$item->description}}</h4>
                                @can('update_unit_instance') </a>
                            <a href="#" data-toggle="modal"
                               data-id="{{$item->id}}"
                               data-route="unitsetupinstancechecklistitem"
                               data-target="#deletedialog">
                                <i class="fa fa-times-circle" style='color: #ff6600'></i></a>
                            @endcan
                        </td>
                    </tr>
                @else
                    <tr class='sortablerow'  @if($item->trigger_email==1)style="background-color: #ffb991" @endif entryid='{{$item->id}}'>
                        @can('update_unit_instance')
                        <td>
                            <div class="handle" style="width: 20px"><i class="fa fa-2x fa-arrows-v handle"
                                                                       style='color: #bf800c'></i></div>
                        </td>
                        @endcan
                        <td>
                            @can('update_unit_instance') <a href="#" data-toggle="modal" data-id="{{$item->id}}"
                                                            data-route="unitsetupinstancechecklistitem"
                                                            data-target="#editchecklistitemdialog">
                                @endcan
                                {{$item->description}}

                                @can('update_unit_instance')</a>@endcan

                                @if($item->trigger_email==1)
                                    <br/><i class="fa fa-exclamation-triangle"
                                            style='color: #d9534f;'></i><strong>Will send email with template @if(isset($item->trigger_email_template))"{{$item->trigger_email_template->label}}" @else "<i>(No email template set, please update)</i>" @endif to coordinator when checked</strong>
                                @endif
                        </td>


                        <td>
                            @if(isset($item['text']))
                                {!! $item['text']!!}
                            @endif
                        </td>
                        @can('update_unit_instance')
                        <td>
                            <a href="#" data-toggle="modal" data-id="{{$item->id}}"
                               data-route="unitsetupinstancechecklistitem"
                               data-target="#deletedialog">
                                <i class="fa fa-2x fa-times-circle" style='color: #ff6600'></i>
                            </a>
                        </td>
                        @endcan
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>

    </div>




    {{--
    Dialogs for various things
    --}}

    <div id="editdetailsdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Template Name</h4>
                </div>
                <div class="modal-body">

                    {!! Form::model($instance, ['class'=>'form-horizontal'])!!}

                    @include('unit_setup.templates.form.uniteditform', ['submitButtonText'=>'Update Templates'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>



    {{--  Adding a checklist item --}}
    <div id="newchecklistitemdialog" class="modal fade newdialog" role="dialog" data-route="unitsetupinstancechecklistitem">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add checklist item</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('unit_setup.templates.form.checklistitemform', ['submitButtonText'=>'Add Checklist Item'])
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>




    {{--  Deleting a thing --}}
    <div id="deletedialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('form_common.deletedialog', [ 'id'=>$instance->id])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    {{--  Editing a list itemt  --}}
    <div id="editchecklistitemdialog" class="modal fade editdialog" role="dialog"
         data-route="unitsetupinstancechecklistitem">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit checklist item</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('unit_setup.templates.form.checklistitemupdateform', ['submitButtonText'=>'Update'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    @if(count($errors) > 0)
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    @endif



@stop
