<div class="form-group row">
    {!! Form::label('description', 'Description', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::text('description', null, ['class'=>'form-control' , 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('heading', '', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <div class="checkbox checkbox-success" >
            {!! Form::checkbox('heading', 1, false, ['id'=>'heading_cb',  'class'=>'styled',  'onclick' => 'if($("#heading_cb").prop("checked")==true){$(".text").hide()}else{$(".text").show()}']) !!}
            <label>This is a heading</label>
        </div>
        {{--{!! Form::label('heading', '', ['class'=>'control-label  col-sm-2 text-left']) !!}--}}
    </div>

</div>
<div class="form-group row text">
    {!! Form::label('template_id', 'Notify Coordinator', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-4">
        <div class="checkbox checkbox-success" >
            {!! Form::checkbox('trigger_email', 1, false, ['class'=>'styled', 'onchange' => 'if($(this).prop("checked")==true){$(".emailtemplate").show()}else{$(".emailtemplate").hide()}']) !!}
            <label>Check to send email to coordinator when this item is checked</label>
        </div>
    </div>
    <div class="col-sm-6 emailtemplate" >
        <div class="col-sm-12">{!! Form::label('template_id', 'Use template', ['class'=>'control-label  text-left']) !!}</div>

        <div class="col-sm-12">
            {!! Form::select('trigger_email_id', $emailtemplates->pluck('label', 'id'), null, ['style'=>"width: 300px", 'class'=>'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group row  text">
    {!! Form::label('text', 'Notes', ['class'=>'control-label  col-sm-5 text-left',]) !!}
</div>
<div class="form-group row  text">

    <div class="col-sm-12">

        {!! Form::textarea('text', null, ['class'=>'form-control tinymce' ]) !!}

    </div>
</div>
<div class="form-group">
    {!! Form::submit('New item', ['class'=>'btn btn-primary form-control']) !!}
</div>