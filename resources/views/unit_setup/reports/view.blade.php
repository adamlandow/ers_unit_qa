@extends('layouts.app')
@section('title')Reports @endsection
@section('content')
    {{--<link rel="stylesheet" href="{{URL::asset('resources/assets/dhtmlx/dhtmlxscheduler_flat.css')}}">--}}
    {{--<link rel="stylesheet" href="{{URL::asset('resources/assets/dhtmlx/common/dhtmlxCombo/dhtmlxcombo.css')}}">--}}
    {{--<script src="{{ URL::asset('resources/assets/dhtmlx/dhtmlxscheduler.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('resources/assets/dhtmlx/ext/dhtmlxscheduler_editors.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('resources/assets/dhtmlx/common/dhtmlxCombo/dhtmlxcombo.js') }}"></script>--}}
    <script src="{{ URL::asset('resources/assets/js/Chart.min.js') }}"></script>
    <script src="https://unpkg.com/popper.js@1"></script>
    <script src="https://unpkg.com/tippy.js@5"></script>
    <style>
        table.note,
        table.note tr,
        table.note td {
            border: 1px solid black;
        }

        table.note {
            border-collapse: collapse;
        }

        table.table-striped td {
            border-left: solid 1px gray;

        }

        #scheduler_here {
            width: 100%;
            height: 600px;
            /*display: block;*/
        }

        .fill {
            min-height: 100%;
            height: 100%;
            width: 100%;
            box-sizing: border-box;
        }

        /*
         * Main content
         */

        .main {
            padding: 20px;
        }

        @can('update_unit_instance')
        table.table-striped tr td:nth-child(2) {
            width: 30%;
        }

        div.handle {
            width: 1%;
            white-space: nowrap;
            cursor: grab;
        }

        div.handle:active {
            width: 1%;
            white-space: nowrap;
            cursor: grabbing;
        }

        @else
        table.table-striped tr td:first-child {
            min-width: 10%;
            width: 30%;

        }
        @endcan

    </style>


    <script>


        // init document

        $(document).ready(function () {

            // set up select2 dropdowns
            $('select').select2();

            tippy('button');


            $('.modalform').on('shown.bs.modal', function () {
                $(this).find("form").validator();

            });




            var facultyctx = document.getElementById('faculty-chart-area').getContext('2d');
            var facultyDoughnutChart = new Chart(facultyctx, {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [{{ $facultystats['total']-($facultystats['in_progress']+$facultystats['complete'])}}, {{ $facultystats['in_progress']}}, {{ $facultystats['complete']}}],
                        backgroundColor:['#FA8072','#99CCFF','#28a745']
                    }],

                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: [
                        'Not started',
                        'In progress',
                        'Complete'
                    ]
                },
               // backgroundColor:['#FA8072','#48D1CC','#FF6600'],

                options: {

                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Faculty'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });
            //facultyDoughnutChart.canvas.parentNode.style.height = '50px';

// school charts
            @foreach($schools as $school)
            var school_{{$school->code}}_ctx = document.getElementById('school-chart-area-{{$school->code}}').getContext('2d');
            var school_{{$school->code}}DoughnutChart = new Chart(school_{{$school->code}}_ctx, {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [{{ $reports[array_search($school->id, array_column($reports, 'school_id'))]['total']-($reports[array_search($school->id, array_column($reports, 'school_id'))]['in_progress']+$reports[array_search($school->id, array_column($reports, 'school_id'))]['complete'])}}, {{ $reports[array_search($school->id, array_column($reports, 'school_id'))]['in_progress']}}, {{$reports[array_search($school->id, array_column($reports, 'school_id'))]['complete']}}],
                        backgroundColor:['#FA8072','#99CCFF','#28a745']
                    }],

                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: [
                        'Not started',
                        'In progress',
                        'Complete'
                    ]
                },
                // backgroundColor:['#FA8072','#48D1CC','#FF6600'],

                options: {

                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: '{{$school->description}}'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });
            @endforeach


        });


    </script>

    <!-- Tabs -->
    {!! Breadcrumbs::render('unitsetupreports') !!}
    {{--{{dd($instance)}}--}}
    <div class="col-md-12 page-header" style="margin-top: 0px">

        <div class="box box-element">
            <div class="view">
                <div class="row col-md-12">
                    <div class="container col-md-4">
                        <h3 style="margin-top: 0px">Reports</h3>
                        <form>
                            <select multiple
                                    class="form-control" name="teachingperiod[]"
                                    id="teaching_period_id_selector"
                                    style="width: 200px">

                                @foreach ($teachingperiods as $teachingperiod)

                                    {{--@if(in_array($teachingperiod->id, explode(',',session('teachingperiod'))))--}}
                                    <option value='{{$teachingperiod->id}}'
                                            {{in_array($teachingperiod->id, session("teachingperiod") ?: []) ? "selected": ""}}
                                    >

                                        {{$teachingperiod->teaching_period_lookup->teaching_period}} {{$teachingperiod->year}}</option>
                                @endforeach
                            </select>
                            <button class="btn btn-primary" data-tippy-content="Show offering statistics for the selected teaching periods" id="filterbut"><i class="fa fa-search" aria-hidden="true"></i>&nbsp;Show stats


                            </button>
                        </form>
                        <br/>
                        <div class="container col-md-6" >
                            <h3>Faculty status:</h3>
                            <h4>Total offerings: {{ $facultystats['total']}}</h4>
                            <h4>In progress: {{ $facultystats['in_progress']}}</h4>
                            <h4>Complete: {{ $facultystats['complete']}}</h4>
                            {{--@if (session('teachingperiod')>0)--}}
                                {{--@if (\Carbon\Carbon::now()->diffInDays($teachingperiods->find(session('teachingperiod'))->start_timestamp,false)>0)--}}
                                    {{--<h4 style="color:#28a745 ">{{\Carbon\Carbon::now()->diffInDays($teachingperiods->find(session('teachingperiod'))->start_timestamp)}}--}}
                                        {{--days--}}
                                        {{--to {{$teachingperiods->find(session('teachingperiod'))->teaching_period_lookup->teaching_period}} {{$teachingperiods->find(session('teachingperiod'))->year}}--}}
                                        {{--start </h4>--}}
                                {{--@else--}}
                                    {{--<h4 style="color:#FA8072 ">{{$teachingperiods->find(session('teachingperiod'))->teaching_period_lookup->teaching_period}} {{$teachingperiods->find(session('teachingperiod'))->year}}--}}
                                        {{--started {{\Carbon\Carbon::now()->diffInDays($teachingperiods->find(session('teachingperiod'))->start_timestamp)}}--}}
                                        {{--days ago </h4>--}}
                                {{--@endif--}}
                            {{--@endif--}}
                        </div>


                    </div>


                    <div class="col-md-4" >
                        <div class="chart-container" >
                        <canvas id="faculty-chart-area"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--<ul class="nav nav-tabs" id="tabslabels">--}}
        {{--@foreach($schools as $school)--}}
        {{--<li  @if ($loop->first) class="active" @endif><a data-toggle="tab" href='#school_{{$school->id}}_tab'>{{$school->description}}</a></li>--}}
        {{--@endforeach--}}
        {{--</ul>--}}
        {{--<div class="tab-content ">--}}
        @foreach($schools as $school)
            {{--<div id="school_{{$school->id}}_tab" class="tab-pane @if ($loop->first) active @endif" style="  height: 300px;">--}}
            <fieldset style="width: 100%;">
                <legend>{{$school->description}}</legend>
                <div class="col-md-12 row">
                    <div class="col-md-3">
                        <h4>Total
                            offerings: {{$reports[array_search($school->id, array_column($reports, 'school_id'))]['total']}}</h4>
                        <h4>In
                            progress: {{$reports[array_search($school->id, array_column($reports, 'school_id'))]['in_progress']}}</h4>
                        <h4>
                            Complete: {{$reports[array_search($school->id, array_column($reports, 'school_id'))]['complete']}}</h4>
                    </div>
                    <div class="col-md-3">
                        <div class="chart-container" >
                        <canvas id="school-chart-area-{{$school->code}}" ></canvas>
                        </div>
                    </div>
                </div>


            </fieldset>
            {{--</div>--}}
        @endforeach

    </div>
    </div>










@stop
