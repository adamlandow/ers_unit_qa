<div class="form-group row">
    <div class="col-sm-12">
        {!! Form::label('label', 'Template Label', ['class'=>'control-label  text-left']) !!}
    </div>
    <div class="col-sm-12">
        {!! Form::text('label',null, [ 'class'=>'form-control', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-12">
        {!! Form::label('subject', 'Subject- use {unit} for unit,  {teachingperiod} for teaching period, {coordinator} for coordinator first name', ['class'=>'control-label text-left']) !!}
    </div>
    <div class="col-sm-12">
        {!! Form::text('subject',null, [ 'class'=>'form-control', 'required']) !!}
    </div>
</div>
<div class="form-group row ">
    <div class="col-sm-12">
        {!! Form::label('text', 'Email text- use {unit} for unit, {teachingperiod} for teaching period, {coordinator} for coordinator first name, {followup} to include follow up notes, {moodleurl} for Moodle URL', ['class'=>'control-label  text-left']) !!}
    </div>
    <div class="col-sm-12">
        {!! Form::textarea('text', null, ['class'=>'form-control tinymce', 'id'=>'text1']) !!}
    </div>
</div>
<div class="form-group">

    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>