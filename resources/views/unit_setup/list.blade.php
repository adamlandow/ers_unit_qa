@extends('layouts.mainapp')
@section('title')
    Unit offering Setup
@endsection
@section('menu')
    @can('update_unit_instance')
        <li>
            <button type="button" class="btn btn-primary" style="width: 220px" data-toggle="modal"
                    data-target="#newdialog">
                New Offering<i
                        class="fa fa-plus"></i>
            </button>
            <p/>
        </li>
        <li>
            <button type="button" class="btn btn-primary" style="width: 220px" data-toggle="modal"
                    data-target="#newcsvdialog">Add using CSV from APEX&nbsp;<i
                        class="fa fa-plus"></i>
            </button>
            <p/>

        </li>
        <li class="active">
            <a href="{{ url('/unitsetuptemplates') }}"><i class="fa fa-fw fa-cog"></i>Offering setup templates</a>
            <p/>
        </li>
        <li class="active">
            <a href="{{ url('/unitemails') }}"><i class="fa fa-fw fa-cog"></i>Email templates</a>
            <p/>
        </li>
        <li class="active">
            <a href="{{ url('/unitsetupreports') }}"><i class="fa fa-fw fa-pie-chart"></i>Status reports</a>
            <p/>
        </li>
        <li >
            <a href="{{URL::to('/unitsetupexportforkantu?'.Request::getQueryString())}}"><i class="fa fa-fw fa-exclamation-triangle"></i>EXPERIMENTAL export for automation</a>
            <p/>
        </li>
    @endcan
@endsection
@section('content')
    <script>

        var currentid = -1;

        $(document).ready(function () {
            {{--@can('update-review-offering')--}}
            $('select').not('.userselect').not('.unitselect').select2();

            @can('update_unit_instance')

            $('.userselect').select2({
                placeholder: 'Search by user name',
                minimumInputLength: 1,
                ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                    url: "{!! URL::to('')!!}/searchuserforselect2",

                    dataType: 'json',
                    quietMillis: 150,
                    data: function (term) {
                        return {
                            q: term, // search term
                        };
                    },

                    cache: true
                },
            });

            $('.unitselect').select2({
                placeholder: 'Search by unit code',
                minimumInputLength: 1,
                ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                    url: "{!! URL::to('')!!}/searchunitforselect2",

                    dataType: 'json',
                    quietMillis: 150,
                    data: function (term) {
                        return {
                            q: term, // search term
                        };
                    },

                    cache: true
                },
            });

            $("#filterform").submit(function (event) {
                {{--event.preventDefault();--}}
                {{--var baseurl = "{{ url('/unitsetup/') }}";--}}
                {{--var getstr = "";--}}
                {{--if ($("#teaching_period_id_selector").val()) {--}}
                {{--var teachingperiods = $("#teaching_period_id_selector").select2('val').join();--}}
                {{--getstr+="?teachingperiod=" + teachingperiods--}}

                {{--} else {--}}
                {{--getstr+="?teachingperiod=-1"--}}
                {{--}--}}
                {{--if ($("#school_id_selector").val()) {--}}
                {{--var schools = $("#school_id_selector").select2('val').join();--}}
                {{--getstr+=((getstr.length>0?"&":"?")+"schoolid=" + schools);--}}
                {{--} else {--}}
                {{--getstr+=((getstr.length>0?"&":"?")+"schoolid=" + schools);--}}
                {{--}--}}

                {{--location.replace("{{ url('/unitsetup/') }}"+getstr);--}}

            });

            {{--@if(session()->has('teachingperiod'))--}}
            {{--$('#teaching_period_id_selector').select2('val', '{{session('teachingperiod')}}')--}}
            {{--@endif--}}

            // make all date class elements into datepicker
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
            });

            $(".yearselector").TouchSpin({
                initval: 2016,
                min: 2016,
                max: 3000,
                stepinterval: 1,
            });

            $('.modalform').on('shown.bs.modal', function () {
                $(this).find("form").validator();
            });

            $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
                currenteditingid = $(this).data('id');
                switch ($(this).data('target')) {
                    case '#deletedialog':
                        currentid = $(this).data('id');
                        break;
                    case '#copydialog':
                        currentid = $(this).data('id');
                        break;
                    default:
                        currentid = -1;
                        break;
                }

            });

            $('.newdialog').submit(function (event) {
                event.preventDefault();
                $(this).modal('hide');
                var vars = $(this).find("form").serializeArray();
                vars.push({name: 'combined_units', value: $("#newdialog :input[name='combined_units_ids']").val()});
                console.log(vars);
                waitingDialog.show();
                submitNewForm(vars, $(this).data('route'));
            });

            $("#copydialog").submit(function (event) {
                $(this).modal('hide');
                // cancels the form submission
                var vars = $(this).find("form").serializeArray();
                vars.push({name: 'id', value: currentid});
                event.preventDefault();
                waitingDialog.show();
                submitCopyForm(vars);
            });

            // all deletes are the same
            $("#deletedialog").submit(function (event) {
                $(this).modal('hide');
                // cancels the form submission
                event.preventDefault();
                waitingDialog.show();
                deleteItem(currentid, 'unitsetup');
            });

            $('#sendemailtoselecteddialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();
                var selectedids = [];
                // hack to get combined units as comma separated values
                $(":checkbox").each(function () {
                    if ($(this).prop('checked') == true) {
                        selectedids.push(($(this).data('id')));
                    }
                });
                vars.push({name: 'ids', value: selectedids});
                console.log(vars);
                //waitingDialog.show();
                submitBulkMailForm(vars);
            });

            $('#archiveselecteddialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();
                var selectedids = [];

                $(":checkbox").each(function () {
                    if ($(this).prop('checked') == true) {
                        selectedids.push(($(this).data('id')));
                    }
                });
                vars.push({name: 'ids', value: selectedids});
                console.log(vars);
                //waitingDialog.show();
                submitArchiveSelectedForm(vars);
            });

            // intercept new condition event dialogue form submission
            $('#newcsvdialog').submit(function (event) {
                console.log('submitting CSV')
                $('#newcsvdialog').modal('hide');
                if ($('#upload_file')[0].files.length > 0) {

                    var data = new FormData();
                    // file
                    jQuery.each($('#upload_file')[0].files, function (i, file) {
                        data.append('csvfile', file);
                    });
                    //
                    data.append('template_id', $("#csv_template_id").val());
                    data.append('eso_id', $("#csv_eso_id").val());
                    data.append('teaching_period_id', $("#csv_teaching_period_id").val());
                    data.append('_token', '{{ csrf_token() }}');
                    // console.log(data);
                    // cancels the form submission
                    event.preventDefault();
                    //   waitingDialog.show();
                    submitCSVForm(data);
                } else {
                    alert('Need to pick a file')
                }
            });

            @endcan

            $("#filterselectform").submit(function (event) {
                event.preventDefault();
                if ($("#teaching_period_id_selector").val()) {
                    var teachingperiods = $("#teaching_period_id_selector").select2('val').join();
                    location.replace("{{ url('/') }}?teachingperiod=" + teachingperiods);
                } else {
                    location.replace("{{ url('/') }}?teachingperiod=-1");
                }
            });
            {{--@endcan--}}
        });

        @can('update_unit_instance')

        function submitNewForm(vars, route) {


            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/create',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (Number(data.id) > 0) {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the insertion');
                    }
                }
            });
        }

        {{--@can('update-review-offering')--}}
        function submitBulkMailForm(vars) {
            $.ajax({
                url: '{!! URL::to('')!!}/unitsetup/sendbulkemail',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();

                    if (Number(data.status) == 0) {
                        location.reload(true);
                    }
                    else if ((data.error).length > 0) {
                        alert(data.error);
                    }
                    else {
                        alert('something went wrong with the operation');
                    }
                }
            });
        }

        function submitArchiveSelectedForm(vars) {
            $.ajax({
                url: '{!! URL::to('')!!}/unitsetup/bulkarchive',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();

                    if (Number(data.status) == 0) {
                        location.reload(true);
                    }
                    else if ((data.error).length > 0) {
                        alert(data.error);
                    }
                    else {
                        alert('something went wrong with the operation');
                    }
                }
            });
        }


        function submitCopyForm(vars) {
            $.ajax({
                url: '{!! URL::to('')!!}/unitsetup/clone',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();

                    if (Number(data.id) > 0) {
                        // take me to the newly created offering
                        location.replace('{!! URL::to('')!!}/unitsetup/' + data.id);
                    }
                    else if ((data.error).length > 0) {
                        alert(data.error);
                    }
                    else {
                        alert('something went wrong with the operation');
                    }
                }
            });
        }

        function submitCSVForm(vars) {
            console.log('submitCSVForm');
            waitingDialog.show('Uploading...');
            $.ajax({
                url: '{{URL::to('unitsetup/csvimport')}}',
                type: 'post',
                data: vars,
                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (typeof(data.error) === "undefined") {
                        location.reload(true);

                    } else {
                        alert('something went wrong with the operation: ' + data.value);
                    }
                }
            });
        }

        var allchecked = false;

        function selectall() {
            $(":checkbox.offeringcheckbox").each(function () {
                if (allchecked) {
                    $(this).prop('checked', false);
                } else {
                    $(this).prop('checked', true);
                }

            })
            allchecked = !allchecked;
        }

        function deleteItem(id, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/destroy',
                type: 'post',
                data: ([{name: 'id', value: id}, {
                    name: '_token',
                    value: '{{csrf_token()}}'
                }]),
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status == "0") {
                        location.reload();
                    } else {
                        alert('something went wrong with the delete');
                    }
                }
            });
        }

        function submitMailForm(vars) {


            $.ajax({
                url: '{!! URL::to('')!!}/unitsetup/sendemailtoselected',
                type: 'get',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (Number(data.status) == 0) {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the insertion');
                    }
                }
            });
        }

        @endcan
        /*
         *
         * Helper functions
         *
         */

        // populates a form with data returned from Laravel
        function populate(frm, data) {
            $.each(data, function (key, value) {
                var $ctrl = $('[name=' + key + ']', frm);
                if ($ctrl.is("select")) {
                    if ($ctrl.attr('multiple')) {
                        $ctrl.select2('val', value.split(',')).trigger('change');
                    } else {
                        $ctrl.select2('val', value);
                    }

                } else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":
                            $ctrl.val(value);
                            break;
                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).attr("checked", value);
                                }
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }
            });
        }


        {{--@endcan--}}


    </script>
    {!! Breadcrumbs::render('unitsetupwithcount.index', $offerings) !!}
    <fieldset style="padding-left: 15px; padding-right: 15px; margin-top: 0">
        {{--@can('update-review-offering')--}}

        <div class="col-md-12" style="padding-left: 0px">
            <div class="col-md-12 text-left" style="padding-left: 0px">
                <form action="{{URL::to('unitsetup')}}" class="form-inline" style="padding-left: 0px" id="filterform">
                    <div class="form-group" style="padding-right: 5px"
                    >

                        {!! Form::label('teaching_period_id_selector', 'Teaching Period', ['class'=>'control-label  text-left']) !!}
                        <br/>


                        <select class="form-control" name="teachingperiod[]" id="teaching_period_id_selector" multiple>
                            {{--<option value='-1'>Show all</option>--}}
                            @foreach ($teachingperiods as $teachingperiod)
                                <option value='{{$teachingperiod->id}}'
                                        {{in_array($teachingperiod->id, session("teachingperiod") ?: []) ? "selected": ""}}
                                        {{--@if(in_array($teachingperiod->id, explode(',',session('teachingperiod'))))--}}
                                        {{--selected--}}
                                        {{--@endif--}}
                                >
                                    {{$teachingperiod->teaching_period_lookup->teaching_period}} {{$teachingperiod->year}}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="form-group" style="padding-right: 5px"
                    >

                        {!! Form::label('school_id_selector', 'School', ['class'=>'control-label  text-left']) !!}<br/>

                        {{--{{dd(session('schoolid'))}}--}}
                        <select class="form-control" name="school[]" id="school_id_selector" multiple
                        >
                            {{--<option value='-1'>Show all</option>--}}
                            @foreach ($schools as $school)
                                <option value='{{$school->id}}'
                                        {{--@if(request('school')!==null)--}}
                                        {{--@if (((in_array($school->id, explode(',',session('schoolid')))&&(request('schoolid')>-1))--}}
                                        {{--@if(in_array($school->id, explode(',',session('schoolid'))))--}}
                                        {{in_array($school->id, session("schoolid") ?: []) ? "selected": ""}}
                                        {{--@endif--}}
                                        {{-->--}}

                                        {{--@else--}}

                                        {{--@if(Auth::user()->schools->count()>0)--}}
                                        {{--<option value='{{$school->id}}'--}}
                                        {{--@if((Auth::user()->schools->first()->id==$school->id))--}}
                                        {{--selected="yes"--}}
                                        {{--@endif--}}
                                        {{-->--}}
                                        {{--@endif--}}
                                >
                                    {{--@endif--}}
                                    {{$school->description}} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group"
                    >
                        @can('update_unit_instance')
                        {!! Form::label('show_other_offerings', 'Show other ESO\'s units' , ['class'=>'control-label  text-left']) !!}
                        <br/>

                        <input type="hidden" name="show_others[0]" value="0"/>

                            <div class="checkbox checkbox-success">
                                <input type="checkbox" value="1" name="show_others[1]"
                                        {{in_array('1', session("show_others") ?: []) ? "checked": ""}}
                                >
                                {{--{{((request('show_others') == '1')||(session('show_others')=='1'))?'checked':''}} >--}}
                                <label></label>
                            </div>
                        @endcan

                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-filter" aria-hidden="true"></i>
                            Filter
                        </button>
                    </div>
                </form>
            </div>

            <div class="col-md-2" style="padding-left: 0px">
                {{ $offerings->appends(\Illuminate\Support\Facades\Input::except('page'))->links() }}
            </div>

        </div>

        <table class="table table-striped">
            <thead class="thead-inverse">
            <tr>
                @can('update_unit_instance')
                    @can('send_emails')
                        <th class="headerSortable header">
                            <div class="btn-group btn-group-sm" role="group" style="width: 100px">
                                <button class="btn btn-sm btn-primary" onclick="selectall()"><i
                                            class="fa fa-check-square-o "></i></button>
                                <button class="btn btn-sm btn-primary" data-toggle="modal"
                                        data-target="#sendemailtoselecteddialog"><i class="fa fa-envelope"></i></button>
                                <button class="btn btn-sm btn-primary" data-target="#archiveselecteddialog"
                                        data-toggle="modal"><i class="fa fa-trash"></i></button>
                            </div>
                        </th>
                    @endcan
                @endcan
                <th class="headerSortable header">@sortablelink('unit_code', 'Unit')</th>
                {{--<th class="headerSortable header">Unit</th>--}}
                <th class="headerSortable header">Combined units</th>
                <th>School</th>


                <th class="headerSortable header">@sortablelink('discipline', 'Discipline')</th>
                <th class="headerSortable header">Teaching period</th>
                &nbsp;@if($offerings->where('has_olx', '1')->count()>0)
                    <th class="headerSortable header">@sortablelink('has_olx','OLX')</th>
                @endif
                &nbsp;@if($offerings->where('is_accredited', '1')->count()>0)
                    <th class="headerSortable header">@sortablelink('is_accredited','Accredited')</th>
                @endif
                <th class="headerSortable header">@sortablelink('coordinator_name', 'Co-ordinator')</th>
                <th class="headerSortable header">@sortablelink('eso_name', 'Assigned ESO')</th>
                <th class="headerSortable header">@sortablelink('complete_percent', 'Completion')</th>

                @can('update_unit_instance')
                    <th></th>
                @endcan

            </tr>
            </thead>
            @if(isset($offerings))
                @foreach ($offerings as $offering)

                    <tr
                            {!! ($offering->follow_up=='true'?'style="background-color: #ffb991"':"") !!}>
                        @can('update_unit_instance')
                            @can('send_emails')
                                <td>
                                    <div class="checkbox checkbox-success">
                                        <input type="checkbox" value="1" class="offeringcheckbox"
                                               data-id="{{$offering->id}}"> <label></label>
                                    </div>
                                </td>
                            @endcan
                        @endcan
                        <td>
                            <a href="{{action('OfferingsController@show', $offering->id)}}">
                                {{$offering['unit_code']}}
                            </a>

                        </td>
                        <td>
                            @foreach($offering->combined_units as $combined_unit)
                                {{$combined_unit->unit['unit_code']}}{{($combined_unit != $offering->combined_units->last())?",":""}}
                            @endforeach
                        </td>
                        <td>
                            @if(isset($offering->unit->discipline))
                                {{$offering->unit->discipline->school->description}}
                            @else
                            (Unknown school
                            @can('is_admin')
                            , please <a href="{!! URL::to('')!!}/unit/{{$offering->unit->id}}">update</a>
                            @endcan)
                            @endif
                        </td>
                        <td>

                            @if(isset($offering->unit->discipline))
                                {{$offering->discipline}}
                            @else
                            (Unknown discipline
                            @can('is_admin')
                            , please <a href="{!! URL::to('')!!}/unit/{{$offering->unit->id}}">update</a>
                            @endcan)
                            @endif
                        </td>
                        <td>
                            {{$offering->teaching_period->teaching_period_lookup->teaching_period}} {{$offering->teaching_period['year']}}
                        </td>
                        &nbsp;@if($offerings->where('has_olx', '1')->count()>0)
                            <td @if($offering->has_olx=='1') style="background-color: #7D5177" @endif>
                                @if($offering->has_olx=='1')<strong style="color: #F3F3F3">Yes</strong> @endif
                            </td>
                        @endif
                        &nbsp;@if($offerings->where('is_accredited', '1')->count()>0)
                            <td @if($offering->is_accredited=='1') style="background-color: #D9534F" @endif>
                                @if($offering->is_accredited=='1')<strong style="color: #F3F3F3">Yes</strong> @endif
                            </td>
                        @endif
                        <td>
                            {{$offering->coordinator_id>0?$offering['coordinator_name']:'(Unknown coordinator)'}}
                        </td>
                        <td>
                            {{$offering['eso_name']}}
                        </td>

                        <td>
                            <div class="progress">
                                @if(($offering->complete_percent)>0)
                                    <div class="progress-bar" role="progressbar"
                                         aria-valuenow="{{($offering->status=='complete')?100:$offering->complete_percent}}"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="min-width: 2em; width:{{$offering->complete_percent}}%;
                                         @if($offering->status=='complete')
                                                 background-color:#28a745
                                         @endif">
                                        @if($offering->status=='complete')
                                            Complete
                                        @else
                                            {{($offering->status=='complete')?100:$offering->complete_percent}}%
                                        @endif
                                    </div>
                                @endif
                            </div>

                        </td>
                        {{--@can('update-review-offering')--}}
                        @can('update_unit_instance')
                            <td>
                                <a href="#" data-toggle="modal" data-id="{{$offering->id}}"
                                   data-target="#deletedialog">
                                    <i class="fa fa-2x fa-times-circle" style='color: #ff6600'></i>
                                </a>
                            </td>
                    @endcan
                    {{--@endcan--}}

                @endforeach
            @endif
        </table>
    </fieldset>
    {{ $offerings->appends(\Illuminate\Support\Facades\Input::except('page'))->links() }}
    {{--@can('update-review-offering')--}}
    <div id="newdialog" class="modal fade newdialog modalform" role="dialog"
         data-route="unitsetup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Offering</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open( ['class'=>'form-horizontal', 'role'=>'form'])!!}

                    @include('unit_setup.form.newofferingform', ['submitButtonText'=>'Add offering'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div id="newcsvdialog" class="modal fade  modalform" role="dialog"
         data-route="unitsetup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add using CSV</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['class'=>'form-horizontal', 'role'=>'form'])!!}
                    @include('unit_setup.form.csvupload')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Copying a unit review event  --}}
    <div id="copydialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Copy a unit</h4>
                </div>
                <div class="modal-body">
                    Coming soon!
                </div>
            </div>
        </div>
    </div>

    {{--  Deleting a unit review event  --}}
    <div id="deletedialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('form_common.deletedialog')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    {{--@endcan--}}
    {{--Adding a note --}}
    <div id="sendemailtoselecteddialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Send an email to selected</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('unit_setup.form.sendemailform', ['submitButtonText'=>'Send email'])
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>


    <div id="archiveselecteddialog" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really archive?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}

                    @include('form_common.confirmdialog')
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>
@stop

