@extends('layouts.app')
@section('title'){{$instance->unit['unit_code']}} {{$instance->teaching_period->teaching_period_lookup->teaching_period}} {{$instance->teaching_period['year']}}@endsection
@section('content')
    {{--<link rel="stylesheet" href="{{URL::asset('resources/assets/dhtmlx/dhtmlxscheduler_flat.css')}}">--}}
    {{--<link rel="stylesheet" href="{{URL::asset('resources/assets/dhtmlx/common/dhtmlxCombo/dhtmlxcombo.css')}}">--}}
    {{--<script src="{{ URL::asset('resources/assets/dhtmlx/dhtmlxscheduler.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('resources/assets/dhtmlx/ext/dhtmlxscheduler_editors.js') }}"></script>--}}
    {{--<script src="{{ URL::asset('resources/assets/dhtmlx/common/dhtmlxCombo/dhtmlxcombo.js') }}"></script>--}}
    <style>
        table.note,
        table.note tr,
        table.note td {
            border: 1px solid black;
        }

        table.note {
            border-collapse: collapse;
        }

        table.table-striped td {
            border-left: solid 1px gray;

        }

        #scheduler_here {
            width: 100%;
            height: 600px;
            /*display: block;*/
        }

        .fill {
            min-height: 100%;
            height: 100%;
            width: 100%;
            box-sizing: border-box;
        }

        /*
         * Main content
         */

        .main {
            padding: 20px;
        }

        .dhtmlx_popup_text {
            background: #f0f0f0;
            color: red;
        }

        @can('update_unit_instance')
        table.table-striped tr td:nth-child(2) {
            width: 30%;
        }

        div.handle {
            width: 1%;
            white-space: nowrap;
            cursor: grab;
        }

        div.handle:active {
            width: 1%;
            white-space: nowrap;
            cursor: grabbing;
        }

        @else
        table.table-striped tr td:first-child {
            min-width: 10%;
            width: 30%;

        }
        @endcan

    </style>
    <script src="{{ URL::asset('resources/assets/js/tinymce/tinymce.min.js') }}"></script>

    <script>

        //tinymce Bootstrap fix
        // https://www.tinymce.com/docs/integrations/bootstrap/
        $(document).on('focusin', function (e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });

        // init document

        $(document).ready(function () {

            // set up select2 dropdowns
            $('select').select2();


            // Set up tab persistence across reloads
            if (location.hash.substr(0, 2) == "#!") {
                $("a[href='#" + location.hash.substr(2) + "']").tab("show");
            }

            $("a[data-toggle='tab']").on("shown.bs.tab", function (e) {
                var hash = $(e.target).attr("href");
                if (hash.substr(0, 1) == "#") {
                    location.replace("#!" + hash.substr(1));
                }

            });


            @if(!$readonly)

            // init timyMCE
            tinymce.init({
                selector: '.tinymce',
                height: 500,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                content_css: '//www.tinymce.com/css/codepen.min.css'
            });

            $('.editdialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();
                vars.push({name: 'id', value: currenteditingid});
                vars.push({name: '_method', value: 'PATCH'});

                waitingDialog.show();
                submitUpdateForm(vars, $(this).data('route'));
            });



            {{--@can('update-review-instance')--}}
            // drag and drop reordering
            dragula([document.getElementById('checklisttablebody')], {
                moves: function (el, container, handle) {

                    return handle.classList.contains('handle');
                }
            }).on('drop', function (el, target, source, sibling) {
                // send an array of elements to backend to reorder
                //    waitingDialog.show();
                console.log(sibling);

                var orderArray = ([]);
                var orderint = 0;
                $(".sortablerow").each(function () {
                    orderArray.push({id: $(this).attr('entryid'), order: orderint})
                    orderint++;
                    console.log($(this).attr('entryid'));
                });
                // for some reason, the last element of this is always the element being moved.
                orderArray.pop();

                var dataObj = ([{name: '_token', value: '{{csrf_token()}}'}, {
                    name: 'order',
                    value: JSON.stringify(orderArray)
                }]);

                $.ajax({
                    url: '{!! URL::to('')!!}/unitsetupinstancechecklistitem/reorder',
                    type: 'post',
                    data: dataObj,
                    error: function (jqXHR, textStatus, errorThrown) {
                        waitingDialog.hide();
                        alert(errorThrown);
                    },
                    success: function (data) {
                        //   waitingDialog.hide();
                        if (data.status.toString() == "0") {
                            location.reload(true);
                        } else {
                            waitingDialog.hide();
                            alert('something went wrong with the update');
                        }
                    }
                });

            });

            {{--@endcan--}}
            @endif
            $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
                currenteditingid = $(this).data('id');
                console.log($(this).data('target'))
                $(".emailtemplate").hide()
                switch ($(this).data('target')) {
                    @if(!$readonly)
                    case '#editdetailsdialog':
                        $("#combined_units_ids").val([
                            @foreach($instance->combined_units as $combined_unit)
                                "{{$combined_unit->units_id}}",
                            @endforeach
                        ]).trigger("change");
                        $("#teachers").val([
                            @foreach($instance->teachers as $teacher)
                                "{{$teacher->id}}",
                            @endforeach
                        ]).trigger("change");
                        break;
                    case '#editchecklistitemdialog':
                        currenteditingid = $(this).data('id');
                        console.log(currenteditingid)
                        getDetails(currenteditingid, 'unitsetupinstancechecklistitem', 'editchecklistitemdialog');
                        break;

                    case '#editmediadialog':
                        currenteditingid = $(this).data('id');
                        console.log(currenteditingid)
                        getDetails(currenteditingid, 'unitmedia', 'editmediadialog');
                        break;


                    case '#followupdialog':
                        //currenteditingid = $(this).data('id');
                        //console.log(currenteditingid)
                        //  getDetails(currenteditingid, 'unitmedia', 'editmediadialog');
                        break;

                    case '#deletedialog':
                        currentdeleteroute = $(this).data('route');
                        console.log($(this).data('route'));
                        currentdeletingid = $(this).data('id');
                        break;
                    @endif
                    case '#viewmessagedialog':

                        getEmail($(this).data('id'));
                        break;
                    case'#itememailpreview':

                        getEmailPreview($(this).data('id'));

                    default:
                        currenteditingid = -1;
                        currentdeletingid = -1;
                        break;
                }

            });

            @if(!$readonly)

            $('.modalform').on('shown.bs.modal', function () {
                $(this).find("form").validator();

            });

            {{--@can('update-review-instance')--}}

            $('#followupdialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();
                // hack to get combined units as comma separated values

                vars.push({name: 'id', value: '{{$instance->id}}'});
                vars.push({name: 'action', value: 'follow_up'});
                waitingDialog.show();
                submitUpdateForm(vars, 'unitsetup/{{$instance->id}}');
            });

            $('#editdetailsdialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();
                // hack to get combined units as comma separated values
                // vars.push({
                //     name: 'combined_units',
                //     value: $("#editdetailsdialog :input[name='combined_units_ids']").val()
                // });
                vars.push({
                    name: 'teachers_ids',
                    value: $("#editdetailsdialog :input[name='teachers']").val()
                });
                vars.push({name: 'id', value: '{{$instance->id}}'});
                waitingDialog.show();
                submitUpdateForm(vars, 'unitsetup/{{$instance->id}}');
            });

            $('#newnoteitemdialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();
                // hack to get combined units as comma separated values

                vars.push({name: 'id', value: '{{$instance->id}}'});
                waitingDialog.show();
                submitMailForm(vars);
            });


            {{--@endcan--}}

            $('.newdialog').submit(function (event) {
                event.preventDefault();
                $(this).modal('hide');
                var vars = $(this).find("form").serializeArray();
                waitingDialog.show();
                submitNewForm(vars, $(this).data('route'));
            });


            // all deletes are the same
            $("#deletedialog").submit(function (event) {
                $(this).modal('hide');
                // cancels the form submission
                event.preventDefault();
                waitingDialog.show();
                deleteItem(currentdeletingid, currentdeleteroute);
            });


            $('#archivedialog').submit(function (event) {
                // cancels the form submission
                event.preventDefault();
                $(this).modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                var vars = $(this).find("form").serializeArray();

                waitingDialog.show();
                $.ajax({
                    url: '{!! URL::to('')!!}/unitsetup/{{$instance->id}}/archive',
                    type: 'post',
                    data: vars,
                    error: function (jqXHR, textStatus, errorThrown) {
                        waitingDialog.hide();
                        alert(errorThrown);
                    },
                    success: function (data) {
                        //   waitingDialog.hide();
                        if (data.status.toString() == "1") {
                            location.replace('{!! URL::to('')!!}/unitsetup');
                        } else {
                            waitingDialog.hide();
                            alert('something went wrong with the update');
                        }
                    }
                });
            });


            // intercept new condition event dialogue form submission
            $('#newmediadialog').find("form").submit(function (event) {
                $('#newmediadialog').modal('hide');
                if ($('#upload_file')[0].files.length > 0) {
                    //  var vars = $("#newmediadialog").find("form").serializeArray();
                    var data = new FormData();
                    // file

                    jQuery.each($('#upload_file')[0].files, function (i, file) {
                        data.append('userfile', file);
                    });
                    data.append('unit_id', '{{$instance->id}}');
                    // data.append('description', $descriptionctrl.val());
                    var $descriptionctrl = $('[name=description]', $("#newmediadialog").find("form"));
                    //console.log($descriptionctrl.val());
                    data.append('description', $descriptionctrl.val());
                    data.append('_token', '{{ csrf_token() }}');
                    // console.log(data);
                    // cancels the form submission
                    event.preventDefault();
                    //   waitingDialog.show();
                    submitNewMediaForm(data);
                } else {
                    alert('Need to pick a file')
                }
            });

            // intercept edit media event dialogue form submission
            $('#editmediadialog').submit(function (event) {
                $('#editmediadialog').modal('hide');
                //  var vars = $("#newmediadialog").find("form").serializeArray();
                console.log('editmediadialog submission');
                var data = new FormData();
                // file
                jQuery.each($('#altupload_file')[0].files, function (i, file) {
                    data.append('userfile', file);
                });
                var $descriptionctrl = $('[name=description]', $("#editmediadialog").find("form"));

                data.append('description', $descriptionctrl.val());
                data.append('id', currenteditingid);
                data.append('_method', 'PATCH');
                data.append('_token', '{{ csrf_token() }}');

                // cancels the form submission
                event.preventDefault();
                waitingDialog.show();
                submitEditMediaForm(data);
            });

            {{--@endcan--}}
            @endif

        });

        // experimental: attach Moodle url if not set
        @if(!isset($instance->moodle_url))
        getmoodleurl(false);
                @endif


        var editbydragging = false;



        @if(!$readonly)


        /*
         *
         * Helper functions
         *
         */



        // populates a form with data returned from Laravel
        function populate(frm, data) {
            $.each(data, function (key, value) {
                var $ctrl = $('[name=' + key + ']', frm);
                if ($ctrl.is("select")) {
                    if ($ctrl.attr('multiple')) {
                        console.log(key, value)
                        $ctrl.val(value.split(',')).trigger('change');
                    } else {
                        console.log(key, value)
                        $ctrl.val(value).trigger('change');
                    }
                } else if ($ctrl.is("textarea")) {
                    if ($ctrl.hasClass('tinymce')) {
                        // fix for this weird bug where tinymce chucks a wobbly if the content is set to a null value
                        tinyMCE.get($ctrl.attr('id')).setContent((value === null) ? '' : value);
                    } else {
                        $ctrl.val((value === null) ? '' : value);
                    }
                }
                else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":
                            $ctrl.val(value);
                            $ctrl.trigger('change');
                            break;

                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).prop("checked", true);
                                } else {
                                    $(this).prop("checked", false);
                                }
                                $(this).trigger('change');
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }

            });
        }

        function getDetails(id, route, formid) {
            waitingDialog.show();
            //  console.log('getDetails(' + id + "," + route + "," + formid + ")");
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/' + id,
                type: 'GET',
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (data) {
                    //      console.log(data);
                    populate($("#" + formid).find("form"), data);
                    waitingDialog.hide();
                }
            });
        }


        // Edit
        function submitUpdateForm(vars, route) {
            // console.log(route);
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/update',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    //   waitingDialog.hide();
                    if (data.status.toString() == "1") {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert('something went wrong with the update');
                    }
                }
            });
        }


        function submitNewForm(vars, route) {
            vars.push({name: 'offering_id', value: '{{$instance->id}}'})

            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/create',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (Number(data.id) > 0) {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the insertion');
                    }
                }
            });
        }

        function updateFollowUp(status) {
            //
            $("#waiting_cog_followup").show();
            var vars = new Array();
            vars.push({name: '_token', value: '{{csrf_token()}}'});
            vars.push({name: 'follow_up', value: status});
            vars.push({name: 'id', value: {{$instance->id}}});
            $.ajax({
                url: '{!! URL::to('')!!}/unitsetup/{{$instance->id}}/update',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    $("#waiting_cog_followup").hide();
                    if (Number(data.status) > 0) {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the update');
                    }
                }
            });
        }

        function updatehandover(status) {
            //
            $("#waiting_cog_handover").show();
            var vars = new Array();
            vars.push({name: '_token', value: '{{csrf_token()}}'});
            vars.push({name: 'status', value: status});
            vars.push({name: 'id', value: {{$instance->id}}});
            $.ajax({
                url: '{!! URL::to('')!!}/unitsetup/{{$instance->id}}/updatestatus',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    $("#waiting_cog_handover").hide();
                    if (!(Number(data.status) > 0)) {

                        alert('something went wrong with the update');
                    }
                }
            });
        }

        function updateStatus(id, status, warn) {
            // confirm the email is about to be sent
            console.log()
            if (warn && status) {
                r = confirm('This will email the coordinator. You sure?');
                if (!r) {
                    // if the user wusses out, reset the checkbox
                    $("input[type=checkbox][data-id=" + id + "]").prop('checked', (status != 1))
                    return;
                }
            }
            $("#waiting_cog_" + id).show();
            var vars = new Array();
            vars.push({name: '_token', value: '{{csrf_token()}}'});
            vars.push({name: 'id', value: id});
            vars.push({name: 'status', value: status});
            $.ajax({
                url: '{!! URL::to('')!!}/unitsetupinstancechecklistitem/updatestatus',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    $("#waiting_cog_" + id).hide();
                    if (Number(data.status) > 0) {
                        //  location.reload(true);
                        $('#progress_bar').css('width', (data.progress / Number({{$instance->checklist()->get()->where('heading', '<>', '1')->count()}}) * 100) + '%').attr('aria-valuenow', (data.progress / Number({{$instance->checklist()->get()->where('heading', '<>', '1')->count()}}) * 100));
                        $('#progress_bar').html(Math.round(data.progress / Number({{$instance->checklist()->get()->where('heading', '<>', '1')->count()}}) * 100) + '%');
                        if (warn) {
                            if (Number(data.emailstatus) > 0) {
                                alert('Email sending failed')
                            }
                        }

                    } else {
                        alert('something went wrong with the update');
                    }
                }
            });
        }

        // Delete contact event
        function deleteItem(id, route) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/destroy',
                type: 'post',
                data: ([{name: 'id', value: id}, {name: '_method', value: 'DELETE'}, {
                    name: '_token',
                    value: '{{csrf_token()}}'
                }]),
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status == "1") {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the delete');
                    }
                }
            });
        }

        // Media-specific functions
        // New media
        function submitNewMediaForm(vars) {
            $.ajax({
                url: '{{URL::to('unitmedia/create')}}',
                type: 'post',
                data: vars,
                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data > 0) {
                        location.reload(true);
                    }
                }
            });
        }

        // Edit media
        function submitEditMediaForm(vars) {
            $.ajax({
                url: '{{URL::to('unitmedia/update')}}',
                type: 'post',
                data: vars,
                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {

                    if (data.status == "1") {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert('something went wrong with the update');
                    }
                }
            });
        }

        function getmoodleurl(showfeedback) {
            var vars = new Array();
            //  vars.push({name: '_token', value: '{{csrf_token()}}'});
            console.log(vars);
            $.ajax({
                url: '{!! URL::to('')!!}/unitsetup/{{$instance->id}}/detectmoodleurl',
                type: 'get',
                // data: vars,
                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {

                    if (data.status == "1") {
                        //update the URL
                        $("#moodleurl").html('<a href="' + data.value + '">' + data.value + '</a>');
                    } else {
                        waitingDialog.hide();
                        if (showfeedback) {
                            alert('Could not find Moodle URL');
                        }
                    }
                }
            });
        }

        function updateCalendar() {
            waitingDialog.show();
            var vars = new Array();
            //  vars.push({name: '_token', value: '{{csrf_token()}}'});
            console.log(vars);
            $.ajax({
                url: '{!! URL::to('')!!}/calendar/{{$instance->id}}/updatefromcentral',
                type: 'get',
                // data: vars,
                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();

                    if (Number(data.status) > 0) {
                        //update the URL
                        reloadcalendar();
                    } else {

                        alert('something went wrong with the update');
                    }
                }
            });
        }

        function submitMailForm(vars) {
            vars.push({name: 'unit_instances_id', value: '{{$instance->id}}'})

            $.ajax({
                url: '{!! URL::to('')!!}/unitreviews/{{$instance->id}}/sendemail',
                type: 'get',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (Number(data.status) == 0) {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the insertion');
                    }
                }
            });
        }

        @endif

        // get an email
        function getEmail(id) {
            waitingDialog.show();
            //  console.log('getDetails(' + id + "," + route + "," + formid + ")");
            $.ajax({
                url: '{!! URL::to('')!!}/unitsetup/{{$instance->id}}/viewmail/' + id,
                type: 'GET',
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (data) {
                    console.log(data.fulltext);
                    $("#viewmessagecontent").html(data.fulltext);
                    waitingDialog.hide();
                }
            });
        }

        // get an email preview
        function getEmailPreview(id) {
            waitingDialog.show();
            var vars = new Array();
            vars.push({name: '_token', value: '{{csrf_token()}}'});
            vars.push({name: 'id', value: id});

            $.ajax({
                url: '{!! URL::to('')!!}/unitsetupinstancechecklistitem/previewemail',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (data) {
                    $("#itememailpreviewsubject").html(data.subject);
                    $("#itememailpreviewcontent").html(data.fulltext);
                    waitingDialog.hide();
                }
            });
        }

    </script>

    <!-- Tabs -->
    {!! Breadcrumbs::render('unitsetup.show', $instance) !!}
    {{--{{dd($instance)}}--}}
    <div class="col-md-12 page-header" style="margin-top: 0px">

        <div class="box box-element">
            <div class="view">
                <div class="row" @if($instance->follow_up == 'true')
                style="background-color:#ffb991"
                        @endif>
                    <div class="container col-md-6">
                        <h3 style="margin-top: 0px">Setup
                            for {{trim($instance->unit['unit_code'])}}{{($instance->combined_units->count()>0?'/'.$instance->combined_units->implode('unit.unit_code', '/'):'')}}
                            {{--@foreach($instance->combined_units as $combined_unit)--}}
                            {{--@if(isset($combined_unit->unit))/{{$combined_unit->unit['unit_code']}}--}}
                            {{--@endif--}}
                            {{--@endforeach--}}
                            {{$instance->unit['description']}} {{$instance->teaching_period->teaching_period_lookup->teaching_period}} {{$instance->teaching_period['year']}}
                            @if(!$readonly)
                                <a href="#" data-toggle="modal" data-target="#editdetailsdialog">(Update)</a>
                            @endif
                        </h3>
                        <div class="container col-md-6">


                            <strong>Coordinator:</strong> @if($instance->coordinator_id>0)
                                {{$instance->coordinator['name']}} ({{$instance->coordinator['username']}})
                                {{($instance->coordinator['id']==Auth::user()->id)?'(you)':''}}
                            @else
                                                              (Unknown coordinator)
                            @endif

                            <br/>
                            <strong>Teachers:</strong>
                            @foreach($instance->teachers as $teacher)
                                {{$teacher->name}} ({{$teacher->username}})@if (!$loop->last), @endif

                            @endforeach

                            <br/>
                            <strong>Assigned
                                ESO:</strong> {{$instance->eso['name']}} {{($instance->eso['id']==Auth::user()->id)?'(you)':''}}

                            <br/>

                        </div>
                        <div class="container col-md-6">
                            <strong>Discipline:</strong> {{$instance->unit->discipline->description}}
                            @if($instance->has_olx=='1')
                                <div class="container col-md-12"
                                     style="background-color: #7D5177; color: #F3F3F3; font-weight: bold">Has OLX
                                </div>@endif
                            @if($instance->is_accredited=='1')
                                <div class="container col-md-12"
                                     style="background-color: #D9534F; color: #F3F3F3; font-weight: bold">Accredited
                                    unit
                                </div>@endif
                        </div>


                    </div>
                    <div class="container col-md-6" style="border-left: #0f0f0f 1px solid">

                        <div class="row">
                            <div class="col-md-2"><strong>Complete:</strong></div>
                            <div class="progress">
                                @if($instance->checklist()->get()->where('heading', '<>', '1')->count()>0)
                                    <div class="progress-bar" id="progress_bar" role="progressbar"
                                         aria-valuenow="{{round(($instance->checklistcomplete/($instance->checklist()->get()->where('heading', '<>', '1')->count()))*100)}}"
                                         aria-valuemin="0" aria-valuemax="100"
                                         style="width:{{round(($instance->checklistcomplete/($instance->checklist()->get()->where('heading', '<>', '1')->count()))*100)}}%">
                                        {{round(($instance->checklistcomplete/($instance->checklist()->get()->where('heading', '<>', '1')->count()))*100)}}
                                        %
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row" style="padding-bottom: 15px">
                            <div class="col-md-2"><label for="status">Status:</label></div>
                            <select name="status" style="width: 200px"
                                    @if(!$readonly)
                                    onchange="updatehandover(this.value)"
                                    @else
                                    disabled
                                    @endif
                            >
                                <option value="in_progress"
                                        @if ($instance->status=='in_progress')
                                        selected
                                        @endif>In Progress
                                </option>
                                <option value="handover"
                                        @if ($instance->status=='handover')
                                        selected
                                        @endif>Handed over
                                </option>
                                <option value="complete"
                                        @if ($instance->status=='complete')
                                        selected
                                        @endif>Complete
                                </option>
                            </select>
                            <i id="waiting_cog_handover" style="display: none"
                               class="fa fa-cog fa-spin a-fw"></i>

                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                @if(!$readonly)
                                    <button class="btn btn-info" data-toggle="modal"
                                            data-target="#followupdialog">Follow-up
                                    </button>
                                @endif
                            </div>
                            <div class="col-md-10">
                                @if($instance->follow_up == 'true')
                                    <i style="font-size: 2em; color: orangered" class="fa fa-exclamation-triangle"
                                       aria-hidden="true"></i><strong>Flagged: {{$instance->follow_up_text}}</strong>

                                @endif
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <ul class="nav nav-tabs" id="tabslabels">
            <li class="active"><a data-toggle="tab" href='#checklisttab'>Setup Checklist Items</a></li>
            <li class=""><a data-toggle="tab" href='#resourcestab'>Resources</a></li>
            <li class=""><a data-toggle="tab" href='#mediatab'>Media</a></li>
            <li class=""><a data-toggle="tab" href='#notestab'>Messages</a></li>
        </ul>
        <div class="tab-content">
            <div id="checklisttab" class="tab-pane active" style="  height: 300px;">
                <fieldset style="width: 100%;  height: 300px;">
                    <legend>Setup Checklist...
                        @if(!$readonly)
                            <button class="btn btn-info btn-lg" data-toggle="modal"
                                    data-target="#newchecklistitemdialog">New item
                            </button>
                        @endif
                    </legend>
                    <table class="table table-striped  pre-scrollable">
                        <thead class="thead-inverse">
                        @if(!$readonly)
                            @can('update_unit_instance')
                                <th>
                                    Move
                                </th>
                            @endcan
                        @endif
                        <th>
                            Item
                            @if(!$readonly)
                                @can('update_unit_instance')(click to update)@endcan
                            @endif
                        </th>
                        {{--@if($instance->coordinator['id']==Auth::user()->id||)--}}
                        <th>
                            Status
                        </th>
                        {{--@endif--}}


                        <th>
                            notes
                        </th>
                        @if(!$readonly)
                            @can('update_unit_instance')
                                <th>
                                </th>
                            @endcan
                        @endif
                        </thead>
                        <tbody id="checklisttablebody">
                        @foreach($instance->checklist as $item)
                            @if(($item->heading)==1)
                                <tr style="background-color: #7ab800" class='sortablerow' entryid='{{$item->id}}'>
                                    @if(!$readonly)
                                        @can('update_unit_instance')
                                            <td>
                                                <div class="handle" style="width: 20px"><i
                                                            class="fa fa-2x fa-arrows-v handle"
                                                            style='color: #bf800c'></i></div>
                                            </td>
                                        @endcan
                                    @endif
                                    <td colspan="100%">
                                        @if(!$readonly)
                                            @can('update_unit_instance')
                                                <a href="#" data-toggle="modal"
                                                   data-id="{{$item->id}}"
                                                   data-route="unitsetupinstancechecklistitem"
                                                   data-target="#editchecklistitemdialog">
                                                    @endcan
                                                    @endif

                                                    <h4> {{$item->description}}</h4>
                                                    @if(!$readonly)
                                                        @can('update_unit_instance') </a>
                                                <a href="#" data-toggle="modal"
                                                   data-id="{{$item->id}}"
                                                   data-route="unitsetupinstancechecklistitem"
                                                   data-target="#deletedialog">
                                                    <i class="fa fa-times-circle" style='color: #ff6600'></i></a>
                                            @endcan
                                        @endif
                                    </td>
                                </tr>
                            @else
                                <tr class='sortablerow' @if($item->trigger_email==1)style="background-color: #ffb991"
                                    @endif entryid='{{$item->id}}'>
                                    @if(!$readonly)
                                        @can('update_unit_instance')
                                            <td>
                                                <div class="handle" style="width: 20px"><i
                                                            class="fa fa-2x fa-arrows-v handle"
                                                            style='color: #bf800c'></i></div>
                                            </td>
                                        @endcan
                                    @endif
                                    <td>
                                        @if(!$readonly)
                                            @can('update_unit_instance') <a href="#" data-toggle="modal"
                                                                            data-id="{{$item->id}}"
                                                                            @if(isset($item->trigger_email_template))
                                                                            @if($item->trigger_email==1)
                                                                            data-warn="true"
                                                                            @endif
                                                                            @endif
                                                                            data-route="unitsetupinstancechecklistitem"
                                                                            data-target="#editchecklistitemdialog">
                                                @endcan
                                                @endif
                                                {{$item->description}}
                                                @if(!$readonly)
                                                    @can('update_unit_instance')</a>@endcan
                                        @endif
                                        @if($item->trigger_email==1)
                                            <br/><i class="fa fa-exclamation-triangle"
                                                    style='color: #d9534f;'></i><strong>Will send email with
                                                template
                                                @if(isset($item->trigger_email_template))
                                                    "{{$item->trigger_email_template->label}}" @else "<i>(No email
                                                    template set, please update)</i>"@endif to coordinator when
                                                checked <a href="#" data-toggle="modal" data-id="{{$item->id}}"
                                                           data-target="#itememailpreview">(preview)</a> </strong>
                                        @endif


                                    </td>


                                    <td>
                                        {{--if this is the unit coordinator or has the ability to update checkboxes--}}
                                        @if(!$readonly)
                                            @if(Auth::user()->can('update_other_unit_instance_checklist_status'))
                                                <div class="checkbox checkbox-success">
                                                    <input type="checkbox" value="1"
                                                           {{($item->status == '1')?'checked':''}} data-id="{{$item->id}}"
                                                           onclick="updateStatus($(this).data('id'), $(this).prop('checked') @if(($item->trigger_email==1)&&(isset($item->trigger_email_template))) ,true @endif );">
                                                    <label></label>
                                                    <i id="waiting_cog_{{$item->id}}" style="display: none"
                                                       class="fa fa-cog fa-spin a-fw"></i>
                                                </div>
                                            @endif
                                        @else
                                            {!! ($item->status == '1')?'<i class="fa fa-check fa-2x" style="color:#5CB85C" aria-hidden="true"></i>':'<i class="fa fa-times fa-2x" aria-hidden="true" style="color:#FF6600"></i>'!!}
                                        @endif

                                    </td>

                                    <td>
                                        @if(isset($item['text']))
                                            {!! $item['text']!!}
                                        @endif
                                    </td>
                                    @if(!$readonly)
                                        @can('update_unit_instance')
                                            <td>
                                                <a href="#" data-toggle="modal" data-id="{{$item->id}}"
                                                   data-route="unitsetupinstancechecklistitem"
                                                   data-target="#deletedialog">
                                                    <i class="fa fa-2x fa-times-circle" style='color: #ff6600'></i>
                                                </a>
                                            </td>
                                        @endcan
                                    @endif
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </fieldset>
            </div>

            <div id="resourcestab" class="tab-pane">
                <fieldset style="width: 90%">
                    <legend>
                        Resources
                    </legend>
                    <div><strong>Moodle URL:</strong><br/> <span id="moodleurl">
    @if(strlen($instance->moodle_url)>0)
                                <a href="{{$instance->moodle_url}}" target="_blank"> {{$instance->moodle_url}}</a>
                            @endif
                </span>
                        @if(!$readonly)
                            <button class="btn btn-info btn-sm"
                                    onclick="getmoodleurl(true)">Get/update Moodle URL
                            </button>
                        @endif
                    </div>
                    <div><strong>CAUC URLs:</strong><br/>


                        <a href="https://my.une.edu.au/courses/units/{{$instance->unit->unit_code}}"
                           target="_blank"> https://my.une.edu.au/courses/units/{{$instance->unit->unit_code}}</a>


                        @if($instance->combined_units->count()>0)
                            @foreach($instance->combined_units as $combined_unit)
                                <br/>
                                @if(isset($combined_unit->unit))
                                    <a href="https://my.une.edu.au/courses/units/{{$combined_unit->unit->unit_code}}"
                                       target="_blank">
                                        https://my.une.edu.au/courses/units/{{$combined_unit->unit->unit_code}}</a>
                                @endif
                            @endforeach

                        @endif
                    </div>
                    {{--<div><strong>Download Unit Information and Assessment Overview book</strong>--}}
                    {{--<br/>--}}
                    {{--<a href="{!! URL::to('')!!}/unitsetup/{{$instance->id}}/getuiaobook">{{$instance->unit->unit_code}}</a>--}}
                    {{--@if($instance->combined_units->count()>0)--}}
                    {{--@foreach($instance->combined_units as $combined_unit)--}}
                    {{--<br/>--}}
                    {{--@if(isset($combined_unit->unit))--}}
                    {{--<a href="{!! URL::to('')!!}/unitsetup/{{$combined_unit->id}}/getuiaobook">{{$combined_unit->unit['unit_code']}}</a>--}}
                    {{--@endif--}}
                    {{--@endforeach--}}

                    {{--@endif--}}
                    {{--</div>--}}
                    <div><strong>Coordinator block boilerplate</strong>
                        <br/>
                        <textarea style="width: 500px; height: 200px">
                        @if($instance->coordinator_id>0)
                                <p><span style="font-size: small;">
                                 <img src="{{ (isset($instance->coordinator->image_url)?$instance->coordinator->image_url:'https://www.une.edu.au/__data/assets/image/0005/97178/blank-avatar.png')}}"
                                      alt="{{$instance->coordinator->name}}, Unit Coordinator"/></span></p>
                                <p><span style="font-size: 16px;"><a
                                                href="{{(isset($instance->coordinator->url)?$instance->coordinator->url:'')}}"
                                                target="_blank">
            {{(isset($instance->coordinator->title)?$instance->coordinator->title:'')}} {{$instance->coordinator->name}}</a></span>
<br/>Email: <a href="mailto:{{$instance->coordinator->username}}@une.edu.au">{{$instance->coordinator->username}}
                                        @une.edu.au</a></p>
                            @else
                                (Unknown coordinator)
                            @endif
                   </textarea>
                    </div>

                    <div><strong>Teacher page boilerplate</strong>
                        <br/>
                        <textarea style="width: 500px; height: 200px">
                        @foreach($instance->teachers as $teacher)
                                <p><span style="font-size: small;"><img
                                                src="{{ (isset($teacher->image_url)?$teacher->image_url:'https://www.une.edu.au/__data/assets/image/0005/97178/blank-avatar.png')}}"
                                                alt="{{$teacher->name}}, Teacher"/></span></p>
                                <p><span style="font-size: 16px;"><a href="{{(isset($teacher->url)?$teacher->url:'')}}"
                                                                     target="_blank">
            {{(isset($teacher->title)?$teacher->title:'')}} {{$teacher->name}}</a></span>
<br/>Email: <a href="mailto:{{$teacher->username}}@une.edu.au">{{$teacher->username}}@une.edu.au</a></p>
                                </p>
                            @endforeach
                   </textarea>
                    </div>
                </fieldset>
            </div>


            <div id="mediatab" class="tab-pane">
                <fieldset style="width: 90%">
                    <legend>
                        Associated Media
                        @if(!$readonly)
                            <button id="new_file_but" class="btn btn-info btn-lg" data-toggle="modal"
                                    data-target="#newmediadialog">Add
                            </button>
                        @endif
                    </legend>
                    <table class="table table-striped">
                        <thead class="thead-inverse">
                        <th>
                            Thumb
                        </th>
                        <th>
                            File name (click to download)
                        </th>
                        <th>
                            Description
                        </th>
                        <th>
                        </th>
                        <th>
                        </th>
                        </thead>
                        @foreach($instance->media as $media)
                            <tr>
                                <td>
                                    <img src="{{URL::asset('unitmedia/thumb/'.$media->id)}}">
                                </td>
                                <td>
                                    <a href="{{URL::asset('unitmedia/download/'.$media->id)}}">{{$media->name}}</a>
                                </td>
                                <td>
                                    {{$media->description}}
                                </td>
                                <td>
                                    <a href="#" data-toggle="modal" data-id="{{$media->id}}"
                                       data-target="#editmediadialog">
                                        Update
                                    </a>
                                </td>
                                <td>
                                    <a href="#" data-toggle="modal" data-id="{{$media->id}}"
                                       data-target="#deletedialog"
                                       data-route="unitmedia">
                                        <i class="fa fa-2x fa-times-circle" style='color: #ff6600'></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </fieldset>
            </div>

            <div id="notestab" class="tab-pane ">
                <fieldset style="width: 90%">
                    <legend>Messages

                        <button class="btn btn-info btn-lg" data-toggle="modal"
                                data-target="#newnoteitemdialog">New message
                        </button>

                    </legend>
                    <table class="table table-striped">
                        <thead class="thead-inverse">
                        <th>
                            Template
                        </th>
                        <th>
                            Sent at
                        </th>
                        <th>
                            Sent to
                        </th>
                        <th>
                            Sent by
                        </th>
                        <th/>
                        <th/>
                        </thead>

                        @foreach($instance->emailLog as $item)
                            <tr>
                                <td>
                                    {{$item->template['label']}}
                                </td>
                                <td>

                                    {{$item->created_at}}


                                </td>
                                <td>
                                    {{$item->sent_to['name']}}
                                </td>

                                <td>
                                    {{$item->sent_by['name']}}

                                </td>
                                <td>
                                    @if(!$readonly)
                                        <a href="#" data-toggle="modal"
                                           data-id="{{$item->id}}"
                                           data-route="unitemails"
                                           data-target="#viewmessagedialog">
                                            View contents
                                        </a>
                                    @endif
                                </td>

                                {{--<td>--}}

                                {{--<a href="#" data-toggle="modal" data-id="{{$item->id}}"--}}
                                {{--data-route="reviewnotesitem"--}}
                                {{--data-target="#deletedialog">--}}
                                {{--<i class="fa fa-2x fa-times-circle" style='color: red'></i>--}}
                                {{--</a>--}}

                                {{--</td>--}}
                            </tr>
                        @endforeach
                    </table>
                </fieldset>
            </div>


        </div>

        {{--
        Dialogs for various things
        --}}

        <div id="editdetailsdialog" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Offering</h4>
                    </div>
                    <div class="modal-body">

                        {!! Form::model($instance, ['class'=>'form-horizontal'])!!}

                        @include('unit_setup.form.uniteditform', ['submitButtonText'=>'Update Unit Instance'])

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>


        {{--  Adding a checklist item --}}
        <div id="newchecklistitemdialog" class="modal fade newdialog" role="dialog"
             data-route="unitsetupinstancechecklistitem">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add checklist item</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('unit_setup.form.checklistitemform', ['submitButtonText'=>'Add Checklist Item'])
                        {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>


        {{--Adding a note --}}
        <div id="newnoteitemdialog" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Send an email</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}

                        @include('unit_setup.form.sendemailform', ['submitButtonText'=>'Send email'])
                        {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>


        {{--Add a file to this record--}}
        <div id="newmediadialog" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add File</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('unit_setup.form.fileupload', ['submitButtonText'=>'Add File',  'unitid'=>$instance->id, 'required'=>'required', 'fileuploadcontrolid'=>'upload_file', 'commentscontrolid'=>'description'])
                        {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>

        {{--Update a file--}}
        <div id="editmediadialog" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update File</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('unit_setup.form.fileupload', ['submitButtonText'=>'Update File', 'unitid'=>$instance->id, 'required'=>'', 'fileuploadcontrolid'=>'altupload_file', 'commentscontrolid'=>'description'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>


        {{--  Deleting a thing --}}
        <div id="deletedialog" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 300px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Really delete?</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('form_common.deletedialog', [ 'id'=>$instance->id])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div id="archivedialog" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width: 300px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Really archive?</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('form_common.confirmdialog', [ 'id'=>$instance->id])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        {{--  Editing a list itemt  --}}
        <div id="editchecklistitemdialog" class="modal fade editdialog" role="dialog"
             data-route="unitsetupinstancechecklistitem">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit checklist item</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('unit_setup.form.checklistitemupdateform', ['submitButtonText'=>'Submit'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        {{--Follw up dialog--}}
        <div id="followupdialog" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Follow up</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open()!!}
                        @include('unit_setup.form.followupform', ['submitButtonText'=>'Update'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        {{--Show a sent message--}}
        <div id="viewmessagedialog" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Show message</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group row">
                            <div id="viewmessagecontent" class="control-label text-left col-sm-12"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="itememailpreview" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Preview</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="control-label text-left col-sm-12" style="font-weight: bold">Subject:</div>
                            <div id="itememailpreviewsubject" class=" text-left col-sm-12"></div>

                        </div>
                        <div class="form-group row">
                            <div class="control-label text-left col-sm-12" style="font-weight: bold">Content:</div>
                            <div id="itememailpreviewcontent" class=" text-left col-sm-12"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        @if(count($errors) > 0)
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
    @endif



@stop
