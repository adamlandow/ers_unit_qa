<div class="form-group row">
    <div class="col-sm-12" style="font-style: italic">Download CSV from APEX: <a href="https://my.une.edu.au/apex/f?p=107:5196::::5196::">https://my.une.edu.au/apex/f?p=107:5196::::5196::</a></div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
    {!! Form::label('upload_file', 'Select CSV File') !!}

    <input type="file" id="upload_file" name="upload_file" required>

    <div id="currentfilename"></div>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('teaching_period_id', 'Teaching Period', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-10">
        <select class="form-control" name="teaching_period_id" id="csv_teaching_period_id" style="width: 300px">
            @foreach ($teachingperiods as $teachingperiod)
                <option value='{{$teachingperiod->id}}'>{{$teachingperiod->year}} {{$teachingperiod->teaching_period_lookup->teaching_period}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('template_id', 'Template', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">
        <select class="form-control" name="template_id" id="csv_template_id" style="width: 300px">
            @foreach ($templates as $template)
                <option value='{{$template->id}}'>{{$template->label}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('csv_eso_id', 'Assigned ESO', ['class'=>'control-label  text-left']) !!}</div>

    <div class="col-sm-12">
        {!! Form::select('csv_eso_id', [], null, ['style'=>"width: 300px", 'class'=>'form-control userselect', 'required']) !!}
        {{--{!! Form::select('csv_eso_id', $users->pluck('name', 'id'), null, ['style'=>"width: 300px", 'class'=>'form-control', 'required']) !!}--}}
    </div>
</div>
<div class="form-group">
    {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
    {!! Form::submit('Upload CSV', ['class'=>'btn btn-primary form-control']) !!}
</div>



