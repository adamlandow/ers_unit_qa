<div class="form-group">

    {!! Form::label($fileuploadcontrolid, 'Select File (image, documents only)') !!}

    <input type="file" id="{{$fileuploadcontrolid}}" name="{{$fileuploadcontrolid}}" {{$required}}>

    <div id="currentfilename"></div>
</div>
<div class="form-group">
    {!! Form::label($commentscontrolid, 'Description') !!}
    {!! Form::textarea($commentscontrolid, null, ['class'=>'form-control', 'id'=>$commentscontrolid, ]) !!}
    <div class="help-block with-errors"></div>
</div>
<div class="form-group">
    {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>



