
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('template_id', 'Use template', ['class'=>'control-label  text-left']) !!}</div>

    <div class="col-sm-12">
        {!! Form::select('template_id', $emailtemplates->pluck('label', 'id'), null, ['style'=>"width: 300px", 'class'=>'form-control', 'required']) !!}
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('testing', 'Testing (sends to you instead of coordinators)', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">
        <div class="checkbox checkbox-success">
            <input type="checkbox" value="1"
                   name="testing"><label></label>
        </div></div>

</div>

<div class="form-group">

    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>