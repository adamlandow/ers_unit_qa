{!! Form::hidden('is_template', 'true', ['id'=>'is_template']) !!}

    <div class="well"> <i class="fa fa-2x fa-info-circle" style='color: #0066ff'></i>Copying a template makes a new template, with all the checklist items copied across</div>


<div class="form-group row">
    <div class="col-sm-12"> {!! Form::label('label', 'Template Label', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">
        {!! Form::input('text', 'label', null, ['class'=>'form-control' , 'id'=>'copy_label', 'required']) !!}
    </div>
</div>
<div class="form-group">

    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>