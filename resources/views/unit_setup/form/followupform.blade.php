<div class="form-group row">
    {!! Form::label('follow_up', 'Flag for follow up', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <div class="checkbox checkbox-danger">
            <input type="checkbox" name="follow_up" value="true"
                   onclick="if($(this).prop('checked')==true){$('#follow_up_text').show()}else{$('#follow_up_text').hide()};"
                    {{($instance->follow_up == 'true')?'checked':''}}
            >
            <label></label>
        </div>
    </div>
</div>
<div class="form-group row" id='follow_up_text' @if($instance->follow_up != 'true')
style="display:none"
        @endif>
    {!! Form::label('follow_up_text', 'Reasons for follow up', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <textarea style="width: 100%" name="follow_up_text">{{$instance->follow_up_text}}</textarea>
    </div>
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>