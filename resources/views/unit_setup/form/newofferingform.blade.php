<div class="form-group row">
    <div class="col-sm-12">    {!! Form::label('type', 'Unit', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">
        <select class="form-control unitselect" name="unit_id" style="width: 100%" required>
            {{--@foreach ($units as $unit)--}}

                {{--<option value='{{$unit->id}}'>{{$unit['unit_code']}}</option>--}}

            {{--@endforeach--}}
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">    {!! Form::label('combined_units_ids', 'Combined unit(s)', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">
        <select class="form-control unitselect" name="combined_units_ids" style="width: 100%" multiple="multiple">
            {{--@foreach ($units as $unit)--}}

                {{--<option value='{{$unit->id}}'>{{$unit['unit_code']}}</option>--}}

            {{--@endforeach--}}
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('coordinator_id', 'Coordinator', ['class'=>'control-label  text-left']) !!}</div>

    <div class="col-sm-12">
        {!! Form::select('coordinator_id', [], null, ['style'=>"width: 300px", 'class'=>'form-control userselect', 'required']) !!}
        {{--{!! Form::select('coordinator_id', $users->pluck('name', 'id'), null, ['style'=>"width: 300px", 'class'=>'form-control', 'required']) !!}--}}
    </div>

</div>

<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('eso_id', 'Assigned ESO', ['class'=>'control-label  text-left']) !!}</div>

    <div class="col-sm-12">
        {!! Form::select('eso_id', [], null, ['style'=>"width: 300px", 'class'=>'form-control userselect', 'required']) !!}
        {{--{!! Form::select('eso_id', $users->pluck('name', 'id'), null, ['style'=>"width: 300px", 'class'=>'form-control', 'required']) !!}--}}
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('teaching_period_id', 'Teaching Period', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-10">
        <select class="form-control" name="teaching_period_id" style="width: 300px">
            @foreach ($teachingperiods as $teachingperiod)
                <option value='{{$teachingperiod->id}}'>{{$teachingperiod->year}} {{$teachingperiod->teaching_period_lookup->teaching_period}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-2">{!! Form::label('has_olx', 'OLX?', ['class'=>'control-label  text-left']) !!}</div>

    <div class="col-sm-10">
        <div class="checkbox checkbox-purple">
            {!! Form::checkbox('has_olx', 1, false, [  'class'=>'styled', ]) !!}
            <label>Has OLX</label>
        </div>
        {{--{!! Form::label('heading', '', ['class'=>'control-label  col-sm-2 text-left']) !!}--}}
    </div>
</div>
<div class="form-group row">
   <div class="col-sm-12">{!! Form::label('template_id', 'Template', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">
        <select class="form-control" name="template_id" style="width: 300px">
        @foreach ($templates as $template)
            <option value='{{$template->id}}'>{{$template->label}}</option>
        @endforeach
        </select>
    </div>
</div>

<div class="form-group">

    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>