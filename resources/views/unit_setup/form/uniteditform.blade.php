<div class="form-group row">
    {!! Form::label('units_id', 'Unit', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::select('unit_id', $units->pluck('unit_code', 'id'), null, ['style'=>'width: 300px', 'class'=>'form-control', 'required'] ) !!}
        {{--<select class="select2 form-control" name="unit_id" style="width: 300px">--}}
        {{--@foreach ($units as $unit)--}}
        {{--<option value='{{$unit->id}}'>{{$unit['unit_code']}}</option>--}}
        {{--@endforeach--}}

        {{--</select>--}}
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">    {!! Form::label('combined_units_id[]', 'Combined unit(s)', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-12">
        <select class="select2 form-control" name="combined_units_id[]" style="width: 100%" multiple="multiple">
            @foreach ($units as $unit)

                <option value='{{$unit->id}}' {{(in_array($unit->id,$instance->combined_units_ids)?"selected=true":'')}}>{{$unit['unit_code']}}</option>

            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    {!! Form::label('coordinator_id', 'Coordinator', ['class'=>'control-label  text-left col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::select('coordinator_id', $users->pluck('name', 'id'), null, ['style'=>"width: 300px", 'class'=>'form-control', 'required']) !!}
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-2">    {!! Form::label('teachers', 'Teaching staff', ['class'=>'control-label text-left col-sm-2']) !!}</div>
    <div class="col-sm-10">
            {!! Form::select('teachers', $users->pluck('name', 'id'), null, ['style'=>"width: 300px", 'id'=>"teachers", 'multiple'=>"multiple", 'class'=>'form-control', ]) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('eso_id', 'Assigned ESO', ['class'=>'control-label  text-left col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::select('eso_id', $users->pluck('name', 'id'), null, ['style'=>"width: 300px", 'class'=>'form-control', 'required']) !!}
    </div>
</div>

<div class="form-group row">
    {!! Form::label('teaching_period_id', 'Teaching Period', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <select class="form-control" name="teaching_period_id" style="width: 300px">
            @foreach ($teachingperiods as $teachingperiod)
                <option value='{{$teachingperiod->id}}' {{($teachingperiod->id==$instance->teaching_period_id)?'selected':''}}>{{$teachingperiod->year}} {{$teachingperiod->teaching_period_lookup->teaching_period}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-2">{!! Form::label('has_olx', 'OLX?', ['class'=>'control-label  text-left']) !!}</div>

    <div class="col-sm-10">
        <div class="checkbox checkbox-purple">
            {!! Form::checkbox('has_olx', '1', $instance->has_olx=='1', [  'class'=>'styled', ]) !!}
            <label>Has OLX</label>
        </div>
        {{--{!! Form::label('heading', '', ['class'=>'control-label  col-sm-2 text-left']) !!}--}}
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-2">{!! Form::label('is_accredited', 'Accredited unit?', ['class'=>'control-label  text-left']) !!}</div>

    <div class="col-sm-10">
        <div class="checkbox checkbox-danger">
            {!! Form::checkbox('is_accredited', '1', $instance->is_accredited=='1', [  'class'=>'styled', ]) !!}
            <label>Accredited unit</label>
        </div>
        {{--{!! Form::label('heading', '', ['class'=>'control-label  col-sm-2 text-left']) !!}--}}
    </div>
</div>
<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>