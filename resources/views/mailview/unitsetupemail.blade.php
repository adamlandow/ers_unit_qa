<?php

$unitStr = "";
//dd($offering->coordinator);
if (isset($offering->unit->unit_code)) {
    $unitStr = $offering->unit->unit_code;
    foreach ($offering->combined_units as $combined_unit) {
        $unitStr .= ('/' . $combined_unit->unit->unit_code);
    }
}
?>
@if(isset($offering))
    @if(isset($offering->follow_up )&&($offering->follow_up == 'true'))
        {!! str_replace(["{coordinator}","{unit}", "{teachingperiod}","{followup}","{moodleurl}"],
        [explode(' ',$offering->coordinator->name)[0],$unitStr, $offering->teaching_period->teaching_period_lookup->teaching_period.' '.$offering->teaching_period->year, $offering->unit->follow_up_text, $offering->moodle_url], $template->text) !!}
    @else
        {!! str_replace(["{coordinator}","{unit}", "{teachingperiod}", "{moodleurl}"],
        [explode(' ',$offering->coordinator->name)[0],$unitStr, $offering->teaching_period->teaching_period_lookup->teaching_period.' '.$offering->teaching_period->year, $offering->moodle_url], $template->text) !!}
    @endif
@endif
