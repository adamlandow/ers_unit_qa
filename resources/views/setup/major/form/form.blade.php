<div class="form-group row">
    {!! Form::label('course_code', 'Course Code', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {{ Form::input('text', 'course_code', null, ['class'=>'form-control', 'required']) }}

    </div>
</div>
<div class="form-group row">
    {!! Form::label('label', 'Title', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {{ Form::input('text', 'label', null, ['class'=>'form-control', 'required']) }}

    </div>
</div>
<div class="form-group row">
    {!! Form::label('url', 'Web age', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {{ Form::input('text', 'url', null, ['class'=>'form-control', 'required']) }}
    </div>
</div>


<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>