@extends('layouts.app')

@section('content')
        <!-- include form specific libraries -->
@include('layouts.formincludes')
{{--@section('title')--}}
{{--{{$user->name}}--}}

{{--@endsection--}}

<script>

    var currentmediaid = 0;

    $(document).ready(function () {



        // Bootstrap AJAX
        //http://webdesign.tutsplus.com/tutorials/building-a-bootstrap-contact-form-using-php-and-ajax--cms-23068


        $('#editdetailsdialog').on('shown.bs.modal', function () {
            $('#editdetailsdialog').find("form").validator();
//            changevalidation('editdetailsdialog');
        });

        // Update user dialogue
        $('#editdetailsdialog').submit(function (event) {
            $('#editdetailsdialog').modal('hide');
            var vars = $("#editdetailsdialog").find("form").serializeArray();
            // little hack to get the multiple values in from the subspecialties input dialog box

            vars.push({name: 'id', value: '{{$course->id}}'});
            vars.push({name: '_token', value: '{{ csrf_token() }}'});
            // cancels the form submission so that we can submit it using AJAX
            event.preventDefault();
            waitingDialog.show();
            submitUpdateForm(vars);
        });


    });


    function submitUpdateForm(vars) {
        $.ajax({
            url: '{{URL::to('/major/ajaxupdate')}}',
            type: 'post',
            data: vars,
            error: function (jqXHR, textStatus, errorThrown) {
                waitingDialog.hide();
                alert(errorThrown);
            },
            success: function (data) {
                if (data.status > 0) {
                    location.reload(true);
                } else {
                    waitingDialog.hide();
                    alert(data.statusText);
                }
            }
        });
    }


</script>


{!! Breadcrumbs::render('course.show', $course) !!}


<div id="detailstab" class="tab-pane active">
    <fieldset style="width: 90%">
        <legend>Course details

            <button class="btn btn-info btn-lg" data-toggle="modal"
                    data-target="#editdetailsdialog">Update
            </button>

        </legend>


        <div class="col-md-12">
            <div class="col-md-4">

                <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                    <div class="col-md-4">
                        <strong>Course Title:</strong>
                    </div>
                    <div class="col-md-8">
                        {{$course->label}}
                    </div>
                </div>
                <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                    <div class="col-md-4">
                        <strong>Course code:</strong>
                    </div>
                    <div class="col-md-8">
                        {{$course->course_code}}
                    </div>
                </div>
                <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                    <div class="col-md-4">
                        <strong>Convenor:</strong>
                    </div>
                    <div class="col-md-8">
                        {{$course->convenor->name}}
                    </div>
                </div>
                <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                    <div class="col-md-4">
                        <strong>Web page:</strong>
                    </div>
                    <div class="col-md-8">
                        {{$course->url}}
                    </div>
                </div>
                <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                    <div class="col-md-4">
                        <strong>Description</strong>
                    </div>
                    <div class="col-md-8">
                        {{$course->description}}
                    </div>
                </div>


            </div>
            <div class="col-md-4">
                <strong>Majors</strong>
                <div class="col-md-12">
                    <div class="col-md-12">
                        <table class="table table-bordered table-condensed table-striped">
                            <tr>
                                <th colspan="4">Core</th>
                            </tr>
                            <tr>
                                <th>Label</th>
                                <th>Description</th>
                                <th>URL</th>
                                <th>Delete</th>
                            </tr>
                            @foreach($course->majors as $major)
                                <tr>
                                    <td>{{$major->label}} </td>
                                    <td>{{$major->description}}</td>
                                    <td>{{$major->url}}</td>
                                    <td>X</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <strong>Units in this course</strong>(filter by major)
                <div class="col-md-12">
                    <div class="col-md-12">
                        <table class="table table-bordered table-condensed table-striped">
                            <tr>
                                <th colspan="4">Core</th>
                            </tr>
                            <tr>
                                <th>Code</th>
                                <th>Label</th>
                                <th>Major</th>
                                <th>Delete</th>
                            </tr>
                            @foreach($course->coreunits as $unit)
                                <tr>
                                    <td>{{$unit->unit_code}} </td>
                                    <td>{{$unit->description}}</td>
                                    <td> @if(isset($unit->pivot->major_id))
                                            {{\App\Fieldofstudy::find($unit->pivot->major_id)->first()->label}}
                                        @endif</td>
                                    <td>X</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th colspan="4">Prescribed</th>
                            </tr>
                            @foreach($course->prescribedunits as $unit)
                                <tr>
                                    <td>{{$unit->unit_code}} </td>
                                    <td>{{$unit->description}}</td>
                                    <td> @if(isset($unit->pivot->major_id))
                                            {{\App\Fieldofstudy::find($unit->pivot->major_id)->first()->label}}
                                        @endif</td>
                                    <td>X</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th colspan="4">Listed</th>
                            </tr>
                            @foreach($course->listedunits as $unit)
                                <tr>
                                    <td>{{$unit->unit_code}} </td>
                                    <td>{{$unit->description}}</td>
                                    <td> @if(isset($unit->pivot->major_id))
                                            {{\App\Fieldofstudy::find($unit->pivot->major_id)->first()->label}}
                                        @endif</td>
                                    <td>X</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </fieldset>
</div>


{{--  Dialog for editing the users details  --}}
<div id="editdetailsdialog" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Details</h4>
            </div>
            <div class="modal-body">
                {!! Form::model($course, ['class'=>'form-horizontal', 'role'=>'form', 'id'=>'updateform', 'data-toggle'=>'validator'])!!}

                @include('setup.course.form.form', ['submitButtonText'=>'Update',  'formid'=>'updateform'])

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


@stop
 