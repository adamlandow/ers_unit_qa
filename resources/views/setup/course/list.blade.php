@extends('layouts.app')
@section('title')Course setup
@endsection

@section('content')
    <!-- include form specific libraries -->
    @include('layouts.formincludes')
    <script>

        var currentid = -1;

        $(document).ready(function () {

            $('select').select2({
                placeholder: 'Select an option'
            });


            $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
                if ((typeof $(this).data('id')) !== 'undefined') {
                    currentid = $(this).data('id');
                    if ($(this).data('target') == "#editdialog") {
                        getCourseDetails(currentid);
                    }
                }
            });


            $('#newdialog').on('shown.bs.modal', function () {

            });


            // New patient event dialogue
            $('#newdialog').submit(function (event) {
                if (!event.isDefaultPrevented()) {
                    $('#newdialog').modal('hide');

                    var vars = $("#newform").serializeArray();
                    // vars.push({name: 'roles', value: $("#newuserform :input[name='role_id']").val()});
                    console.log(vars);
                    // cancels the form submission
                    event.preventDefault();
                    waitingDialog.show();
                    submitNewForm(vars);
                }
            });


            $('#deletedialog').submit(function (event) {
                $('#deletedialog').modal('hide');
                var vars = $("#deletedialog").find("form").serializeArray();
                vars.push({name: 'id', value: currentid});
                //vars.push({name: '_method', value: 'DELETE'});
                // cancels the form submission
                event.preventDefault();
                waitingDialog.show();
                deleteCourse(vars);
            });

        });


        /*
         *
         * New user AJAX
         *
         */
        function submitNewForm(vars) {
            $.ajax({
                url: '{{URL::to('course/ajaxstore')}}',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    console.log(data.status)
                    if (data.status.toString() == '0') {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert(data.statusText);
                    }
                }
            });
        }


        function deleteCourse(vars) {
            $.ajax({
                url: '{{URL::to('course/ajaxdestroy')}}',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data == "1") {
                        location.reload(true);
                    } else {
                        alert('something went wrong with the delete');
                    }
                }
            });
        }


        /*
         *
         * Helper functions
         *
         */

        // populates a form with data returned from Laravel
        function populate(frm, data) {

            $.each(data, function (key, value) {
                var $ctrl = $('[name="' + key + '"]', frm);
                if ($ctrl.is("select")) {
                    if ($ctrl.attr('multiple')) {
                        console.log(key, value)
                        //  $ctrl.select2('val', value.split(',')).trigger('change');
                    } else {
                        //$ctrl.select2('val', value);
                    }
                } else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":


                            $ctrl.val(value);
                            // special case if a colorpicker
                            if ($ctrl.parent().hasClass('colorpicker-component')) {
                                $ctrl.parent().colorpicker('setValue', value);
                            }
                            break;
                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).attr("checked", value);
                                }
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }
            });
        }

    </script>
    {!! Breadcrumbs::render('course.index') !!}
    <fieldset style="padding-left: 15px; padding-right: 15px; margin-top: 0">
        {{--<div style="width: 100%; ">--}}
        {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newdialog">New Course--}}
        {{--<i class="fa fa-plus"></i></button>--}}
        {{--</div>--}}


        <div style="float: left; padding-left: 10px"> {!! Form::open( ['class'=>'form-inline', 'role'=>'form', 'url'=>Request::path(), 'method' => 'get'])!!}
            <input type="text" class="form-control" name="search"
                   value="{{isset(Request::all()['search'])?Request::all()['search']:''}}">
            <button type="submit" class="btn btn-primary" style="vertical-align: bottom">Search <i
                        class="fa fa-search" aria-hidden="true"></i></button>{!! Form::close()!!}</div>
    </fieldset>

    &nbsp;
    <table class="table table-striped">
        <thead class="thead-inverse">
        <tr>
            <th class="headerSortable header">Code</th>
            <th class="headerSortable header">Title</th>
            <th class="headerSortable header">School</th>
            <th class="headerSortable header">Faculty</th>
            <th></th>
        </tr>
        </thead>

        @foreach ($courses as $course)
            <tr>
                <td>
                    <a href="{{action('CourseController@show', $course->id)}}">
                        {{$course->course_code}}
                    </a>
                </td>
                <td>
                    {{$course->label}}
                </td>
                <td>
                    @if(isset($course->school))
                        {{$course->school->description}}
                    @endif
                </td>
                <td>
                    @if(isset($course->school->faculty))
                        {{$course->school->faculty->description}}
                    @endif
                </td>
                <td>
                    <a href="#" data-toggle="modal" data-id="{{$course->id}}"
                       data-target="#deletedialog">
                        Delete
                    </a>
                </td>

        @endforeach

    </table>
    {{ $courses->appends(\Illuminate\Support\Facades\Input::except('page'))->links()}}
    </fieldset>

    <div id="newdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Course</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open( ['class'=>'form-horizontal', 'role'=>'form', 'id'=>'newform', 'data-toggle'=>'validator'])!!}

                    @include('setup.course.form.form', ['submitButtonText'=>'Add Course',   'formid'=>'newform'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    {{--  Deleting a contact event  --}}
    <div id="deletedialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete course?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('form_common.deletedialog')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@stop
