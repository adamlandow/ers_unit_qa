<div class="form-group row">
    {!! Form::label('label', 'Label', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {{ Form::input('text', 'label', null, ['class'=>'form-control', 'required']) }}

    </div>
</div>
<div class="form-group row">
    {!! Form::label('url', 'Web page', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {{ Form::input('text', 'url', null, ['class'=>'form-control', 'required']) }}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('trim_reference', 'TRIM container', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {{ Form::input('text', 'trim_container', null, ['class'=>'form-control', 'required']) }}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('trim_reference', 'TRIM reference', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {{ Form::input('text', 'trim_reference', null, ['class'=>'form-control']) }}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('description', 'Description', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {{ Form::textarea('description') }}
    </div>
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>