<div class="form-group row">

    {!! Form::label('units_id', 'Unit', ['class'=>'control-label  col-sm-2  text-left']) !!}
    <div class="col-sm-10">
        <select class="select2 form-control" name="units_id" id="addunitform_units_id" style="width: 300px">

        </select>

    </div>
</div>
<div class="form-group row">
    {!! Form::label('major_id', 'Fieldofstudy', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        <select class="form-control" name="major_id" style="width: 300px">
            <option value='-1'>No major</option>
            @foreach ($fields_of_study as $major)
                <option value='{{$major->id}}'>{{$major->label}}</option>
            @endforeach
        </select>
        {{--{!! Form::select('major_id', $majors->pluck('label', 'id')->prepend('No major'), null, ['style'=>"width: 300px", 'class'=>'form-control']) !!}--}}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('relationship', 'Relationship', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::select('relationship', ['core'=>'Core', 'prescribed'=>'Prescribed', 'listed'=>'Listed'], null, ['style'=>"width: 300px", 'class'=>'form-control', 'required']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>