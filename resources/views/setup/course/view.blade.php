@extends('layouts.app')
@section('title')Setup: {{$course->course_code}}@endsection
@section('content')

    <!-- include form specific libraries -->
    {{--@include('layouts.formincludes')--}}
    <script>
        $.fn.modal.Constructor.prototype.enforceFocus = function () {
        };
    </script>
    {{--@section('title')--}}
    {{--{{$user->name}}--}}

    {{--@endsection--}}

    <script>

        var currenteditingid = 0;
        var currentdeletingid = 0;
        var currentdeleteroute = "";

        $(document).ready(function () {

            $('select').select2()

            $("#addunitform_units_id").select2({
                ajax: {
                    url: "{{URL::to('/unit/lookupunit/q')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term

                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data,

                        };
                    },

                    cache: true
                },
                minimumInputLength: 1,
            });
            // Bootstrap AJAX
            //http://webdesign.tutsplus.com/tutorials/building-a-bootstrap-contact-form-using-php-and-ajax--cms-23068


            $('#editdetailsdialog').on('shown.bs.modal', function () {
                $('#editdetailsdialog').find("form").validator();
//            changevalidation('editdetailsdialog');
            });

            // Update course dialogue
            $('#editdetailsdialog').submit(function (event) {
                if (!event.isDefaultPrevented()) {
                    $('#editdetailsdialog').modal('hide');
                    var vars = $("#editdetailsdialog").find("form").serializeArray();
                    // little hack to get the multiple values in from the subspecialties input dialog box
                    vars.push({name: 'id', value: '{{$course->id}}'});
                    vars.push({name: '_token', value: '{{ csrf_token() }}'});
                    // cancels the form submission so that we can submit it using AJAX
                    event.preventDefault();
                    waitingDialog.show();
                    submitUpdateForm(vars);
                }
            });

            // New unit dialogue
            $('#newunitdialog').submit(function (event) {
                if (!event.isDefaultPrevented()) {
                    $('#newunitdialog').modal('hide');
                    var vars = $("#newunitform").serializeArray();
                    vars.push({name: 'id', value: '{{$course->id}}'});
                    // cancels the form submission
                    event.preventDefault();
                    waitingDialog.show();
                    submitNewUnitForm(vars);
                }
            });


            // New major dialogue
            $('#addmajordialog').submit(function (event) {
                if (!event.isDefaultPrevented()) {
                    console.log('submitting new major')
                    $('#addmajordialog').modal('hide');
                    var vars = $("#addmajorform").serializeArray();
                    vars.push({name: 'courses_id', value: '{{$course->id}}'});
                    // cancels the form submission
                    event.preventDefault();
                    waitingDialog.show();
                    submitNewMajorForm(vars);
                }
            });

            // New major dialogue
            $('#editmajordialog').submit(function (event) {
                if (!event.isDefaultPrevented()) {
                    $('#editmajordialog').modal('hide');
                    var vars = $("#editmajorform").serializeArray();
                    vars.push({name: 'id', value: currenteditingid});
                    // cancels the form submission
                    event.preventDefault();
                    waitingDialog.show();
                    submitEditMajorForm(vars);
                }
            });

            $('#deletedialog').submit(function (event) {
                $(this).modal('hide');
                event.preventDefault();
                waitingDialog.show();
                deleteItem(currentdeletingid, currentdeleteroute);
            });


            // New unit to course dialogue
            $('#addunitdialog').submit(function (event) {
                if (!event.isDefaultPrevented()) {
                    $('#addunitdialog').modal('hide');
                    var vars = $("#addcourseform").serializeArray();
                    vars.push({name: 'id', value: '{{$course->id}}'});
                    vars.push({name: 'unit', value: $("#addunitform_units_id option:selected").text()});
                    console.log(vars);
                    // cancels the form submission
                    event.preventDefault();
                    waitingDialog.show();
                    submitNewUnitForm(vars);
                }
            });

            // New trim reference to course dialogue
            $('#addcoursetrimdialog').submit(function (event) {
                if (!event.isDefaultPrevented()) {
                    $('#addcoursetrimdialog').modal('hide');
                    var vars = $("#addcoursetrimform").serializeArray();
                    vars.push({name: 'id', value: '{{$course->id}}'});
                    // cancels the form submission
                    event.preventDefault();
                    waitingDialog.show();
                    submitNewCourseTrimForm(vars);
                }
            });

            $('#addfostrimdialog').submit(function (event) {
                if (!event.isDefaultPrevented()) {
                    $('#addfostrimdialog').modal('hide');
                    var vars = $("#addfostrimform").serializeArray();
                    vars.push({name: 'id', value: currenteditingid});
                    // cancels the form submission
                    event.preventDefault();
                    waitingDialog.show();
                    submitNewFosTrimForm(vars);
                }
            });


            $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
                currenteditingid = $(this).data('id');
                switch ($(this).data('target')) {

                    case '#editmajordialog':
                        getDetails(currenteditingid, 'major', 'editmajordialog');
                        break;
                    case '#deletedialog':
                        currentdeleteroute = $(this).data('route');
                        currentdeletingid = $(this).data('id');
                        break;
                    case '#addfostrimdialog':
                        break;
                    default:
                        currenteditingid = -1;
                        currentdeletingid = -1;
                        break;
                }

            });
        });

        /**
         * Update the current course details
         * @param vars
         */
        function submitUpdateForm(vars) {
            $.ajax({
                url: '{{URL::to('/course/ajaxupdate')}}',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    if (data.status > 0) {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert(data.statusText);
                    }
                }
            });
        }

        function submitNewUnitForm(vars) {
            console.log(vars);
            $.ajax({
                url: '{{URL::to('/course/addunit')}}',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    console.log(data.status)
                    if (data.status.toString() == '0') {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert(data.statusText);
                    }
                }
            });
        }

        function submitNewMajorForm(vars) {
            $.ajax({
                url: '{{URL::to('/major/ajaxstore')}}',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    console.log(data.status)
                    if (data.status.toString() == '0') {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert(data.statusText);
                    }
                }
            });
        }

        function submitEditMajorForm(vars) {
            $.ajax({
                url: '{{URL::to('/major/ajaxupdate')}}',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    console.log(data.status)
                    if (data.status.toString() == '0') {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert(data.statusText);
                    }
                }
            });
        }

        function submitNewCourseTrimForm(vars) {
            $.ajax({
                url: '{{URL::to('/course/addtrim')}}',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    console.log(data.status)
                    if (data.status.toString() == '0') {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert(data.statusText);
                    }
                }
            });
        }

        function submitNewFosTrimForm(vars) {
            console.log('Adding fos trim');
            $.ajax({
                url: '{{URL::to('/major/addtrim')}}',
                type: 'post',
                data: vars,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    console.log(data.status)
                    if (data.status.toString() == '0') {
                        location.reload(true);
                    } else {
                        waitingDialog.hide();
                        alert(data.statusText);
                    }
                }
            });
        }

        function getDetails(id, route, formid) {
            $.ajax({
                url: '{!! URL::to('')!!}/' + route + '/' + id,
                type: 'GET',
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (data) {
                    populate($("#" + formid).find("form"), data);
                    waitingDialog.hide();
                }
            });
        }

        function deleteItem(id, route) {
            $.ajax({
                url: ((route == 'courseunit') ? '{!! URL::to('')!!}/course/detachunit' : '{!! URL::to('')!!}/' + route + '/destroy'),
                type: 'post',
                data: ([{name: 'id', value: ((route == 'courseunit') ? '{{$course->id}}' : id)},
                    ((route == 'courseunit') ? {name: 'pivot_id', value: currentdeletingid} : {
                        name: '_method',
                        value: 'DELETE'
                    }),
                    {
                        name: '_token',
                        value: '{{csrf_token()}}'
                    }]),
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (data.status == "0") {
                        //      alert(data.status);
                        location.reload();
                    } else {
                        alert(data.statusText);
                    }
                }
            });
        }

        // populates a form with data returned from Laravel
        function populate(frm, data) {
            $.each(data, function (key, value) {
                var $ctrl = $('[name="' + key + '"]', frm);
                if ($ctrl.is("select")) {
                    if ($ctrl.attr('multiple')) {
                        $ctrl.select2('val', value.split(',')).trigger('change');
                    } else {
                        $ctrl.select2('val', value);
                    }
                } else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":


                            $ctrl.val(value);
                            // special case if a colorpicker
                            if ($ctrl.parent().hasClass('colorpicker-component')) {
                                $ctrl.parent().colorpicker('setValue', value);
                            }
                            break;
                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).attr("checked", value);
                                }
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }
            });
        }

    </script>


    {!! Breadcrumbs::render('course.show', $course) !!}


    <div id="detailstab" class="tab-pane active">
        <fieldset style="width: 100%">


            <div class="col-md-12">
                <div class="col-lg-4 ">
                    <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                        <legend>Course details

                            <button class="btn btn-info btn-lg" data-toggle="modal"
                                    data-target="#editdetailsdialog">Update
                            </button>
                        </legend>


                    </div>
                    <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                        <div class="col-md-4">
                            <strong>Course Title:</strong>
                        </div>
                        <div class="col-md-8">
                            {{$course->label}}
                        </div>
                    </div>
                    <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                        <div class="col-md-4">
                            <strong>Course code:</strong>
                        </div>
                        <div class="col-md-8">
                            {{$course->course_code}}
                        </div>
                    </div>
                    <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                        <div class="col-md-4">
                            <strong>Convenor:</strong>
                        </div>
                        <div class="col-md-8">
                            @if(isset($course->convenor->name))
                                {{$course->convenor->name}}
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                        <div class="col-md-4">
                            <strong>Web page:</strong>
                        </div>
                        <div class="col-md-8">
                            {{$course->url}}
                        </div>
                    </div>

                    <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                        <div class="col-md-4">
                            <strong>Description</strong>
                        </div>
                        <div class="col-md-8">
                            {{$course->description}}
                        </div>
                    </div>
                    <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9; padding-top: 20px">
                        <legend>TRIM container

                            <button class="btn btn-info btn-sm" data-toggle="modal"
                                    data-target="#addcoursetrimdialog">+
                            </button>
                        </legend>
                        @foreach($course->trim as $coursetrim)
                            <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">

                                <div class="col-md-10">
                                    {{$coursetrim->reference}} (Version {{$coursetrim->version}})

                                </div>
                                <div class="col-md-2">
                                    <a href="#" data-target="#deletedialog" data-toggle="modal"
                                       data-id="{{$coursetrim->id}}" data-route="coursetrim">
                                        <i class="fa fa-times" style="color: red" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <div class="col-lg-4 ">
                    <strong>Fields of Study</strong>
                    <button class="btn btn-sm btn-info" data-target="#addmajordialog" data-toggle="modal">Add field of
                        study to
                        this
                        course
                    </button>
                    <br/>


                    <table class="table table-bordered table-condensed table-striped">
                        <tr>
                            <th>Label</th>

                            <th>URL</th>
                            <th>TRIM reference(s)</th>
                            <th>Delete</th>
                        </tr>
                        @foreach($course->fields_of_study as $field_of_study)
                            <tr>
                                <td><a href="#" data-target="#editmajordialog" data-toggle="modal"
                                       data-id="{{$field_of_study->id}}" data-route="field_of_study">
                                        {{$field_of_study->label}}
                                    </a></td>

                                <td>{{$field_of_study->url}}</td>
                                <td>
                                    <div class="col-sm-12 text-right">
                                    <a href="#" data-target="#addfostrimdialog" data-toggle="modal"
                                       data-id="{{$field_of_study->id}}"><i class="fa fa-plus" style="color: #4cae4c" aria-hidden="true">
                                        </i>
                                    </a><br/>
                                    </div>
                                    @foreach($field_of_study->trim as $fostrim)

                                        {{$fostrim->reference}} (version {{$fostrim->version}}) <a href="#" data-target="#deletedialog" data-toggle="modal"
                                                                                                   data-id="{{$fostrim->id}}" data-route="majortrim">
                                            <i class="fa fa-times" style="color: red" aria-hidden="true"></i>
                                        </a>
                                    @endforeach
                                </td>
                                <td>
                                    @if($field_of_study->units->count()<1)
                                        <a href="#" data-target="#deletedialog" data-toggle="modal"
                                           data-id="{{$field_of_study->id}}" data-route="major">
                                            <i class="fa fa-times" style="color: red" aria-hidden="true"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>


                </div>

                <div class="col-lg-4 ">
                    <div class="col-md-12">
                        <strong>Units in this course</strong>
                        <button class="btn btn-sm btn-info" data-target="#addunitdialog" data-toggle="modal">Add unit to
                            this
                            course
                        </button>
                        <form action="{{URL::to("course/{$course->id}")}}" class="col-sm-10" style="padding-left: 0px">
                            Filter by field of study
                            <select class="form-control" name="major" id="major_selector"
                                    onchange=" $(this).closest('form').submit();" style="width: 300px">
                                <option value='-1'>Show all</option>
                                @foreach ($course->fields_of_study as $field_of_study)
                                    <option value='{{$field_of_study->id}}'
                                    @if(session()->has('major'))
                                        {{($field_of_study->id==session('major'))? 'selected':''}}
                                            @endif
                                    >{{$field_of_study->label}}</option>
                                @endforeach
                            </select>
                        </form>


                        <div class="col-md-12">
                            <div class="col-md-12">
                                <table class="table table-bordered table-condensed table-striped">
                                    <tr>
                                        <th colspan="4">Core</th>
                                    </tr>
                                    <tr>
                                        <th>Code</th>
                                        <th>Title</th>
                                        <th>Field of study</th>
                                        <th>Delete</th>
                                    </tr>
                                    {{--{{dd($course->coreunits())}}--}}
                                    @foreach($course->coreunits as $unit)
                                        @if(isset($unit->pivot->field_of_study_id))
                                            @if(session()->has('major') && session('major') > 0)
                                                @if($unit->pivot->major_id==session('major'))
                                                    <tr>
                                                        <td>
                                                            <a href="{{URL::to("unit/{$unit->id}")}}">{{$unit->unit_code}}</a>
                                                        </td>
                                                        <td>{{$unit->description}}</td>
                                                        <td>
                                                        {{\App\Field_of_study::find($unit->pivot->field_of_study_id)->label}}
                                                        <td>
                                                            <a href="#" data-target="#deletedialog" data-toggle="modal"
                                                               data-id="{{$unit->id}}" data-route="courseunit">
                                                                <i class="fa fa-times" style="color: red"
                                                                   aria-hidden="true"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endif
                                        @else
                                            <tr>
                                                <td><a href="{{URL::to("unit/{$unit->id}")}}">{{$unit->unit_code}}</a>
                                                </td>
                                                <td>{{$unit->title}}</td>
                                                <td> @if(isset($unit->pivot->field_of_study_id))
                                                        {{\App\Field_of_study::find($unit->pivot->field_of_study_id)->label}}
                                                    @endif</td>
                                                <td><a href="#" data-target="#deletedialog" data-toggle="modal"
                                                       data-id="{{$unit->id}}" data-route="courseunit">
                                                        <i class="fa fa-times" style="color: red"
                                                           aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <tr>
                                        <th colspan="4">Prescribed</th>
                                    </tr>
                                    @foreach($course->prescribedunits as $unit)
                                        {{--@if(strlen($unit->pivot->major_id)>0)--}}
                                        @if(session()->has('major') && session('major') > 0)
                                            @if($unit->pivot->field_of_study_id==session('major'))
                                                <tr>
                                                    <td>
                                                        <a href="{{URL::to("unit/{$unit->id}")}}">{{$unit->unit_code}}</a>
                                                    </td>
                                                    <td>{{$unit->description}}</td>
                                                    <td>
                                                        {{\App\Field_of_study::find($unit->pivot->field_of_study_id)->label}}
                                                    </td>
                                                    <td><a href="#" data-target="#deletedialog" data-toggle="modal"
                                                           data-id="{{$unit->id}}" data-route="courseunit">
                                                            <i class="fa fa-times" style="color: red"
                                                               aria-hidden="true"></i>
                                                        </a></td>
                                                </tr>
                                            @endif
                                            {{--@endif--}}
                                        @else
                                            <tr>
                                                <td><a href="{{URL::to("unit/{$unit->id}")}}">{{$unit->unit_code}}</a>
                                                </td>
                                                <td>{{$unit->description}}</td>
                                                <td> @if(isset($unit->pivot->field_of_study_id))
                                                        {{\App\Field_of_study::find($unit->pivot->field_of_study_id)->label}}
                                                    @endif</td>
                                                <td><a href="#" data-target="#deletedialog" data-toggle="modal"
                                                       data-id="{{$unit->id}}" data-route="courseunit">
                                                        <i class="fa fa-times" style="color: red"
                                                           aria-hidden="true"></i>
                                                    </a></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <tr>
                                        <th colspan="4">Listed</th>
                                    </tr>
                                    @foreach($course->listedunits as $unit)
                                        {{--@if(isset($unit->pivot->major_id))--}}
                                        @if(session()->has('major') && session('major') > 0)
                                            @if($unit->pivot->major_id==session('major'))
                                                <tr>
                                                    <td>
                                                        <a href="{{URL::to("unit/{$unit->id}")}}">{{$unit->unit_code}}</a>
                                                    </td>
                                                    <td>{{$unit->description}}</td>
                                                    <td> @if(isset($unit->pivot->field_of_study_id))
                                                            {{\App\Field_of_study::find($unit->pivot->field_of_study_id)->label}}
                                                        @endif</td>
                                                    <td><a href="#" data-target="#deletedialog" data-toggle="modal"
                                                           data-id="{{$unit->id}}" data-route="courseunit">
                                                            <i class="fa fa-times" style="color: red"
                                                               aria-hidden="true"></i>
                                                        </a></td>
                                                </tr>
                                            @endif
                                            {{--@endif--}}
                                        @else
                                            <tr>
                                                <td><a href="{{URL::to("unit/{$unit->id}")}}">{{$unit->unit_code}}</a>
                                                </td>
                                                <td>{{$unit->description}}</td>
                                                <td> @if(isset($unit->pivot->field_of_study_id))
                                                        {{\App\Field_of_study::find($unit->pivot->field_of_study_id)->label}}
                                                    @endif</td>
                                                <td><a href="#" data-target="#deletedialog" data-toggle="modal"
                                                       data-id="{{$unit->id}}" data-route="courseunit">
                                                        <i class="fa fa-times" style="color: red"
                                                           aria-hidden="true"></i>
                                                    </a></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </fieldset>
    </div>


    {{--  Dialog for editing the course details  --}}
    <div id="editdetailsdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Details</h4>
                </div>
                <div class="modal-body">
                    {!! Form::model($course, ['class'=>'form-horizontal', 'role'=>'form', 'id'=>'updateform', 'data-toggle'=>'validator'])!!}

                    @include('setup.course.form.form', ['submitButtonText'=>'Update',  'formid'=>'updateform'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Dialog for adding a major--}}
    <div id="addmajordialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add field of study</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open( ['class'=>'form-horizontal', 'role'=>'form', 'id'=>'addmajorform', 'data-toggle'=>'validator'])!!}

                    @include('setup.course.form.majorform', ['submitButtonText'=>'Add',  'formid'=>'addmajorform'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Dialog for editing a major--}}
    <div id="editmajordialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update field of study</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['class'=>'form-horizontal', 'role'=>'form', 'id'=>'editmajorform', 'data-toggle'=>'validator'])!!}

                    @include('setup.course.form.majorform', ['submitButtonText'=>'Update',  'formid'=>'editmajorform'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div id="deletedialog" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 300px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Really delete?</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open()!!}
                    @include('form_common.deletedialog')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Dialog for adding a unit to this course--}}
    <div id="addunitdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Unit</h4>
                </div>
                <div class="modal-body">
                    {!! Form::model($course, ['class'=>'form-horizontal', 'role'=>'form', 'id'=>'addcourseform', 'data-toggle'=>'validator'])!!}

                    @include('setup.course.form.addunitform', ['submitButtonText'=>'Add',  'formid'=>'addcourseform'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Dialog for adding a trim reference to this course--}}
    <div id="addcoursetrimdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Trim Container Reference to course</h4>
                </div>
                <div class="modal-body">
                    {!! Form::model($course, ['class'=>'form-horizontal', 'role'=>'form', 'id'=>'addcoursetrimform', 'data-toggle'=>'validator'])!!}

                    @include('setup.course.form.coursetrimform', ['submitButtonText'=>'Add',  'formid'=>'addcoursetrimform'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    {{--  Dialog for adding a trim reference to this course--}}
    <div id="addfostrimdialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Trim Reference to field of study</h4>
                </div>
                <div class="modal-body">
                    {!! Form::model($course, ['class'=>'form-horizontal', 'role'=>'form', 'id'=>'addfostrimform', 'data-toggle'=>'validator'])!!}

                    @include('setup.course.form.fostrimform', ['submitButtonText'=>'Add',  'formid'=>'addfostrimform'])

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop
 