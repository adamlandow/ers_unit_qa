<div class="form-group">

    {!! Form::label('code', 'Code') !!}
    {!! Form::text('code', null, ['class'=>'form-control', 'required',]) !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Description') !!}
    {!! Form::text('description', null, ['class'=>'form-control', 'required',]) !!}
</div>
<div class="form-group">
    {!! Form::label('year', 'Year') !!}
    <select class="form-control" name="year" style="width: 300px">
        <option value='2016'>2016</option>
        <option value='2017'>2017</option>
        <option value='2018'>2018</option>
        <option value='2019'>2019</option>
        <option value='2020'>2020</option>
        <option value='2021'>2021</option>
        <option value='2022'>2022</option>
        <option value='2023'>2023</option>
    </select>
</div>
<div class="form-group">
    {!! Form::label('term', 'Trimester') !!}
    <select class="form-control" name="term" style="width: 300px">
        <option value='1'>1</option>
        <option value='2'>2</option>
        <option value='3'>3</option>
        <option value='FY'>FY</option>
    </select>
</div>
<div class="form-group">
    {!! Form::label('owner_id', 'Owner') !!}
    <select class="select2 form-control" name="owner_id"  style="width: 300px">
        <option value='-1'>None</option>
        @foreach ($users as $owner)
            <option value='{{$owner->id}}'>{{$owner->name}}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>



