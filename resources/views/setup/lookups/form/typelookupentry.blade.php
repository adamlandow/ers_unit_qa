<div class="form-group">

    {!! Form::label('text', 'Description') !!}
    {!! Form::text('text', null, ['class'=>'form-control', 'required',]) !!}
</div>
<div class="form-group colorpicker-component">
    <div  class="input-group colorpicker-component">
        <input type="text" name="color" value="#00AABB" class="form-control" />
        <span class="input-group-addon"><i></i></span>
    </div>
</div>
<div class="form-group">
    {!! Form::label('alarm_prior_secs', 'Alarm at (in seconds)') !!}
    {!! Form::text('alarm_prior_secs', null, ['class'=>'form-control touchspin']) !!}
</div>
<div class="form-group">
    {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>



