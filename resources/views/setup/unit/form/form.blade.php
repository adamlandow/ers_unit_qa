<div class="form-group row">
    {!! Form::label('unit_code', 'Unit Code', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {{ Form::input('text', 'unit_code', null, ['class'=>'form-control', 'required']) }}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('description', 'Description', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {{ Form::input('text', 'description', null, ['class'=>'form-control', 'required']) }}
    </div>
</div>
<div class="form-group row" >
    {!! Form::label('description', 'Description', ['class'=>'control-label  col-sm-2 text-left']) !!}

    <div class="col-sm-10">
        {!! Form::select('discipline_id', $disciplines->pluck('description', 'id'), null, ['style'=>"width: 300px", 'class'=>'form-control']) !!}
    </div>
</div>
{{--<div class="form-group row">--}}
    {{--{!! Form::label('color', 'Calendar colour', ['class'=>'control-label  col-sm-2 text-left']) !!}--}}
    {{--<div class="input-group colorpicker-component">--}}
        {{--<input type="text" name="color" value="#00AABB" class="form-control"/>--}}
        {{--<span class="input-group-addon"><i></i></span>--}}
    {{--</div>--}}
{{--</div>--}}
<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>


