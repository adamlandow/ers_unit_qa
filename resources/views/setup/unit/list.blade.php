@extends('layouts.app')


@section('content')
        <!-- include form specific libraries -->
@include('layouts.formincludes')
<link href="{{ asset('resources/assets/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('resources/assets/js/bootstrap-colorpicker.min.js') }}"></script>
<script>

    var currentid = -1;

    $(document).ready(function () {

        $('.colorpicker-component').colorpicker();

        $('select').select2({
            placeholder: 'Select an option'
        });


        $('a[data-toggle=modal], button[data-toggle=modal]').click(function () {
            if ((typeof $(this).data('id')) !== 'undefined') {
                currentid = $(this).data('id');
                if ($(this).data('target') == "#editdialog") {
                    getCourseDetails(currentid);
                }
            }
        });


        $('#newunitdialog').on('shown.bs.modal', function () {

            $("#newunitform").validator();
        });


        // New patient event dialogue
        $('#newunitdialog').submit(function (event) {
            console.log('submitting new unit dialog')
            $('#newunitdialog').modal('hide');

            var vars = $("#newunitform").serializeArray();
            // vars.push({name: 'roles', value: $("#newuserform :input[name='role_id']").val()});
            console.log(vars);
            // cancels the form submission
            event.preventDefault();
            waitingDialog.show();
            submitNewUnitForm(vars);
        });

        $('#editdialog').on('shown.bs.modal', function () {
            $("#editunitform").validator();
        });


        $('#deletedialog').submit(function (event) {
            $('#deletedialog').modal('hide');
            var vars = $("#deletedialog").find("form").serializeArray();
            vars.push({name: 'id', value: currentid});
            //vars.push({name: '_method', value: 'DELETE'});
            // cancels the form submission
            event.preventDefault();
            waitingDialog.show();
            deleteUnit(vars);
        });

    });


    /*
     *
     * New user AJAX
     *
     */
    function submitNewUnitForm(vars) {
        $.ajax({
            url: '{{URL::to('unit/ajaxstore')}}',
            type: 'post',
            data: vars,
            error: function (jqXHR, textStatus, errorThrown) {
                waitingDialog.hide();
                alert(errorThrown);
            },
            success: function (data) {
                console.log(data.status)
                if (data.status.toString() == '0') {
                    location.reload(true);
                } else {
                    waitingDialog.hide();
                    alert(data.statusText);
                }
            }
        });
    }


    function deleteUnit(vars) {
        $.ajax({
            url: '{{URL::to('unit/ajaxdestroy')}}',
            type: 'post',
            data: vars,
            error: function (jqXHR, textStatus, errorThrown) {
                waitingDialog.hide();
                alert(errorThrown);
            },
            success: function (data) {
                waitingDialog.hide();
                if (data == "1") {
                    location.reload(true);
                } else {
                    alert('something went wrong with the delete');
                }
            }
        });
    }


    /*
     *
     * Helper functions
     *
     */

    // populates a form with data returned from Laravel
    function populate(frm, data) {

        $.each(data, function (key, value) {
            var $ctrl = $('[name="' + key + '"]', frm);
            if ($ctrl.is("select")) {
                if ($ctrl.attr('multiple')) {
                    console.log(key, value)
                    //  $ctrl.select2('val', value.split(',')).trigger('change');
                } else {
                    //$ctrl.select2('val', value);
                }
            } else {

                switch ($ctrl.attr("type")) {
                    case "text" :
                    case "hidden":


                        $ctrl.val(value);
                        // special case if a colorpicker
                        if ($ctrl.parent().hasClass('colorpicker-component')) {
                            $ctrl.parent().colorpicker('setValue', value);
                        }
                        break;
                    case "radio" :
                    case "checkbox":
                        $ctrl.each(function () {
                            if ($(this).attr('value') == value) {
                                $(this).attr("checked", value);
                            }
                        });
                        break;

                    default:
                        $ctrl.val(value);
                }

            }
        });
    }

</script>
{!! Breadcrumbs::render('unit.index') !!}
<fieldset style="padding-left: 15px; padding-right: 15px; margin-top: 0">
    <div style="width: 100%; ">
        <button type="button" class="btn btn-primary" data-toggle="modal" style="float: right"
                data-target="#newunitdialog">New Unit
            <i class="fa fa-plus"></i></button>
    </div>
    <div style="float: left; padding-left: 10px">{!! Form::open( ['class'=>'form-inline', 'role'=>'form', 'url'=>Request::path(), 'method' => 'get'])!!}
        <div class="form-group row"><input type="text" class="form-control" name="search"
                                           value="{{isset(Request::all()['search'])?Request::all()['search']:''}}">
            <button type="submit" class="btn btn-primary" style="vertical-align: bottom">Search <i
                        class="fa fa-search" aria-hidden="true"></i></button>
        </div>{!! Form::close()!!}</div>
    &nbsp;
    <table class="table table-striped">
        <thead class="thead-inverse">
        <tr>
            <th class="headerSortable header">Code</th>
            {{--<th class="headerSortable header">Current coordinator</th>--}}
            <th class="headerSortable header">Description</th>
            <th class="headerSortable header">Discipline</th>
            <th class="headerSortable header">School</th>
            <th class="headerSortable header">Calendar colour</th>

            <th></th>
        </tr>
        </thead>

        @foreach ($units as $unit)
            <tr>
                <td>
                    <a href="{{action('UnitController@show', $unit->id)}}">
                        {{$unit->unit_code}}
                    </a>
                </td>
                {{--<td>--}}
                    {{--(last cooordinator here)--}}
                {{--</td>--}}
                <td>
                    {{$unit->description}}
                </td>
                <td>
                    @if(isset($unit->discipline)){{$unit->discipline->description}}@endif
                </td>
                <td>
                    @if(isset($unit->discipline)){{$unit->discipline->school->description}}@endif
                </td>
                <td><span><i style="background-color: {{isset($unit->color)?$unit->color :''}};">&nbsp;&nbsp;&nbsp;&nbsp;</i>{{isset($unit->color)?$unit->color :'No color set'}}</span>
                </td>
                <td>
                    <a href="#" data-toggle="modal" data-id="{{$unit->id}}"
                       data-target="#deletedialog">
                        Delete
                    </a>
                </td>

        @endforeach

    </table>
    {{ $units->links() }}
</fieldset>

<div id="newunitdialog" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New Unit</h4>
            </div>
            <div class="modal-body">
                {!! Form::open( ['class'=>'form-horizontal', 'role'=>'form', 'id'=>'newunitform', 'data-toggle'=>'validator'])!!}

                @include('setup.unit.form.form', ['submitButtonText'=>'Add Unit',   'formid'=>'newunitform'])

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


{{--  Deleting a contact event  --}}
<div id="deletedialog" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Really delete unit?</h4>
            </div>
            <div class="modal-body">
                {!! Form::open()!!}
                @include('form_common.deletedialog')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


@stop
