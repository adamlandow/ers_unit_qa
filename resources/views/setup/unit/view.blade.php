@extends('layouts.app')

@section('content')
        <!-- include form specific libraries -->
@include('layouts.formincludes')
<link href="{{ asset('resources/assets/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
<script src="{{ asset('resources/assets/js/bootstrap-colorpicker.min.js') }}"></script>
{{--@section('title')--}}
{{--{{$user->name}}--}}

{{--@endsection--}}

<script>

    var currentmediaid = 0;

    $(document).ready(function () {


        $('.colorpicker-component').colorpicker();
        @if(isset($unit->color))
        $('.colorpicker-component').colorpicker('setValue', '{{$unit->color}}');
        @endif
        // Bootstrap AJAX
        //http://webdesign.tutsplus.com/tutorials/building-a-bootstrap-contact-form-using-php-and-ajax--cms-23068


        $('#editdetailsdialog').on('shown.bs.modal', function () {
            $('#editdetailsdialog').find("form").validator();
//            changevalidation('editdetailsdialog');
        });

        // Update user dialogue
        $('#editdetailsdialog').submit(function (event) {
            $('#editdetailsdialog').modal('hide');
            var vars = $("#editdetailsdialog").find("form").serializeArray();
            // little hack to get the multiple values in from the subspecialties input dialog box

            vars.push({name: 'id', value: '{{$unit->id}}'});
            vars.push({name: '_token', value: '{{ csrf_token() }}'});
            // cancels the form submission so that we can submit it using AJAX
            event.preventDefault();
            waitingDialog.show();
            submitUpdateForm(vars);
        });


    });


    function submitUpdateForm(vars) {
        $.ajax({
            url: '{{URL::to('/unit/ajaxupdate')}}',
            type: 'post',
            data: vars,
            error: function (jqXHR, textStatus, errorThrown) {
                waitingDialog.hide();
                alert(errorThrown);
            },
            success: function (data) {
                if (data.status > 0) {
                    location.reload(true);
                } else {
                    waitingDialog.hide();
                    alert(data.statusText);
                }
            }
        });
    }


</script>


{!! Breadcrumbs::render('unit.show', $unit) !!}


<div id="detailstab" class="tab-pane active">
    <fieldset style="width: 90%">
        <legend>Unit details

            <button class="btn btn-info btn-lg" data-toggle="modal"
                    data-target="#editdetailsdialog">Update
            </button>

        </legend>


        <div class="col-md-12">
            <div class="col-md-6">

                <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                    <div class="col-md-4">
                        <strong>Unit code:</strong>
                    </div>
                    <div class="col-md-8">
                        {{$unit->unit_code}}
                    </div>
                </div>
                <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                    <div class="col-md-4">
                        <strong>Description</strong>
                    </div>
                    <div class="col-md-8">
                        {{$unit->description}}
                    </div>
                </div>
                <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                    <div class="col-md-4">
                        <strong>Current coordinator:</strong>
                    </div>
                    <div class="col-md-8">
                        (last coordinator here)
                    </div>
                </div>
                <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                    <div class="col-md-4">
                        <strong>Discipline:</strong>
                    </div>
                    <div class="col-md-8">
                        @if(isset($unit->discipline)){{$unit->discipline->description}}@endif
                    </div>
                </div>
                <div class="col-md-12" style="border-bottom: 1px solid #d9d9d9">
                    <div class="col-md-4">
                        <strong>School:</strong>
                    </div>
                    <div class="col-md-8">
                        @if(isset($unit->discipline)){{$unit->discipline->school->description}}@endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <strong>Included in Courses</strong>
                <div class="col-md-12">

                    <div class="col-md-12">
                        <ul>
                            @foreach($unit->courses as $course)
                                <li> <a href="{{URL::to("course/{$course->id}")}}">{{$course->label}}</a>
                                    @if(isset($course->pivot->major_id))
                                       ({{\App\Fieldofstudy::find($course->pivot->major_id)->label}})
                                    @endif

                                    ({{$course->pivot->relationship}})</li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </div>
        </div>

    </fieldset>
</div>


{{--  Dialog for editing details  --}}
<div id="editdetailsdialog" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Details</h4>
            </div>
            <div class="modal-body">
                {!! Form::model($unit, ['class'=>'form-horizontal', 'role'=>'form', 'id'=>'updateform', 'data-toggle'=>'validator'])!!}

                @include('setup.unit.form.form', ['submitButtonText'=>'Update',  'formid'=>'updateform'])

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


@stop
 