@extends('layouts.app')

@section('content')
    <style>
        .modal-body {
            max-height: calc(100vh - 210px);
            overflow-y: auto;
        }

        .fa-check {
            color: green;
        }

        .fa-times {
            color: red;
        }
    </style>
    <script>


        $(document).ready(function () {

            $('select').select2();


        });


        /*
         *
         * Helper functions
         *
         */

        // populates a form with data returned from Laravel
        function populate(frm, data) {
            $.each(data, function (key, value) {
                var $ctrl = $('[name=' + key + ']', frm);
                if ($ctrl.is("select")) {
                    $ctrl.select2('val', value);
                } else {

                    switch ($ctrl.attr("type")) {
                        case "text" :
                        case "hidden":
                            $ctrl.val(value);
                            break;
                        case "radio" :
                        case "checkbox":
                            $ctrl.each(function () {
                                if ($(this).attr('value') == value) {
                                    $(this).attr("checked", value);
                                }
                            });
                            break;

                        default:
                            $ctrl.val(value);
                    }

                }
            });
        }
    </script>
    {!! Breadcrumbs::render('roles.index') !!}
    <p>&nbsp;</p>
    <h3>User roles</h3>
    <!-- Tabs -->


    <div style="padding-left: 15px; padding-right: 15px; margin-top: 0; border: 1px solid gray">
        <table class="table table-striped">
            <thead class="thead-inverse">
            <tr>
                <th class="headerSortable header"> Description</th>
                <th class="headerSortable header">Is admin (overrides all)</th>
                <th class="headerSortable header">Manage reviews</th>
                <th class="headerSortable header">View others unit reviews</th>
                <th class="headerSortable header">Manage unit setup instances</th>
                <th class="headerSortable header">View others unit setup instance</th>
                <th class="headerSortable header">Update others unit setup instance checklist status</th>
                <th class="headerSortable header">Send emails</th>
                <th></th>
            </tr>
            </thead>

            @foreach ($roles as $role)
                <tr>
                    <th>
                        <a href="#" data-toggle="modal" data-target="#updateroledialog">{{$role->text}}</a>
                    </th>
                    <td>
                        {!!$role->is_admin=='true'?'<i class="fa fa-check" aria-hidden="true"></i>':'<i class="fa fa-times" aria-hidden="true"></i>'!!}
                    </td>
                    <td>
                        {!!$role->update_review_instance=='true'?'<i class="fa fa-check" aria-hidden="true"></i>':'<i class="fa fa-times" aria-hidden="true"></i>'!!}
                    </td>
                    <td>
                        {!!$role->view_other_review_instances=='true'?'<i class="fa fa-check" aria-hidden="true"></i>':'<i class="fa fa-times" aria-hidden="true"></i>'!!}
                    </td>
                    <td>
                        {!!$role->update_unit_instance=='true'?'<i class="fa fa-check" aria-hidden="true"></i>':'<i class="fa fa-times" aria-hidden="true"></i>'!!}
                    </td>
                    <td>
                        {!!$role->view_other_unit_instance=='true'?'<i class="fa fa-check" aria-hidden="true"></i>':'<i class="fa fa-times" aria-hidden="true"></i>'!!}
                    </td>
                    <td>
                        {!!$role->update_other_unit_instance_checklist_status=='true'?'<i class="fa fa-check" aria-hidden="true"></i>':'<i class="fa fa-times" aria-hidden="true"></i>'!!}
                    </td>
                    <td>
                        {!!$role->send_emails=='true'?'<i class="fa fa-check" aria-hidden="true"></i>':'<i class="fa fa-times" aria-hidden="true"></i>'!!}
                    </td>

                    <td>
                        <a href="#" data-toggle="modal" data-id="{{$role->id}}"
                           data-target="#deletedialog">
                            Delete
                        </a>
                    </td>

            @endforeach

        </table>


    </div>

    {{--Update a role--}}
    <div id="updateroledialog" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Role detail for <span id="assessmentdetail"></span></h4>
                </div>
                <div class="modal-body">
                    @include('setup.roles.form.form',['submitButtonText'=>'Update Role'])
                </div>
            </div>
        </div>
    </div>






@stop

