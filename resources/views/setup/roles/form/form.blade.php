
<div class="form-group row">
    {!! Form::label('text', 'Description', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::text("text", null, ['class' => 'form-control', 'id' => 'text', 'required']) !!}
        {{--<input type="text" name="studentid" id="studentid" class='form-control' required>--}}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('is_admin', 'Is Admin (overrides all)', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::checkbox("is_admin", null, ['class' => 'form-control', 'id' => 'is_admin',]) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('update_review_instance', 'Manage Reviews', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::checkbox("update_review_instance", null, ['class' => 'form-control', 'id' => 'update_review_instance']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('view_other_review_instances', 'View others unit reviews', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::checkbox("view_other_review_instances", null, ['class' => 'form-control', 'id' => 'view_other_review_instances']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('update_unit_instance', 'Manage unit setup instances', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::checkbox("update_unit_instance", null, ['class' => 'form-control', 'id' => 'update_unit_instance']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('view_other_unit_instance', 'View others unit setup instance', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::checkbox("view_other_unit_instance", null, ['class' => 'form-control', 'id' => 'view_other_unit_instance']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('update_other_unit_instance_checklist_status', 'Update others unit setup instance checklist status', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::checkbox("update_other_unit_instance_checklist_status", null, ['class' => 'form-control', 'id' => 'update_other_unit_instance_checklist_status']) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label('send_emails', 'Send emails', ['class'=>'control-label  col-sm-2 text-left']) !!}
    <div class="col-sm-10">
        {!! Form::checkbox("send_emails", null, ['class' => 'form-control', 'id' => 'send_emails']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class'=>'btn btn-primary form-control']) !!}
</div>