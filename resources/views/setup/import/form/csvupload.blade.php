<div class="form-group">

    {!! Form::label('upload_file', 'Select CSV File') !!}

    <input type="file" id="upload_file" name="upload_file" required>

    <div id="currentfilename"></div>
</div>
<div class="form-group row">
    <div class="col-sm-12">{!! Form::label('model', 'Import what?', ['class'=>'control-label  text-left']) !!}</div>
    <div class="col-sm-10">
        <select class="form-control" name="model"  id="model" style="width: 300px">
            <option value='course'>Courses</option>
            <option value='unit'>Units</option>
            <option value='offering'>Offerings</option>
            {{--<option value='faculty'>Faculties</option>--}}
            {{--<option value='faculty'>Schools</option>--}}
        </select>
    </div>
</div>
{{--<div class="form-group row">--}}
{{--<div class="col-sm-12">{!! Form::label('template_id', 'Template', ['class'=>'control-label  text-left']) !!}</div>--}}
{{--<div class="col-sm-12">--}}
{{--<select class="form-control" name="template_id" id="csv_template_id" style="width: 300px">--}}
{{--@foreach ($templates as $template)--}}
{{--<option value='{{$template->id}}'>{{$template->label}}</option>--}}
{{--@endforeach--}}
{{--</select>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="form-group row">--}}
{{--<div class="col-sm-12">{!! Form::label('csv_eso_id', 'Assigned ESO', ['class'=>'control-label  text-left']) !!}</div>--}}

{{--<div class="col-sm-12">--}}
{{--{!! Form::select('csv_eso_id', $users->pluck('name', 'id'), null, ['style'=>"width: 300px", 'class'=>'form-control', 'required']) !!}--}}
{{--</div>--}}
{{--</div>--}}
<div class="form-group">
    {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
    {!! Form::submit('Upload CSV', ['class'=>'btn btn-primary form-control']) !!}
</div>



