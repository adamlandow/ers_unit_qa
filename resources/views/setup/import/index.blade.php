@extends('layouts.app')
@section('title')Import @endsection
@section('content')
    <script>
        $(document).ready(function () {


            // intercept new condition event dialogue form submission
            $('#newcsvdialog').submit(function (event) {
                event.preventDefault();
                console.log('submitting CSV')
                $('#newcsvdialog').modal('hide');
                if ($('#upload_file')[0].files.length > 0) {

                    var data = new FormData();
                    // file
                    jQuery.each($('#upload_file')[0].files, function (i, file) {
                        data.append('csvfile', file);
                    });
                    //
                    data.append('model', $('#model').val());
                    data.append('_token', '{{ csrf_token() }}');
                    // console.log(data);
                    // cancels the form submission
                    //   waitingDialog.show();
                    submitCSVForm(data);
                } else {
                    alert('Need to pick a file')
                }
            });



        });

        function submitCSVForm(vars) {
            console.log('submitCSVForm');
            waitingDialog.show('Uploading...');
            $.ajax({
                url: '{{URL::to('import/csvimport')}}',
                type: 'post',
                data: vars,
                processData: false,
                contentType: false,
                error: function (jqXHR, textStatus, errorThrown) {
                    waitingDialog.hide();
                    alert(errorThrown);
                },
                success: function (data) {
                    waitingDialog.hide();
                    if (typeof(data.error) === "undefined") {
                        location.reload(true);

                    } else {
                        alert('something went wrong with the operation: ' + data.value);
                    }
                }
            });
        }



        var init = true;



    </script>

    <!-- Tabs -->
    {{--{!! Breadcrumbs::render('user.show') !!}--}}

    <fieldset style="width: 100%" class="col-md-12">
        <legend>
            Import (directly from APEX exports)
        </legend>
        <button type="button" class="btn btn-primary" style="width: 220px" data-toggle="modal"
                data-target="#newcsvdialog">Import stuff by CSV<i
                    class="fa fa-plus"></i>
        </button>
    </fieldset>

    <div id="newcsvdialog" class="modal fade  modalform newcsvdialog" role="dialog"
         data-route="course">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add using CSV</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['class'=>'form-horizontal', 'role'=>'form'])!!}
                    @include('setup.import.form.csvupload')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
 