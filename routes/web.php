<?php

// php7.2 compatibiliy fix
if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
// Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
// error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);
//function () {
    //return view('welcome');
//});

// provides authentication routes.
Auth::routes();
// hack as logout
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/auth/getbycn', 'UserController@getDetailsByFullName');

// Users
Route::resource('user', 'UserController');

Route::get('user/show', 'UserController@show');
Route::post('user/ajaxupdate', 'UserController@ajaxupdate');
Route::post('user/ajaxstore', 'UserController@ajaxstore');
Route::post('user/ajaxdestroy', 'UserController@ajaxdestroy');
Route::get('searchuserforselect2', 'UserController@searchforselect2');

/////////////////////////////////////////////////
// User switching
/////////////////////////////////////////////////
Route::post('/sudosu/login-as-user', 'UserController@loginAsUser')
    ->name('sudosu.login_as_user');

Route::post('/sudosu/return', 'UserController@returnToUser')
    ->name('sudosu.return');

/*
 *  unit reviews
 */
Route::resource('unitreviews', 'ReviewInstancesController');
Route::get('unitreviews', 'ReviewInstancesController@index')->name('unitreviews');
Route::get('unitreviews/{id}', 'ReviewInstancesController@show')->name('unitreviews.show');
Route::post('unitreviews/create', 'ReviewInstancesController@store');
Route::post('unitreviews/clone', 'ReviewInstancesController@clone_instance');
Route::post('unitreviews/{id}/update', 'ReviewInstancesController@ajaxupdate');
Route::post('unitreviews/{id}/makelive', 'ReviewInstancesController@makelive');
Route::post('unitreviews/{id}/archive', 'ReviewInstancesController@archive');
Route::get('unitreviews/{id}/sendemail', 'ReviewInstancesController@sendemail');

Route::get('unitreviews/{id}/updatecaucdata', 'ReviewInstancesController@updatecaucdata');

Route::get('unitreviews/{id}/getoutcomesasword', 'ReviewInstancesController@getoutcomesasword');

Route::get('unitreviews/{id}/getfeedbacksummaryasword', 'ReviewInstancesController@getfeedbacksummaryasword');

Route::get('unitreviews/{id}/getaspdf', 'ReviewInstancesController@getaspdf');

// templates
Route::get('unitreviewstemplates', 'ReviewInstancesController@templateindex')->name('unitreviewstemplates');

Route::get('unitreviewstemplates/{id}', 'ReviewInstancesController@templateshow')->name('unitreviewstemplates.show');
Route::post('unitreviewstemplates/create', 'ReviewInstancesController@templatestore');
Route::post('unitreviewstemplates/clone', 'ReviewInstancesController@templateclone_instance');


// checklist items for unit reviews
Route::resource('reviewinstancechecklistitem', 'ReviewInstancesChecklistsItemsController');
Route::post('reviewinstancechecklistitem/update', 'ReviewInstancesChecklistsItemsController@update');
Route::post('reviewinstancechecklistitem/create', 'ReviewInstancesChecklistsItemsController@store');
Route::post('reviewinstancechecklistitem/reorder', 'ReviewInstancesChecklistsItemsController@reorder');
Route::post('reviewinstancechecklistitem/updatecoordinatorstatus', 'ReviewInstancesChecklistsItemsController@updatecoordinatorstatus');
Route::post('reviewinstancechecklistitem/updatepeerreviewerstatus', 'ReviewInstancesChecklistsItemsController@updatepeerreviewerstatus');
Route::post('reviewinstancechecklistitem/updateldstatus', 'ReviewInstancesChecklistsItemsController@updateldstatus');

// notes items for unit reviews
Route::resource('reviewnotesitem', 'ReviewInstancesNotesController');
Route::post('reviewnotesitem/update', 'ReviewInstancesNotesController@update');
Route::post('reviewnotesitem/create', 'ReviewInstancesNotesController@store');

// media for reviews
// add a media to a review
Route::post('reviewmedia/create', 'ReviewInstancesController@createmedia');



// make the media controller a review
Route::resource('reviewmedia', 'ReviewInstancesMediaController');



// update a media to a review
Route::post('reviewmedia/update', 'ReviewInstancesMediaController@update');

// show the actual image (or an icon if a document)
Route::get('reviewmedia/show/{id}', 'ReviewInstancesMediaController@display');

// force a download
Route::get('reviewmedia/download/{id}', 'ReviewInstancesMediaController@download');

// show a thumbnail
Route::get('reviewmedia/thumb/{id}', 'ReviewInstancesMediaController@thumb');



// emails for reviews
Route::get('reviewemails', 'ReviewEmailsController@index')->name('reviewemails');
Route::get('reviewemails/{id}', 'ReviewEmailsController@show');
Route::post('reviewemails/create', 'ReviewEmailsController@store');
Route::post('reviewemails/{id}/update', 'ReviewEmailsController@update');
Route::post('reviewemails/destroy', 'ReviewEmailsController@destroy');

// outcomes for reviews
Route::post('reviewoutcome/create', 'ReviewInstancesOutcomesController@store');
Route::post('reviewoutcome/update', 'ReviewInstancesOutcomesController@update');
Route::post('reviewoutcome/destroy', 'ReviewInstancesOutcomesController@destroy');

//Route::post('reviewlooutcome/create', 'ReviewInstancesOutcomesController@lostore');
Route::post('reviewlooutcome/update', 'ReviewInstancesOutcomesController@loupdate');

//Route::post('reviewassessmenttasksoutcome/create', 'ReviewInstancesOutcomesController@lostore');
Route::post('reviewassessmenttasksoutcome/update', 'ReviewInstancesOutcomesController@assessmentupdate');


// Unit setup
//Route::resource('unitsetup', 'OfferingsController');
Route::get('unitsetup', 'OfferingsController@index')->name('unitsetup')->middleware(['persistence']);

Route::get('unitsetup/{id}', 'OfferingsController@show')->name('unitsetup.show');
Route::post('unitsetup/create', 'OfferingsController@store');
Route::post('unitsetup/destroy', 'OfferingsController@destroy');

Route::post('unitsetup/clone', 'OfferingsController@clone_instance');
Route::post('unitsetup/csvimport', 'OfferingsController@csvimport');
Route::post('unitsetup/{id}/update', 'OfferingsController@ajaxupdate');
Route::post('unitsetup/{id}/updatestatus', 'OfferingsController@updateStatus');
Route::post('unitsetup/{id}/archive', 'OfferingsController@archive');
Route::get('unitsetup/{id}/detectmoodleurl', 'OfferingsController@detectmoodleurl');
Route::get('unitsetup/{id}/getuiaobook/{forcerefresh?}', 'OfferingsController@getUIAObook');
Route::get('unitsetup/{id}/sendemail', 'OfferingsController@sendemail');
Route::get('unitsetup/{id}/viewmail/{emailid}', 'OfferingsController@getemail');
Route::post('unitsetup/{id}/followup', 'OfferingsController@updatefollowup');
Route::post('unitsetup/{id}/updatecaucdata', 'OfferingsController@updatecaucdata');
Route::get('searchofferingforselect2', 'OfferingsController@searchforselect2');

//Experimental! Export a list of units in a useful format for Kantu browser automation
Route::get('unitsetupexportforkantu', 'OfferingsController@exportCSVForKantu');
//Experimental! update the Moodle URLs
Route::get('unitsetupbulkurlupdate', 'OfferingsController@batchMoodleSiteUpdate');

// bulk actions
Route::post('unitsetup/sendbulkemail', 'OfferingsController@sendbulkemail');
Route::post('unitsetup/bulkarchive', 'OfferingsController@bulkarchive');

// maintenance
Route::get('unitsetupmaintenance/updatecompletion', 'OfferingsController@updateCompletion');

// templates
Route::get('unitsetuptemplates', 'OfferingsController@templateindex')->name('unitsetuptemplates');
Route::get('unitsetuptemplates/{id}', 'OfferingsController@templateshow')->name('unitsetuptemplates.show');
Route::post('unitsetuptemplates/create', 'OfferingsController@templatestore');
Route::post('unitsetuptemplates/{id}/update', 'OfferingsController@templateajaxupdate');



// checklist items for unit setups
Route::resource('unitsetupinstancechecklistitem', 'OfferingChecklistsItemsController');
Route::post('unitsetupinstancechecklistitem/update', 'OfferingChecklistsItemsController@update');
Route::post('unitsetupinstancechecklistitem/create', 'OfferingChecklistsItemsController@store');
Route::post('unitsetupinstancechecklistitem/reorder', 'OfferingChecklistsItemsController@reorder');
Route::post('unitsetupinstancechecklistitem/updatestatus', 'OfferingChecklistsItemsController@updatestatus');
Route::post('unitsetupinstancechecklistitem/previewemail', 'OfferingChecklistsItemsController@previewemail');


// media for unit instances
Route::post('unitmedia/create', 'OfferingsController@createmedia');
Route::resource('unitmedia', 'OfferingMediaController');
Route::post('unitmedia/update', 'OfferingMediaController@update');
Route::get('unitmedia/show/{id}', 'OfferingMediaController@display');
Route::get('unitmedia/download/{id}', 'OfferingMediaController@download');
Route::get('unitmedia/thumb/{id}', 'OfferingMediaController@thumb');


// emails for unit setups
Route::get('unitemails', 'UnitEmailsController@index')->name('unitemails');
Route::get('unitemails/{id}', 'UnitEmailsController@show');

Route::post('unitemails/create', 'UnitEmailsController@store');
Route::post('unitemails/{id}/update', 'UnitEmailsController@update');
Route::post('unitemails/destroy', 'UnitEmailsController@destroy');


//reporting on unit setup
Route::get('unitsetupreports', 'ReportController@index')->name('unitsetupreports');

// lookups
Route::get('setup/lookups', 'SystemLookupsController@index')->name('systemlookups.index');
Route::post('setup/coursemapcsvupload', 'SystemLookupsController@importCourseMappingsByCSV');
////////////
//Units
////////////
Route::resource('unit', 'UnitController');
Route::post('unit/ajaxupdate', 'UnitController@update');
Route::post('unit/ajaxstore', 'UnitController@ajaxstore');
Route::post('unit/ajaxdestroy', 'UnitController@ajaxdestroy');
Route::get('unit/lookupunit/q', 'UnitController@getUnitCodeLookup');
Route::get('searchunitforselect2', 'UnitController@searchforselect2');

////////////
//Courses
////////////
Route::resource('course', 'CourseController');
Route::post('course/ajaxupdate', 'CourseController@update');
Route::post('course/ajaxstore', 'CourseController@ajaxstore');
Route::post('course/ajaxdestroy', 'CourseController@ajaxdestroy');

Route::post('course/addunit', 'CourseController@addunit');
Route::post('course/detachunit', 'CourseController@detachunit');
Route::post('course/getmajors', 'HomeController@getmajors');

Route::post('course/addtrim', 'CourseController@ajaxaddtrim');
Route::post('course/deletetrim', 'CourseController@ajaxdeletetrim');

// course trim
Route::resource('coursetrim', 'CourseTrimController');

////////////
//Majors
////////////
Route::resource('major', 'FieldOfStudyController');
Route::post('major/ajaxupdate', 'FieldOfStudyController@update');
Route::post('major/ajaxstore', 'FieldOfStudyController@ajaxstore');
Route::post('major/ajaxdestroy', 'FieldOfStudyController@ajaxdestroy');
Route::post('major/addtrim', 'FieldOfStudyController@ajaxaddtrim');

// Major trim
Route::resource('majortrim', 'FieldOfStudyTrimController');

// event types
Route::resource('eventtypelookup', 'EventTypeLookupController');
Route::post('eventtypelookup/update', 'EventTypeLookupController@update');
Route::post('eventtypelookup/create', 'EventTypeLookupController@store');

// disciplines
Route::resource('discipline_lookup', 'DisciplineLookupController');
Route::post('discipline_lookup/update', 'DisciplineLookupController@update');
Route::post('discipline_lookup/create', 'DisciplineLookupController@store');
//
// roles
Route::get('setup/roles', 'UserRolesController@index')->name('roles.index');

// course management extension
Route::get('unitcoursemanagement', 'CourseLookupController@index')->name('coursemanagement.index');
Route::get('unitcoursemanagement/unit/{id}', 'UnitController@view')->name('coursemanagement.unit.show');

// Review archiving
Route::get('unitreviewsarchive', 'ReviewInstancesController@archiveindex')->name('unitreviewsarchive');
Route::get('unitreviewsarchive/{id}', 'ReviewInstancesController@showarchive')->name('unitreviewsarchive.show');

// Dates: KPI's and teaching periods
Route::get('setup/dates', 'KeyDatesController@index');
Route::post('setup/dates/update', 'EventTypeLookupController@update');
Route::post('setup/dates/createkpi', 'EventTypeLookupController@store');


///////////////////////////////////////////////////////
///
///   experiments and one-offs
///
/// //////////////////////////////////////////////////////

// importing things
Route::get('import', 'ImportController@index')->name('import.index');

Route::post('import/csvimport', 'ImportController@csvimport');

// updating course structures from a folder
Route::get('refreshcourseunitsfromfolder/{directory}', 'ScrapeController@updateCourseUnitsTableFromDirectory');

///   experimental import of review media
Route::get('reviewmediaimport', 'ReviewInstancesMediaController@import');