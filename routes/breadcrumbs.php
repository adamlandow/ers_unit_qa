<?php
/**
 * Created by PhpStorm.
 * User: alandow
 * Date: 29/02/2016
 * Time: 2:20 PM
 */
// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});


Breadcrumbs::register('user.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Users", route('user.index'));
});

Breadcrumbs::register('unitreviews.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Unit reviews", route('unitreviews'));
});

Breadcrumbs::register('unitreviews.show', function ($breadcrumbs, $review) {
    $breadcrumbs->parent('unitreviews.index');
    $breadcrumbs->push("{$review->reviewed_offering->unit['unit_code']} {$review->reviewed_offering->teaching_period->teaching_period_lookup['teaching_period']} {$review->reviewed_offering->teaching_period['year']}", route('unitreviews.show', $review->id));
});

Breadcrumbs::register('unitreviewstemplates.index', function ($breadcrumbs) {
    $breadcrumbs->parent('unitreviews.index');
    $breadcrumbs->push("Unit review templates", route('unitreviewstemplates'));
});

Breadcrumbs::register('unitreviewstemplates.show', function ($breadcrumbs, $instance) {
    $breadcrumbs->parent('unitreviewstemplates.index');
    $breadcrumbs->push("{$instance->label}", route('unitreviewstemplates.show', $instance->id));
});

Breadcrumbs::register('unitreviews.archive', function ($breadcrumbs) {
    $breadcrumbs->parent('unitreviews.index');
    $breadcrumbs->push("Unit review archive", route('unitreviewsarchive'));
});

Breadcrumbs::register('unitreviews.archive.show', function ($breadcrumbs, $review) {
    $breadcrumbs->parent('unitreviews.archive');
    $breadcrumbs->push("{$review->reviewed_offering->unit['unit_code']} {$review->reviewed_offering->teaching_period->teaching_period_lookup['teaching_period']} {$review->reviewed_offering->teaching_period['year']}", route('unitreviewsarchive.show', $review->id));
});

Breadcrumbs::register('reviewemails.index', function ($breadcrumbs) {
    $breadcrumbs->parent('unitreviews.index');
    $breadcrumbs->push("Review email templates", route('reviewemails'));
});

Breadcrumbs::register('unitsetupwithcount.index', function ($breadcrumbs, $offerings) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Offering setup (Total offering count: {$offerings->total()})", route('unitsetup'));
});

Breadcrumbs::register('unitsetup.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Offering setup", route('unitsetup'));
});

Breadcrumbs::register('unitsetup.show', function ($breadcrumbs, $instance) {
    $breadcrumbs->parent('unitsetup.index');
    $breadcrumbs->push("{$instance->unit['unit_code']} {$instance->teaching_period->teaching_period_lookup->teaching_period} {$instance->teaching_period['year']}", route('unitsetup.show', $instance->id));
});

Breadcrumbs::register('unitsetuptemplates.index', function ($breadcrumbs) {
    $breadcrumbs->parent('unitsetup.index');
    $breadcrumbs->push("Offering setup templates", route('unitsetuptemplates'));
});

Breadcrumbs::register('unitsetuptemplates.show', function ($breadcrumbs, $instance) {
    $breadcrumbs->parent('unitsetuptemplates.index');
    $breadcrumbs->push("{$instance->label}", route('unitsetuptemplates.show', $instance->id));
});

Breadcrumbs::register('unitemails.index', function ($breadcrumbs) {
    $breadcrumbs->parent('unitsetup.index');
    $breadcrumbs->push("Offering setup email templates", route('unitemails'));
});

Breadcrumbs::register('systemlookups.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("System lookups", route('systemlookups.index'));
});


Breadcrumbs::register('roles.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("System roles", route('roles.index'));
});


Breadcrumbs::register('calendar', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Calendar", route('calendar'));
});


Breadcrumbs::register('unit.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Unit management", route('unit.index'));
});

Breadcrumbs::register('unit.show', function ($breadcrumbs, $unit) {
    $breadcrumbs->parent('unit.index');
    $breadcrumbs->push("{$unit->description}", route('unit.show', $unit->id));
});

Breadcrumbs::register('course.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push("Course management", route('course.index'));
});

Breadcrumbs::register('course.show', function ($breadcrumbs, $course) {
    $breadcrumbs->parent('course.index');
    $breadcrumbs->push("{$course->label}", route('course.show', $course->id));
});

Breadcrumbs::register('unitsetupreports', function($breadcrumbs)
{
    $breadcrumbs->parent('unitsetup.index');
    $breadcrumbs->push("Unit setup status reports", route('unitsetupreports'));
});
//

