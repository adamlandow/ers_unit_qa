<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $table = 'faculty';

    protected $fillable = [
        'code',  'description'
    ];

    public function schools()
    {
        return $this->hasMany('App\School', 'faculty_id', 'id');
    }


}
