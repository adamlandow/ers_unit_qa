<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Sortable_offering extends Model
{

    use Sortable;


    protected $table = 'sortable_offering';

    /**
     * The attributes that are sortable.
     *
     * @var array
     */
    protected $sortable = ['unit_id', 'unit_code', 'coordinator_id', 'coordinator_name', 'eso_id', 'eso_name', 'follow_up', 'complete_percent', 'discipline', 'has_olx', 'is_accredited'];

    public function coordinator()
    {
        return $this->belongsTo('App\User', 'coordinator_id', 'id');
    }

    public function eso()
    {
        return $this->belongsTo('App\User', 'eso_id', 'id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id', 'id');
    }


    public function teaching_period()
    {
        return $this->belongsTo('App\Teaching_period', 'teaching_period_id', 'id');
    }

    // Tracking combined units: many to many relationship with myself
    // Based on https://github.com/laravel/framework/issues/441
    public function combined_units()
    {

        return $this->belongsToMany('App\Offering', 'offering_offering', 'unit_id', 'combined_unit_id');
    }

    public function units_combined()
    {

        return $this->belongsToMany('App\Offering', 'offering_offering', 'combined_unit_id', 'unit_id');
    }


    // filter- a way of finding any units with the currently logged on user as coordinator
    public function scopeUnitsforcoordinator($query, $userid)
    {
        $query
            // coordinators
            ->where(function ($query) use ($userid) {
                $query->where('coordinator_id', $userid);
            });

        return $query;
    }

    // filter- a way of finding any units with the currently logged on user as coordinator
    public function scopeUnitsforcoordinatororeso($query, $userid)
    {
       // dd($userid);
        $query
            // coordinators
            ->where(function ($query) use ($userid) {
                $query->where('eso_id', $userid);

            })
            ->orWhere(function ($query) use ($userid) {
                $query->where('coordinator_id', $userid);
            });
        return $query;
    }

    // filter- a way of finding any units with the currently logged on user as eso
    public function scopeUnitsforeso($query, $userid)
    {
        $query
            // coordinators
            ->where(function ($query) use ($userid) {
                $query->where('eso_id', $userid);
            });

        return $query;
    }

    // filter- a way of finding any units with the currently logged on user as coordinator
    public function scopeExcludeTemplates($query)
    {
        $query
            // is a template
            ->where(function ($query) {
                $query->where('is_template', '<>', 'true');
            });

        return $query;
    }


}
