<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Olx_checklist_items extends Model
{
    use SoftDeletes;

    protected $table = 'offering_checklist_items';

    protected $fillable = [
        'olx_id', 'heading','description', 'text', 'order', 'status',  'trigger_email', 'trigger_email_id'
    ];

    public function olx()
    {
        return $this->belongsTo('App\Olx', 'olx_id', 'id');
    }

    public function trigger_email_template()
    {
        return $this->belongsTo('App\Emails_template', 'trigger_email_id', 'id');
    }
}
