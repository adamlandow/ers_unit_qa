<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Offering_offering extends Model
{


    protected $fillable = [
        'units_id', 'combined_unit_id'
    ];

    protected $table = 'offering_offering';


}
