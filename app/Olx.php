<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Olx extends Model
{
    use SoftDeletes;
    use Sortable;

    protected $table = 'olx';

    protected $fillable = [
        'is_template',
        'offering_id',
        'olx_admin_id',
        'complete_percent',
        'label',
        'status',
        'url'
    ];

    public function offering()
    {
        return $this->belongsTo('App\Offering', 'offering_id', 'id');
    }



    public function olx_admin()
    {
        return $this->belongsTo('App\User', 'olx_admin_id', 'id');
        // for later: return $this->belongsToMany('App\User', 'offering_eso');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id', 'id');
    }

    public function checklist()
    {
        return $this->hasMany('App\Olx_checklist_items', 'olx_id', 'id')->orderBy('order');
    }

    public function media()
    {
        return $this->hasMany('App\Olx_media', 'olx_id', 'id');
    }

    public function getChecklistcompleteAttribute()
    {
        return $this->checklist()->get()->where('heading', '<>', '1')->where('status', '1')->count();
    }
}
