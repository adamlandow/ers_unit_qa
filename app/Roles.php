<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text',
        'is_admin',
        'update_review_instance',
        'view_review_instance',
        'view_other_review_instance',
        'create_review_note',
        'update_review_notes',
        'delete_review_notes',
        'create_review_media',
        'update_review_media',
        'delete_review_media',
        'view_other_unit_instance',
        'update_unit_instance',
        'update_unit_instance_checklist_status',
        'update_other_unit_instance_checklist_status',
        'send_emails',
        'update_templates'

    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'users_roles');
    }
}
