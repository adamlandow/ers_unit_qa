<?php

namespace App\Providers;

use App\Review_instances;
use App\Roles;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use SebastianBergmann\CodeCoverage\Report\Xml\Unit;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // define permissions

        $capabilities = Roles::first()->getFillable();

        foreach ($capabilities as $capability) {
            Gate::define($capability, function ($user) use ($capability) {

                // check that one of the roles contains admin
                $admin = $user->roles->first(function ($value, $key) {
                    return $value['is_admin'] == 'true';
                });
                // if admin, just return true. We don't need to go any further
                if (isset($admin)) {
                    return true;
                }
                // now check that any of the other roles contains teh capability
                $role = $user->roles->first(function ($value, $key) use ($capability) {
                    return $value[$capability] == 'true';
                });

                return (isset($role));

            });
        }
        // dd($capabilities);
//        Gate::define('is_admin', function ($user) {
//            return 'true';
//        });
//        // can they view their own review instances?
//        Gate::define('view_review_instance', function ($user) {
//            return ($user->role['is_admin']=='true')||($user->role['view_review_instance']=='true');
//        });
//        // can they view other people's review instances?
//        Gate::define('view-other-review-instance', function ($user) {
//            return ($user->role['is_admin']=='true')||($user->role['view_other_review_instances']=='true');
//        });
//        // can they modify review instances? (createm update,
//        Gate::define('update-review-instance', function ($user) {
//            return ($user->role['is_admin']=='true')||($user->role['update_review_instance']=='true');
//        });
    }


}
