<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teaching_period_lookup extends Model
{
    //
    use SoftDeletes;

    protected $table = 'teaching_period_lookup';

    protected $fillable = [
        'teaching_period', 'cauc_label', 'description'
    ];

//    public function unit_instance()
//    {
//        return $this->hasMany('App\Unit_instance', 'teaching_period_id', 'id');
//    }

}
