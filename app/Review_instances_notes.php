<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Review_instances_notes extends Model
{
    //
    use SoftDeletes;
    use Sortable;

    public function created_by()
    {
        return $this->belongsTo('App\User', 'created_by_id', 'id');
    }

    protected $table = 'review_instances_notes';

    protected $fillable = [
        'review_instances_id', 'description', 'text', 'created_by_id'
    ];


}
