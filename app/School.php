<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = 'school';

    protected $fillable = [
        'code',  'description', 'faculty_id'
    ];

    public function disciplines()
    {
        return $this->hasMany('App\Discipline', 'school_id', 'id');
    }

    public function faculty()
    {
        return $this->hasOne('App\Faculty', 'id', 'faculty_id');
    }

}
