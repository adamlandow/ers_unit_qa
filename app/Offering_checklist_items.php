<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offering_checklist_items extends Model
{
    //
    use SoftDeletes;

    protected $table = 'offering_checklist_items';

    protected $fillable = [
        'offering_id', 'description', 'text', 'order', 'status', 'heading', 'trigger_email', 'trigger_email_id'
    ];

    public function offering()
    {
        return $this->belongsTo('App\Offering', 'offering_id', 'id');
    }

    public function trigger_email_template()
    {
        return $this->belongsTo('App\Emails_template', 'trigger_email_id', 'id');
    }

}
