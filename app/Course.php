<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

    // the table this model refers to.
    protected $table = 'course';

    protected $fillable = [
        'course_code', 'label', 'description', 'convenor_id', 'url', 'course_status', 'trim_reference', 'trim_container', 'school_id'
    ];

    public function fields_of_study()
    {
        return $this->hasMany('App\Field_of_study', 'course_id');
    }

    public function trim()
    {
        return $this->hasMany('App\Course_trim', 'course_id');
    }

    public function units()
    {
        return $this->belongsToMany('App\Unit', 'unit_course_field_of_study')->withPivot('field_of_study_id', 'relationship');
    }

    public function coreunits()
    {
        return $this->belongsToMany('App\Unit', 'unit_course_field_of_study')->withPivot('field_of_study_id')->wherePivot('relationship', '=', 'core');
    }

    public function prescribedunits()
    {
        return $this->belongsToMany('App\Unit', 'unit_course_field_of_study')->withPivot('field_of_study_id', 'relationship', 'unit_id', 'id')->wherePivot('relationship', '=', 'prescribed');
    }

    public function listedunits()
    {
        return $this->belongsToMany('App\Unit', 'unit_course_field_of_study')->withPivot('field_of_study_id', 'relationship', 'unit_id', 'id')->wherePivot('relationship', '=', 'listed');
    }

    public function convenor()
    {
        return $this->belongsTo('App\User');
    }

    public function school()
    {
        return $this->belongsTo('App\School');
    }




}
