<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course_trim extends Model
{
    // // the table this model refers to.
       protected $table = 'course_trim';
       use SoftDeletes;

    protected $fillable = [
        'reference', 'course_id', 'version'
    ];

    public function course(){
        return $this->belongsTo('App\Course');
    }
}
