<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Kyslik\ColumnSortable\Sortable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use Sortable;

    protected $app;
    protected $auth;
    protected $session;
    protected $sessionKey = 'sudosu.original_id';
    protected $usersCached = null;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'email', 'password', 'url', 'image_url', 'title', 'type'
    ];

    /**
     * The attributes that are sortable.
     *
     * @var array
     */
    protected $sortable = ['username',
        'name',
        'type',
    ];


    public function __construct()
    {
        parent::__construct();
        $this->session = session();
    }

    public function loginAsUser($userId, $currentUserId)
    {
        $this->session->put('sudosu.has_sudoed', true);
        $this->session->put($this->sessionKey, $currentUserId);

        Auth::loginUsingId($userId);
    }

    public function injectToView($user)
    {
        $packageContent = view('auth.user-selector', [
            'hasSudoed' => $this->hasSudoed(),
            'originalUser' => $this->getOriginalUser(),
            'currentUser' => Auth::user(),
            'users' => \App\User::all(),
            'user' => $user
        ])->render();

        return $packageContent;
    }

    public function hasSudoed()
    {
        return $this->session->has('sudosu.has_sudoed');
    }

    public function getOriginalUser()
    {
        if (!$this->hasSudoed()) {
            return $this;
        }

        $userId = $this->session->get($this->sessionKey);

        return $this::findOrFail($userId);
    }

    public function returnToUser()
    {
        if (!$this->hasSudoed()) {
            return false;
        }

        Auth()->logout();

        $originalUserId = $this->session->get($this->sessionKey);

        if ($originalUserId) {
            Auth()->loginUsingId($originalUserId);
        }

        $this->session->forget($this->sessionKey);
        $this->session->forget('sudosu.has_sudoed');

        return true;
    }

    public function getRoleslistAttribute()
    {
        return $this->roles();
    }

    public function roles()
    {
        return $this->belongsToMany('App\Roles', 'users_roles');
    }

    public function emails_sent_to()
    {
        return $this->hasMany('Unit_setup_emails_log', 'sent_to_id', 'id');
    }

    public function emails_sent_by()
    {
        return $this->hasMany('Unit_setup_emails_log', 'sent_by_id', 'id');
    }

    public function schools()
    {
        return $this->belongsToMany('App\School', 'users_schools');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


}
