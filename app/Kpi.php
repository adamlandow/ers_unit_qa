<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kpi extends Model
{
    use SoftDeletes;

    protected $table = 'kpi_dates';

    protected $fillable = [
        'description', 'teaching_period_id', 'date'
    ];

    public function getDates()
    {
        return [
            'start_timestamp'
        ];
    }


    public function teaching_period()
    {
        return $this->hasOne('App\Teaching_period', 'id', 'teaching_period_id');
    }
}
