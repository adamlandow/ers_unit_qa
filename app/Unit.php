<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
    use SoftDeletes;

    protected $table = 'unit';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'unit_code', 'description', 'color', 'level', 'credit_points', 'discipline_id'
    ];

    /**
     * The attributes that are sortable.
     *
     * @var array
     */
    protected $sortable = ['unit_code', 'description', 'level'];


    public function courses()
    {
        return $this->belongsToMany('App\Course', 'unit_course_field_of_study')->withPivot('course_id', 'field_of_study_id', 'relationship');
    }


    public function offerings()
    {
        return $this->hasMany('App\Offering', 'unit_id', 'id');
    }

    public function fields_of_study()
    {
        return $this->belongsToMany('App\Field_of_study', 'unit_course_field_of_study')->withPivot('field_of_study_id', 'relationship');
    }

    public function discipline(){
        return $this->hasOne('App\Discipline', 'id', 'discipline_id');
    }

}
