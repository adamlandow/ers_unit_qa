<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    //
    use SoftDeletes;
    // the events table
    protected $table = 'events';

    protected $fillable = [
        'description', 'units_instance_id', 'type_id', 'location_id', 'start_timestamp', 'duration', 'url', 'onoffcampus'
    ];


    // define the relationship between events and unit instances
    public function unit_instance(){
        return $this->hasOne('App\Offering', 'id', 'units_instance_id');
    }

    public function type(){
        return $this->hasOne('App\Event_type_lookup', 'id', 'type_id');
    }

    public function location(){
        return $this->hasOne('App\Location_lookup', 'id', 'location_id');
    }

    // filter by units
    public function scopeFilter($query, $params){
        $query
            //name
            ->where(function ($query) use ($params) {
                if (isset($params['units'])) {
                    if (strlen($params['units']) > 0) {
                        {

                            $query->whereIn('unit_id', explode(',', $params['units']));
                        }
                    }
                };

                if (isset($params['units'])) {
                    if (strlen($params['units']) > 0) {
                        {

                            $query->whereIn('unit_id', explode(',', $params['units']));
                        }
                    }
                };

                if (isset($params['onoffcampus'])) {
                    if (strlen($params['onoffcampus']) > 0) {
                        {
                            $query->whereIn('onoffcampus',  explode(',', $params['onoffcampus']));
                        }
                    }
                }
                if (isset($params['type'])) {
                    if (strlen($params['type']) > 0) {
                        {
                            $query->whereIn('type_id',  explode(',', $params['type']));
                        }
                    }
                }

            });
    }
}
