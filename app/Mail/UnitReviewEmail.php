<?php

namespace App\Mail;

use App\Emails_template;
use App\Review_instances;
use App\Offering;
use App\Review_instances_checklist_items;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class UnitReviewEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $template;
    protected $review_instance;
    protected $checked_item;
    protected $currentuserid;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( Review_instances $review_instance, Review_instances_checklist_items $checked_item,  Emails_template $template, $currentuserid)
    {
       // dd($unit_instance);
        $this->template = $template;
        $this->review_instance= $review_instance;
        $this->checked_item= $checked_item;
        $this->currentuserid= $currentuserid;
    }

    public function getfulltext(){
        // offering string
        $offeringStr = "{$this->review_instance->reviewed_offering->unit['unit_code']} {$this->review_instance->reviewed_offering->teaching_period->teaching_period_lookup['teaching_period']} {$this->review_instance->reviewed_offering->teaching_period['year']}";
        // the item that was checked
        $itemStr = $this->checked_item->description;
//        return (str_replace(["{offering}","{item}","{checkedby}","{moodleurl}"],
//            [ $offeringStr, $itemStr, Auth::user()->name ], $this->template->text));
        return view('mailview.unitreviewemail')
            ->with('template', $this->template)
            ->with('offeringStr', $offeringStr)
            ->with('itemStr', $itemStr)
            ->with('url', URL::to('').'/unitreviews/'.$this->review_instance->id)
            ->with('nameStr',  User::find($this->currentuserid)->name);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // offering string
        $offeringStr = "{$this->review_instance->reviewed_offering->unit['unit_code']} {$this->review_instance->reviewed_offering->teaching_period->teaching_period_lookup['teaching_period']} {$this->review_instance->reviewed_offering->teaching_period['year']}";
        // the item that was checked
        $itemStr = $this->checked_item->description;
        return $this->from('ers_unit_qa@une.edu.au')->view('mailview.unitreviewemail')
            ->with('template', $this->template)
            ->with('offeringStr', $offeringStr)
            ->with('itemStr', $itemStr)
            ->with('nameStr',  User::find($this->currentuserid)->name)
            ->with('url', URL::to('').'/unitreviews/'.$this->review_instance->id)
            ->subject(str_replace(["{offering}","{item}","{checkedby}"],
                [ $offeringStr, $itemStr, User::find($this->currentuserid)->name], $this->template->subject));
    }
}
