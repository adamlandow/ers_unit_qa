<?php

namespace App\Mail;

use App\Emails_template;
use App\Offering;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UnitSetupEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $template;
    protected $offering;
    private $unitStr;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Offering $offering, Emails_template $template)
    {
      //  dd($offering);
        $this->template = $template;
        $this->offering= $offering;
        $this->unitStr = $this->offering->unit->unit_code;
        foreach($this->offering->combined_units as $combined_unit){
            $this->unitStr .= ('/'.$combined_unit->unit->unit_code);
        }
    }

    public function getfulltext(){
        return view('mailview.unitsetupemail')
            ->with('template', $this->template)
            ->with('offering', $this->offering)->render();
    }

    public function getsubject(){

        return str_replace(["{coordinator}","{unit}","{teachingperiod}","{moodleurl}"],
            [ explode(' ',$this->offering->coordinator->name)[0], $this->unitStr, $this->offering->teaching_period->teaching_period_lookup->teaching_period.' '.$this->offering->teaching_period->year, $this->offering->moodle_url], $this->template->subject);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        $this->unitStr = $this->offering->unit->unit_code;
//        foreach($this->offering->combined_units as $combined_unit){
//            $this->unitStr .= ('/'.$combined_unit->unit->unit_code);
//        }

//dd($this->offering);
        return $this->from($this->offering->eso->email)->view('mailview.unitsetupemail')
            ->with('template', $this->template)
            ->with('offering', $this->offering)
            ->subject(str_replace(["{coordinator}","{unit}","{teachingperiod}","{moodleurl}"],
                [ explode(' ',$this->offering->coordinator->name)[0], $this->unitStr, $this->offering->teaching_period->teaching_period_lookup->teaching_period.' '.$this->offering->teaching_period->year, $this->offering->moodle_url], $this->template->subject));
                //str_replace("{coordinator}", explode(' ',$this->unit_instance->coordinator['name'])[0], str_replace("{unit}", $this->unit_instance->unit['unit_code'], $this->template->subject)));
    }
}
