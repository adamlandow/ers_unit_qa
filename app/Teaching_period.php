<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teaching_period extends Model
{
    //
    use SoftDeletes;

    protected $table = 'teaching_period';

    protected $fillable = [
        'teaching_period', 'year', 'start_timestamp'
    ];

    public function getDates()
    {
        return [
            'start_timestamp'
        ];
    }

    public function unit_instance()
    {
        return $this->hasMany('App\Offering', 'teaching_period_id', 'id');
    }

    public function teaching_period_lookup()
    {
        return $this->hasOne('App\Teaching_period_lookup', 'id', 'teaching_period_lookup_id');
    }

}
