<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Review_instances extends Model
{
    use SoftDeletes;
    use Sortable;

    protected $table = 'review_instances';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'units_id', 'reviewed_offering_id', 'notes', 'peer_reviewer_id', 'ld_id', 'start_date', 'end_date', 'live', 'is_template', 'label',
        'override_coordinator_id',
        'outcomes_unit_code',
        'outcomes_unit_title',
        'outcomes_combined_units',
        'outcomes_credit_points',
        'outcomes_unit_description',
        'outcomes_unit_prerequisites',
        'outcomes_unit_corequisites',
        'outcomes_offerings',
        'outcomes_learning_outcomes',
        'outcome_assessment_tasks',
        'outcome_coordinators'
    ];

    /**
     * The attributes that are sortable.
     *
     * @var array
     */
    protected $sortable = ['units_id', 'reviewed_offering_id', 'peer_reviewer_id', 'ld_id', 'start_date', 'end_date', 'live'];



    // custom attributes
    // How many of the checklist items are complete
    public function getChecklistcompleteAttribute()
    {
        $coordcomplete = $this->checklist()->get()->whereIn('coordinator_status', ['Meets criterion', 'N/A', 'Does not meet criterion'])->where('heading', '<>', '1')->count();
        $peerreviewcomplete = $this->checklist()->get()->whereIn('peer_reviewer_status', ['Meets criterion', 'N/A', 'Does not meet criterion'])->where('heading', '<>', '1')->count();
        $ldcomplete = $this->checklist()->get()->whereIn('ld_status', ['Meets criterion', 'N/A', 'Does not meet criterion'])->where('heading', '<>', '1')->count();
        return $coordcomplete + $peerreviewcomplete + $ldcomplete;
    }

    public function checklist()
    {
        return $this->hasMany('App\Review_instances_checklist_items', 'review_instances_id', 'id')->orderBy('order');
    }


    public function review_instances_outcomes()
    {
        return $this->hasOne('App\Review_instances_outcomes', 'review_instances_id', 'id');
    }

    public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = date_create_from_format('j/m/Y', strval($value))->format('Y-m-d H:i:s');
    }

    public function getStartDateAttribute($value)
    {

        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('j/m/Y');

    }

    public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = date_create_from_format('j/m/Y', strval($value))->format('Y-m-d H:i:s');
    }

    public function getEndDateAttribute($value)
    {

        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('j/m/Y');

    }

    public function override_coordinator()
    {
        return $this->belongsTo('App\User', 'override_coordinator_id', 'id');
    }

    public function peer_reviewer()
    {
        return $this->belongsTo('App\User', 'peer_reviewer_id', 'id');
    }

    public function ld()
    {
        return $this->belongsTo('App\User', 'ld_id', 'id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Units', 'units_id', 'id');
    }

    public function reviewed_offering()
    {
        return $this->belongsTo('App\Offering', 'reviewed_offering_id', 'id');
    }

    public function media()
    {
        return $this->hasMany('App\Review_instances_media', 'review_instances_id', 'id');
    }

    public function log()
    {
        return $this->hasMany('App\Review_instances_notes', 'review_instances_id', 'id');
    }

    public function emaillog()
    {
        return $this->hasMany('App\Emails_log', 'instance_id', 'id')->where(function ($query) {
            $query->where('context', 'unitreview');
        });
    }


    // filter- a way of finding coordinators
    public function scopeCoordinators($query, $coordinatorid)
    {
        $query
            ->where(function ($query) use ($coordinatorid) {
                $query->whereHas('reviewed_unit_instance', function ($q) use ($coordinatorid) {
                    $q->whereIn('coordinator_id', $coordinatorid);

                });
            });
        return $query;
    }

    // filter- a way of finding any reviews associated with the currently logged on user
    public function scopeReviewsforuser($query, $userid)
    {
        $query
            // coordinators
            ->where(function ($query) use ($userid) {
                $query->whereHas('reviewed_unit_instance', function ($q) use ($userid) {
                    // $q->whereHas('conditions_lookup', function ($q2) use ($params) {
                    $q->whereIn('coordinator_id', $userid);
                    //});
                });
            })
            // peer reviewers
            ->orWhere(function ($query) use ($userid) {
                $query->where('peer_reviewer_id', $userid);
            })
            // learning designers
            ->orWhere(function ($query) use ($userid) {
                $query->where('ld_id', $userid);
            });
        return $query;
    }


}
