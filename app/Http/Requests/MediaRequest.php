<?php

namespace Illuminate\Http\Request;

use Illuminate\Http\Request;

class MediaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *These rules can be dynamic
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
