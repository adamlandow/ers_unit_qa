<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function username()
    {
        return 'username';
    }

    use AuthenticatesUsers {
        // expose the login method from AuthenticatesUsers
        // method as exposedmethod
        login as parentlogin;
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    //override login, insert ldap here
    public function login(\Illuminate\Http\Request $data)
    {
        $input = $data->all();
        //     dd($input);

        // LDAP here
        //if (function_exists('ldap-connect')) {
        //  dd(config('app.LDAP_AUTH_SEARCHPARAM'));
        try {
            // log on to AD here
            // update: added ability to look at another server
            //  dd(config('app.LDAP_AUTH_SEARCHPARAM'));
            $adldap = ldap_connect(config('app.LDAP_AUTH_SERVER')) or die("cannot connect to " . config('app.LDAP_AUTH_SERVER'));
            // set options for AD
            ldap_set_option($adldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($adldap, LDAP_OPT_REFERRALS, 0);
            // test against AD
            //if (@ldap_bind($adldap, $input['username'] . '@ad.une.edu.au', $input['password'])) {
            if (@ldap_bind($adldap, config('app.LDAP_AUTH_SEARCHPARAM') . "={$input['username']}" . config('app.LDAP_AUTH_SUFFIX'), $input['password'])) {

                // get some details to update the user table
                $ldap_base_dn = config('app.LDAP_AUTH_BASE_DN');
                $search_filter = config('app.LDAP_AUTH_SEARCHPARAM') . "={$input['username']}";
                $attributes = array();
                $attributes[] = 'displayName';
                $attributes[] = 'mail';
                $result = ldap_search($adldap, $ldap_base_dn, $search_filter, $attributes);
                $entries = ldap_get_entries($adldap, $result);
                //        dd($entries);
                // check to see if the user exists in the users table
                $user = User::where('username', $input['username'])->first();
                if (isset($user)) {
                    // update if it's found
                    $user->name = $entries[0]['displayname'][0];
                    $user->email = $entries[0]['mail'][0];
                    $user->password = bcrypt($input['password']);
                    $user->save();
                }

            }
            return $this->parentlogin($data);
        } catch (Exception $e) {
            // something went wrong with AD
            dd($e);
        }
//        } else {
//            return $this->login($data);
//        }
    }

    // tap into the LDAP server here
    public static function getDetailsByUsername($username)
    {
        // LDAP here
        try {

            $entries = [];
            // log on to AD here
            // update: added ability to look at another server
            $adldap = ldap_connect(config('app.LDAP_AUTH_SERVER')) or die("cannot connect to " . config('app.LDAP_AUTH_SERVER'));

            // set options for AD
            ldap_set_option($adldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($adldap, LDAP_OPT_REFERRALS, 0);

            // test against AD
            //if (@ldap_bind($adldap, $input['username'] . '@ad.une.edu.au', $input['password'])) {


            if (@ldap_bind($adldap, env('LDAP_ADMINUSER') . env('LDAP_AUTH_SUFFIX'), env('LDAP_ADMINUSERPASS'))) {
                // get some details to update the user table
                $ldap_base_dn = config('app.LDAP_AUTH_BASE_DN');
                $search_filter = config('app.LDAP_AUTH_SEARCHPARAM') . "={$username}";
                //$search_filter = env('LDAP_AUTH_SEARCHPARAM') . "={$username}";
                $attributes = array();
                $attributes[] = 'displayName';
                $attributes[] = 'mail';
                $result = ldap_search($adldap, $ldap_base_dn, $search_filter, $attributes);
                $entries = ldap_get_entries($adldap, $result);

                if ($entries['count'] > 0) {
                    return $entries;
                }

            }


            // second server
            $adldap = ldap_connect(env('LDAP_AUTH_SERVER2')) or die("cannot connect to " . env('LDAP_AUTH_SERVER2'));
            if (@ldap_bind($adldap, env('LDAP_ADMINUSER2') . env('LDAP_AUTH_SUFFIX2'), env('LDAP_ADMINUSERPASS2'))) {

                // get some details to update the user table
                $ldap_base_dn = env('LDAP_AUTH_BASE_DN2');
                $search_filter = env('LDAP_AUTH_SEARCHPARAM2') . "={$username}";
                $attributes = array();
                $attributes[] = 'displayName';
                $attributes[] = 'mail';
                $result = ldap_search($adldap, $ldap_base_dn, $search_filter, $attributes);
                $entries = ldap_get_entries($adldap, $result);

                if ($entries['count'] > 0) {
                    return $entries;
                }

            }
            return $entries;

        } catch (Exception $e) {
            // something went wrong with AD
            dd($e);
        }
        //dd($input);
    }


}
