<?php

namespace App\Http\Controllers;

use App\Offering;
use App\Sortable_offering;
use Illuminate\Http\Request;
use RobTrehy\LaravelUserPreferences\UserPreferences;

class ReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function index(\Illuminate\Support\Facades\Request $request){
        $input = $request::all();
        // teaching period persistence
//        if (isset($input['teachingperiod'])) {
//            session(['teachingperiod' => $input['teachingperiod']]);
//        }
//        $filter = [];
//        if (session()->has('teachingperiod') && session('teachingperiod') > 0) {
//            $filter['teaching_period_id'] = session('teachingperiod');
//        }


        $teachingperiodfilter = [];
        //  session(['teachingperiod' => []]);
        // little fix
        if (isset($input['teachingperiod'])) {
            session(['teachingperiod' => $input['teachingperiod']]);
            UserPreferences::set('reports_teachingperiod', $input['teachingperiod']);
        }
        //session(['teachingperiod' => []]);
        // session(['schoolid' => []]);
        //  dd(session('teachingperiod'));
//
        if (UserPreferences::has('reports_teachingperiod')) {
            session(['teachingperiod' => UserPreferences::get('reports_teachingperiod')]);
        }
        if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {
            //$teachingperiodfilter = explode(',',session('teachingperiod'));
            $teachingperiodfilter = session('teachingperiod');
        }

        //
        $teachingperiods = \App\Teaching_period::orderBy('year', 'desc')->get();
        $schools = \App\School::where('include_in_list', '=', 'true')->where('faculty_id', '=', 2)->get();

//dd($schools);

        $facultystats =['total'=>0,
            'in_progress'=>0,
            'complete'=>0];
        $reports = [];
//dd($teachingperiodfilter);
        // get stats for each school
        foreach ($schools as $school){
//dd(Offering::with('checklist')->get());
            $total_query = Offering::whereNull('is_template')->whereNull('archived_at')->doesntHave('units_combined')->hasSchool($school->id);//->where($filter)
                //->get()->count();
            if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {
                $total_query->whereIn('teaching_period_id', $teachingperiodfilter);

            }

            $total = $total_query->get()->count();

            $complete_query =Offering::whereNull('is_template')->whereNull('archived_at')->doesntHave('units_combined')->hasSchool($school->id)->where('status', '=', 'complete');
               if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {
                   $complete_query->whereIn('teaching_period_id', $teachingperiodfilter);
               }
            $complete = $complete_query->get()->count();

            $in_progress_query = Offering::whereNull('is_template')->whereNull('archived_at')->doesntHave('units_combined')->hasSchool($school->id)->where('complete_percent', '>', 0);
                if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {
                    $in_progress_query->whereIn('teaching_period_id', $teachingperiodfilter);
                }
            $in_progress = $in_progress_query->get()->count()-$complete;
//            if($school->id == 70){
//                dd($complete);
//            }
            $reports[] = ['school_id'=>$school->id,
                'total'=>$total,
                'in_progress'=>$in_progress,
                'complete'=>$complete];
          //  dd($report[0]->)
         //   dd(count($reports));
            $facultystats['total']+=$reports[count($reports)-1]['total'];
            $facultystats['in_progress']+=$reports[count($reports)-1]['in_progress'];
            $facultystats['complete']+=$reports[count($reports)-1]['complete'];
        }

       // dd($reports);
        return view('unit_setup.reports.view')
            ->with('teachingperiods', $teachingperiods)
            ->with('reports', $reports)
            ->with('facultystats', $facultystats)
            ->with('schools', $schools);
            ;

    }
}
