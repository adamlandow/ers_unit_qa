<?php

namespace App\Http\Controllers;

use App\Emails_log;
use App\Emails_template;
use App\Mail\UnitReviewEmail;
use App\Review_instances;
use App\Review_instances_checklist_items;
use App\Review_instances_media;
use App\Review_instances_outcomes;
use App\Review_instances_outcomes_assessment_tasks;
use App\Review_instances_outcomes_learning_outcomes;
use App\Sortable_review_instances;
use App\Offering;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpWord\Writer\Word2007;
use Request;

class ReviewInstancesOutcomesController extends Controller
{
    //
    public function __construct()
    {

        $this->middleware('auth');
    }

    public function index()
    {
        return null;
    }

    public function show($id)
    {
        return null;
    }

    public function store(Request $request)
    {
        $input = Request::all();
        //dd($input);
        $instance = \App\Review_instances_outcomes::create($input);

        $status = strval($instance->id);
        $response = array(
            'status' => $status,
        );
        return $response;
    }


    public function update(Request $request)
    {

        $input = Request::all();
        //   dd($input);
        $instance = \App\Review_instances_outcomes::findOrFail($input['id']);
//dd($instance);
        $status = strval($instance->update($input));
        $response = array(
            'status' => $status,
        );
        return $response;
    }

    public function destroy(Request $request)
    {
        $input = $request::all();
        //dd($input);
        $status = strval(\App\Review_instances::destroy($input['id']));
        $response = array(
            'status' => $status,
        );
        return $response;
    }

    public function loupdate(Request $request)
    {
        $input = Request::all();

        // get the IDs of existing entries
        $existingids = Review_instances_outcomes_learning_outcomes::where('review_instances_outcomes_id', '=', $input['id'])->pluck('id')->toArray();

        $newids = [];
        if (isset($input['items'])) {
            $items = json_decode($input['items'])->items;
            //     dd($items);

            foreach ($items as $item) {
                $params = [];
                $params['text'] = $item->text;
                $params['review_instances_outcomes_id'] = $input['id'];
                $newitem = Review_instances_outcomes_learning_outcomes::updateOrCreate(['id' => $item->id], $params);
                $newids[] = $newitem->id;

            }

            // work out what to delete, then get rid of them
            $deleteids = array_diff($existingids, $newids);
            Review_instances_outcomes_learning_outcomes::destroy($deleteids);
        }

        $response = array(
            'status' => 0,
        );
        return $response;
    }

    public function assessmentupdate(Request $request)
    {
        $input = Request::all();

        // get the IDs of existing entries
        $existingids = Review_instances_outcomes_assessment_tasks::where('review_instances_outcomes_id', '=', $input['id'])->pluck('id')->toArray();

        $newids = [];
        if (isset($input['items'])) {
            $items = json_decode($input['items'])->items;
        //         dd($items);
            foreach ($items as $item) {
                $params = [];
                $params['review_instances_outcomes_id'] = $input['id'];
                $params['must_complete'] = $item->must_complete;
                $params['component'] = $item->component;
                $params['exam_length'] = $item->exam_length;
                $params['weight'] = $item->weight;
                $params['word_count'] = $item->word_count;
                $params['notes'] = $item->notes;
                $params['relates_to_lo'] = $item->relates_to_lo;
                $newitem = Review_instances_outcomes_assessment_tasks::updateOrCreate(['id' => $item->id], $params);
                $newids[] = $newitem->id;

            }

            // work out what to delete, then get rid of them
            $deleteids = array_diff($existingids, $newids);
            Review_instances_outcomes_assessment_tasks::destroy($deleteids);
        }

        $response = array(
            'status' => 0,
        );
        return $response;
    }


}
