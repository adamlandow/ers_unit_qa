<?php

namespace App\Http\Controllers;

use App\Course;
use App\Discipline;
use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Psy\Util\Json;

class UnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     * @return type
     */
    public function index(Request $request)
    {

        $params = $request->all();
        $disciplines = Discipline::all();
        if (isset($params['search'])) {
            $units = Unit::where('unit_code', 'like', "%{$params['search']}%")
                ->orWhere('description', 'like', "%{$params['search']}%");
            $unit = $units->orderBy('unit_code')->paginate(20);
        }else {
            $unit = Unit::orderBy('unit_code')->paginate(20);
        }
        return view("setup.unit.list")
            ->with('units', $unit)
            ->with('disciplines', $disciplines);
    }

    public function search(Request $request)
    {
        $input = $request->all();

        $results = \App\Unit::where('description', 'like', "%{$input['searchstr']}%");

        return $results->id;
    }

    public function create(Request $request)
    {
        $input = $request->all();
        // check that this unit doesn't already exist...
        if (Units::where('unit_code', $input['unit_code'])->count() == 0) {
            $newentry = \App\Unit::create($input);
        } else {
            $newentry = Unit::where('unit_code', $input['unit_code'])->first();
            $newentry->update($input);
        }
        return $newentry->id;
    }


    /**
     * Updates the record
     * @param $id
     * @param Request $request
     * @return $this
     */
    public function update(Request $request)
    {

        $input = $request->all();
        //dd($input);
        $unit = \App\Unit::findOrNew($input['id']);

        $status = strval($unit->update($input));
//        if (isset($input['course_ids'])) {
//            $courseupdate = $unit->courses()->sync($input['course_ids']);
//        }
//        $courseupdatesuccess = (count($courseupdate['attached'] > 0) || count($courseupdate['detached'] > 0) || count($courseupdate['attached'] > 0));
        // update
        // $status = ($unit->update($input) || $courseupdatesuccess);
        $response = array(
            'status' => $status ? '1' : '0',
        );
        return $response;
    }


    public function store(Request $request)
    {
        $input = $request->all();
        return \App\Unit::create($input)->id;
    }

    /**
     * read CAUC for units information. Saves having to make a new unit entry
     *
     * @param string $unitcode the unit code to lookup
     * @return \App\Unit a newly created unit model. Maybe the ID is more efficient?
     */
    public static function addFromCAUC($unitcode)
    {
        // first, make sure it's not already there
//dd('Adding from caud:'.$unitcode);
        if(Unit::where('unit_code', $unitcode)->exists()){
            return Unit::where('unit_code', $unitcode)->first();
        }

        // else, go to CAUC and get it
        $url = "https://my.une.edu.au/courses/units/{$unitcode}.json";

        try {

            $str = file_get_contents($url);
            $json = json_decode($str, true);
            $newentry = \App\Unit::create(['unit_code' => $unitcode,
                'description' => $json['title'],
                'color' => '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT)]);

        } catch (\Exception $err) {
            //  dd($err);
            return -1;
        }
        return $newentry;
    }


    // unit code lookup, augmented from CAUC
    public function getUnitCodeLookup(Request $request)
    {
        $input = $request->all();
        $searchstr = $input['q'];

// non-cauc (ie already in the system)
        $resultsArr = [];
        $localresults = Unit::where('unit_code', 'LIKE', "%$searchstr%")->get();
        foreach ($localresults as $localresult) {
            $resultsArr [] = array("id" => $localresult->id, "text" => $localresult->unit_code);
        }

        // look at CAUC if there's no results
        if (count($resultsArr) < 1) {
            $searchurl = "https://my.une.edu.au/courses/units/bykeyword?export=true&unitKeyword={$searchstr}";
            // load the search results into a usable object
            $doc = new \DOMDocument();
            // search page is not well formed!
            $doc->recover = true;
            $doc->strictErrorChecking = false;
            @$doc->loadHTMLFile($searchurl);
            // find the results- they all have class 'coursename'
            $table = $doc->getElementById('unitTable');

            // get results

            $i = 0;
            foreach ($table->getElementsByTagName('tr') as $tr) {
                $i++;
                if ($i > 2) {
                    if (isset($tr->getElementsByTagName('td')->item(0)->nodeValue)) {
                        $code = preg_replace('/\s+/', ' ', $tr->getElementsByTagName('td')->item(0)->nodeValue);
                        // add to the array if there's not a local match
                        if (Unit::where('unit_code', $code)->get()->count() == 0) {
                            //@TODO should we add this to the local list?

                            $resultsArr[] = array("id" => "-1", "text" => trim(preg_replace('/\s+/', ' ', $tr->getElementsByTagName('td')->item(0)->nodeValue)));;
                        }
                    }
                }
            }
        }
        return $resultsArr;
    }


    public
    function ajaxstore(Request $request)
    {
        $input = $request->all();
        $status = \App\Unit::create($input)->id;
        return array(
            'status' => ($status > 0) ? '0' : '1',
        );

    }

    public
    function show($id)
    {
        $unit = \App\Unit::findOrFail($id);
        $courses = [];
        $courselist = Course::all();
        $disciplines = Discipline::all();

//        foreach ($unit->courses as $course) {
//            $courses[] = $course->pivot->course_lookup_id;
//        }
        $unit['course_ids[]'] = implode(',', $courses);
        return view("setup.unit.view")->with('unit', $unit)
            ->with('courselist', $courselist)
            ->with('disciplines', $disciplines)
            ;
        //return \App\Units::with('courses')->findOrFail($id);
    }


    public
    function destroy(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        return \App\Unit::destroy($input['id']);
    }

    public
    function ajaxdestroy(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        return \App\Unit::destroy($input['id']);
    }

    ///////////////////////////////////////////////////////////////////////////////
    ///
    /// Search results for select2 controls
    ///
    /// //////////////////////////////////////////////////////////////////////////

    public function searchforselect2(\Illuminate\Support\Facades\Request $request)
    {
        $input = $request::all();
        $search = $input['q']['term'];
        //  dd($search);
        $returnStr = [];
        $units = Unit::where('unit_code', 'LIKE', "%{$search}%")->get();
        foreach ($units as $unit){
            $returnStr[]= ['id'=>$unit->id, 'text'=>"{$unit->unit_code}" ];
        }
        return \json_encode(['results'=>$returnStr]);
        //dd($offerings);
    }
}
