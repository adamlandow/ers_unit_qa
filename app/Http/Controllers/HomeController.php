<?php

namespace App\Http\Controllers;

use Adldap\Laravel\Facades\Adldap;
use App\Review_instances;
use App\Sortable_review_instances;
use App\Offering;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RobTrehy\LaravelUserPreferences\UserPreferences;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Support\Facades\Request $request)
    {
        $input = $request::all();
        // teaching period persistence
        if (isset($input['teachingperiod'])) {
            session(['teachingperiod' => $input['teachingperiod']]);
            UserPreferences::set('index_teachingperiod',$input['teachingperiod']);
        }
        $filter = [];
        if(UserPreferences::has('index_teachingperiod')){
            session(['teachingperiod' => UserPreferences::get('index_teachingperiod')]);
        }
      //  dd(session('teachingperiod'));
        if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {
            $filter = session('teachingperiod');
        }

        //
        $teachingperiods = \App\Teaching_period::orderBy('year', 'desc')->get();
        // get reviews that logged on user is as a coordinator
        $myunitreviewsascoordinator = Sortable_review_instances::reviewsforuser([Auth::user()->id])->get();
        //dd($myunitreviewsascoordinator);
        //$myunitreviewsascoordinatorlist = Review_instances::coordinators([Auth::user()->id])->where('override_coordinator_id', '=', Auth::user()->id)->orWhere();
        //$myunitreviewsascoordinator = $myunitreviewsascoordinator->get();
        //dd($myunitreviewsascoordinator);
        // get reviews that this user is a peer reviewer
        $myunitreviewsaspeerreviewer = Review_instances::all()->where('peer_reviewer_id', Auth::user()->id);
         //get reviews for which this user is the Learning Designer
        $myunitreviewsasld = Review_instances::all()->where('ld_id', Auth::user()->id);

        // my unit setups

        $myofferings =  Offering::whereNull('is_template')->whereIn('teaching_period_id', $filter)->doesntHave('units_combined')->orderBy('teaching_period_id', 'desc')->unitsforcoordinatororeso([Auth::user()->id])->get();
        $complete =Offering::whereNull('is_template')->whereNull('archived_at')->whereIn('teaching_period_id', $filter)->where('status', '=', 'complete')
            ->doesntHave('units_combined')->unitsforcoordinatororeso([Auth::user()->id])->get()->count();
        $in_progress = Offering::whereNull('is_template')->whereNull('archived_at')->whereIn('teaching_period_id', $filter)
            ->doesntHave('units_combined')->unitsforcoordinatororeso([Auth::user()->id])->get()->where('complete_percent', '>', 0)->count();

        $myofferingcount = $myofferings->count();



        return view('home')
            ->with('myunitreviewsascoordinator', $myunitreviewsascoordinator)
            ->with('teachingperiods', $teachingperiods)
            ->with('myunitreviewsaspeerreviewer', $myunitreviewsaspeerreviewer)
            ->with('myunitreviewsasld', $myunitreviewsasld)
            ->with('myofferings', $myofferings)
            ->with('in_progress', $in_progress-$complete)
            ->with('complete', $complete)
        ->with('myofferingscount', $myofferingcount);
    }
}
