<?php

namespace App\Http\Controllers;

use App\Contact_history;

use App\Event;
use Illuminate\Support\Facades\Gate;
use Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ContactRequest;

class EventTypeLookupController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * List all contacts.
     * @todo make some filters
     * @return type
     */
    public function index()
    {
        return null;
    }


    public function create(Request $request)
    {
        if (Gate::denies('is_admin')) {
            return 0;
        }

        $input = $request::all();

        $newentry = \App\Event_type_lookup::create($input);

        $response = array(
            'status' => 'success',
            'msg' => 'Setting created successfully',

        );
        return $newentry->id;
    }

    /**
     * Updates the record
     * @param $id
     * @param PatientRequest $request
     * @return $this
     */
    public function update(Request $request)
    {
        if (Gate::denies('is_admin')) {
            return 0;
        }
        $input = $request::all();
        $contact = \App\Event_type_lookup::findOrNew($input['id']);

        $status = strval($contact->update($input));
        //dd($input);
        $response = array(
            'status' => $status,
        );
        return $response;
    }

    /**
     * Using explicit request. Does an auth check! might be handy :)
     * @param \App\Http\Requests\CreatePatientRequest $request
     * @return type
     */
    public function store(Request $request)
    {
        if (Gate::denies('is_admin')) {
            return 0;
        }
        $input = $request::all();
        return \App\Event_type_lookup::create($input)->id;

    }

    public function show($id)
    {
        if (Gate::denies('is_admin')) {
            return 0;
        }
        return \App\Event_type_lookup::findOrFail($id);
    }


    public function destroy(Request $request)
    {

        $input = $request::all();

// destroy all
        return \App\Event_type_lookup::destroy($input['id']);
    }
}
