<?php

namespace App\Http\Controllers;


use App\Emails_template;
use App\Jobs\SendSetupEmail;
use App\Mail\UnitSetupEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Mews\Purifier\Facades\Purifier;
use App\Emails_log;
use Request;

class OfferingChecklistsItemsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        return \App\Offering_checklist_items::find($id);
    }

    public function create(Request $request)
    {
        $input = $request::all();


        $newentry = \App\Offering_checklist_items::create($input);
        $response = array(
            'status' => '0',
            'id' => $newentry->id,
            'msg' => 'Item created successfully',

        );
        return $response;
    }

    public function store(Request $request)
    {
        $input = $request::all();

        if (isset($input['text']) && (strlen($input['text']) > 0)) {
            // do a little bit of formatting and sanitizing on text. Because we're allowing rich text we need to do this...
            // sanitise
            Purifier::clean($input['text']);
            // add a 'note' class to tables
            $doc = new \DOMDocument();
            $doc->loadHTML($input['text']);
            $tables = $doc->getElementsByTagName('table');
            foreach ($tables as $table) {
                $table->setAttribute('class', 'note');
            }
            $input['text'] = $doc->saveHTML();
            $input['text'] = preg_replace('~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i', '', $input['text']);
        }
// get the highest order,
        $highestorder = \App\Offering_checklist_items::where('offering_id', $input['offering_id'])->max('order');
        // make this entry the last on the list
        $input['order'] = $highestorder + 1;
        // perform entry
        $newentry = \App\Offering_checklist_items::create($input);
        // return response
        $response = array(
            'status' => '0',
            'id' => $newentry->id,
            'msg' => 'Item created successfully',

        );
        return $response;
    }

    /**
     * Updates the record
     * @param $id
     * @param PatientRequest $request
     * @return $this
     */
    public
    function update(Request $request)
    {
        $input = $request::all();

        $item = \App\Offering_checklist_items::findOrNew($input['id']);
        if (isset($input['description'])) {
            $item->description = $input['description'];
        }
        if (isset($input['text']) && (strlen($input['text']) > 0)) {
            // do a little bit of formatting and sanitizing on text. Because we're allowing rich text we need to do this...
            // sanitise
            Purifier::clean($input['text']);
            // add a 'note' class to tables
            $doc = new \DOMDocument();
            $doc->loadHTML($input['text']);
            $tables = $doc->getElementsByTagName('table');
            foreach ($tables as $table) {
                $table->setAttribute('class', 'note');
            }
            $input['text'] = $doc->saveHTML();
            $input['text'] = preg_replace('~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i', '', $input['text']);
        }

        if (!isset($input['trigger_email'])) {
            $input['trigger_email'] = null;
            $input['trigger_email_id'] = null;
        }

        $status = strval($item->update($input));

        $response = array(
            'status' => $status,

        );
        return $response;
    }

    /**
     * Updates the record
     * @param $id
     * @param PatientRequest $request
     * @return $this
     */
    public
    function updatestatus(Request $request)
    {
        $input = $request::all();

        $item = \App\Offering_checklist_items::findOrNew($input['id']);
        if (isset($input['status'])) {
            $input['status'] = ($input['status'] == 'true') ? '1' : 0;
        }

        $status = strval($item->update($input));
        $emailstatus = '0';
        // update completion cache- means we don't have to do maths in the view we use to list the offerings
        $offering = $item->offering;
      //  dd($offering);
        // $offering = \App\Offering::findOrFail($input['id']);

        // working out completion percentage
        $percentComplete = round(($offering->checklistcomplete / ($offering->checklist()->get()->where('heading', '<>', '1')->count())) * 100);
      //  round(($instance->checklistcomplete/($instance->checklist()->get()->where('heading', '<>', '1')->count()))*100
     //   dd($offering->checklistcomplete);
        if (!is_nan($percentComplete)) {
            $offering->complete_percent = $percentComplete;
            $offering->save();
        }

        // send an email if specified
        if (($item->trigger_email == 1) && ($input['status'] == '1')&&isset($item->trigger_email_template)) {
//            dd('Sending email');
            $template = Emails_template::find($item->trigger_email_id);
          //  $offering = $item->offering;
            try {
                // mail to unit coordinator
                $email = new UnitSetupEmail($offering, $template);
               // dd($email->getfulltext());
                // dispatch to the job queue
                dispatch(new SendSetupEmail($email, $offering->coordinator->id, Auth::user()->id, $template, $offering->id));
                //dispatch(new SendSetupEmail($email, (isset($input['testing']) ? Auth::user()->id : $unit->coordinator->id), Auth::user()->id, $template, $id));
                // log success
                $emailstatus = '0';
                $log = new Emails_log();
                $log->email_id = $template->id;
                $log->instance_id = $offering->id;
                $log->sent_to_id = $offering->coordinator->id;
                $log->sent_by_id = Auth::user()->id;
                $log->context = 'offeringsetup';
                $log->fulltext = $email->getfulltext();
                $log->save();
            } catch (\Exception $e) {
                // handle failure
                dd($e->getMessage());
                $emailstatus = '1';
            }
        }


        $response = array(
            'emailstatus' => $emailstatus,
            'status' => $status,
            'progress' => $item->offering->checklistcomplete
        );
        return $response;
    }

    // previews an email about to be sent
    public function previewemail(Request $request)
    {
        $input = $request::all();
        $item = \App\Offering_checklist_items::findOrNew($input['id']);

        $offering = $item->offering;
        $template = $item->trigger_email_template;
        $email = new UnitSetupEmail($offering, $template);
        $response = array(
            'subject' => $email->getsubject(),
            'fulltext' => $email->getfulltext(),
            'status'=>0
        );
        return ($response);

    }

    public
    function reorder(Request $request)
    {
        $input = $request::all();
        foreach (json_decode($input['order']) as $item) {
            $checklistitem = \App\Offering_checklist_items::find($item->id);
            $checklistitem->order = $item->order;
            $checklistitem->save();
        }
        return array(
            'status' => 0,
        );
    }


    public
    function destroy(Request $request)
    {
        $input = $request::all();
        //dd($input);
        $status = strval(\App\Offering_checklist_items::destroy($input['id']));
        $response = array(
            'status' => $status,
        );
        return $response;
    }

}
