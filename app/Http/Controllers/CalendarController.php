<?php

namespace App\Http\Controllers;


use App\CustomClasses\iCalReader;
use App\Discipline_lookup;
use App\Event;
use App\Location_lookup;
use App\Event_type_lookup;
use App\Unit;
use App\Offering;
use App\Units_lookup;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CalendarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $instances = Offering::all();
        $types = Event_type_lookup::all();
        $locations = Location_lookup::all();
        return view('calendar.index')->with('instances', $instances)->with('types', $types)->with('locations', $locations);
//
//        $review = \App\Review_instances::findOrFail('1');
//        $users = \App\User::all();
//        $unit_instances = Offering::all();
//
//              $pdf = \PDF::loadView('reviews.view', compact('review', 'users', 'unit_instances'));
//        return $pdf->download('invoice.pdf');
    }


    public function show()
    {
        return null;
    }

    public function importindex()
    {

        $units = Unit::all();
        //dd($units);
        return view('import.index')->with('units', $units);
    }

    public function importicalfromurl($unitid, $url)
    {

        dd($url);
       // if (!empty($request->file('userfile'))) {

            $count = 0;
            // handle file
            // get the file. @TODO do some validation to ensure it's actually a calendar file
            $ical = new  iCalReader($url);
            $events = $ical->events();
            foreach ($events as $event) {
//            print($event['DTSTART']);
//            dd(Carbon::createFromFormat('Ymd\THis', $event['DTSTART']));
                // check to see that there's not already an event with that description and time
                preg_match('/^P([0-9]{1,2}[W])?([0-9]{1,2}[D])?([T]{0,1})?([0-9]{1,2}[H])?([0-9]{1,2}[M])?([0-9]{1,}[S])?/', $event['DURATION'], $duration);

                $weeks = isset($duration[1]) ? str_replace('W', '', $duration[1]) : 0;
                $days = isset($duration[2]) ? str_replace('D', '', $duration[2]) : 0;
                $hours = isset($duration[4]) ? str_replace('H', '', $duration[4]) : 0;
                $minutes = isset($duration[5]) ? str_replace('M', '', $duration[5]) : 0;
                $seconds = isset($duration[6]) ? str_replace('S', '', $duration[6]) : 0;

                // Convert seconds to hours, minutes, and seconds
                if ($seconds > 60) {
                    $rem_seconds = $seconds % 60;
                    $minutes = $minutes + (($seconds - $rem_seconds) / 60);
                    $seconds = $rem_seconds;
                }
                if ($minutes > 60) {
                    $rem_minutes = $minutes % 60;
                    $hours = $hours + (($minutes - $rem_minutes) / 60);
                    $minutes = $rem_minutes;
                }

                $duration_secs = ($weeks * 60 * 60 * 24 * 7) + ($days * 60 * 60 * 24) + ($hours * 60 * 60) + ($minutes * 60) + ($seconds);
                // get the title
                $title = [];
                preg_match_all("/(?<=Title - )(.*?)(?= Type)/s", @$event['DESCRIPTION'], $title);
                // get the type
                $typeArr = [];
                $typestr = '';
                preg_match_all("/(?<=Type - )(.*)/s", @$event['DESCRIPTION'], $typeArr);
                $typestr = $typeArr[0][0];
                //dd(@$event['LOCATION']);
// get the location URL
                if (isset($event['LOCATION'])) {
                    $locationurlArr = explode('&quot;;', $event['LOCATION']);
                    //   preg_match_all("/(?<=Title - )(.*?)(?= Type)/s", @$event['LOCATION'], $locationurlArr);
//            dd($locationurlArr);
                    $locationurl = 'https:' . $locationurlArr[0];
                    $locationtitleArr = [];
                    preg_match_all("/LANGUAGE=en:(.*)/s", @$event['LOCATION'], $locationtitleArr);

                }
                //      dd(count($locationtitleArr[1]));
                //  dd($locationtitleArr);
                //       $description =
//perform the check. If there's a match, then we don't duplicate
                $check = Event::where('start_timestamp', Carbon::createFromFormat('Ymd\THis', $event['DTSTART']))
                    ->where('duration', $duration_secs)
                    ->where('description', $title[0][0])
                    ->where('unit_id', $unitid);
                //  dd(count($event['LOCATION']));
                //->where('type_id', Type_lookup::where('text', $typestr)->first()->id);
                if ($check->count() < 1) {
                    $newEvent = new Event();
                    $newEvent->description = $title[0][0];
                    $newEvent->unit_id = $unitid;
                    $newEvent->type_id = Event_type_lookup::firstOrCreate(['text' => $typestr,])->id;
                    $newEvent->start_timestamp = Carbon::createFromFormat('Ymd\THis', $event['DTSTART']);
                    $newEvent->duration = $duration_secs;

                    if (isset($event['LOCATION'])) {
                        try {
                            if ($event['LOCATION'] == 'Off Campus') {
                                $newEvent->onoffcampus = "OFF";
                            } else {
                                $newEvent->onoffcampus = "ON";
                            }
                            $newEvent->location_id = Location_lookup::firstOrCreate(['text' => count($locationtitleArr[1]) > 0 ? $locationtitleArr[1][0] : $event['LOCATION'], 'url' => $locationurl])->id;
                        } catch (\Exception $e) {
                            // dd($e);
                            dd($event);
                            dd(count($locationtitleArr[1]));
                            dd(count($event['LOCATION']));
                        }
                    }
                    //dd($newEvent);
                    $newEvent->save();
                }
                $count++;
            }

            //$units = Unit::all();
            //dd($units);
            //return view('import.index')->with('units', $units)->with('count', $count);
        return $count;
    }

    /**
     * Import en masse from a CSV file. This works by reading UNE's calendar system, hardcoding a dependency on UNE's current system. Not ideal, but hey, whatever..
     * @param ImportRequest $request
     * @return $this
     */
    public function csvimport(ImportRequest $request)
    {
        if (!empty($request->file('csvfile'))) {
            $input = $request->all();
            $input["user_id"] = Auth::user()->id;
            $count = 0;
            $unitrow = 0;
            $descriptionrow = 0;
            $disciplinerow = 0;
            $yearrow = 0;
            $termrow = 0;
            $isfirstrow = true;
            // handle file
            // get the file. @TODO do some validation to ensure it's actually a CSV file
            $file = $request->file('csvfile');
            if (($handle = fopen($file, "r")) !== FALSE) {
                // loop through the CSV
                while (($data = fgetcsv($handle)) !== FALSE) {
                    // get the first row, work out the indices from teh headers
                    if ($isfirstrow) {
                        if (array_search('unit', $data) !== false) {
                            $unitrow = array_search('unit', $data);
                        }
                        if (array_search('description', $data) !== false) {
                            $descriptionrow = array_search('description', $data);
                        }
                        if (array_search('discipline', $data) !== false) {
                            $disciplinerow = array_search('discipline', $data);
                        }
                        if (array_search('year', $data) !== false) {
                            $yearrow = array_search('year', $data);
                        }
                        if (array_search('term', $data) !== false) {
                            $termrow = array_search('term', $data);
                        }
                        $isfirstrow = false;
                    } else {
                        // load the ical from UNE corresponding to this unit
                        $ical = new  iCalReader("https://my.une.edu.au/timetables/{$data[$yearrow]}/unit/{$data[$unitrow]}/T{$data[$termrow]}.ics");

                        // get the events from this ical
                        $events = $ical->events();

                        // a little segue: ensure that this unit exists and has the correct discipline
                        // work out discipline
                        $disciplineid = Discipline_lookup::firstOrCreate(['text' => $data[$disciplinerow]])->id;
                        // work out unit ID
                        $unitid = Unit::firstOrCreate(['code' => $data[$unitrow], 'description' => $data[$descriptionrow], 'year' => $data[$yearrow], 'term' => $data[$termrow], 'discipline_id' => $disciplineid])->id;

                        // loop through the event
                        foreach ($events as $event) {

                            // check to see that there's not already an event with that description and time
                            preg_match('/^P([0-9]{1,2}[W])?([0-9]{1,2}[D])?([T]{0,1})?([0-9]{1,2}[H])?([0-9]{1,2}[M])?([0-9]{1,}[S])?/', $event['DURATION'], $duration);

                            $weeks = isset($duration[1]) ? str_replace('W', '', $duration[1]) : 0;
                            $days = isset($duration[2]) ? str_replace('D', '', $duration[2]) : 0;
                            $hours = isset($duration[4]) ? str_replace('H', '', $duration[4]) : 0;
                            $minutes = isset($duration[5]) ? str_replace('M', '', $duration[5]) : 0;
                            $seconds = isset($duration[6]) ? str_replace('S', '', $duration[6]) : 0;

                            // Convert seconds to hours, minutes, and seconds
                            if ($seconds > 60) {
                                $rem_seconds = $seconds % 60;
                                $minutes = $minutes + (($seconds - $rem_seconds) / 60);
                                $seconds = $rem_seconds;
                            }
                            if ($minutes > 60) {
                                $rem_minutes = $minutes % 60;
                                $hours = $hours + (($minutes - $rem_minutes) / 60);
                                $minutes = $rem_minutes;
                            }

                            $duration_secs = ($weeks * 60 * 60 * 24 * 7) + ($days * 60 * 60 * 24) + ($hours * 60 * 60) + ($minutes * 60) + ($seconds);
                            // get the title
                            $title = [];
                            preg_match_all("/(?<=Title - )(.*?)(?= Type)/s", @$event['DESCRIPTION'], $title);
                            // get the type
                            $typeArr = [];
                            $typestr = '';
                            preg_match_all("/(?<=Type - )(.*)/s", @$event['DESCRIPTION'], $typeArr);

                            if (isset($typeArr[0][0])) {
                                $typestr = $typeArr[0][0];
                            }
                            // get the location URL
                            if (isset($event['LOCATION'])) {
                                $locationurlArr = explode('&quot;;', $event['LOCATION']);
                                $locationurl = 'https:' . $locationurlArr[0];
                                $locationtitleArr = [];
                                preg_match_all("/LANGUAGE=en:(.*)/s", @$event['LOCATION'], $locationtitleArr);

                            }

//perform the check. If there's a match, then we don't duplicate
                            $check = Event::where('start_timestamp', Carbon::createFromFormat('Ymd\THis', $event['DTSTART']))
                                ->where('duration', $duration_secs)
                                ->where('description', isset($title[0][0]) ? $title[0][0] : "")
                                ->where('unit_id', $unitid);
                            if ($check->count() < 1) {
                                $newEvent = new Event();
                                $newEvent->description = isset($title[0][0]) ? $title[0][0] : "";
                                $newEvent->type_id = Event_type_lookup::firstOrCreate(['text' => $typestr,])->id;
                                $newEvent->unit_id = $unitid;
                                $newEvent->start_timestamp = Carbon::createFromFormat('Ymd\THis', $event['DTSTART']);
                                $newEvent->duration = $duration_secs;

                                if (isset($event['LOCATION'])) {
                                    try {
                                        if ($event['LOCATION'] == 'Off Campus') {
                                            $newEvent->onoffcampus = "OFF";
                                        } else {
                                            $newEvent->onoffcampus = "ON";
                                        }
                                        $newEvent->location_id = Location_lookup::firstOrCreate(['text' => count($locationtitleArr[1]) > 0 ? $locationtitleArr[1][0] : $event['LOCATION'], 'url' => $locationurl])->id;
                                    } catch (\Exception $e) {
                                        // dd($e);
                                        dd($event);
                                        dd(count($locationtitleArr[1]));
                                        dd(count($event['LOCATION']));
                                    }
                                }
                                //dd($newEvent);
                                $newEvent->save();
                            }
                            $count++;
                        }
                    }
                }


                $units = Unit::all();
                //dd($units);
                return view('import.index')->with('units', $units)->with('count', $count);
            } else {
                $units = Unit::all();
                //dd($units);
                return view('import.index')->with('units', $units);
            }
        }
    }

    /**
     * Import from myUNE given a unit instance ID
     * @param Request $request
     * @return $this
     */
    public function webimport($unit_instance_id)
    {

        $instance = Offering::find($unit_instance_id);

        // load the ical from UNE corresponding to this unit
        //$ical = new  iCalReader("https://my.une.edu.au/timetables/{$instance->teaching_period->year}/unit/{$instance->unit->unit_code}/{$instance->teaching_period->teaching_period}.ics");
        $ical = new  iCalReader("https://my.une.edu.au/timetables/{$instance->teaching_period->year}/unit/{$instance->unit->unit_code}/{$instance->teaching_period->teaching_period}.ics");

        // get the events from this ical
        $events = $ical->events();

        // loop through the events
        foreach ($events as $event) {

            // check to see that there's not already an event with that description and time
            preg_match('/^P([0-9]{1,2}[W])?([0-9]{1,2}[D])?([T]{0,1})?([0-9]{1,2}[H])?([0-9]{1,2}[M])?([0-9]{1,}[S])?/', $event['DURATION'], $duration);

            $weeks = isset($duration[1]) ? str_replace('W', '', $duration[1]) : 0;
            $days = isset($duration[2]) ? str_replace('D', '', $duration[2]) : 0;
            $hours = isset($duration[4]) ? str_replace('H', '', $duration[4]) : 0;
            $minutes = isset($duration[5]) ? str_replace('M', '', $duration[5]) : 0;
            $seconds = isset($duration[6]) ? str_replace('S', '', $duration[6]) : 0;

            // Convert seconds to hours, minutes, and seconds
            if ($seconds > 60) {
                $rem_seconds = $seconds % 60;
                $minutes = $minutes + (($seconds - $rem_seconds) / 60);
                $seconds = $rem_seconds;
            }
            if ($minutes > 60) {
                $rem_minutes = $minutes % 60;
                $hours = $hours + (($minutes - $rem_minutes) / 60);
                $minutes = $rem_minutes;
            }

            $duration_secs = ($weeks * 60 * 60 * 24 * 7) + ($days * 60 * 60 * 24) + ($hours * 60 * 60) + ($minutes * 60) + ($seconds);
            // get the title
            $title = [];
            preg_match_all("/(?<=Title - )(.*?)(?= Type)/s", @$event['DESCRIPTION'], $title);
            // get the type
            $typeArr = [];
            $typestr = '';
            preg_match_all("/(?<=Type - )(.*)/s", @$event['DESCRIPTION'], $typeArr);

            if (isset($typeArr[0][0])) {
                $typestr = $typeArr[0][0];
            }
            // get the location URL
            if (isset($event['LOCATION'])) {
                $locationurlArr = explode('&quot;;', $event['LOCATION']);
                $locationurl = 'https:' . $locationurlArr[0];
                $locationtitleArr = [];
                preg_match_all("/LANGUAGE=en:(.*)/s", @$event['LOCATION'], $locationtitleArr);

            }

//perform the check. If there's a match, then we don't duplicate
            $check = Event::where('start_timestamp', Carbon::createFromFormat('Ymd\THis', $event['DTSTART']))
                ->where('duration', $duration_secs)
                ->where('description', isset($title[0][0]) ? $title[0][0] : "")
                ->where('units_instance_id', $instance->id);
            if ($check->count() < 1) {
                $newEvent = new Event();
                $newEvent->description = isset($title[0][0]) ? $title[0][0] : "";
                $newEvent->type_id = Event_type_lookup::firstOrCreate(['text' => $typestr,])->id;
                $newEvent->units_instance_id = $instance->id;
                $newEvent->start_timestamp = Carbon::createFromFormat('Ymd\THis', $event['DTSTART']);
                $newEvent->duration = $duration_secs;

                if (isset($event['LOCATION'])) {
                    try {
                        if ($event['LOCATION'] == 'Off Campus') {
                            $newEvent->onoffcampus = "OFF";
                        } else {
                            $newEvent->onoffcampus = "ON";
                        }
                        $newEvent->location_id = Location_lookup::firstOrCreate(['text' => count($locationtitleArr[1]) > 0 ? $locationtitleArr[1][0] : $event['LOCATION'], 'url' => $locationurl])->id;
                    } catch (\Exception $e) {
                        // dd($e);
                        dd($event);
                        dd(count($locationtitleArr[1]));
                        dd(count($event['LOCATION']));
                    }
                }
                //dd($newEvent);
                $newEvent->save();
            }
        }
        return $instance->events->count();
    }

    public function updatefromcentral($unitid)
    {
        return array(
            'status' => $this->webimport($unitid),
        );

    }




    // output the calendar as an XML file for use in a DTHMLX calendar
    public function xmlcalendar($unitinstanceids, $onoff = null)
    {
        //ini_set('memory_limit', '1024M');
        $unitsfilter = explode(',', $unitinstanceids);
        if (isset($onoff)) {
            $onofffilter = [$onoff];
        } else {
            $onofffilter = ["ON", "OFF", ""];
        }
        $events = \App\Event::wherein('units_instance_id', $unitsfilter)
            ->wherein('onoffcampus', $onofffilter)
            ->get();

        $i = 0;
        $returnStr = '<data>';
        foreach ($events as $event) {
            //dd($event);
            $location = (isset($event->location->text) ? $event->location->text : "");
            $locationurl = (isset($event->location->url) ? $event->location->url : "");
            $locationhref = ((strlen($location) > 0) && (strlen($locationurl) > 0)) ? "<a href='" . $locationurl . "' target='_blank'>" . $location . "</a>" : $location;
            $type = isset($event->type->text) ? $event->type->text : "No type specified";
            $unit = isset($event->unit->description) ? $event->unit->description : "";
            $start = new Carbon($event->start_timestamp);
            $end = $start->addSecond($event->duration);
            $returnStr .= "<event id=\"{$event->id}\">";
            $returnStr .= "<start_date><![CDATA[" . $event->start_timestamp . "]]></start_date>";
            $returnStr .= "<end_date><![CDATA[" . $end->toDateTimeString() . "]]></end_date>";
            $returnStr .= "<description><![CDATA[" . $event->description . "]]></description>";
            $returnStr .= "<location><![CDATA[" . (isset($event->location->text) ? $event->location->text : "") . "]]></location>";
            $returnStr .= "<locationurl><![CDATA[" . (isset($event->location->url) ? $event->location->url : "") . "]]></locationurl>";
            $returnStr .= "<locationid><![CDATA[" . (isset($event->location->id) ? $event->location->id : "") . "]]></locationid>";
            $returnStr .= "<text><![CDATA[<strong>" . $event->unit_instance->unit->unit_code . '<br/>' . $type . "</strong>: " . $event->description . "<br/><strong>Location: </strong>" . $locationhref . "]]></text>";
            $returnStr .= "<type><![CDATA[" . $type . "]]></type>";
            $returnStr .= "<typeid><![CDATA[" . (isset($event->type->id) ? $event->type->id : '0') . "]]></typeid>";
            $returnStr .= "<unit><![CDATA[" . $unit . "]]></unit>";
            $returnStr .= "<unitid><![CDATA[" . (isset($event->unit->id) ? $event->unit->id : '0') . "]]></unitid>";
            if (isset($input['colorbydiscipline'])) {
                // dd($event->unit->discipline);
                $returnStr .= (isset($event->unit->discipline->color) ? ("<color>" . $event->unit->discipline->color . "</color>") : "");
            } else {
                $returnStr .= (isset($event->type->color) ? ("<color>" . $event->type->color . "</color>") : "");
            }
            $returnStr .= "<onoffcampus><![CDATA[" . (isset($event->onoffcampus) ? $event->onoffcampus : 'ON') . "]]></onoffcampus>";

            $returnStr .= "<location><![CDATA[" . $location . "]]></location></event>";
            $i++;
        }
        $returnStr .= "</data>";
        $response = new \Illuminate\Http\Response($returnStr, '200');
        $response->header("Content-Type", 'text/xml');
        // suggest to browser a download
        //  $response->header("Content-Disposition", 'attachment; filename='.$media->name);
        return $response;
        //return
    }

    public function ical($unitid){

    }


}
