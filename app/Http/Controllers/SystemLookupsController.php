<?php

namespace App\Http\Controllers;


use App\Course_lookup;
use App\Discipline_lookup;
use App\Event_type_lookup;
use App\Type_lookup;
use App\Units;
use App\User;
use Illuminate\Support\Facades\Request;


class SystemLookupsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $units = Units::orderBy('unit_code')->get();
        $types = Event_type_lookup::all();
        $disciplines = Discipline_lookup::all();
        $courses = Course_lookup::orderBy('text')->get();
        $users = User::all();
        return view('setup.lookups.index')
            ->with('units', $units)
            ->with('users', $users)
            ->with('types', $types)
            ->with('courses', $courses)
            ->with('disciplines', $disciplines);
    }

    public function importCourseMappingsByCSV(Request $request)
    {
        $input = $request::all();


        if (!empty($request::file('csvfile'))) {
            $courserow = 0;
            $trimrow = 0;
            $isfirstrow = true;
            $currentunit = new Units();
            // handle file
            // get the file. @TODO do some validation to ensure it's actually a CSV file
            $file = $request::file('csvfile');
            if (($handle = fopen($file, "r")) !== FALSE) {
                // loop through the CSV
                while (($data = fgetcsv($handle)) !== FALSE) {
                    // get the first row, work out the indices from teh headers
                    if ($isfirstrow) {
                        if (array_search('course', $data) !== false) {
                            $courserow = array_search('course', $data);
                        } else {
                            return array('status' => '1',
                                'value' => 'Need to have a header called "course"'
                            );
                        }

                        if (array_search('trim', $data) !== false) {
                            $trimrow = array_search('trim', $data);
                        } else {
                            return array('status' => '1',
                                'value' => 'Need to have a header called "trim"'
                            );
                        }
                        $isfirstrow = false;
                    } else {
                        // check if the entry is a unit. If so, set it as the unit ID
                        if (\App\Units::where('unit_code', $data[$courserow])->count() > 0) {
                            $currentunit = \App\Units::where('unit_code', $data[$courserow])->firstOrFail();
                            //dd($currentunit);
                        } else {
                            // the next thing should therefore be a course
                            // does it already exist? if not create it
                            if (Course_lookup::where('text', $data[$courserow])->count() > 0) {
                                $course = Course_lookup::where('text', $data[$courserow])->firstOrFail();
                                $course->trim_reference = $data[$trimrow];
                            } else {
                                $course = new Course_lookup();;
                                $course->text = $data[$courserow];
                                $course->trim_reference = $data[$trimrow];
                            }
                            $course->save();
                            // associate the course with the current unit
                            $currentunit->courses()->attach($course->id);
                        }

                    }
                }
            }
        } else {
            dd('no file');
        }
        $response = array(
            'status' => 0,
        );
        return $response;
    }
}
