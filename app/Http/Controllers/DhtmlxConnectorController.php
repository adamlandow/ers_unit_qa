<?php

require_once("../../CustomClasses/dhtmlx/scheduler_connector.php");
require_once("../../CustomClasses/dhtmlx/db_phplaravel.php");


class DhtmlxConnectorController extends BaseController {

    /**
     * Event Model
     * @var Event
     */
    protected $events;


    /**
     * Inject the models.
     * @param Event $events

     */
    public function __construct(\App\Event $events)
    {
        parent::__construct();

        $this->event = $events;

    }

    /**
     * Draws the scheduler.
     *
     * @return View
     */
    public function getIndex()
    {
        return View::make('scheduler/index');
    }
    /**
     * Loads all events.
     *
     * @return events
     */
    public function load(){



        $connector = new SchedulerConnector($this->event,"PHPLaravel");
        $connector->configure("events","id","start_date,end_date,event_name");
        $connector->render();
    }
    /**
     * Saves events.
     *
     * @return events
     */
    public function save(){

        $connector = new SchedulerConnector($this->event, "PHPLaravel");
        $connector->configure("events","id","start_date,end_date,event_name");
        $connector->render();
    }

}