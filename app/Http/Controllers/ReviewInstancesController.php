<?php

namespace App\Http\Controllers;

use App\Emails_log;
use App\Emails_template;
use App\Mail\UnitReviewEmail;
use App\Review_instances;
use App\Review_instances_checklist_items;
use App\Review_instances_media;
use App\Review_instances_outcomes;
use App\Review_instances_outcomes_assessment_tasks;
use App\Review_instances_outcomes_learning_outcomes;
use App\Sortable_review_instances;
use App\Offering;
use Barryvdh\DomPDF\Facade;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use PhpOffice\PhpWord\Writer\Word2007;
use Request;

class ReviewInstancesController extends Controller
{
    //
    public function __construct()
    {

        $this->middleware('auth');
    }

    public function index(\Illuminate\Http\Request $request)
    {

        //  dd(Auth::user()->can('view_other_review_instance'));
        if (Auth::user()->can('view_review_instance')) {
            $params = $request->all();
            if (isset($params['search'])) {
                $reviews = new Collection();
                if (Auth::user()->can('view_other_review_instance')) {
                    $reviews = Sortable_review_instances::where(function ($q) use ($params) {
                        $q->where(function ($q2) use ($params) {
                            $q2->where('unit_code', 'like', "%{$params['search']}%")
                                ->orWhere('coordinator_name', 'like', "%{$params['search']}%")
                                ->orWhere('override_coordinator_name', 'like', "%{$params['search']}%")
                                ->orWhere('peer_reviewer_name', 'like', "%{$params['search']}%")
                                ->orWhere('ld_name', 'like', "%{$params['search']}%");
                        })->whereNull('archived_at');
                    })->sortable()->paginate(20);
                } else {
                    $reviews = Sortable_review_instances::where(function ($q) use ($params) {
                        $q->where(function ($q2) use ($params) {
                            $q2->where('unit_code', 'like', "%{$params['search']}%")
                                ->orWhere('coordinator_name', 'like', "%{$params['search']}%")
                                ->orWhere('override_coordinator_name', 'like', "%{$params['search']}%")
                                ->orWhere('peer_reviewer_name', 'like', "%{$params['search']}%")
                                ->orWhere('ld_name', 'like', "%{$params['search']}%");
                        })->whereNull('archived_at');
                    })->sortable()->reviewsforuser([Auth::user()->id])->paginate(20);
                }
            } else {
                if (Auth::user()->can('view_other_review_instance')) {
                    $reviews = Sortable_review_instances::where(function ($q){$q->whereNull('archived_at');})->sortable()->paginate(20);
                } else {
                    $reviews = Sortable_review_instances::where(function ($q){$q->whereNull('archived_at');})->sortable()->reviewsforuser([Auth::user()->id])->paginate(20);
                }
            }
            //$reviews = Review_instances::sortable()->paginate(20);
            $users = \App\User::all();
            $unit_instances = Offering::whereNull('is_template')->get(['id', 'unit_id', 'coordinator_id', 'teaching_period_id']);
            $templates = \App\Review_instances::whereNotNull('is_template')->get(['id', 'label']);
            $emailtemplates = Emails_template::where('context', 'unitreview')->get();
            //dd($reviews);
            return view('reviews.list')
                ->with('users', $users)
                ->with('unit_instances', $unit_instances)
                ->with('templates', $templates)
                ->with('emailtemplates', $emailtemplates)
                ->with('reviews', $reviews);
        } else {
            abort(500, 'You don\'t have permission to do that');
        }
    }

    public function show($id)
    {

        //$this->authorize('view', Review_instances::class);
        if (Auth::user()->can('view_review_instance')) {
            $review = \App\Review_instances::findOrFail($id);
            // does this have a review instance outcomes table entry?
            $newreviews = \App\Review_instances_outcomes::firstOrCreate(['review_instances_id' => $id], ['review_instances_id' => $id]);
            // has the JSON data been soted?
//
//            if($review->review_instances_outcomes->learning_outcomes!==null){
//                $this->updatecaucdata($id);
//            }
//            dd($review->review_instances_outcomes->learning_outcomes);
            //    dd($newreviews);
            $users = \App\User::all();
            $unit_instances = Offering::all();
            //  $caucdata = json_decode($review->reviewed_unit_instance->cauc_data_cache, true);
            // dd($caucdata['code']);
            $emailtemplates = Emails_template::where('context', 'unitreview')->get();
            return view('reviews.view')
                ->with('review', $review)
                ->with('users', $users)
                ->with('emailtemplates', $emailtemplates)
                // ->with('caucdata', $caucdata)
                ->with('unit_instances', $unit_instances);
        } else {
            abort(500, 'You don\'t have permission to do that');
        }
    }

    public function store(Request $request)
    {

        $input = $request::all();
        // dd($input);
        // check that the users are different
        $userids = [$input['ld_id'], $input['peer_reviewer_id'], Offering::find($input['reviewed_offering_id'])->coordinator['id']];
        if (count($userids) == count(array_unique($userids))) {
            // make a new review instance
            $newreview = \App\Review_instances::create($input);

            // get the JSON object of the unit information
            $url = "https://my.une.edu.au/courses/{$newreview->reviewed_offering->teaching_period['year']}/units/{$newreview->reviewed_offering->unit->unit_code}.json";
            try {
                $str = file_get_contents($url);
                $json = json_decode($str, true);
            } catch (\Exception $err) {
                // @TODO handle this better
                dd($err);
            }

            // get the template checklist items

            $checklistitems = Review_instances::find($input['template_id'])->checklist;

            // build an array of new items to be inserted
            $newitems = [];
            foreach ($checklistitems as $item) {
                if ((stripos($item['template_text'], '{cauc_description}') > 0)
                    || (stripos($item['template_text'], '{cauc_description_table}') > 0)
                    || (stripos($item['template_text'], '{cauc_learning_outcomes_comparison_table}') > 0)
                    || (stripos($item['template_text'], '{cauc_assessments_table}') > 0)) {

                    $item['template_text'] = self::search_replace_cauc($newreview->reviewed_offering, $json, $item['template_text']);
                }
                $newitems[] = new Review_instances_checklist_items(['description' => $item['description'],
                    'template_text' => $item['template_text'],
                    'coordinator_text' => $item['template_text'],
                    'peer_reviewer_text' => $item['template_text'],
                    'ld_text' => $item['template_text'],
                    'order' => $item['order'],
                    'heading' => $item['heading'],
                    'trigger_email'=>$item['trigger_email'],
                    'trigger_email_id'=>$item['trigger_email_id']
                ]);
            }
// just gunna have to assume this works. Doesn't return anything if it fails...
            $newreview->checklist()->saveMany($newitems);

            $response = array(
                'id' => strval($newreview->id),
            );
        } else {
            $response = array(
                'error' => 'Must have different people for different roles',
            );
        }
        return $response;
    }


    public function clone_instance(Request $request)
    {

        $input = $request::all();
        //    dd($input);
        // check that the users are different
        $userids = [$input['ld_id'], $input['peer_reviewer_id'], Offering::find($input['reviewed_offering_id'])->coordinator['id']];
        $originalid = $input['id'];

        if (count($userids) == count(array_unique($userids))) {
// we're all good, proceed
// get the checklist items
            $checklistitems = Review_instances::find($originalid)->checklist;
            //  dd($checklistitems);
            // get the new instance ID
            // kill off id, or there'll be issues
            unset($input['id']);
            $newinstance = \App\Review_instances::create($input);
            // dd($newinstance);
            // build an array of new items to be inserted
            $newitems = [];
            foreach ($checklistitems as $item) {
                $newitems[] = new Review_instances_checklist_items(['description' => $item['description'],
                    'order' => $item['order'],
                    'heading' => $item['heading'],
                    'template_text' => $item['template_text'],
                    'trigger_email'=>$item['trigger_email'],
                    'trigger_email_id'=>$item['trigger_email_id']]);
            }
// just gunna have to assume this works. Doesn't return anything if it fails...
            $newinstance->checklist()->saveMany($newitems);

            $response = array(
                'id' => $newinstance->id,
            );
        } else {
            $response = array(
                'error' => 'Must have different people for different roles',
            );
        }
        return $response;
    }

    public function ajaxstore(Request $request)
    {

        $input = $request::all();
        $status = strval(\App\Review_instances::create($input)->id);
        $response = array(
            'status' => $status,
        );
        return $response;

    }

    public function ajaxupdate(Request $request)
    {

        $input = Request::all();
        //dd($input);
        $review = \App\Review_instances::findOrFail($input['id']);
        if (isset($input['reviewed_offering_id'])) {
            $review->reviewed_offering_id = $input['reviewed_offering_id'];
        }
        if (isset($input['peer_reviewer_id'])) {
            $review->peer_reviewer_id = $input['peer_reviewer_id'];
        }
        if (isset($input['ld_id'])) {
            $review->ld_id = $input['ld_id'];
        }
        if (isset($input['coordinator_id'])) {
            if ($input['coordinator_id'] == $review->reviewed_offering->coordinator['id']) {
                $review->override_coordinator_id = null;
            } else {
                $review->override_coordinator_id = $input['coordinator_id'];
            }
        }
        $status = strval($review->update($input));
        $response = array(
            'status' => $status,
        );
        return $response;
    }

    public function destroy(Request $request)
    {
        $input = $request::all();
        //dd($input);
        $status = strval(\App\Review_instances::destroy($input['id']));
        $response = array(
            'status' => $status,
        );
        return $response;
    }

    public function createmedia(Request $request)
    {

        $input = $request::all();
        $input["user_id"] = Auth::user()->id;
        // handle file
        //dd($input);
        // $uploadedfile = $input["userfile"];
        $file = Request::file('userfile');
        // check it's not too big
        if ($file->getMaxFilesize() > $file->getSize()) {
            // get type
            $type = $file->getClientOriginalExtension();
            // @todo is this an allowed type?

            // get the md5 hash of the contents. This allows for different files with the same name in teh same directory...
            $md5name = md5(file_get_contents($file->getPath() . DIRECTORY_SEPARATOR . $file->getFilename()));

            // store it to disk
            if (!(Storage::disk('local')->put('media' . DIRECTORY_SEPARATOR . $md5name, File::get($file)))) {
                return '-1';
            }
            // save the record
            $newentry = new Review_instances_media();
            $newentry->review_instances_id = $input["review_id"];
            $newentry->name = $file->getClientOriginalName();
            $newentry->type = $type;
            $newentry->size = $file->getSize();
            $newentry->description = $input["description"];
            $newentry->path = 'media' . DIRECTORY_SEPARATOR . $md5name;
            $newentry->save();
            return $newentry->id;

        } else {
            return '-1';
        }
        //$response = array(
        //  'status' => 'success',
        //'msg' => 'Setting created successfully',
        //);

        return '-1';
    }

    public function makelive($id)
    {
        $review = \App\Review_instances::findOrFail($id);
        $review->live = 'true';
        $status = strval($review->save());
        $response = array(
            'status' => $status,
        );
        return $response;
    }

    public function archive($id)
    {
        $review = \App\Review_instances::findOrFail($id);
        $review->archived_at = Carbon::now();
        $status = strval($review->save());
        $response = array(
            'status' => $status,
        );
        return $response;
    }

    private function search_replace_cauc(Offering $instance, $json, $inputStr)
    {

        // search and replace to save typing
        // description
        $inputStr = str_replace("{cauc_description}", $json['description'], $inputStr);

        // CAUC description table
        if (stripos($inputStr, '{cauc_description_table}') > 0) {
            $returnStr = "
<table class=\"note\" border=\"2\" cellspacing=\"2\" cellpadding=\"2\">
<tbody>
<tr>
<td><strong>CAUC ({$instance->teaching_period['year']})</strong></td>
<td><strong>Study Guide ({$instance->teaching_period['year']})</strong></td>
<td><strong>Comments</strong></td>
</tr><tr><td>{$json['description']}</td><td><div><strong>External/Internal</strong></div></td><td>&nbsp;</td></tr>
</tbody>
</table>";
            $inputStr = str_replace(["{cauc_description_table}"], [$returnStr], $inputStr);
        }

        // Learning outcomes comparison table
        if (stripos($inputStr, '{cauc_learning_outcomes_comparison_table}') > 0) {
            $returnStr = "<table class=\"note\" border=\"2\" cellspacing=\"2\" cellpadding=\"2\">
<tbody>
<tr>
<td><strong>CAUC ({$instance->teaching_period['year']})</strong></td>
<td><strong>Study Guide ({$instance->teaching_period['year']})</strong></td>
<td><strong>Comments</strong></td>
</tr>";
            foreach ($json['learningOutcomes'] as $learningOutcome) {
                $returnStr .= "<tr><td><p class=\"default\">{$learningOutcome['description']}</p></td>&nbsp;<td><td>&nbsp;</td></tr>";

            }
            $returnStr .= "</tbody></table>";

            $inputStr = str_replace(["{cauc_learning_outcomes_comparison_table}"], [$returnStr], $inputStr);
        }

        // Learning outcomes topics table
        if (stripos($inputStr, '{cauc_learning_outcomes_topics_table}') > 0) {
            $returnStr = "<table class=\"note\" border=\"2\" cellspacing=\"2\" cellpadding=\"2\">
<tbody>
<tr>
<td><strong>Learning outcomes ({$instance->teaching_period['year']})</strong></td>
<td><strong>Topics</strong></td>
</tr>";
            foreach ($json['learningOutcomes'] as $learningOutcome) {
                $returnStr .= "<tr><td><p class=\"default\"><p class=\"default\">{$learningOutcome['description']}</p></td>&nbsp;<td></tr>";

            }
            $returnStr .= "</tbody></table>";

            $inputStr = str_replace(["{cauc_learning_outcomes_topics_table}"], [$returnStr], $inputStr);
        }

        // Assessment comparison table
        if (stripos($inputStr, '{cauc_assessments_table}') > 0) {
            $returnStr = "<table class=\"note\" border=\"2\" cellspacing=\"2\" cellpadding=\"2\">
<tbody>
<tr>
<td><strong>Summative Assessment task in CAUC</strong></td>
<td><strong>Summative Assessment task in Study guide/Moodle page</strong></td>
<td><strong>Assessment Criteria provided (Please tick (√)where applicable)</strong></td>
</tr>";
            foreach ($json['assessments'] as $assessment) {
                $returnStr .= "<tr><td><p class=\"default\"><strong>{$assessment['title']} ({$assessment['weightString']}%)</strong></p>
<p class=\"default\">Compulsory: " . ($assessment['compulsory'] ? 'Yes' : 'No') . "</p>
<p class=\"default\">Word count: {$assessment['wordCount']}</p>
<p class=\"default\">Notes: {$assessment['notes']}</p></td>&nbsp;<td></td>&nbsp;<td></tr>";
            }
            $returnStr .= "</tbody></table>";

            $inputStr = str_replace(["{cauc_assessments_table}"], [$returnStr], $inputStr);
        }

        // unit relationship table
        if (stripos($inputStr, '{cauc_unit_relationship_table}') > 0) {

            $returnStr = "<table class=\"note\" style=\"border-color: black; width: 799px;\" border=\"2\" cellspacing=\"2\" cellpadding=\"2\">
<tbody>
<tr>
<td style=\"border-color: black; width: 299px;\" colspan=\"2\">
<p>&nbsp;</p>
</td>
<td style=\"width: 496px;\">
<p>Clearly justified and appropriate to the majors and courses that the unit contributes to (Please tick (&radic;) where it meets it. Provide explanation where it does not)</p>
</td>
</tr>
<tr>
<td style=\"width: 171.1px;\">
<p>Pre-requisites</p>
</td>
<td style=\"width: 127.9px;\">" . (strlen($json['prerequisites']) > 0 ? $json['prerequisites'] : 'None') . "</td>
<td style=\"width: 496px;\">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style=\"width: 171.1px;\">
<p>Co-requisite:</p>
</td>
<td style=\"width: 127.9px;\">" . (strlen($json['corequisites']) > 0 ? $json['corequisites'] : 'None') . "</td>
<td style=\"width: 496px;\">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style=\"width: 171.1px;\">
<p>Restrictions</p>
</td>
<td style=\"width: 127.9px;\">
<p>" . (strlen($json['restrictions']) > 0 ? $json['restrictions'] : 'None') . "</p>
</td>
<td style=\"width: 496px;\">
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>";

            $inputStr = str_replace(["{cauc_unit_relationship_table}"], [$returnStr], $inputStr);
        }

        return $inputStr;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //email
    //////////////////////////////////////////////////////////////////////////////////////////////
    public function sendemail($id, Request $request)
    {
        $input = $request::all();
        //dd($input);
        // we're using
        $template = Emails_template::find($input['template_id']);
        $unit = Review_instances::find($id);
        $text = $input['text'];
        $subject = $input['subject'];
        //print_r($input);
        try {
            // mail to unit coordinator
            Mail::to('alandow@une.edu.au')->send(new UnitReviewEmail($unit, $template, $subject, $text), $unit, $template);
            // log success
            $response = array(
                'status' => '0',
            );
            Emails_log::create(['email_id' => $input['template_id'],
                'instances_id' => $id,
                'sent_to_id' => $unit->reviewed_unit_instance->coordinator['id'],
                'context' => 'unitreview',
                'sent_by_id' => Auth::user()->id,
                'fulltext' => $text]);
            //dd($log);
//            $log->email_id = $input['template_id'];
//            $log->instances_id = $id;
//            $log->sent_to_id = $unit->reviewed_unit_instance->coordinator['id'];
//            $log->sent_by_id = Auth::user()->id;
//            $log->fulltext = $text;
//            $log->save();
        } catch (\Exception $e) {
            dd($e);
            // handle failure
            $response = array(
                'status' => '1',
            );
        }
        return $response;
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    //CAUC/outcomes functionality
    /////////////////////////////////////////////////////////////////////////////////////////

    // read CAUC for current units information. We use this to generate a comparison for outcomes.

    public function updatecaucdata($id)
    {

        $instance = \App\Review_instances::findOrFail($id);

        $url = "https://my.une.edu.au/courses/{$instance->reviewed_offering->teaching_period->year}/units/{$instance->reviewed_offering->unit->unit_code}.json";
        try {
            // get the data
            $str = file_get_contents($url);
            $json = json_decode($str, true);

            // combined units
            $combinedunitsStr = "";
            foreach ($json['combinedUnits'] as $combinedUnit) {
                $combinedunitsStr .= $combinedUnit['code'] . '<br/>';
            }

            // offerings

            $offeringsStr = "";
            foreach ($json['offerings'] as $offering) {
                //dd($offering['teachingPeriod']);
                $offeringsStr .= $offering['teachingPeriod']['description'] . ", " . $offering['studyMode']['description'] . '<br/>';
            }

            // coordinators
            $coordinatorsStr = "";
            foreach ($json['coordinators'] as $coordinator) {
                $coordinatorsStr .= $coordinator['description'] . ": " . $coordinator['name'] . '<br/>';
            }

            // dd(json_encode($json['learningOutcomes']));
// update review

            $outcome = Review_instances_outcomes::updateOrCreate(['review_instances_id' => $id], [
                'review_instances_id' => $id,
                'unit_code' => $instance->reviewed_offering->unit->unit_code,
                'unit_title' => $json['title'],
                'combined_units' => $combinedunitsStr,
                'credit_points' => $json['creditPoints'],
                'description' => $json['description'],
                'prerequisites' => $json['prerequisites'],
                'corequisites' => $json['corequisites'],
                'offerings' => $offeringsStr,
                'coordinators' => $coordinatorsStr,
                'learning_outcomes' => json_encode($json['learningOutcomes']),
                'assessment_tasks' => json_encode($json['assessments'])
            ]);

            // create assessments
//            foreach ($json['assessments'] as $assessment) {
//                Review_instances_outcomes_assessment_tasks::updateOrCreate([
//                    'review_instances_outcomes_id' => $outcome->id,
//                    'must_complete' => $assessment['compulsory'],
//                    'component' => $assessment['title'],
//                    'exam_length' => $assessment['examLength'],
//                    'weight' => $assessment['weightString'],
//                    'mode' => $assessment['studyMode'],
//                    'word_count' => $assessment['wordCount'],
//                    'notes' => $assessment['notes'],
//                    'relates_to_lo' => $assessment['notesOutcomesAttributes']
//                ]);
//            }

            // create learning outcomes
//            $learningoutcomenumber = 1;
//            foreach ($json['learningOutcomes'] as $learningOutcome) {
//                Review_instances_outcomes_learning_outcomes::updateOrCreate([
//                    'review_instances_outcomes_id' => $outcome->id,
//                    'number' => $learningoutcomenumber,
//                    'text' => $learningOutcome['description']
//                ]);
//                $learningoutcomenumber++;
//            }

        } catch (\Exception $err) {
            // @TODO handle this better
            dd($err);
            return -1;
        }
        return $outcome->review_instances_id;
    }

    ///////////////////////////////////////////////////////////////
    // template functions
    ///////////////////////////////////////////////////////////////
    public function templateindex(Request $request)
    {

        if (Auth::user()->can('update_templates')) {
            $users = \App\User::all();

            $templates = \App\Review_instances::all()->where('is_template', '=', 'true');

            //    dd($templates);
            //dd($reviews);
            return view('reviews.templates.list')
                ->with('instances', $templates)
                ->with('users', $users);
        } else {
            abort(500, 'You don\'t have permission to do that');
        }
    }

    public function templateshow($id)
    {
        //$this->authorize('view', Review_instances::class);

        $instance = \App\Review_instances::findOrFail($id);
        $users = \App\User::all();
        $emailtemplates = Emails_template::where('context', 'unitreview')->get();
        return view('reviews.templates.view')
            ->with('review', $instance)
            ->with('users', $users)
            ->with('emailtemplates', $emailtemplates);
    }

    public function templatestore(Request $request)
    {
        $input = $request::all();
        $input['is_template'] = 'true';
        $newinstance = \App\Review_instances::create($input);
        $response = array(
            'id' => strval($newinstance->id),
        );
        return $response;
    }

    public function templateajaxupdate(Request $request)
    {
        $input = $request::all();
        $instance = \App\Review_instances::findOrFail($input['id']);
        $status = strval($instance->update($input));
        $response = array(
            'status' => $status ? '1' : 0,
        );
        return $response;
    }

    // Clone a template
    public function templateclone_instance(Request $request)
    {
        $input = $request::all();
        $input['is_template'] = 'true';
        $originalid = $input['id'];

// get the checklist items
        $checklistitems = Review_instances::find($originalid)->checklist;
        //  dd($checklistitems);
        // get the new instance ID
        // kill off id, or there'll be issues
        unset($input['id']);
        $newinstance = \App\Review_instances::create($input);

        // dd($newinstance);
        // build an array of new items to be inserted
        $newitems = [];
        foreach ($checklistitems as $item) {
            $newitems[] = new Review_instances_checklist_items(['description' => $item['description'],
                'order' => $item['order'],
                'heading' => $item['heading'],
                'template_text' => $item['template_text'],
                'trigger_email'=>$item['trigger_email'],
                'trigger_email_id'=>$item['trigger_email_id']]);
        }
// just gunna have to assume this works. Doesn't return anything if it fails...
        $newinstance->checklist()->saveMany($newitems);

        $response = array(
            'id' => $newinstance->id,
        );

        return $response;
    }

    //////////////////////////////////////////////////
    //
    //Outcomes reporting
    //
    //////////////////////////////////////////////////

    /**
     * Gets the outcomes as a Word document
     * @param $id
     */
    public function getoutcomesasword($id)
    {
        // kill the debugbar briefly
        \Debugbar::disable();
        $instance = \App\Review_instances::findOrFail($id);
        $caucdata = json_decode($instance->reviewed_offering->cauc_data_cache, true);

        $phpwordObj = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpwordObj->addSection();
        $tableStyle = array(
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
        );

        $headerCellStyle = array(
            'bgColor' => 'D9D9D9',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6
        );

        $headerCellStyle1 = array(
            'bgColor' => 'EEEEEE',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6
        );

        $headerCellStyle2 = array(
            'bgColor' => 'D2D2D2',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,

        );

        $headerSpanCellStyle1 = array(
            'bgColor' => 'E5B8B7',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
            'gridSpan' => 2,
        );

        $headerSpanCellStyle2 = array(
            'bgColor' => '808080',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
            'gridSpan' => 2,
        );

        $headerSpanCellStyle3 = array(
            'bgColor' => 'FFC000',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
            'gridSpan' => 2,
        );

        $headerSpanCellStyle4 = array(
            'bgColor' => 'FFFF00',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
            'gridSpan' => 2,
        );

        $bodyCellStyle = array(
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
            'NumberingLevel' => 'restart'
        );

        $bodyCellStyleSpan6 = array(
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
            'gridSpan' => 6,
        );

        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell(3000, $headerCellStyle)->addText('2017 Unit Information', array('name' => 'Tahoma', 'size' => 12, 'bold' => true));
        $table->addCell(3000, $headerCellStyle)->addText('2018 Suggested Amendment', array('name' => 'Tahoma', 'size' => 12, 'bold' => true));

        $table->addRow();
        $cell = $table->addCell(6000, $headerSpanCellStyle1);
        $cell->addText('Unit Code and Number', array('name' => 'Tahoma', 'bold' => true));
        $cell->addText('(prior to making a change to the unit code please check in Callista to ensure that the code has not been used previously). A rationale for the change is required. Changing the numeric part of the code within the same level eg ABC101 to ABC105 is usually not permitted.', array('name' => 'Tahoma', 'bold' => false));

        $table->addRow();
        $table->addCell(3000, $bodyCellStyle)->addText($instance->review_instances_outcomes->new_unit_code);
        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->review_instances_outcomes->unit_code);

        $table->addRow();
        $cell = $table->addCell(6000, $headerSpanCellStyle2);
        $cell->addText('Unit Title', array('name' => 'Tahoma', 'bold' => true));
        $cell->addText(' - This is not to exceed 50 characters including spaces between words', array('name' => 'Tahoma'));


        $table->addRow();
        $table->addCell(3000, $bodyCellStyle)->addText($instance->review_instances_outcomes->unit_title);
        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->review_instances_outcomes->new_unit_title);

        $table->addRow();
        $cell = $table->addCell(6000, $headerSpanCellStyle1);
        $cell->addText('Combined Unit ', array('name' => 'Tahoma', 'bold' => true));
        $cell->addText('(adding a level to an existing unit requires a New Unit Form)', array('name' => 'Tahoma', 'bold' => false));

        $table->addRow();
        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->review_instances_outcomes->combined_units);
//        if (isset($caucdata['combinedUnits'])) {
//            foreach ($caucdata['combinedUnits'] as $unit) {
//                $cell->addListItem("{$unit['code']} {$unit['title']}");
//            }
//        }
        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->review_instances_outcomes->new_combined_units);

        $table->addRow();
        $cell = $table->addCell(6000, $headerSpanCellStyle2);
        $cell->addText('Credit Point value', array('name' => 'Tahoma', 'bold' => true));

        $table->addRow();
        $table->addCell(3000, $bodyCellStyle)->addText($instance->review_instances_outcomes->credit_points);
        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->review_instances_outcomes->new_credit_points);

        $table->addRow();
        $cell = $table->addCell(6000, $headerSpanCellStyle2);
        $cell->addText('Unit Outline', array('name' => 'Tahoma', 'bold' => true));
        $cell->addText(' - This is the official statement used to describe what the unit is about and what the student outcomes will be. Not to exceed 750 characters including spaces.', array('name' => 'Tahoma', 'bold' => false));

        $table->addRow();
        $cell = $table->addCell(3000, $bodyCellStyle);
        //dd($caucdata['description']);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, preg_replace("/class=[a-zA-Z0-9]+/", "", $instance->review_instances_outcomes->description));
        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, preg_replace("/class=[a-zA-Z0-9]+/", "", $instance->review_instances_outcomes->new_description));
//
        $table->addRow();
        $cell = $table->addCell(6000, $headerSpanCellStyle3);
        $cell->addText('Prerequisites', array('name' => 'Tahoma', 'bold' => true));

        $table->addRow();
        $table->addCell(3000, $bodyCellStyle)->addText($instance->review_instances_outcomes->prerequisites);
        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->review_instances_outcomes->new_prerequisites);

        $table->addRow();
        $cell = $table->addCell(6000, $headerSpanCellStyle3);
        $cell->addText('Corequisites', array('name' => 'Tahoma', 'bold' => true));

        $table->addRow();
        $table->addCell(3000, $bodyCellStyle)->addText($instance->review_instances_outcomes->corequisites);
        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->review_instances_outcomes->new_corequisites);

        $table->addRow();
        $cell = $table->addCell(6000, $headerSpanCellStyle1);
        $cell->addText('Restrictions', array('name' => 'Tahoma', 'bold' => true));

        $table->addRow();
        $table->addCell(3000, $bodyCellStyle)->addText($instance->review_instances_outcomes->restrictions);
        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->review_instances_outcomes->new_restrictions);

        $table->addRow();
        $cell = $table->addCell(6000, $headerSpanCellStyle2);
        $cell->addText('Location(s), Mode(s) and Teaching Period(s) for each offering ', array('name' => 'Tahoma', 'bold' => true));
        $cell->addText('(include partnerships)', array('name' => 'Tahoma', 'bold' => false));

        $table->addRow();

        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->review_instances_outcomes->offerings);

        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->review_instances_outcomes->new_offerings);

        $table->addRow();
        $cell = $table->addCell(6000, $headerSpanCellStyle4);
        $cell->addText('Learning Outcomes ', array('name' => 'Tahoma', 'bold' => true));
        $cell->addText('There should be between 3 and 6 statements. These statements are to set out what the student will be able to do. If the unit is offered at more than one level, it is expected that the Learning Outcome statements will clearly differentiate between the expectations for each level. Learning Outcomes should be linked to assessments.', array('name' => 'Tahoma', 'bold' => false));

        $table->addRow();
//
        $cell = $table->addCell(3000, $bodyCellStyle);
        $i = 1;
        if (isset($instance->review_instances_outcomes->learning_outcomes)) {

            foreach (json_decode($instance->review_instances_outcomes->learning_outcomes) as $learningOutcome) {
                //     dd($learningOutcome);
                $cell->addText("{$i}) {$learningOutcome->description}");
                $i++;
                //$cell->addListItem("{$learningOutcome['description']}", null, null, array('listType' => \PhpOffice\PhpWord\Style\ListItem::TYPE_NUMBER), array('numLevel' => 0));

            }
        }

        $cell = $table->addCell(3000, $bodyCellStyle);
        $i = 1;
        foreach ($instance->review_instances_outcomes->review_instances_outcomes_learning_outcomes as $learningOutcome) {
            $cell->addText("{$i}) {$learningOutcome['text']}");
            $i++;
            //$cell->addListItem("{$learningOutcome['text']}", null, null, array('listType' => \PhpOffice\PhpWord\Style\ListItem::TYPE_NUMBER), array('numLevel' => 1));
        }

        //\PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->outcomes_learning_outcomes);

        $table->addRow();
        $cell = $table->addCell(6000, $headerSpanCellStyle4);
        $cell->addText('Assessment tasks ', array('name' => 'Tahoma', 'bold' => true));
        $cell->addText(' See the Assessment procedures at: ', array('name' => 'Tahoma', 'bold' => false));

        $cell->addLink('http://policies.une.edu.au/view.current.php?id=00290&dvid=1', htmlentities('http://policies.une.edu.au/view.current.php?id=00290&dvid=1'), array('color' => '0000FF', 'underline' => \PhpOffice\PhpWord\Style\Font::UNDERLINE_SINGLE));
//
        $table->addRow();

        $cell = $table->addCell(3000, $bodyCellStyle);


        $assessmentArr = [];

        //build array of assessment data
        foreach (json_decode($instance->review_instances_outcomes->assessment_tasks) as $assessment) {
            if ((strlen($assessment->formattedExamLength) > 0)) {
                array_push($assessmentArr, $assessment);
            } else {
                array_unshift($assessmentArr, $assessment);
            }
        }


        $assessmenttable = $cell->addTable($tableStyle);
        $assessmenttable->addRow();
        $assessmenttable->addCell(300, $headerCellStyle2)->addText('Must Complete', array('size' => 8, 'bold' => true));
        $assessmenttable->addCell(500, $headerCellStyle2)->addText('Component', array('size' => 8, 'bold' => true));
        $assessmenttable->addCell(300, $headerCellStyle2)->addText('Exam length', array('size' => 8, 'bold' => true));
        $assessmenttable->addCell(300, $headerCellStyle2)->addText("Weight", array('size' => 8, 'bold' => true));
        $assessmenttable->addCell(300, $headerCellStyle2)->addText("Mode", array('size' => 8, 'bold' => true));
        $assessmenttable->addCell(300, $headerCellStyle2)->addText("No. Words", array('size' => 8, 'bold' => true));
        foreach ($assessmentArr as $assessment) {
            $assessmenttable->addRow();
            $assessmenttable->addCell(null, $headerCellStyle1)->addText($assessment->compulsory ? 'Yes' : 'No', array('size' => 8));
            $assessmenttable->addCell(null, $headerCellStyle1)->addText($assessment->title, array('size' => 8));
            $assessmenttable->addCell(null, $headerCellStyle1)->addText((strlen($assessment->formattedExamLength) > 0) ? $assessment->formattedExamLength : "", array('size' => 8));
            $assessmenttable->addCell(null, $headerCellStyle1)->addText($assessment->weightString, array('size' => 8));
            $assessmenttable->addCell(null, $headerCellStyle1)->addText($assessment->studyMode, array('size' => 8));
            $assessmenttable->addCell(null, $headerCellStyle1)->addText(isset($assessment->wordCount) ? "Word count: {$assessment->wordCount} words" : "", array('size' => 8));
            $assessmenttable->addRow();
            $currentassessmentcell = $assessmenttable->addCell(2000, $bodyCellStyleSpan6);
            $currentassessmentcell->addText("Assessment notes:", array('italic' => true, 'size' => 8));
            if (isset($assessment->notes)) {
                $currentassessmentcell->addText($assessment->notes, array('size' => 8));
            }

            $currentassessmentcell->addText("Relates to Learning Outcomes (LO):", array('size' => 8, 'italic' => true));
            $currentassessmentcell->addText(isset($assessment->notesOutcomesAttributes) ? $assessment->notesOutcomesAttributes : '', array('size' => 8));

        }


        $cell = $table->addCell(3000, $bodyCellStyle);


        $assessmenttable1 = $cell->addTable($tableStyle);
        $assessmenttable1->addRow();
        $assessmenttable1->addCell(300, $headerCellStyle2)->addText('Must Complete', array('size' => 8, 'bold' => true));
        $assessmenttable1->addCell(500, $headerCellStyle2)->addText('Component', array('size' => 8, 'bold' => true));
        $assessmenttable1->addCell(300, $headerCellStyle2)->addText('Exam length', array('size' => 8, 'bold' => true));
        $assessmenttable1->addCell(300, $headerCellStyle2)->addText("Weight", array('size' => 8, 'bold' => true));
        $assessmenttable1->addCell(300, $headerCellStyle2)->addText("Mode", array('size' => 8, 'bold' => true));
        $assessmenttable1->addCell(300, $headerCellStyle2)->addText("No. Words", array('size' => 8, 'bold' => true));
        foreach ($instance->review_instances_outcomes->review_instances_outcomes_assessment_tasks as $assessment) {
            $assessmenttable1->addRow();
            $assessmenttable1->addCell(null, $headerCellStyle1)->addText((strlen($assessment['must_complete']) > 0) ? $assessment['must_complete'] : '', array('size' => 8));
            $assessmenttable1->addCell(null, $headerCellStyle1)->addText($assessment['component'], array('size' => 8));
            $assessmenttable1->addCell(null, $headerCellStyle1)->addText((strlen($assessment['exam_length']) > 0) ? $assessment['exam_length'] : "", array('size' => 8));
            $assessmenttable1->addCell(null, $headerCellStyle1)->addText($assessment['weight'], array('size' => 8));
            $assessmenttable1->addCell(null, $headerCellStyle1)->addText((strlen($assessment['mode']) > 0) ? $assessment['mode'] : "", array('size' => 8));
            $assessmenttable1->addCell(null, $headerCellStyle1)->addText(isset($assessment['word_count']) ? "Word count: {$assessment['word_count']} words" : "", array('size' => 8));
            $assessmenttable1->addRow();
            $currentassessmentcell = $assessmenttable1->addCell(2000, $bodyCellStyleSpan6);
            $currentassessmentcell->addText("Assessment notes:", array('italic' => true, 'size' => 8));
            if (isset($assessment['notes'])) {
                $currentassessmentcell->addText($assessment['notes'], array('size' => 8));
            }

            $currentassessmentcell->addText("Relates to Learning Outcomes (LO):", array('size' => 8, 'italic' => true));
            $currentassessmentcell->addText(isset($assessment['relates_to_lo']) ? $assessment['relates_to_lo'] : '', array('size' => 8));

        }
//
        $table->addRow();
        $cell = $table->addCell(6000, $headerSpanCellStyle1);
        $cell->addText('Unit Coordinator/s – if only one coordinator complete Unit Coordinator only ', array('name' => 'Tahoma', 'bold' => true));


        $table->addRow();
        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->outcome_coordinators);

        $cell = $table->addCell(3000, $bodyCellStyle);
        \PhpOffice\PhpWord\Shared\Html::addHtml($cell, $instance->new_outcome_coordinators);

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpwordObj, 'Word2007');
        header('Content-Type: application/application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Disposition: attachment;filename="outcomes.docx"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    /**
     * Gets the feedback as a Word document for Neelam
     * @param $id
     */
    public function getfeedbacksummaryasword($id)
    {
        // kill the debugbar briefly
        \Debugbar::disable();
        $instance = \App\Review_instances::findOrFail($id);


        $phpwordObj = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpwordObj->addSection();
        $tableStyle = array(
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
        );

        $headerCellStyle = array(
            'bgColor' => 'D9D9D9',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6
        );

        $headerCellStyle1 = array(
            'bgColor' => 'EEEEEE',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6
        );

        $headerCellStyle2 = array(
            'bgColor' => 'D2D2D2',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,

        );

        $headerSpanCellStyle1 = array(
            'bgColor' => 'E5B8B7',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
            'gridSpan' => 2,
        );

        $headerSpanCellStyle2 = array(
            'bgColor' => '808080',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
            'gridSpan' => 2,
        );

        $headerSpanCellStyle3 = array(
            'bgColor' => 'FFC000',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
            'gridSpan' => 2,
        );

        $headerSpanCellStyle4 = array(
            'bgColor' => 'FFFF00',
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
            'gridSpan' => 2,
        );

        $bodyCellStyle = array(
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
            'NumberingLevel' => 'restart'
        );

        $bodyCellStyleSpan6 = array(
            'border' => 'single',
            'borderColor' => '000000',
            'borderSize' => 6,
            'gridSpan' => 6,
        );

        // header
        $table = $section->addTable($tableStyle);
        $table->addRow();
        $table->addCell(1000, $headerCellStyle)->addText('2017 Unit Information', array('name' => 'Tahoma', 'size' => 12, 'bold' => true));
        $table->addCell(3000, $headerCellStyle)->addText('2018 Suggested Amendment', array('name' => 'Tahoma', 'size' => 12, 'bold' => true));


        $table->addRow();
//
        $cell = $table->addCell(3000, $bodyCellStyle);
        $i = 1;

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpwordObj, 'Word2007');
        header('Content-Type: application/application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Disposition: attachment;filename="' . "{$instance->reviewed_unit_instance->unit['unit_code']} {$instance->reviewed_unit_instance->teaching_period['teaching_period']} {$instance->reviewed_unit_instance->teaching_period['year']}" . ' review outcomes.docx"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    public function getaspdf($id)
    {

        // kill the debugbar briefly
        \Debugbar::disable();
        $review = \App\Review_instances::findOrFail($id);
        //$pdf = App::make('dompdf');
        \PDF::setOptions(['dpi' => 96, "isPhpEnabled"=> true, 'isRemoteEnabled', true]);

        $pdf = \PDF::loadView('reviews.pdfexportview',['review' => $review])
            ->setPaper('a3', 'landscape')

            ;


        return $pdf->stream('result.pdf', array('Attachment'=>0));
        return view('reviews.pdfexportview')->with('review', $review);


    }

    // archive
    public function archiveindex(\Illuminate\Http\Request $request)
    {

        //  dd(Auth::user()->can('view_other_review_instance'));
        if (Auth::user()->can('view_review_instance')) {
            $params = $request->all();
            if (isset($params['search'])) {
                $reviews = new Collection();

                $reviews = Sortable_review_instances::where(function ($q) use ($params) {
                    $q->where('unit_code', 'like', "%{$params['search']}%")
                        ->orWhere('coordinator_name', 'like', "%{$params['search']}%")
                        ->orWhere('override_coordinator_name', 'like', "%{$params['search']}%")
                        ->orWhere('peer_reviewer_name', 'like', "%{$params['search']}%")
                        ->orWhere('ld_name', 'like', "%{$params['search']}%");
                })->whereNotNull('archived_at')->sortable()->paginate(20);

            } else {

                $reviews = Sortable_review_instances::whereNotNull('archived_at')->sortable()->paginate(20);

            }

            return view('reviews.archive.list')
                ->with('reviews', $reviews);
        } else {
            abort(500, 'You don\'t have permission to do that');
        }
    }

    public function showarchive($id)
    {

        //$this->authorize('view', Review_instances::class);
        if (Auth::user()->can('view_review_instance')) {
            $review = \App\Review_instances::findOrFail($id);

            return view('reviews.archive.view')
                ->with('review', $review);
        } else {
            abort(500, 'You don\'t have permission to do that');
        }
    }

}
