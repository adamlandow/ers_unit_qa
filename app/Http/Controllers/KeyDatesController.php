<?php

namespace App\Http\Controllers;

use App\Kpi;
use App\Teaching_period;
use App\Teaching_period_lookup;
use Illuminate\Support\Facades\Request;

class KeyDatesController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     *
     * @return type
     */
    public function index(Request $request)
    {

        $teaching_period_lookups = Teaching_period_lookup::all();
        $teaching_periods = Teaching_period::all();
        $kpis = Kpi::all();

        return view("setup.dates.index")
            ->with('kpis', $kpis)
            ->with('teaching_period_lookups', $teaching_period_lookups)
            ->with('teaching_periods', $teaching_periods);
    }

}
