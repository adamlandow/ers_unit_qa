<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\LoginController;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
// filter out anyone who can't view
        //$this->authorize('view', User::class);
        $params = $request::all();
//dd($params['search']);
        if (isset($params['search'])) {
            $user = User::where('username', 'like', "%{$params['search']}%")
                ->orWhere('name', 'like', "%{$params['search']}%")->orderBy('name');
            $users = $user->sortable()->paginate(20);
        } else {
            $user = User::orderBy('name')->sortable()->paginate(20);
            $users = $user;
        }

        $roles = \App\Roles::all();
        $schools = \App\School::where('include_in_list', '=', 'true')->get();
        return view("user.list")->with('roles', $roles)->with('schools', $schools)->with('users', $users);
//        } else {
//            return redirect('home');
//        }
    }

    public function show($id)
    {
        $user = \App\User::findOrFail($id);
        $roleids = [];
        foreach($user->roles as $role){
            $roleids[] = $role->pivot->roles_id;
        }
        $schoolids = [];
        foreach($user->schools as $school){
            $schoolids[] = $school->pivot->school_id;
        }
        $user['role_id[]'] = implode(',',$roleids);
        $user['school_id[]'] = implode(',',$schoolids);
        //dd($user['role_ids']);
        return $user;
    }



    public function ajaxstore(Request $request)
    {

        //$this->authorize('create', User::class);
        $input = $request::all();

        // find out if there's an existing user
        $user = \App\User::where('username', $input['username'])->first();
        if (isset($user)) {
            $response = array(
                'status' => '-1',
                'statusText' => 'that user already exists',
            );
        } else {
            $newuser = new User();
            if ($input['type'] == 'manual') {
                $newuser->name = $input['name'];
                $newuser->password = Hash::make($input['password']);
            } else {
                // get AD values for user here

                $userdetails = LoginController::getDetailsByUsername($input['username']);
              // dd($userdetails);
                if ((count($userdetails) > 0) &&($userdetails['count']>0)) {
                    $newuser->email = $userdetails[0]["mail"][0];
                    $newuser->name = $userdetails[0]["displayname"][0];
                } else {
                    $response = array(
                        'status' => '-1',
                        'statusText' => 'User not found in directory',
                    );
                    return $response;
                }

            }
            $newuser->username = $input['username'];
            $newuser->type = $input['type'];

            // get user image from UNE webpage if a URL is supplied
            //@TODO this is dependent on the existence of a default image at the specific URL. There needs to be a better way...
            if(isset($input['url'])){
                if(strlen($input['url'])>0) {
                    $doc = new \DOMDocument();
                    // search page is not well formed!
                    $doc->recover = true;
                    $doc->strictErrorChecking = false;
                    @$doc->loadHTMLFile($input['url']);
                    $finder = new \DomXPath($doc);
                    $classname = "detail-thumb img-wrapper";
                    $nodes = $finder->query("//*[@class='" . $classname . "']");
                    if(count($nodes)>0) {
                        try {
                            $newuser->image_url = @str_replace("http://", "https://", $nodes[0]->lastChild->getAttribute('src'));
                        } catch (\Error $err) {
                            $newuser->image_url = 'https://www.une.edu.au/__data/assets/image/0005/97178/blank-avatar.png';
                        }
                    }else{
                        $newuser->image_url = 'https://www.une.edu.au/__data/assets/image/0005/97178/blank-avatar.png';
                    }
                }else{
                    $newuser->image_url = 'https://www.une.edu.au/__data/assets/image/0005/97178/blank-avatar.png';
                }

            }

           //  $newuser->role_id = $input['role_id'];
            $status = strval($newuser->save());
            try {
                // add roles
                $rolesupdate = $newuser->roles()->sync($input['role_id']);
                $rolesupdatesuccess = (count($rolesupdate['attached']>0)||count($rolesupdate['detached']>0)||count($rolesupdate['attached']>0));
                // update user
                $status = ($newuser->update($input)||$rolesupdatesuccess);

            } catch (\Exception $e) {
                // do task when error
                dd($e->getMessage());   // insert query
            }
            try {
                // add roles
                $schoolsupdate = $newuser->schools()->sync($input['school_id']);
                //$schoolsupdatesuccess = (count($schoolsupdate['attached']>0)||count($schoolsupdate['detached']>0)||count($schoolsupdate['attached']>0));
                // update user
                $status = ($newuser->update($input));

            } catch (\Exception $e) {
                // do task when error
                dd($e->getMessage());   // insert query
            }
            $response = array(
                'status' => $status,
            );
        }

        return $response;

    }

    /**
     * @param Request $request
     * @return array
     */
    public function ajaxupdate(Request $request)
    {
//        if (Gate::denies('is_admin')) {
//            return array(
//                'status' => -1,
//            );
//        }

        $input = $request::all();

         //  dd($input);

        $user = \App\User::find($input['id']);


        if ($input['type'] == 'manual') {
            if (strlen($input["password"]) > 0) {
                $input['password'] = Hash::make($input['password']);
            } else {
                unset($input['password']);
            }
        } else {
            // get AD values for user here
            $userdetails = LoginController::getDetailsByUsername($input['username']);
            //  dd($userdetails);
            if ($userdetails['count'] > 0) {
                $input['email'] = $userdetails[0]["mail"][0];
                $input['name'] = $userdetails[0]["displayname"][0];
            } else {
                $response = array(
                    'status' => '-1',
                    'statusText' => 'User not found in directory',
                );
                return $response;
            }
        }

        // get user image from UNE webpage if a URL is supplied
        //@TODO this is dependent on the existence of a default image at the specific URL. There needs to be a better way...
        if(isset($input['url'])){
            $doc = new \DOMDocument();
            // search page is not well formed!
            $doc->recover = true;
            $doc->strictErrorChecking = false;
            @$doc->loadHTMLFile($input['url']);
            $finder = new \DomXPath($doc);
            $classname = "detail-thumb img-wrapper";
            $nodes = $finder->query("//*[@class='" . $classname . "']");
            if(isset($nodes[0])) {
                if ($nodes[0]->lastChild->hasAttribute('src')) {
                    $input['image_url'] = str_replace("http://", "https://", $nodes[0]->lastChild->getAttribute('src'));
                } else {
                    $input['image_url'] = 'https://www.une.edu.au/__data/assets/image/0005/97178/blank-avatar.png';
                }
            }else{
                $input['image_url'] = 'https://www.une.edu.au/__data/assets/image/0005/97178/blank-avatar.png';
            }

        }

        try {
            // add roles
            $rolesupdate = $user->roles()->sync($input['role_id']);
           // $rolesupdatesuccess = (count($rolesupdate['attached']>0)||count($rolesupdate['detached']>0)||count($rolesupdate['attached']>0));
            // update user

            $status = ($user->update($input));


        } catch (\Exception $e) {
            // do task when error
            dd($e->getMessage());   // insert query
        }
        try {
            // add roles
            $schoolsupdate = $user->schools()->sync($input['school_id']);
          //  $schoolsupdatesuccess = (count($schoolsupdate['attached']>0)||count($schoolsupdate['detached']>0)||count($schoolsupdate['attached']>0));
            // update user
             $status = ($user->update($input));

        } catch (\Exception $e) {
            // do task when error
            dd($e->getMessage());   // insert query
        }
        return array(
            'status' => $status
        );

    }

    public function ajaxdestroy(Request $request)
    {
        //   dd($request);
//        if (Gate::denies('is_admin')) {
//            return 0;
//        }
        $input = $request::all();
        //dd($input);
        return \App\User::destroy($input['id']);
    }

    // tap into the LDAP server here
    public static function getDetailsByFullName($fullname)
    {
        //dd($fullname);
        // LDAP here
        try {

            $entries = [];
            // log on to AD here
            // update: added ability to look at another server
            $adldap = ldap_connect(config('app.LDAP_AUTH_SERVER')) or die("cannot connect to " . config('app.LDAP_AUTH_SERVER'));

            // set options for AD
            ldap_set_option($adldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($adldap, LDAP_OPT_REFERRALS, 0);

            // test against AD
            //if (@ldap_bind($adldap, $input['username'] . '@ad.une.edu.au', $input['password'])) {


            if (@ldap_bind($adldap, env('LDAP_ADMINUSER') . env('LDAP_AUTH_SUFFIX'), env('LDAP_ADMINUSERPASS'))) {
                // get some details to update the user table
                $ldap_base_dn = 'ou=People,dc=une,dc=edu,dc=au';
                $search_filter = "CN={$fullname}";
                //$search_filter = env('LDAP_AUTH_SEARCHPARAM') . "={$username}";
                $attributes = array();
                $attributes[] = 'displayName';
                $attributes[] = 'mail';
                $attributes[] = 'uid';
                $result = ldap_search($adldap, $ldap_base_dn, $search_filter, $attributes);
                $entries = ldap_get_entries($adldap, $result);

                if ($entries['count'] > 0) {
                    return $entries;
                }

            }
            $entries['count'] = 0;
            return $entries;

        } catch (Exception $e) {
            // something went wrong with AD
            dd($e);
        }
        //dd($input);
    }


/////////////////////////////////////////////////////////////////////////////
//
//Logging in as other users. This is a very high-level (and kinda risky) thing to allow,
// but it might be useful in the context of a system that has users of differing abilities
//
////////////////////////////////////////////////////////////////////////////

    /**
     * Log in as a user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function loginAsUser(Request $request)
    {
        $input = $request::all();
        Auth::user()->loginAsUser($input['userId'], $input['originalUserId']);
// @TODO log this action- who logged in as who
        return redirect()->back();
    }

    /**
     * Return to the previously logged in user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function returnToUser(Request $request)
    {
        Auth::user()->returnToUser();
// @TODO log this action- who logged out and as who
        return redirect()->back();
    }


    ///////////////////////////////////////////////////////////////////////////////
    ///
    /// Search results for select2 controls
    ///
    /// //////////////////////////////////////////////////////////////////////////

    public function searchforselect2(Request $request)
    {
        $input = $request::all();
        $search = $input['q']['term'];
        //  dd($search);
        $returnStr = [];
        $users = User::where('name', 'LIKE', "%{$search}%")->get();
        foreach ($users as $user){
            $returnStr[]= ['id'=>$user->id, 'text'=>"{$user->name}" ];
        }
        return \json_encode(['results'=>$returnStr]);
        //dd($offerings);
    }

}
