<?php

namespace App\Http\Controllers;

use App\Course;
use App\Discipline;
use App\Major;
use App\Offering;
use App\School;
use App\Teaching_period;
use App\Teaching_period_lookup;
use App\Unit;
use App\User;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view("setup.import.index");
    }

    public function show($id)
    {
        return;
    }


    // @todo properly implement this- with a bit of error checking etc.
    public function csvimport(Request $request)
    {

        $returnstatusval = 0;
        $input = $request->all();
        //  dd($input);
        $model = $input['model'];
        if (!empty($request->file('csvfile'))) {

            switch ($model) {
                case 'course':
                    $count = 0;
                    $titlerow = 0;
                    $abbreviationrow = 0;
                    $schoolcoderow = 0;
                    $schooldescrow = 0;
                    $course_coderow = 0;
                    $isfirstrow = true;
                    // handle file
                    // get the file. @TODO do some validation to ensure it's actually a CSV file
                    $file = $request->file('csvfile');
                    if (($handle = fopen($file, "r")) !== FALSE) {
                        // loop through the CSV
                        while (($data = fgetcsv($handle)) !== FALSE) {
                            // get the first row, work out the indices from teh headers
                            if ($isfirstrow) {
                                if (array_search('Course Cd', $data) !== false) {
                                    $course_coderow = array_search('Course Cd', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Course Cd"'
                                    );
                                }
                                if (array_search('Course Status', $data) !== false) {
                                    $course_statusrow = array_search('Course Status', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Course Status"'
                                    );
                                }
                                if (array_search('Title', $data) !== false) {
                                    $titlerow = array_search('Title', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Title"'
                                    );
                                }
                                if (array_search('Abbreviation', $data) !== false) {
                                    $abbreviationrow = array_search('Abbreviation', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Abbreviation"'
                                    );
                                }
                                if (array_search('Responsible Org Unit Cd', $data) !== false) {
                                    $schoolcoderow = array_search('Responsible Org Unit Cd', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Org Unit Desc"'
                                    );
                                }
                                if (array_search('Org Unit Desc', $data) !== false) {
                                    $schooldescrow = array_search('Org Unit Desc', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Org Unit Desc"'
                                    );
                                }

                                $isfirstrow = false;
                            } else {
// check that the school exists- populate from here a bit. We'll fix it up later
                                $school = School::updateOrCreate(['code' => $data[$schoolcoderow]],
                                    ['code' => $data[$schoolcoderow], 'description' => $data[$schooldescrow]]);
// populate the course table
                                $course = Course::updateOrCreate(['course_code' => $data[$course_coderow]],
                                    ['course_code' => $data[$course_coderow],
                                        'label' => $data[$titlerow],
                                        'abbreviation' => $data[$abbreviationrow],
                                        'school_id' => $school->id,
                                        'course_status' => $data[$course_statusrow]]);

                            }
                        }
                    }
                    break;
                case 'unit':
                    $count = 0;
                    $titlerow = 0;
                    $unit_coderow = 0;
                    $description = 0;
                    $credit_pointsrow = 0;
                    $versionrow = 0;
                    $schoolrow = 0;
                    $discipline_coderow = 0;
                    $disciplinerow = 0;
                    $isfirstrow = true;
                    // handle file
                    // get the file. @TODO do some validation to ensure it's actually a CSV file
                    $file = $request->file('csvfile');
                    if (($handle = fopen($file, "r")) !== FALSE) {
                        // loop through the CSV
                        while (($data = fgetcsv($handle)) !== FALSE) {
                            // get the first row, work out the indices from teh headers
                            if ($isfirstrow) {

                                if (array_search('Title', $data) !== false) {
                                    $titlerow = array_search('Title', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Title"'
                                    );
                                }
                                if (array_search('Caur Unit Cd', $data) !== false) {
                                    $unit_coderow = array_search('Caur Unit Cd', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Caur Unit Cd"'
                                    );
                                }
                                if (array_search('Credit Points', $data) !== false) {
                                    $credit_pointsrow = array_search('Credit Points', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Credit Points"'
                                    );
                                }
                                if (array_search('Caur Version No', $data) !== false) {
                                    $versionrow = array_search('Caur Version No', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Caur Version No"'
                                    );
                                }
                                if (array_search('Discipline Cd', $data) !== false) {
                                    $discipline_coderow = array_search('Discipline Cd', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Discipline Cd"'
                                    );
                                }
                                if (array_search('Discipline', $data) !== false) {
                                    $disciplinerow = array_search('Discipline', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Discipline"'
                                    );
                                }
                                if (array_search('School', $data) !== false) {
                                    $schoolrow = array_search('School', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "School"'
                                    );
                                }
                                $isfirstrow = false;
                            } else {

                                // level
                                preg_match('/[0-9]/', $data[$unit_coderow], $results);
// check that the school exists- populate from here a bit. We'll fix it up later
                                $school = School::updateOrCreate(
                                    ['description' => strtoupper($data[$schoolrow])]);
//dd($school->count());
                                $discipline = Discipline::updateOrCreate(['code' => $data[$discipline_coderow]],
                                    ['code' => $data[$discipline_coderow],
                                        'description' => $data[$disciplinerow],
                                        'school_id' => $school->id]);
// populate the unit table
                                $course = Unit::updateOrCreate(['unit_code' => $data[$unit_coderow]],
                                    ['unit_code' => $data[$unit_coderow],
                                        'description' => $data[$titlerow],
                                        'credit_points' => $data[$credit_pointsrow],
                                        'version' => $data[$versionrow],
                                        'discipline_id' => $discipline->id,
                                        'level' => $results[0],
                                        'include_in_timetable' => 'true']
                                );

                            }
                        }
                    }
                    break;
                case 'offering':
                    $count = 0;
                    $unit_coderow = 0;
                    $teachingperiodrow = 0;
                    $yearrow = 0;
                    $moderow = 0;
                    $isfirstrow = true;
                    // handle file
                    // get the file. @TODO do some validation to ensure it's actually a CSV file
                    $file = $request->file('csvfile');
                    if (($handle = fopen($file, "r")) !== FALSE) {
                        // loop through the CSV
                        while (($data = fgetcsv($handle)) !== FALSE) {
                            // get the first row, work out the indices from teh headers
                            if ($isfirstrow) {
                                if (array_search('Title', $data) !== false) {
                                    $titlerow = array_search('Title', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Title"'
                                    );
                                }
                                if (array_search('Credit Points', $data) !== false) {
                                    $credit_pointsrow = array_search('Credit Points', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Credit Points"'
                                    );
                                }
                                if (array_search('Caur Unit Cd', $data) !== false) {
                                    $unit_coderow = array_search('Caur Unit Cd', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Caur Unit Cd"'
                                    );
                                }
                                if (array_search('Teaching Period', $data) !== false) {
                                    $teachingperiodrow = array_search('Teaching Period', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Teaching Period"'
                                    );
                                }
                                if (array_search('Academic Year', $data) !== false) {
                                    $yearrow = array_search('Academic Year', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Academic Year"'
                                    );
                                }
                                if (array_search('Teaching Mode', $data) !== false) {
                                    $moderow = array_search('Teaching Mode', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Teaching Mode"'
                                    );
                                }
                                if (array_search('Discipline Cd', $data) !== false) {
                                    $discipline_coderow = array_search('Discipline Cd', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Discipline Cd"'
                                    );
                                }
                                if (array_search('Discipline', $data) !== false) {
                                    $disciplinerow = array_search('Discipline', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "Discipline"'
                                    );
                                }
                                if (array_search('School', $data) !== false) {
                                    $schoolrow = array_search('School', $data);
                                } else {
                                    return array('error' => '1',
                                        'value' => 'Need to have a header called "School"'
                                    );
                                }
                                $isfirstrow = false;
                            } else {
                                if (in_array($data[$teachingperiodrow],['Trimester 1', 'Trimester 2','Trimester 3'])) {
                                    // check that the school exists- populate from here a bit. We'll fix it up later
                                    $school = School::updateOrCreate(
                                        ['description' => strtoupper($data[$schoolrow])]);
//dd($school->count());
                                    $discipline = Discipline::updateOrCreate(['code' => $data[$discipline_coderow]],
                                        ['code' => $data[$discipline_coderow],
                                            'description' => $data[$disciplinerow],
                                            'school_id' => $school->id]);

                                    try {
                                        $unit_id = Unit::firstOrCreate(['unit_code' => $data[$unit_coderow]],
                                            ['description' => $data[$titlerow],
                                                'credit_points' => $data[$credit_pointsrow],
                                                'discipline_id' => $discipline->id,
                                                'include_in_timetable' => 'true'])->id;
                                    } catch (\Exception $err) {
                                        dd($err);
                                    }
                                  //  dd(Teaching_period::where(['year' => $data[$yearrow], 'teaching_period_lookup_id' => Teaching_period_lookup::where('description', '=', $data[$teachingperiodrow])->first()->id])->first());
                                    $teaching_period_lookup_id = Teaching_period::where(['year' => $data[$yearrow], 'teaching_period_lookup_id' => Teaching_period_lookup::where('description', '=', $data[$teachingperiodrow])->first()->id])->first()->id;

                                    // build the online parameters

                                    $onlineparams = [];
                                    if ($data[$moderow] == 'On Campus') {
                                        $onlineparams['oncampus'] = 'true';
                                    }
                                    if ($data[$moderow] == 'Online') {
                                        $onlineparams['online'] = 'true';

                                    }

                                    // create (or update) the offering.
                                    $offeringinstance = Offering::updateOrCreate(['unit_id' => $unit_id,
                                        'teaching_period_id' => $teaching_period_lookup_id,
                                        'label'=>$data[$titlerow]

                                    ],
                                        $onlineparams);

                                }
                            }
                        }
                    }

                    break;
                default:
                    return array('error' => '1',
                        'value' => "Something went wrong!"
                    );
                    break;
            }

        } else {
            return array('error' => '1',
                'value' => "No file!"
            );
        }

        return array('status' => '0',

        );
    }


}
