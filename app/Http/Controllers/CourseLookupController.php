<?php

namespace App\Http\Controllers;

use App\Course_lookup;
use App\Units;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class CourseLookupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $units = Units::orderBy('unit_code')->get();


        $courses = Course_lookup::orderBy('text')->get();
        $users = User::all();

        return view("unit_course_management.index") ->with('units', $units)
            ->with('users', $users)
            ->with('courses', $courses)
        ;
    }

    public function search(Request $request)
    {
        $input = $request->all();

        $results = \App\Course_lookup::where('description',  'like', "%{$input['searchstr']}%");

        return $results->id;
    }

    public function create(Request $request)
    {
        $input = $request->all();
        $newentry = \App\Course_lookup::create($input);
        return $newentry->id;
    }

    /**
     * Updates the record
     * @param $id
     * @param Request $request
     * @return $this
     */
    public function update(Request  $request)
    {

        $input = $request->all();
     //   dd($input);
        $contact = \App\Course_lookup::findOrNew($input['id']);

        $status = strval($contact->update($input));
        //dd($input);
        $response = array(
            'status' => $status,
        );
        return $response;
    }


    public function store(Request $request)
    {
        $input = $request->all();
        if(\App\Course_lookup::where('text', '=', $input['text'])->count()>0){
            return 'Entry already exists';
        }else{
            return \App\Course_lookup::create($input)->id;
        }

    }

    public function show($id)
    {
        return \App\Course_lookup::findOrFail($id);
    }


    public function destroy(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        return \App\Course_lookup::destroy($input['id']);
    }
}
