<?php

namespace App\Http\Controllers;

use App\Review_instances_notes;
use Request;
use Illuminate\Support\Facades\Auth;

class ReviewInstancesNotesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        return Review_instances_notes::find($id);
    }

    public function create(Request $request)
    {
        $input = $request::all();
        $input['created_by_id']=Auth::User()->id;
        $newentry = \App\Review_instances_notes::create($input);
        $response = array(
            'status' => '0',
            'id'=>$newentry->id,
            'msg' => 'Item created successfully',

        );
        return $response;
    }

    public function store(Request $request)
    {
        $input = $request::all();
        $input['created_by_id']=Auth::User()->id;
        $newentry = \App\Review_instances_notes::create($input);
        $response = array(
            'status' => '0',
            'id'=>$newentry->id,
            'msg' => 'Item created successfully',

        );
        return $response;
    }

    /**
     * Updates the record
     * @param $id
     * @param PatientRequest $request
     * @return $this
     */
    public function update(Request $request)
    {
        $input = $request::all();
        $item = \App\Review_instances_notes::findOrNew($input['id']);

        $status = strval($item->update($input));
        $response = array(
            'status' => $status,
        );
        return $response;
    }


    public function ajaxstore(Request $request)
    {

        $input = $request::all();
        $status = strval(\App\Review_instances_notes::create($input)->id);
        $response = array(
            'status' => $status,
        );
        return $response;

    }

    public function ajaxupdate(Request $request)
    {
        $input = $request::all();
        $review= \App\Review_instances_notes::findOrNew($input['id']);
        $status = strval($review->update($input));
        $response = array(
            'status' => $status,
        );
        return $response;
        return array(
            'status' => $status,
        );
    }

    public function ajaxdestroy(Request $request)
    {
        $input = $request::all();
        //dd($input);
        return \App\Review_instances_notes::destroy($input['id']);
    }

    public function destroy(Request $request)
    {
        $input = $request::all();
        //dd($input);
        $status = strval(\App\Review_instances_notes::destroy($input['id']));
        $response = array(
            'status' => $status,
        );
        return $response;
    }
}
