<?php

namespace App\Http\Controllers;

use App\Field_of_study_trim;
use App\Fieldofstudy;
use Illuminate\Http\Request;

class FieldOfStudyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        return \App\Field_of_study::find($id);
    }

    public function ajaxstore(Request $request)
    {
        $input = $request->all();
        $status = \App\Field_of_study::create($input)->id;
        return array(
            'status' => ($status > 0) ? '0' : '1',
        );
    }

    /**
     * Updates the record
     * @param $id
     * @param Request $request
     * @return $this
     */
    public function update(Request $request)
    {

        $input = $request->all();
        //dd($input);
        $item = \App\Field_of_study::findOrNew($input['id']);

        $status = strval($item->update($input));

        $response = array(
            'status' => $status ? '0' : '1',
        );
        return $response;
    }

    public function destroy(Request $request)
    {
        $input = $request->all();
        // find if this is being used
        if (Field_of_study::find($input['id'])->units->count() > 0) {

            return array(
                'status' => '1',
                'statusText' => 'Can\'t delete: this field of study has some units still attached'
            );
        } else {
            return array(
                'status' => \App\Field_of_study::destroy($input['id']),
            );
        }
    }

    // add a TRIM reference to the course
    public function ajaxaddtrim(Request $request){
        //dd('yo');
        $input = $request->all();
        $result = Field_of_study_trim::updateOrCreate(['reference'=>$input['reference'], 'field_of_study_id'=>$input['id'], 'version'=>$input['version']]);
        $response = array(
            'status' => ($result) ? '0' : '1',
        );
        return $response;
    }


}
