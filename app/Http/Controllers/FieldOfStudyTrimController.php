<?php

namespace App\Http\Controllers;

use App\Fieldofstudy;
use Illuminate\Http\Request;

class FieldOfStudyTrimController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
     return;
    }


    /**
     * Updates the record
     * @param $id
     * @param Request $request
     * @return $this
     */
    public function update(Request $request)
    {
        return;
    }

    function destroy(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        return array(
            'status' => (\App\Field_of_study_trim::destroy($input['id']) ? '0' : '1')
        );
    }
}
