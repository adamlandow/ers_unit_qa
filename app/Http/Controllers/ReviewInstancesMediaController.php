<?php

namespace App\Http\Controllers;

use App\Media;
use App\Patient;
use App\Review_instances;
use App\Review_instances_checklist_items;
use App\Review_instances_media;
use App\Sortable_review_instances;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Request;


class ReviewInstancesMediaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {

    }

    // shows the raw file. If it's an image, display it, if anything else, show an icon.
    public function display($id)
    {

        $media = \App\Review_instances_media::findOrFail($id);

        switch (strtolower($media->type)) {
            case 'jpg':
                $path = $media->path;
                $content_typestr = 'image/jpeg';
                break;
            case 'png':
                $path = $media->path;
                $content_typestr = 'image/png';
                break;
            case 'bmp':
                $path = $media->path;
                $content_typestr = 'image/bmp';
                break;
            case 'pdf':
                $path = '/public/pdf.gif';
                $content_typestr = 'image/gif';
                break;
            case 'doc':
            case 'docx':
                $path = '/public/microsoft-word.png';
                $content_typestr = 'image/png';
                break;
            case 'ppt':
            case 'pptx':
                $path = '/public/powerpoint_logo.gif';
                $content_typestr = 'image/gif';
                break;
            case 'xls':
            case 'xlsx':
                $path = '/public/excel.png';
                $content_typestr = 'image/png';
                break;
            default:
                $path = '/public/unknown.png';
                $content_typestr = 'image/png';
                break;
        }
        $file = Storage::get($path);
        $response = new \Illuminate\Http\Response($file, '200');
        $response->header("Content-Type", $content_typestr);
        return $response;
    }

    public function show($id)
    {

        return \App\Review_instances_media::findOrFail($id);
    }

    public function download($id)
    {

        $media = \App\Review_instances_media::findOrFail($id);
        $content_typestr = "";
        $path = $media->path;
        switch (strtolower($media->type)) {
            case 'jpg':

                $content_typestr = 'image/jpeg';
                break;
            case 'png':
                $content_typestr = 'image/png';
                break;
            case 'bmp':
                $content_typestr = 'image/bmp';
                break;
            case 'pdf':
                $content_typestr = 'application/pdf';
                break;
            case 'doc':
            case 'docx':
                $content_typestr = 'application/msword';
                break;
            case 'ppt':
            case 'pptx':
                $content_typestr = 'application/vnd.ms-powerpoint';
                break;
            case 'xls':
            case 'xlsx':
                $content_typestr = 'application/vnd.ms-excel';
                break;
            default:
                $content_typestr = 'application/octet-stream';
                break;
        }
        //  dd(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().$path);
        //dd($path);
        $file = Storage::get($path);

        $response = new \Illuminate\Http\Response($file, '200');
        $response->header("Content-Type", $content_typestr);
        // suggest to browser a download
        if ($content_typestr != 'image/png'){

            $response->header("Content-Disposition", 'download; filename=' . $media->name);
    }
        return $response;
    }

    // display a thumbnail from an id
    public function thumb($id)
    {

        $media = \App\Review_instances_media::findOrFail($id);

        switch (strtolower($media->type)) {
            case 'jpg':
            case 'png':
            case 'bmp':
                $path = $media->path;
                $file = Storage::get($path);
                break;
            case 'pdf':
                $file = Storage::get('/public/pdf.gif');
                break;
            case 'doc':
            case 'docx':
                $file = Storage::get('/public/microsoft-word.png');
                break;
            case 'ppt':
            case 'pptx':
                $file = Storage::get('/public/powerpoint_logo.gif');
                break;
            case 'xls':
            case 'xlsx':
                $file = Storage::get('/public/excel.png');
                break;
            default:
                $file = Storage::get('/public/unknown.png');
                break;
        }
        $image = Image::make($file);
        // get image height and width
        $origheight = $image->height();
        $origwidth = $image->width();
        // work out the ratio
        $ratio = ($origheight / $origwidth);
// work out size
        $size = array(($ratio < 0 ? (100 * $ratio) : 100), ($ratio < 0 ? 100 : (100 * $ratio)));
        // resize, dump out
        return $image->resize($size[0], $size[1])->response();

    }


    /**
     * Updates the record
     * @param $id
     * @param PatientRequest $request
     * @return $this
     */
    public function update(Request $request)
    {

        $input = $request::all();
        $input["user_id"] = Auth::user()->id;
        // handle file
        $media = \App\Review_instances_media::findOrFail($input['id']);
        $file = Request::file('userfile');
        if (isset($file)) {
            // check it's not too big
            if ($file->getMaxFilesize() > $file->getSize()) {
                // get type
                $type = $file->getClientOriginalExtension();
                // @todo is this an allowed type?
                //dd($file);
                // get the md5 hash of the contents. This allows for different files with the same name in teh same directory...
                $md5name = md5(file_get_contents($file->getPath() . DIRECTORY_SEPARATOR . $file->getFilename()));

                // store it to disk
                if (!(Storage::disk('local')->put('media' . DIRECTORY_SEPARATOR . $md5name, \Illuminate\Support\Facades\File::get($file)))) {
                    return '-1';
                }
                // save the record


                $media->name = $file->getClientOriginalName();
                $media->type = $type;
                $media->size = $file->getSize();
                $media->path = 'media' . DIRECTORY_SEPARATOR . $md5name;


            } else {
                return '-1';
            }
        }
        $media->description = $input["description"];
        return array(
            'status' => strval($media->save())
        );
    }

    public function destroy(Request $request)
    {
        $input = $request::all();
        $status = Review_instances_media::destroy($input['id']);
        $response = array(
            'status' => $status,
        );
        return $response;

    }

    public function import(Request $request)
    {
        //dd('Import!');
        $input = $request::all();
        $input["user_id"] = Auth::user()->id;

        $i = 0;
        // the courses folder URL
        $folder = 'media_import';

        $dir = new \DirectoryIterator($folder);
// iterate through folder for folders

        foreach ($dir as $fileinfo) {
            if (!$fileinfo->isDot()) {
// get the review ID
                //            if ($i < 10) {
                try {
                    $review_id = Sortable_review_instances::where('unit_code', '=', explode('_', $fileinfo)[0])->first()->id;
                   // print("$review_id<br/>");
                    $md5name = md5(file_get_contents('media_import' . DIRECTORY_SEPARATOR . $fileinfo));
                    //dd(file_get_contents('media_import'.DIRECTORY_SEPARATOR .$fileinfo));
                    //$type = $fileinfo->getClientOriginalExtension();                // store it to disk
                    if (!(Storage::disk('local')->put('media' . DIRECTORY_SEPARATOR . $md5name, file_get_contents('media_import' . DIRECTORY_SEPARATOR . $fileinfo)))) {
                        dd('Storage failed');
                        return '-1';
                    }
                    // save the record
                    $newentry = Review_instances_media::updateOrCreate(
                        [
                            'review_instances_id' => $review_id,
                            'name' => $fileinfo,
                            'type' => 'PNG',
                           // 'size' => filesize($fileinfo),
                            'description' => 'Grade distribution image',
                            'path' => 'media' . DIRECTORY_SEPARATOR . $md5name,
                        ],
                        [
                            'review_instances_id' => $review_id,
                            'name' => $fileinfo,
                            'type' => 'PNG',
                            //'size' => filesize($fileinfo),
                            'description' => 'Grade distribution image',
                            'path' => 'media' . DIRECTORY_SEPARATOR . $md5name,
                        ]
                    )->id;



                    print("New entry ID: {$newentry}<br/>");

                    // now, update the entries
                    // get the review checklist entry containing https://sps.une.edu.au/sites/biteam/ci-centre/CourseUnitMonitoring/SitePages/CourseUnitMonitoringFrontpage.aspx
                    $checklist_item = Review_instances_checklist_items::where('review_instances_id','=',$review_id)->where('coordinator_text','LIKE',"%<a>https://sps.une.edu.au/sites/biteam/ci-centre/CourseUnitMonitoring/SitePages/CourseUnitMonitoringFrontpage.aspx</a>%")->first();
                    // update
                    $updateStr = "<p><img src=\"../reviewmedia/show/".$newentry."\" width=\"800\"/></p>";
                    $checklist_item->coordinator_text.= $updateStr;
                    $checklist_item->peer_reviewer_text.= $updateStr;
                    $checklist_item->ld_text .= $updateStr;
                    $checklist_item->save();

                } catch (\Exception $e) {
                    print('Exception!' . $e->getMessage() . '<br/>');
                    print("Problem with $fileinfo" . '<br/>');
                }

                //              }
//$i++;
                //var_dump($fileinfo);
                //$md5name = md5(file_get_contents('media_import' . DIRECTORY_SEPARATOR . $fileinfo));
                //dd(file_get_contents('media_import'.DIRECTORY_SEPARATOR .$fileinfo));
//                $type = $fileinfo->getClientOriginalExtension();                // store it to disk
//                if (!(Storage::disk('local')->put('media' . DIRECTORY_SEPARATOR . $md5name, File::get($fileinfo)))) {
//                    return '-1';
//                }
//                // save the record
//                $newentry = new Review_instances_media();
//                $newentry->review_instances_id = $input["review_id"];
//                $newentry->name = $fileinfo->getClientOriginalName();
//                $newentry->type = $type;
//                $newentry->size = $fileinfo->getSize();
//                $newentry->description = $input["description"];
//                $newentry->path = 'media' . DIRECTORY_SEPARATOR . $md5name;
//                $newentry->save();
//                return $newentry->id;
            }
        }


//        $media = \App\Review_instances_media::findOrFail($input['id']);
//        $file = Request::file('userfile');
//        if (isset($file)) {
//            // check it's not too big
//            if ($file->getMaxFilesize() > $file->getSize()) {
//                // get type
//                $type = $file->getClientOriginalExtension();
//                // @todo is this an allowed type?
//                //dd($file);
//                // get the md5 hash of the contents. This allows for different files with the same name in teh same directory...
//                $md5name = md5(file_get_contents($file->getPath() . DIRECTORY_SEPARATOR . $file->getFilename()));
//
//                // store it to disk
//                if (!(Storage::disk('local')->put('media' . DIRECTORY_SEPARATOR . $md5name, \Illuminate\Support\Facades\File::get($file)))) {
//                    return '-1';
//                }
//                // save the record
//
//
//                $media->name = $file->getClientOriginalName();
//                $media->type = $type;
//                $media->size = $file->getSize();
//                $media->path = 'media' . DIRECTORY_SEPARATOR . $md5name;
//
//
//            } else {
//                return '-1';
//            }
//        }
//        $media->description = $input["description"];
//        return array(
//            'status' => strval($media->save())
//        );
    }


}

