<?php

namespace App\Http\Controllers;


use App\Units;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class UnitController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * List all contacts.
     * @todo make some filters
     * @return type
     */
    public function index($id)
    {
        return null;
    }

    public function search(Request $request)
    {
        $input = $request->all();

        $results = \App\Units::where('description', 'like', "%{$input['searchstr']}%");

        return $results->id;
    }

    public function create(Request $request)
    {
        $input = $request->all();
        // check that this unit doesn't already exist...
        if (Units::where('unit_code', $input['unit_code'])->count() == 0) {
            $newentry = \App\Units::create($input);
        } else {
            $newentry = Units::where('unit_code', $input['unit_code'])->first();
            $newentry->update($input);
        }
        return $newentry->id;
    }


    /**
     * Updates the record
     * @param $id
     * @param Request $request
     * @return $this
     */
    public function update(Request $request)
    {

        $input = $request->all();
        //dd($input);
        $unit = \App\Units::findOrNew($input['id']);

        $status = strval($unit->update($input));
        if (isset($input['course_ids'])) {
            $courseupdate = $unit->courses()->sync($input['course_ids']);
        }
        $courseupdatesuccess = (count($courseupdate['attached'] > 0) || count($courseupdate['detached'] > 0) || count($courseupdate['attached'] > 0));
        // update
        $status = ($unit->update($input) || $courseupdatesuccess);
        $response = array(
            'status' => $status ? '1' : '0',
        );
        return $response;
    }


    public function store(Request $request)
    {
        $input = $request->all();
        return \App\Units::create($input)->id;
    }

    public function show($id)
    {
        $unit = \App\Units::findOrFail($id);
        $courses = [];
        foreach ($unit->courses as $course) {
            $courses[] = $course->pivot->course_lookup_id;
        }
        $unit['course_ids[]'] = implode(',', $courses);
        return $unit;
        //return \App\Units::with('courses')->findOrFail($id);
    }

    // show the page
    public function view($id)
    {
        $unit = \App\Units::findOrFail($id);
        $courses = [];
        foreach ($unit->courses as $course) {
            $courses[] = $course->pivot->course_lookup_id;
        }
        $unit['course_ids[]'] = implode(',', $courses);

        return view('unit_course_management.unitview')
            ->with('unit', $unit)
            ->with('courses', $courses);
    }


    public function destroy(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        return \App\Units::destroy($input['id']);
    }
}
