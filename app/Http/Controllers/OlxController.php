<?php

namespace App\Http\Controllers;

use App\Emails_log;
use App\Emails_template;
use App\Olx;
use App\Olx_media;
use Illuminate\Http\Request;

class OlxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        // dd($request::all());
//        if (isset($request)) {
        $input = $request::all();
        $teachingperiodfilter = [];
        //  session(['teachingperiod' => []]);
        // little fix

        if (isset($input['teachingperiod'])) {
            session(['teachingperiod' => $input['teachingperiod']]);
            UserPreferences::set('olx_teachingperiod', $input['teachingperiod']);
        }
        //session(['teachingperiod' => []]);
        // session(['schoolid' => []]);
        //  dd(session('teachingperiod'));
//
        if (UserPreferences::has('olx_teachingperiod')) {
            session(['teachingperiod' => UserPreferences::get('olx_teachingperiod')]);
        }
        if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {
            //$teachingperiodfilter = explode(',',session('teachingperiod'));
            $teachingperiodfilter = session('teachingperiod');
        }
        // if(!is_array(session('teachingperiod'))){
        // session_unset('teachingperiod');
        // }
        $schoolfilter = [];
        if (isset($input['school'])) {
            session(['schoolid' => $input['school']]);
            //dd(session('schoolid'));
            UserPreferences::set('offerings_school', $input['school']);
        } else {
            if (UserPreferences::has('offerings_school')) {
                session(['schoolid' => UserPreferences::get('offerings_school')]);
            }
        }
//
//
        //  session(['schoolid' => []]);
        if (session()->has('schoolid') && session('schoolid') > 0) {
            $schoolfilter = session('schoolid');
        }


        if (isset($input['show_others'])) {
            //dd(session('show_others'));
            session(['show_others' => $input['show_others']]);
            //dd(session('schoolid'));
            UserPreferences::set('olx_show_others', $input['show_others']);
        } else {
            if (UserPreferences::has('olx_show_others')) {
                session(['show_others' => UserPreferences::get('olx_show_others')]);
            }
        }

        $offerings_query = Sortable_offering::whereNull('is_template')->whereNull('archived_at')->doesntHave('units_combined');
        if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {
            $offerings_query->whereIn('teaching_period_id', $teachingperiodfilter);
        }

        if (session()->has('schoolid') && count(session('schoolid')) > 0) {
            $offerings_query->whereIn('school_id', $schoolfilter);
        }



        if (!(isset(session('show_others')[1]) && (session('show_others')[1]=='1'))) {
            if(Auth::user()->roles->contains( "text" , "Coordinator")){
                $offerings_query->unitsforcoordinatororeso([Auth::user()->id]);
            }else {
                $offerings_query->unitsforeso([Auth::user()->id]);
            }
        }

        if(Auth::user()->roles->contains( "text" , "Coordinator")){
            $offerings_query->unitsforcoordinatororeso([Auth::user()->id]);
        }

        $offerings = $offerings_query->sortable()->paginate(100);

        // read only if the logged in user is a coordinator and not an ESO


        $users = \App\User::all();
        $units = \App\Unit::orderBy('unit_code', 'asc')->get();
        $teachingperiods = \App\Teaching_period::orderBy('year', 'desc')->get();
        $schools = \App\School::where('include_in_list', '=', 'true')->get();
        $templates = \App\Offering::whereNotNull('is_template')->get();
        $emailtemplates = Emails_template::where('context', 'unitsetup')->get();

        return view('unit_setup.list')
            ->with('offerings', $offerings)
            ->with('units', $units)
            ->with('teachingperiods', $teachingperiods)
            ->with('schools', $schools)
            ->with('templates', $templates)
            ->with('emailtemplates', $emailtemplates)
            ->with('users', $users);
    }

    public
    function show($id)
    {
        //$this->authorize('view', Review_instances::class);
        $instance = \App\Olx::findOrFail($id);
        $allinstances = \App\Olx::all();
        $users = \App\User::all();
        $units = \App\Unit::all();
        // $types = \App\Event_type_lookup::all();
        $locations = Location_lookup::all();
        $teachingperiods = \App\Teaching_period::all();
        $emailtemplates = Emails_template::where('context', 'unitsetup')->get();
        $combinedids = [];
        //dd($instance->combined_units->pluck('unit_id')->contains('594'));
        foreach ($instance->combined_units as $combined_unit) {
            $combinedids[] = $combined_unit->unit_id;
        }
        //  dd($combinedids);
        $instance['combined_units_ids'] = $combinedids;

        // read only if it's the coordinator logged in and you're not the ESO, and it's not archived
        $readonly = (((Auth::user()->id == $instance->coordinator['id'])&&(Auth::user()->id!=$instance->eso_id['id']))||(isset($instance->archived_at)));
        // $readonly = false;

        return view('unit_setup.view')
            ->with('instance', $instance)
            ->with('allinstances', $allinstances)
            ->with('units', $units)
            //   ->with('types', $types)
            ->with('locations', $locations)
            ->with('teachingperiods', $teachingperiods)
            ->with('emailtemplates', $emailtemplates)
            ->with('readonly', $readonly)
            ->with('users', $users);
    }

    public
    function store(Request $request)
    {
        $input = $request::all();
        // make a new unit instance- call as an internal function
        return self::newOlx($input);

    }

// A private new instance function. Internal because we're calling it from a couple places
    private
    function newOlx($input)
    {

        //array('unit_id' => $unitid, "template_id" => $input['template_id'], 'teaching_period_id' => $input['teaching_period_id'], 'eso_id' => $input['eso_id'], 'coordinator_id' => $coordinatorid, 'combined_units' => $combinedunitsstr));
        $newinstance = \App\Olx::updateOrCreate(["offering_id" => $input['offering_id'],
            "olx_admin_id" => $input['olx_admin_id'],
            "coordinator_id" => $input['coordinator_id']]);
        // get the template ID
        $templateid = $input['template_id'];
// get the checklist items
        $checklistitems = Olx::find($templateid)->checklist;
        // get the new instance ID

        // build an array of new items to be inserted
        $newitems = [];
        foreach ($checklistitems as $item) {
            $newitems[] = Olx_checklist_items::updateOrCreate([
                'olx_id' => $newinstance->id,
                'heading' => $item['heading'],
                'description' => $item['description'],
                'text' => $item['text'],
                'order' => $item['order'],
                'trigger_email' => $item['trigger_email'],
                'trigger_email_id' => $item['trigger_email_id']]);
        }


        $response = array(
            'id' => strval($newinstance->id),
            //  'calendarstatus' => strval($calendarstatus)
        );

        return $response;
    }





    private
    function rrmdir($dir)
    {
        foreach (glob($dir . '/*') as $file) {
            if (is_dir($file))
                self::rrmdir($file);
            else
                unlink($file);
        }
        rmdir($dir);
    }


    public
    function clone_instance(Request $request)
    {

        $input = $request::all();
        $originalid = $input['id'];
// get the checklist items
        $checklistitems = Olx::find($originalid)->checklist;
        //  dd($checklistitems);
        // get the new instance ID
        // kill off id, or there'll be issues
        unset($input['id']);
        $newinstance = \App\Olx::create($input);
        // dd($newinstance);
        // build an array of new items to be inserted
        $newitems = [];
        foreach ($checklistitems as $item) {
            $newitems[] = new Olx_checklist_items(['description' => $item['description'], 'text' => $item['text'], 'order' => $item['order'], 'heading' => $item['heading']]);
        }
// just gunna have to assume this works. Doesn't return anything if it fails...
        $newinstance->checklist()->saveMany($newitems);

        $response = array(
            'id' => $newinstance->id,
        );

        return $response;
    }

    public
    function ajaxstore(Request $request)
    {

        $input = $request::all();
        $status = strval(\App\Olx::create($input)->id);
        $response = array(
            'status' => $status,
        );
        return $response;

    }

    public
    function ajaxupdate(Request $request)
    {
        $input = $request::all();
        $instance = \App\Olx::findOrFail($input['id']);
        //  dd($input);

        $status = strval($instance->update($input));

        $response = array(
            'status' => $status ? '1' : '0',
        );
        return $response;
    }

    public
    function updatefollowup($status)
    {

    }

    public function updateStatus(Request $request)
    {
        $input = $request::all();
        $instance = \App\Olx::findOrFail($input['id']);

        // working out completion percentage
        $percentComplete = round(($instance->checklistcomplete / ($instance->checklist()->get()->where('heading', '<>', '1')->count())) * 100);

        if (!is_nan($percentComplete)) {
            $input['complete_percent'] = $percentComplete;
        }

        $status = ($instance->update($input));

        $response = array(
            'status' => $status ? '1' : '0',
        );
        return $response;
    }

    public
    function destroy(Request $request)
    {
        $input = $request::all();
        //dd($input);
        $status = (\App\Olx::destroy($input['id']) > 0 ? '0' : '1');
        $response = array(
            'status' => $status,
        );
        return $response;
    }


    public
    function archive($id)
    {
        $review = \App\Olx::findOrFail($id);
        $review->archived_at = Carbon::now();
        $status = strval($review->save());
        $response = array(
            'status' => $status,
        );
        return $response;
    }

    public
    function createmedia(Request $request)
    {

        $input = $request::all();
        $input["user_id"] = Auth::user()->id;
        // handle file
        //dd($input);
        // $uploadedfile = $input["userfile"];
        $file = Request::file('userfile');
        // check it's not too big
        if ($file->getMaxFilesize() > $file->getSize()) {
            // get type
            $type = $file->getClientOriginalExtension();
            // @todo is this an allowed type?

            // get the md5 hash of the contents. This allows for different files with the same name in teh same directory...
            $md5name = md5(file_get_contents($file->getPath() . DIRECTORY_SEPARATOR . $file->getFilename()));

            // store it to disk
            if (!(Storage::disk('local')->put('media' . DIRECTORY_SEPARATOR . $md5name, File::get($file)))) {
                return '-1';
            }
            // save the record
            $newentry = new Olx_media();
            $newentry->olx_id = $input["olx_id"];
            $newentry->name = $file->getClientOriginalName();
            $newentry->type = $type;
            $newentry->size = $file->getSize();
            $newentry->description = $input["description"];
            $newentry->path = 'media' . DIRECTORY_SEPARATOR . $md5name;
            $newentry->save();
            return $newentry->id;

        } else {
            return '-1';
        }
        //$response = array(
        //  'status' => 'success',
        //'msg' => 'Setting created successfully',
        //);

        return '-1';
    }

    /**
     * Updates the Offerings table completion statistics
     * @param Request $request
     * @return
     */
    public function updateCompletion(Request $request)
    {
        $input = $request::all();
        $instances = \App\Olx::all();

        foreach ($instances as $instance) {
            $percentComplete = round(($instance->checklistcomplete / ($instance->checklist()->get()->where('heading', '<>', '1')->count())) * 100);
            if (!is_nan($percentComplete)) {
                $instance->complete_percent = $percentComplete;
                $instance->save();
//                print ("Updating {$instance->unit['unit_code']} {$instance->teaching_period->teaching_period_lookup->teaching_period} {$instance->teaching_period['year']} to {$instance->complete_percent} <br/>");
            }


            //$instance->complete_percent = round(($instance->checklistcomplete / ($instance->checklist()->get()->where('heading', '<>', '1')->count())) * 100);
            //$instance->save();
            //print ("Updating {$instance->unit['unit_code']} {$instance->teaching_period->teaching_period_lookup->teaching_period} {$instance->teaching_period['year']} to {$instance->complete_percent} <br/>");
        }


        $response = array(
            'status' => '0',
        );
        return $response;
    }

///////////////////////////////////////////////////////////////
// template functions
///////////////////////////////////////////////////////////////
    public
    function templateindex(Request $request)
    {
        //$reviews = Review_instances::sortable()->paginate(20);
        $users = \App\User::all();

        $templates = \App\Offering::all()->where('is_template', '=', 'true');
        $emailtemplates = Olx::where('context', 'olx')->get();
        //    dd($templates);
        //dd($reviews);
        return view('unit_setup.templates.list')
            ->with('instances', $templates)
            ->with('emailtemplates', $emailtemplates)
            ->with('users', $users);
    }

    public
    function templateshow($id)
    {
        //$this->authorize('view', Review_instances::class);

        $instance = \App\Offering::findOrFail($id);
        $emailtemplates = Emails_template::where('context', 'olx')->get();
        $users = \App\User::all();

        return view('unit_setup.templates.view')
            ->with('instance', $instance)
            ->with('emailtemplates', $emailtemplates)
            ->with('users', $users);
    }

    public
    function templatestore(Request $request)
    {
        $input = $request::all();
        //dd(explode(',', $input['combined_units']));
        $newunit = \App\Olx::create($input);
        $response = array(
            'id' => strval($newunit->id),
        );
        return $response;
    }

    public
    function templateajaxupdate(Request $request)
    {
        $input = $request::all();
        $instance = \App\Olx::findOrFail($input['id']);
        $status = strval($instance->update($input));
        $response = array(
            'status' => $status ? '1' : 0,
        );
        return $response;
    }


/////////////////////////////////////////////////////////////////////////////////////////////
//email
//////////////////////////////////////////////////////////////////////////////////////////////
    public
    function sendemail($id, Request $request)
    {
//        $input = $request::all();
//        //dd($input);
//        $template = Emails_template::find($input['template_id']);
//        $unit = Offering::find($id);
//        //print_r($input);
//        try {
//            // mail to unit coordinator
//            $email = new UnitSetupEmail($unit, $template);
//
//            // dispatch to the job queue
//            //dispatch(new SendSetupEmail($email, (isset($input['testing']) ? Auth::user()->id : $unit->coordinator->id), Auth::user()->id, $template, $id));
//            //   dispatch(new SendSetupEmail($email, Auth::user()->id, Auth::user()->id, $template, $id));
//            // log success
//            $response = array(
//                'status' => '0',
//            );
//            $log = new Emails_log();
//            $log->email_id = $input['template_id'];
//            $log->instances_id = $id;
//            $log->context = 'offeringsetup';
//            $log->sent_to_id = $unit->coordinator->id;
//            $log->sent_by_id = Auth::user()->id;
//            $log->fulltext = $email->getfulltext();
//            $log->save();
//        } catch (\Exception $e) {
//            // handle failure
//            $response = array(
//                'status' => '1',
//            );
//        }
//        return $response;
    }



    public
    function getemail($id, $emailid)
    {
        return Emails_log::find($emailid);
    }


    public
    function bulkarchive(Request $request)
    {
        $input = $request::all();
        // get ids

        if (strlen($input['ids']) > 0) {
            $ids = explode(',', $input['ids']);
            $units = Offering::whereIn('id', $ids)->update(['archived_at' => Carbon::now()]);;
            //dd($units);
            //$units->update(['archived_at' => Carbon::now()]);
            //   try {
            //$units->update(['archived_at', Carbon::now()]);
        }

        $response = array(
            'status' => '0',
        );
        return $response;
    }

    private
    function unique_multidim_array($array, $key)
    {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }
}
