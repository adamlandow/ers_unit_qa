<?php

namespace App\Http\Controllers;

use App\Course;
use App\Course_trim;
use App\Field_of_study;
use Illuminate\Database\Eloquent\Collection;
use App\Fieldofstudy;
use App\Unit;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
//@TODO permissions
        $params = $request->all();
        if (isset($params['search'])) {
            $courses = new Collection();

            $courses = Course::where('course_status', '<>', 'DISCONTIN')->where('course_code', 'like', "%{$params['search']}%")->orWhere('label', 'like', "%{$params['search']}%")->paginate(20);

        } else {

            $courses = Course::where('course_status', '<>', 'DISCONTIN')->paginate(20);


        }
        //  $course = Course::orderBy('course_code')->paginate(20);
        $users = User::all();
        return view("setup.course.list")->with('courses', $courses)->with('users', $users);
    }

    public function show($id)
    {
        $input = request()->input();
//dd($input);
        if (isset($input['major'])) {
            if ($input['major'] > 0) {
                session(['major' => $input['major']]);
            } else {
                session()->forget('major');
            }
        }
        //    dd(session('major'));
        $course = \App\Course::findOrFail($id);
        $users = User::all();
        //filter out the units already in this course
      //  $units = Unit::whereNotIn('id', $course->units->pluck('id'))->get();
        $units = Unit::all();

        $fields_of_study = Field_of_study::whereIn('id', $course->fields_of_study->pluck('id'))->get();

        // group units- this allows us to haeve one entry per course, but list the different majors the unit is in
        $groupedcoreunits = $course->coreunits()->withPivot('field_of_study_id', 'unit_id')->get()->groupBy('pivot.unit_id');
        $groupedprescribedunits = $course->prescribedunits()->withPivot('field_of_study_id', 'unit_id')->get()->groupBy('pivot.unit_id');
        $groupedlistedunits = $course->listedunits()->withPivot('field_of_study_id', 'unit_id')->get()->groupBy('pivot.unit_id');

//dd($majors);
        return view("setup.course.view")
            ->with('course', $course)
            ->with('users', $users)
            ->with('units', $units)
            ->with('fields_of_study', $fields_of_study)
            ->with('groupedcoreunits', $groupedcoreunits)
            ->with('groupedprescribedunits', $groupedprescribedunits)
            ->with('groupedlistedunits', $groupedlistedunits);
    }

    public function ajaxstore(Request $request)
    {
        $input = $request->all();
        $status = \App\Course::create($input)->id;
        return array(
            'status' => ($status > 0) ? '0' : '1',
        );
    }

    /**
     * Updates the record
     * @param $id
     * @param Request $request
     * @return $this
     */
    public function update(Request $request)
    {

        $input = $request->all();
        //dd($input);
        $course = \App\Course::findOrNew($input['id']);

        $status = strval($course->update($input));

        $response = array(
            'status' => $status ? '1' : '0',
        );
        return $response;
    }

    public function ajaxdestroy(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        return \App\Course::destroy($input['id']);
    }


    public function addunit(Request $request)
    {
        $input = $request->all();
        $course = \App\Course::findOrNew($input['id']);

        // if the unit to be attachedis not already in the system, the unit has come from CAUC lookup and needs to be added

        if ($input['units_id'] < 0) {
            //    dd(UnitController::addFromCAUC($input['unit']));
            $input['units_id'] = UnitController::addFromCAUC($input['unit']);
        }

        $result = $course->units()->attach($input['units_id'], ['major_id' => ($input['major_id'] > 0 ? $input['major_id'] : null), 'relationship' => $input['relationship']]);

        $response = array(
            'status' => $result ? '1' : '0',
        );
        return $response;
    }


    public function detachunit(Request $request)
    {
        $input = $request->all();
        $course = \App\Course::findOrFail($input['id']);
        //dd($input);
        $result = $course->units()->wherePivot('id',$input['pivot_id'])->detach();
        $response = array(
            'status' => $result ? '0' : '1',
        );
        return $response;
    }

    // add a TRIM reference to the course
    public function ajaxaddtrim(Request $request){
        $input = $request->all();
        $result = Course_trim::updateOrCreate(['reference'=>$input['reference'], 'course_id'=>$input['id'], 'version'=>$input['version']])->id;
        $response = array(
            'status' => ($result>0) ? '0' : '1',
        );
        return $response;
    }



}
