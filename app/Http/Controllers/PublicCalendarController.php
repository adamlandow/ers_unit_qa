<?php

namespace App\Http\Controllers;


use App\CustomClasses\iCalReader;
use App\Event;
use App\Http\Requests\ImportRequest;
use App\Http\Requests\Request;
use App\Location_lookup;
use App\Type_lookup;
use App\Unit;
use App\Offering;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PublicCalendarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
// no auth
    }


    public function index()
    {
        return null;
    }

    public function show()
    {
        return null;
    }



    public function ical($unitid, $oncampus = null)
    {

//$query =
        $events = \App\Event::where((isset($oncampus)? ['onoffcampus' => $oncampus]:[]))->where('units_instance_id', $unitid)->get();
            //    ->toSql();

        $i = 0;

        $calStr = "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-//hacksw/handcal//NONSGML v1.0//EN\nX-WR-CALNAME;VALUE=TEXT:".Offering::find($unitid)->unit['unit_code']." timetable (" . (isset($oncampus) ? ($oncampus == 'ON' ? "On campus" : "Off campus") : "All") . ")\n";
        $calStr .= "BEGIN:VTIMEZONE\nTZID:Australia/Sydney\nBEGIN:STANDARD\nDTSTART:20000402T030000\nRRULE:FREQ=YEARLY;BYMONTH=4;BYDAY=1SU\nTZOFFSETFROM:+1100\nTZOFFSETTO:+1000\nEND:STANDARD\nBEGIN:DAYLIGHT\nDTSTART:20001008T020000\nRRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=1SU\nTZOFFSETFROM:+1000\nTZOFFSETTO:+1100\nEND:DAYLIGHT\nEND:VTIMEZONE\n";

// start ical output

        $length = count($events);


        foreach ($events as $event) {

            $calStr .= "BEGIN:VEVENT\n";
            $calStr .= "UID:".$event->id. "\n";
//dd( $this->dateToCal(time(), false));
            $calStr .= "DTSTAMP:" . $this->dateToCal(time(), false) . "\n";

            $starttime = new Carbon($event->start_timestamp);
            $calStr .= "DTSTART;TZID=Australia/Sydney;VALUE=DATE-TIME:" . $this->dateToCal($starttime->timestamp, false) . "\n";
            $calStr .= "DURATION:" . $event->duration . "\n";


            // add alarm to things with alarms defined
            if (isset($event->type['alarm_prior_secs'])) {
                $alarmtimestr = gmdate('\P\TH\Hi\M', $event->type['alarm_prior_secs']);
                //$alarmtimestr = gmdate('H:i', $event->type['alarm_prior_secs']);
                $calStr .= "BEGIN:VALARM\nTRIGGER:-" . $alarmtimestr . "\nACTION:DISPLAY\nDESCRIPTION:" . $event->description . "\nEND:VALARM\n";
            }

            $calStr .= "LOCATION;ALTREP=\"https://my.une.edu.au/map/#S3\";LANGUAGE=en:" . $event->location['url'] . "\n";
            $calStr .= "SUMMARY:" . $event->description . "\n";
            $calStr .= "DESCRIPTION:" . $event->description . "\n";
            $calStr .= "END:VEVENT\n";
            $i++;

        }

        $calStr .= "END:VCALENDAR";

// clean up vertical whitespace
        $calStr = preg_replace('/^\n+|^[\t\s]*\n+/m', '', $calStr);

//return $calStr;
        return response($calStr)->header("Content-Type", "text/plain");
    }

    private function dateToCal($timestamp, $includetimezone = true)
    {
        return ($includetimezone ? "TZID=Australia/Sydney:" : "") . date('Ymd\THis', $timestamp);
    }
}
