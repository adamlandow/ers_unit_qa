<?php

namespace App\Http\Controllers;

use App\Emails_log;
use App\Emails_template;
use App\Http\Controllers\Auth\LoginController;
use App\Jobs\SendSetupEmail;
use App\Location_lookup;
use App\Mail\UnitSetupEmail;
use App\Review_instances_checklist_items;
use App\Roles;
use App\Sortable_offering;
use App\Offering;
use App\Offering_checklist_items;
use App\Offering_media;
use App\Unit;
use App\Unit_setup_emails_log;
use Barryvdh\Debugbar\Facade;
use DebugBar\DebugBar;
use Carbon\Carbon;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Mockery\CountValidator\Exception;
use PhpSpec\Exception\Example\ErrorException;
use RobTrehy\LaravelUserPreferences\UserPreferences;

class OfferingsController extends Controller
{
    //
    //
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        // dd($request::all());
//        if (isset($request)) {
        $input = $request::all();


        $teachingperiodfilter = [];
        //  session(['teachingperiod' => []]);
        // little fix

        if (isset($input['teachingperiod'])) {
            session(['teachingperiod' => $input['teachingperiod']]);
            UserPreferences::set('offerings_teachingperiod', $input['teachingperiod']);
        }
        //session(['teachingperiod' => []]);
       // session(['schoolid' => []]);
           //  dd(session('teachingperiod'));
//
        if (UserPreferences::has('offerings_teachingperiod')) {
            session(['teachingperiod' => UserPreferences::get('offerings_teachingperiod')]);
        }
        if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {
            //$teachingperiodfilter = explode(',',session('teachingperiod'));
            $teachingperiodfilter = session('teachingperiod');
        }
        // if(!is_array(session('teachingperiod'))){
       // session_unset('teachingperiod');
        // }
        $schoolfilter = [];
        if (isset($input['school'])) {
            session(['schoolid' => $input['school']]);
            //dd(session('schoolid'));
            UserPreferences::set('offerings_school', $input['school']);
        } else {
            if (UserPreferences::has('offerings_school')) {
                session(['schoolid' => UserPreferences::get('offerings_school')]);
            }
        }
//
//
      //  session(['schoolid' => []]);
        if (session()->has('schoolid') && session('schoolid') > 0) {
            $schoolfilter = session('schoolid');
        }


        if (isset($input['show_others'])) {
            //dd(session('show_others'));
            session(['show_others' => $input['show_others']]);
            //dd(session('schoolid'));
            UserPreferences::set('offerings_show_others', $input['show_others']);
        } else {
            if (UserPreferences::has('offerings_show_others')) {
                session(['show_others' => UserPreferences::get('offerings_show_others')]);
            }
        }

        $offerings_query = Sortable_offering::whereNull('is_template')->whereNull('archived_at')->doesntHave('units_combined');
        if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {
            $offerings_query->whereIn('teaching_period_id', $teachingperiodfilter);
        }

        if (session()->has('schoolid') && count(session('schoolid')) > 0) {
            $offerings_query->whereIn('school_id', $schoolfilter);
        }



        if (!(isset(session('show_others')[1]) && (session('show_others')[1]=='1'))) {
            if(Auth::user()->roles->contains( "text" , "Coordinator")){
                $offerings_query->unitsforcoordinatororeso([Auth::user()->id])
                ;
            }else {
                $offerings_query->unitsforeso([Auth::user()->id]);
            }
        }

        if(Auth::user()->roles->contains( "text" , "Coordinator")){
            $offerings_query->unitsforcoordinatororeso([Auth::user()->id]);
        }

       // DB::enableQueryLog();

        $offerings = $offerings_query->orWhereNull('school_id')->sortable()->paginate(100);
      //  dd(DB::getQueryLog());
      //  dd($offerings_query->toSQL());
       // dd($offerings);

        // read only if the logged in user is a coordinator and not an ESO


        $users = \App\User::all();
        $units = \App\Unit::orderBy('unit_code', 'asc')->get();
        $teachingperiods = \App\Teaching_period::orderBy('year', 'desc')->get();
        $schools = \App\School::where('include_in_list', '=', 'true')->get();
        $templates = \App\Offering::whereNotNull('is_template')->get();
        $emailtemplates = Emails_template::where('context', 'unitsetup')->get();

        return view('unit_setup.list')
            ->with('offerings', $offerings)
            ->with('units', $units)
            ->with('teachingperiods', $teachingperiods)
            ->with('schools', $schools)
            ->with('templates', $templates)
            ->with('emailtemplates', $emailtemplates)
            ->with('users', $users);
    }

    public
    function show($id)
    {
        //$this->authorize('view', Review_instances::class);

        $instance = \App\Offering::findOrFail($id);
        $allinstances = \App\Offering::all();
        $users = \App\User::all();
        $units = \App\Unit::all();
        // $types = \App\Event_type_lookup::all();
        $locations = Location_lookup::all();
        $teachingperiods = \App\Teaching_period::all();
        $emailtemplates = Emails_template::where('context', 'unitsetup')->get();
        $combinedids = [];
        //dd($instance->combined_units->pluck('unit_id')->contains('594'));
        foreach ($instance->combined_units as $combined_unit) {
            $combinedids[] = $combined_unit->unit_id;
        }
        //  dd($combinedids);
        $instance['combined_units_ids'] = $combinedids;

        // read only if it's the coordinator logged in and you're not the ESO, and it's not archived
        $readonly = (((Auth::user()->id == $instance->coordinator['id'])&&(Auth::user()->id!=$instance->eso_id['id']))||(isset($instance->archived_at)));
       // $readonly = false;



        return view('unit_setup.view')
            ->with('instance', $instance)
            ->with('allinstances', $allinstances)
            ->with('units', $units)
            //   ->with('types', $types)
            ->with('locations', $locations)
            ->with('teachingperiods', $teachingperiods)
            ->with('emailtemplates', $emailtemplates)
            ->with('readonly', $readonly)
            ->with('users', $users);
    }

    public
    function store(Request $request)
    {
        $input = $request::all();
        // make a new unit instance- call as an internal function
        return self::newOffering($input);

    }

// A private new instance function. Internal because we're calling it from a couple places
    private
    function newOffering($input)
    {

        //array('unit_id' => $unitid, "template_id" => $input['template_id'], 'teaching_period_id' => $input['teaching_period_id'], 'eso_id' => $input['eso_id'], 'coordinator_id' => $coordinatorid, 'combined_units' => $combinedunitsstr));
        $newinstance = \App\Offering::updateOrCreate(["unit_id" => $input['unit_id'],
            "teaching_period_id" => $input['teaching_period_id'],
            "eso_id" => $input['eso_id'],
            "coordinator_id" => $input['coordinator_id']]);
        // get the template ID
        $templateid = $input['template_id'];
// get the checklist items
        $checklistitems = Offering::find($templateid)->checklist;
        // get the new instance ID

        // build an array of new items to be inserted
        $newitems = [];
        foreach ($checklistitems as $item) {
            $newitems[] = Offering_checklist_items::updateOrCreate([
                'offering_id' => $newinstance->id,
                'description' => $item['description'],
                'text' => $item['text'],
                'order' => $item['order'],
                'heading' => $item['heading'],
                'trigger_email' => $item['trigger_email'],
                'trigger_email_id' => $item['trigger_email_id']]);
        }
// just gunna have to assume this works. Doesn't return anything if it fails...
//        $newinstance->checklist()->saveMany($newitems);

        // associate combined units IDs
        try {
            // add combined units
            // check to see if there's an instance of the combined unit in the table
            $combinedunits = explode(',', $input['combined_units']);
            if (count($combinedunits) > 0) {
                $sync_array = [];
                foreach ($combinedunits as $combinedunit) {
                    if (strlen($combinedunit) > 0) {
                        // check the unit, update
                        $sync_array[] = Offering::updateOrCreate(['unit_id' => $combinedunit, 'coordinator_id' => $newinstance['coordinator_id'], 'eso_id' => $newinstance['eso_id'], 'teaching_period_id' => $newinstance['teaching_period_id']])->id;
                    }
                }
                $update = $newinstance->combined_units()->sync($sync_array);
                // $updatesuccess = (count($update['attached'] > 0) || count($update['detached'] > 0) || count($update['attached'] > 0));
                // update user
                //  $status = $updatesuccess;
            }

        } catch (\Exception $e) {
            // do task when error
            dd($e->getMessage());   // insert query
        }

        $response = array(
            'id' => strval($newinstance->id),
            //  'calendarstatus' => strval($calendarstatus)
        );

        return $response;
    }


// @todo properly implement this- with a bit of error checking etc.
// Import from direct AJAX export.
    public
    function csvimport(Request $request)
    {

        $returnstatusval = 0;
        $input = $request::all();
        $dataArr = [];
        // dd($input);
        if (!empty($request::file('csvfile'))) {

            $input["user_id"] = Auth::user()->id;
            $count = 0;
            $unitrow = 0;
            $combinedrow = 0;
            $coordinatorrow = 0;
            $isfirstrow = true;
            // handle file
            // get the file. @TODO do some validation to ensure it's actually a CSV file
            $file = $request::file('csvfile');
            // build an array of info
            if (($handle = fopen($file, "r")) !== FALSE) {
                // loop through the CSV
                while (($data = fgetcsv($handle)) !== FALSE) {
                    // get the first row, work out the indices from teh headers
                    if ($isfirstrow) {
                        if (array_search('Caur Unit Cd', $data) !== false) {
                            $unitrow = array_search('Caur Unit Cd', $data);
                        } else {
                            return array('error' => '1',
                                'value' => 'Need to have a header called "Caur Unit Cd"'
                            );
                        }
                        if (array_search('Combined Unit', $data) !== false) {
                            $combinedrow = array_search('Combined Unit', $data);
                        } else {
                            return array('error' => '1',
                                'value' => 'Need to have a header called "Combined Unit"'
                            );
                        }
                        if (array_search('Coordinator', $data) !== false) {
                            $coordinatorrow = array_search('Coordinator', $data);
                        } else {
                            return array('error' => '1',
                                'value' => 'Need to have a header called "Coordinator"'
                            );
                        }
                        $isfirstrow = false;
                    } else {
                        // check that the current unit isn't a combined unit
                        $dataArr[] = $data;

                    }
                }
            }
        } else {
            return array('error' => '1',
                'value' => "No file!"
            );
        }
        // get unique unit values

        $outArr = [];


        // dd($combinedUnitsArr);
        // loop through data, build a useful array for processing

        foreach ($dataArr as $item) {
            //  dd($item);
            // rebuild the deduped units list
            $unitsOutArr = array_column($outArr, $unitrow);
            $combinedUnitsOutArr = array_column($outArr, $combinedrow);

            // is this already a combined unit somewhere?
            // this looks clumsy, but works. @TODO clean this up one day
            $combinedUnitCode = $item[$combinedrow];
            $notCombinedUnit = true;
            // loop through the combined entry column
            foreach ($combinedUnitsOutArr as $combinedunitentry) {
                // if the current entry unit code is one of the combined units already collected
                if (in_array($item[$unitrow], explode(',', $combinedunitentry))) {
                    // set a flag to false
                    $notCombinedUnit = false;

                }
            }

            // if we've not already looked at this unit, or it's not already in a combined units list
            if ($notCombinedUnit) {
                // make a new entry if it's not already
                if ((!in_array($item[$unitrow], $unitsOutArr))) {
                    $outArr[] = $item;
                    if (strlen($combinedUnitCode) > 0) {
                        if (!in_array($combinedUnitCode, explode(',', end($outArr)[$combinedrow]))) {
                            end($outArr)[$combinedrow] .= (',' . $combinedUnitCode);
                        }
                    }
                } else {
                    // if there's a combined unit coe entry
                    if (strlen($combinedUnitCode) > 0) {
                        // if the combined unit code has not already been added to the combined units entry
                        if (!in_array($combinedUnitCode, explode(',', $outArr[array_search($item[$unitrow], $unitsOutArr)][$combinedrow]))) {
                            $outArr[array_search($item[$unitrow], $unitsOutArr)][$combinedrow] .= (',' . $combinedUnitCode);
                        }
                    }
                }

            }

        }

        //  dd($outArr);
// now we have a useful array in $outArr, perform the task...
        foreach ($outArr as $item) {
//            // get the coordinator for this unit
            //  try {
            // look up from LDAP
            $userdetails = UserController::getDetailsByFullName($item[$coordinatorrow]);
            // dd($userdetails[0]);
// if there's a match
            if ((count($userdetails) > 0) && ($userdetails['count'] > 0)) {
                //see if the usernames match up
                $coordinator = \App\User::where('username', $userdetails[0]["uid"][0])->first();
                // does coordinator already exist?
                if ($coordinator === null) {
                    $newuser = new \App\User();
                    $newuser->email = $userdetails[0]["mail"][0];
                    $newuser->name = $userdetails[0]["displayname"][0];
                    $newuser->username = $userdetails[0]["uid"][0];
                    $newuser->type = 'ad';
                    $newuser->save();
                    $newuser->roles()->sync([Roles::where('text', 'Coordinator')->first()->id]);
                    $coordinatorid = $newuser->id;
                } else {
                    $coordinatorid = $coordinator->id;
                }

            } else {
                // make it a null user

                $response = array(
                    'error' => '1',
                    'value' => "User {$item[$coordinatorrow]} not found in directory",
                );

                // return $response;
                $coordinatorid = -1;
            }

//            } catch (\Exception $e) {
//                // else make a new user from LDAP
//                $newuser = new \App\User();
//
//                // get LDAP values for user here
//
//                $userdetails = UserController::getDetailsByFullName($item[$coordinatorrow]);
//
//
//            }

            // get the unit ID
            // does the unit exist?
            //dd(\App\Unit::where('unit_code',  $item[$unitrow])->count());
            if (\App\Unit::where('unit_code', $item[$unitrow])->count() > 0) {

                $unitid = \App\Unit::where('unit_code', $item[$unitrow])->first()->id;

            } else {
                $unitid = UnitController::addFromCAUC($item[$unitrow])->id;
            }


            // build list of combined units ids
            $combinedunitsarr = explode(',', $item[$combinedrow]);
            $combinedunitsstr = "";

            foreach ($combinedunitsarr as $unit) {


                if (strlen($unit) > 0) {
                    if (\App\Unit::where('unit_code', $unit)->count() > 0) {
                        $combinedunitsstr .= \App\Unit::where('unit_code', $unit)->firstOrFail()->id . ',';
                    } else {
                        $combinedunitsstr .= UnitController::addFromCAUC($unit)->id . ',';
                    }

                }
            }

            $combinedunitsstr = rtrim($combinedunitsstr, ',');

            $this->newOffering(array('unit_id' => $unitid, "template_id" => $input['template_id'], 'teaching_period_id' => $input['teaching_period_id'], 'eso_id' => $input['eso_id'], 'coordinator_id' => $coordinatorid, 'combined_units' => $combinedunitsstr));

        }


        return array('status' => '0',

        );
    }

// read CAUC for units information. Saves having to make a new unit entry
    public
    function createunitfromCAUC($unitcode)
    {
        $url = "https://my.une.edu.au/courses/units/{$unitcode}.json";
        try {

            $str = file_get_contents($url);

            $json = json_decode($str, true);
            if (Unit::where('unit_code', $unitcode)->count() == 0) {
                // cache the JSON
                $newentry = \App\Unit::create(['unit_code' => $unitcode, 'description' => $json['title'], 'cauc_data_cache' => $str]);
            } else {
                $newentry = Unit::where('unit_code', $unitcode)->first();
                $newentry->update(['unit_code' => $unitcode, 'description' => $json['title'], 'cauc_data_cache' => $str]);
            }
        } catch (\Exception $err) {
            // @TODO handle this better
            return -1;
        }
        return $newentry->id;
    }

// get (and store) a Moodle URL for a given course instance
    public
    function detectmoodleurl($id)
    {
        $instance = \App\Offering::findOrFail($id);
        // UNE Moodle search page for a given unit
        $searchurl = "https://moodle.une.edu.au/course/search.php?search=" . $instance->unit->unit_code;
        //  dd($instance->teaching_period->teaching_period_lookup['description']);
        // load the search results into a usable object
        $doc = new \DOMDocument();
        // search page is not well formed!
        $doc->recover = true;
        $doc->strictErrorChecking = false;
        @$doc->loadHTMLFile($searchurl);
        // find the results- they all have class 'coursename'
        $finder = new \DomXPath($doc);
        $classname = "coursename";
        $nodes = $finder->query("//*[@class='" . $classname . "']");
        // loop until we find a match
        foreach ($nodes as $node) {
            if (stripos($node->nodeValue, substr($instance->teaching_period->teaching_period_lookup['description'], -1) . ' ' . $instance->teaching_period['year'] . ' ' . $instance->unit->unit_code) !== false) {
                // update instance\
                $instance->moodle_url = $node->getElementsByTagName('a')[0]->getAttribute('href');
                $instance->save();
                // send a result back to the page
                $response = array(
                    'status' => '1',
                    'value' => $node->getElementsByTagName('a')[0]->getAttribute('href')
                );
                return $response;
            }
        }
        $response = array(
            'status' => '1',
            'value' => 'not found'
        );
        return $response;
    }

// read CAUC for units information. Saves having to make a new unit entry
    public
    function updatecaucdata($id)
    {

        $instance = \App\Offering::findOrFail($id);
        $url = "https://my.une.edu.au/courses/{$instance->teaching_period->year}/units/{$instance->unit->unit_code}.json";
        try {

            $str = file_get_contents($url);

            $instance->update(['cauc_data_cache' => $str]);

        } catch (\Exception $err) {
            // @TODO handle this better
            return -1;
        }
        return $instance->id;
    }

// get a downloadable Unit Information and Assessment Overview book
    public
    function getUIAObook($id, $forcerefresh = 'false')
    {
        // get the instance
        $instance = \App\Offering::findOrFail($id);

        // @TODO check that the file exists- no need to recreate it if we don't need to. Store zip file as blob in database maybe?

        // save file in scratch area
        $folderpath = storage_path('app' . DIRECTORY_SEPARATOR . 'uiao' . DIRECTORY_SEPARATOR . $instance->unit->unit_code . $instance->teaching_period['teaching_period'] . $instance->teaching_period['year'] . DIRECTORY_SEPARATOR);
//dd($folderpath);
        if (!file_exists($folderpath . $instance->unit->unit_code . $instance->teaching_period['teaching_period'] . $instance->teaching_period['year'] . '.zip') || ($forcerefresh == 'true')) {

            if (!file_exists($folderpath)) {
                mkdir($folderpath);
            }
            if (!file_exists($folderpath . "01" . DIRECTORY_SEPARATOR)) {
                mkdir($folderpath . "01" . DIRECTORY_SEPARATOR);
            }

// get the JSON object of the unit information
            $url = "https://my.une.edu.au/courses/units/{$instance->unit->unit_code}.json";
            try {
                $str = file_get_contents($url);
                $json = json_decode($str, true);
            } catch (\Exception $err) {
                // @TODO handle this better
                dd($err);
            }
            // start building the pages
            // Introduction page
            $introductionPageStr = "<html><head><title>Introduction</title></head><body>{$json['description']}</body></html>";

// write page to file
            $introfilepath = $folderpath . "01" . DIRECTORY_SEPARATOR . "01_html.html";
            $introfile = file_put_contents($introfilepath, $introductionPageStr);

            // Learning outcomes
            $learningOutcomesPageStr = "<html><head><title>Learning Outcomes</title></head><body><ol>";
            foreach ($json['learningOutcomes'] as $learningOutcome) {
                $learningOutcomesPageStr .= "<li>{$learningOutcome['description']}</li>";
            }
            $learningOutcomesPageStr .= "</ol></body></html>";

            // make folder for page
            if (!file_exists($folderpath . "02" . DIRECTORY_SEPARATOR)) {
                mkdir($folderpath . "02" . DIRECTORY_SEPARATOR);
            }
// write page to file
            $filepath = $folderpath . "02" . DIRECTORY_SEPARATOR . "02_html.html";
            $learningoutcomefile = file_put_contents($filepath, $learningOutcomesPageStr);

            //
            // Assessment summary
            //
            //

            $assessmentSummaryPageStr = "<html><head><title>Assessment Summary</title><style>
table { border: #000000 1px solid; width: 80%; max-width: 900px; }
th { border: #000000 1px solid; background-color: #7ab800; color: #ffffff; }
td { border: #000000 1px solid; vertical-align: top; text-align: left; }
</style></head><body>";
            $offcampusAssessmentStr = "<h3>{$instance->unit->unit_code} Off Campus</h3><table  cellpadding=\"6\" cellspacing=\"0\" border=\"1\">
<tbody>
<tr><th>Assessment</th><th>Weight</th><th>Must Complete</th><th>Word Length and notes</th><th>Learning<br />Outcomes</th></tr>";
            $oncampusAssessmentStr = "<h3>{$instance->unit->unit_code} On Campus</h3><table  cellpadding=\"6\" cellspacing=\"0\" border=\"1\">
<tbody>
<tr><th>Assessment</th><th>Weight</th><th>Must Complete</th><th>Word Length and notes</th><th>Learning<br />Outcomes</th></tr>";


            $oncampusAssessment = [];
            $offcampusAssessment = [];
            foreach ($json['assessments'] as $assessment) {

                $assessmentItemStr = "<tr></tr></tr><tr><td>{$assessment['title']}</td><td>{$assessment['weightString']}%</td><td>"
                    . ($assessment['compulsory'] ? 'Yes' : 'No') . "</td><td>"
                    . (isset($assessment['wordCount']) ? "Word count: {$assessment['wordCount']} words<br/>" : '')
                    . (isset($assessment['notes']) ? "Notes: {$assessment['notes']}" : '')
                    . ((strlen($assessment['formattedExamLength']) > 0) ? "Exam length: {$assessment['formattedExamLength']}<br/>" : '')
                    . "</td><td>" . $assessment['notesOutcomesAttributes'] . "</td></tr>";

                switch ($assessment['studyMode']) {
                    case 'Off Campus':
                        // if it's an exam, push it to the back
                        if ((strlen($assessment['formattedExamLength']) > 0)) {
                            array_push($offcampusAssessment, $assessmentItemStr);
                        } else {
                            array_unshift($offcampusAssessment, $assessmentItemStr);
                        }

                        break;
                    case 'On Campus':
                        if ((strlen($assessment['formattedExamLength']) > 0)) {
                            array_push($oncampusAssessment, $assessmentItemStr);
                        } else {
                            array_unshift($oncampusAssessment, $assessmentItemStr);
                        }

                        break;
                    case 'On/Off Campus':
                        if ((strlen($assessment['formattedExamLength']) > 0)) {
                            array_push($offcampusAssessment, $assessmentItemStr);
                            array_push($oncampusAssessment, $assessmentItemStr);
                        } else {
                            array_unshift($offcampusAssessment, $assessmentItemStr);
                            array_unshift($oncampusAssessment, $assessmentItemStr);
                        }


                        break;
                    default:
                        if ((strlen($assessment['formattedExamLength']) > 0)) {
                            array_push($oncampusAssessment, $assessmentItemStr);
                        } else {
                            array_unshift($oncampusAssessment, $assessmentItemStr);
                        }
                        break;
                }
            }

            // if the off campus assessment array has data
            if (count($offcampusAssessment) > 0) {
                foreach ($offcampusAssessment as $assessment) {
                    $offcampusAssessmentStr .= $assessment;
                }
                $offcampusAssessmentStr .= "</tbody></table>";
            } else {
                $offcampusAssessmentStr = "";

            }

            if (count($oncampusAssessment) > 0) {
                foreach ($oncampusAssessment as $assessment) {
                    $oncampusAssessmentStr .= $assessment;
                }
                $oncampusAssessmentStr .= "</tbody></table>";
            } else {
                $oncampusAssessmentStr = "";
            }

            // make folder for page
            if (!file_exists($folderpath . "03" . DIRECTORY_SEPARATOR)) {
                mkdir($folderpath . "03" . DIRECTORY_SEPARATOR);
            }
// write page to file
            $filepath = $folderpath . "03" . DIRECTORY_SEPARATOR . "03_html.html";
            $assessmentfile = file_put_contents($filepath, $assessmentSummaryPageStr . $offcampusAssessmentStr . "<p>" . $assessmentSummaryPageStr . $oncampusAssessmentStr . "</p></body></html>");

            // Textbooks string
            $textbookStr = "<html><head><title>Textbooks/Materials</title></head><body><div style=\"clear: both;\">";
            foreach ($json['materials'] as $material) {
                $textbookStr .= "<div style=\"clear: both;\"><h4>" . ($material['mandatory'] ? "Prescribed" : "Optional") . "</h4> <div style=\"float: left; margin-right: 15px;\"><p><a href=\"http://orders.ucb.net.au/ViewBook.aspx?acc=" . $material['reference'] . "\" target=\"_blank\"><img src=\"http://orders.ucb.net.au/images/cache/ViewBooks/" . $material['reference'] . ".jpg\" style=\"width: 150px; height: auto; border: solid 1px gray;\" /></a></p></div><div style=\"float: left;\"><p><b>" . $material['title'] . "</b><br />" . $material['author'] . ", (" . $material['yearPublished'] . ").  (" . $material['edition'] . ".). " . $material['publisher'] . ". <br />ISBN: " . $material['reference'] . "</p><p>This text is for: " . $material['teachingPeriod']['description'] . " " . $material['studyMode']['description'] . " </p> </div></div>
            ";
            }
            $textbookStr .= "<div style=\"clear: both;\"><h4>Need a textbook?</h4>
			<p>You can search the <a href=\"http://orders.ucb.net.au/Educational/Course.aspx?CourseExt={$instance->unit->unit_code}\" target=\"_blank\"><b>United Campus Bookshops</b> catalogue for <b>{$instance->unit->unit_code}</b></a>. On Campus students can pick up textbooks in person and Off Campus students can have them delivered.</p>
			<p>The UNE web site also has some <a  href=\"http://www.une.edu.au/current-students/resources/academic-resources/textbooks\" target=\"_blank\" >helpful information for purchasing textbooks</a>.</p>
			</div>";

            // make folder for page
            if (!file_exists($folderpath . "04" . DIRECTORY_SEPARATOR)) {
                mkdir($folderpath . "04" . DIRECTORY_SEPARATOR);
            }
// write page to file
            $filepath = $folderpath . "04" . DIRECTORY_SEPARATOR . "04_html.html";
            $assessmentfile = file_put_contents($filepath, $textbookStr . "</p></body></html>");

            // Assessment policies string
            $policiesStr = "<html><head><title>Assessment Policies</title></head><body><div style=\"margin: 8px; margin-left: 15px; width: 80%; max-width: 900px; border: #7ab800 1px solid; padding: 8px;\">
<p><b>Please read these documents before submitting your assignment.</b></p>
<p><b><a href=\"http://policies.une.edu.au/view.current.php?id=00228\" target=\"_blank\">The UNE Assessment Policy</a></b>. <br />Assessments will be marked and feedback provided in line with this policy.</p>  </div></body></html>";

            // make folder for page
            if (!file_exists($folderpath . "05" . DIRECTORY_SEPARATOR)) {
                mkdir($folderpath . "05" . DIRECTORY_SEPARATOR);
            }
// write page to file
            $policiesfilepath = $folderpath . "05" . DIRECTORY_SEPARATOR . "05_html.html";
            $policiesfile = file_put_contents($policiesfilepath, $policiesStr);

            // Cyberethics String
            $cyberethicsStr = "<html><head><title>Cyberethics</title></head><body><p>It is important to the School of Environmental and Rural Science that students feel safe in interactions relating to online learning. Students should be aware of the <a href=\"http://policies.une.edu.au/view.current.php?id=00067\" target=\"_blank\">University Cyberethics Policy</a>, which is a guide to good behaviour and staying safe in online communication.</p>
<p>If you feel that there has been a breach of these guidelines you should contact your Unit Coordinator, or to make a formal complaint refer to the <a href=\"http://www.une.edu.au/current-students/support/student-support/complaints-compliments-and-feedback\" target=\"_blank\">Complaints Compliments and Feedback</a> web page.</p>
<p>All students should refer to the links below to ensure that they fully understand their rights and responsibilities associated with online use.</p>
<ul>
<li><a target=\"_blank\" href=\"http://policies.une.edu.au/view.current.php?id=00215\">Student Behaviour Misconduct Rules</a></li>
<li><a target=\"_blank\" href=\"http://policies.une.edu.au/view.current.php?id=00051\">Information Technology Directorate. Email Operating Procedures</a></li>
<li><a target=\"_blank\" href=\"http://policies.une.edu.au/view.current.php?id=00055\">Rules for the Use of Information and Communication Facilities and Services</a></li>
<li><a target=\"_blank\" href=\"http://policies.une.edu.au/view.current.php?id=00166\">Social Media Policy</a></li>
</ul></body></html>";
// make folder for page
            if (!file_exists($folderpath . "06" . DIRECTORY_SEPARATOR)) {
                mkdir($folderpath . "06" . DIRECTORY_SEPARATOR);
            }
// write page to file
            $ethicsfilepath = $folderpath . "06" . DIRECTORY_SEPARATOR . "06_html.html";
            $ethicsfile = file_put_contents($ethicsfilepath, $cyberethicsStr);

            // save them all as a zip file for uploading to Moodle

            Zipper::make(storage_path('app' . DIRECTORY_SEPARATOR . 'uiao' . DIRECTORY_SEPARATOR . $instance->unit->unit_code . $instance->teaching_period['teaching_period'] . $instance->teaching_period['year'] . '.zip'))->add($folderpath)->close();
//dd($folderpath);
            // clean up files
            self::rrmdir($folderpath);

        }

        return response()->download(storage_path('app' . DIRECTORY_SEPARATOR . 'uiao' . DIRECTORY_SEPARATOR . $instance->unit->unit_code . $instance->teaching_period['teaching_period'] . $instance->teaching_period['year'] . '.zip'));
    }

    private
    function rrmdir($dir)
    {
        foreach (glob($dir . '/*') as $file) {
            if (is_dir($file))
                self::rrmdir($file);
            else
                unlink($file);
        }
        rmdir($dir);
    }


    public
    function clone_instance(Request $request)
    {

        $input = $request::all();
        $originalid = $input['id'];
// get the checklist items
        $checklistitems = Offering::find($originalid)->checklist;
        //  dd($checklistitems);
        // get the new instance ID
        // kill off id, or there'll be issues
        unset($input['id']);
        $newinstance = \App\Offering::create($input);
        // dd($newinstance);
        // build an array of new items to be inserted
        $newitems = [];
        foreach ($checklistitems as $item) {
            $newitems[] = new Offering_checklist_items(['description' => $item['description'], 'text' => $item['text'], 'order' => $item['order'], 'heading' => $item['heading']]);
        }
// just gunna have to assume this works. Doesn't return anything if it fails...
        $newinstance->checklist()->saveMany($newitems);

        $response = array(
            'id' => $newinstance->id,
        );

        return $response;
    }

    public
    function ajaxstore(Request $request)
    {

        $input = $request::all();
        $status = strval(\App\Offering::create($input)->id);
        $response = array(
            'status' => $status,
        );
        return $response;

    }

    public
    function ajaxupdate(Request $request)
    {
        $input = $request::all();
        $instance = \App\Offering::findOrFail($input['id']);
        //  dd($input);

        $status = strval($instance->update($input));
        // update combined units
//        foreach ($instance->combined_units as $combined_unit){
//            $combined_unit->update($input);
//        }
        $updatesuccess = false;
        if (isset($input['action'])) {
            $input['follow_up'] = (isset($input['follow_up']) ? 'true' : '');
        } else {
            if (isset($input['combined_units_id'])) {
                try {
                    $combinedunits = $input['combined_units_id'];
                    //   dd($combinedunits);
                    if (count($combinedunits) > 0) {

                        $sync_array = [];
                        foreach ($combinedunits as $combinedunit) {
                            // check the unit, update
                            $sync_array[] = Offering::updateOrCreate(['unit_id' => $combinedunit, 'teaching_period_id' => $input['teaching_period_id']],
                                ['unit_id' => $combinedunit, 'coordinator_id' => $input['coordinator_id'], 'eso_id' => $input['eso_id'], 'teaching_period_id' => $input['teaching_period_id'], 'moodle_url' => $instance['moodle_url']])->id;
                        }

                        $update = $instance->combined_units()->sync($sync_array);
                        // $updatesuccess = (count($update['attached'] > 0) || count($update['detached'] > 0) || count($update['attached'] > 0));
                    }

                } catch (\Exception $e) {
                    // do task when error
                    print('combined_units<br/>');
                    dd($e->getMessage());   // insert query
                }
            } else {
                $instance->combined_units()->detach();
            }

            if (isset($input['teachers_ids']) && (strlen($input['teachers_ids']) > 0)) {
                try {
                    $teachers = explode(',', $input['teachers_ids']);
                    if (count($teachers) > 0) {

                        $sync_array = [];
                        foreach ($teachers as $teacher) {

                            $sync_array[] = $teacher;

                        }

                        $update = $instance->teachers()->sync($sync_array);
                        //$updatesuccess = (count($update['attached'] > 0) || count($update['detached'] > 0) || count($update['attached'] > 0));
                    }

                } catch (\Exception $e) {
                    // do task when error
                    print('teachers<br/>');
                    dd($e->getMessage());   // insert query
                }
            } // if there's none, ensure all are empty
            else {
                $instance->teachers()->detach();
            }

            if (!isset($input['has_olx'])) {
                $input['has_olx'] = '0';
            }

            if (!isset($input['is_accredited'])) {
                $input['is_accredited'] = '0';
            }
        }

        $status = ($instance->update($input));

        $response = array(
            'status' => $status ? '1' : '0',
        );
        return $response;
    }

    public
    function updatefollowup($status)
    {

    }

    public function updateStatus(Request $request)
    {
        $input = $request::all();
        $instance = \App\Offering::findOrFail($input['id']);

        // working out completion percentage
        $percentComplete = round(($instance->checklistcomplete / ($instance->checklist()->get()->where('heading', '<>', '1')->count())) * 100);

        if (!is_nan($percentComplete)) {
            $input['complete_percent'] = $percentComplete;
        }

        $status = ($instance->update($input));

        $response = array(
            'status' => $status ? '1' : '0',
        );
        return $response;
    }

    public
    function destroy(Request $request)
    {
        $input = $request::all();
        //dd($input);
        $status = (\App\Offering::destroy($input['id']) > 0 ? '0' : '1');
        $response = array(
            'status' => $status,
        );
        return $response;
    }


    public
    function archive($id)
    {
        $review = \App\Offering::findOrFail($id);
        $review->archived_at = Carbon::now();
        $status = strval($review->save());
        $response = array(
            'status' => $status,
        );
        return $response;
    }

    public
    function createmedia(Request $request)
    {

        $input = $request::all();
        $input["user_id"] = Auth::user()->id;
        // handle file
        //dd($input);
        // $uploadedfile = $input["userfile"];
        $file = Request::file('userfile');
        // check it's not too big
        if ($file->getMaxFilesize() > $file->getSize()) {
            // get type
            $type = $file->getClientOriginalExtension();
            // @todo is this an allowed type?

            // get the md5 hash of the contents. This allows for different files with the same name in teh same directory...
            $md5name = md5(file_get_contents($file->getPath() . DIRECTORY_SEPARATOR . $file->getFilename()));

            // store it to disk
            if (!(Storage::disk('local')->put('media' . DIRECTORY_SEPARATOR . $md5name, File::get($file)))) {
                return '-1';
            }
            // save the record
            $newentry = new Offering_media();
            $newentry->unit_instances_id = $input["unit_id"];
            $newentry->name = $file->getClientOriginalName();
            $newentry->type = $type;
            $newentry->size = $file->getSize();
            $newentry->description = $input["description"];
            $newentry->path = 'media' . DIRECTORY_SEPARATOR . $md5name;
            $newentry->save();
            return $newentry->id;

        } else {
            return '-1';
        }
        //$response = array(
        //  'status' => 'success',
        //'msg' => 'Setting created successfully',
        //);

        return '-1';
    }

    /**
     * Updates the Offerings table completion statistics
     * @param Request $request
     * @return
     */
    public function updateCompletion(Request $request)
    {
        $input = $request::all();
        $instances = \App\Offering::all();

        foreach ($instances as $instance) {
            $percentComplete = round(($instance->checklistcomplete / ($instance->checklist()->get()->where('heading', '<>', '1')->count())) * 100);
            if (!is_nan($percentComplete)) {
                $instance->complete_percent = $percentComplete;
                $instance->save();
                print ("Updating {$instance->unit['unit_code']} {$instance->teaching_period->teaching_period_lookup->teaching_period} {$instance->teaching_period['year']} to {$instance->complete_percent} <br/>");
            }


            //$instance->complete_percent = round(($instance->checklistcomplete / ($instance->checklist()->get()->where('heading', '<>', '1')->count())) * 100);
            //$instance->save();
            //print ("Updating {$instance->unit['unit_code']} {$instance->teaching_period->teaching_period_lookup->teaching_period} {$instance->teaching_period['year']} to {$instance->complete_percent} <br/>");
        }


        $response = array(
            'status' => '0',
        );
        return $response;
    }

///////////////////////////////////////////////////////////////
// template functions
///////////////////////////////////////////////////////////////
    public
    function templateindex(Request $request)
    {
        //$reviews = Review_instances::sortable()->paginate(20);
        $users = \App\User::all();

        $templates = \App\Offering::all()->where('is_template', '=', 'true');
        $emailtemplates = Emails_template::where('context', 'unitsetup')->get();
        //    dd($templates);
        //dd($reviews);
        return view('unit_setup.templates.list')
            ->with('instances', $templates)
            ->with('emailtemplates', $emailtemplates)
            ->with('users', $users);
    }

    public
    function templateshow($id)
    {
        //$this->authorize('view', Review_instances::class);

        $instance = \App\Offering::findOrFail($id);
        $emailtemplates = Emails_template::where('context', 'unitsetup')->get();
        $users = \App\User::all();

        return view('unit_setup.templates.view')
            ->with('instance', $instance)
            ->with('emailtemplates', $emailtemplates)
            ->with('users', $users);
    }

    public
    function templatestore(Request $request)
    {
        $input = $request::all();
        //dd(explode(',', $input['combined_units']));
        $newunit = \App\Offering::create($input);
        $response = array(
            'id' => strval($newunit->id),
        );
        return $response;
    }

    public
    function templateajaxupdate(Request $request)
    {
        $input = $request::all();
        $instance = \App\Offering::findOrFail($input['id']);
        $status = strval($instance->update($input));
        $response = array(
            'status' => $status ? '1' : 0,
        );
        return $response;
    }


/////////////////////////////////////////////////////////////////////////////////////////////
//email
//////////////////////////////////////////////////////////////////////////////////////////////
    public
    function sendemail($id, Request $request)
    {
        $input = $request::all();
        //dd($input);
        $template = Emails_template::find($input['template_id']);
        $unit = Offering::find($id);
        //print_r($input);
        try {
            // mail to unit coordinator
            $email = new UnitSetupEmail($unit, $template);

            // dispatch to the job queue
            //dispatch(new SendSetupEmail($email, (isset($input['testing']) ? Auth::user()->id : $unit->coordinator->id), Auth::user()->id, $template, $id));
            //   dispatch(new SendSetupEmail($email, Auth::user()->id, Auth::user()->id, $template, $id));
            // log success
            $response = array(
                'status' => '0',
            );
            $log = new Emails_log();
            $log->email_id = $input['template_id'];
            $log->instances_id = $id;
            $log->context = 'offeringsetup';
            $log->sent_to_id = $unit->coordinator->id;
            $log->sent_by_id = Auth::user()->id;
            $log->fulltext = $email->getfulltext();
            $log->save();
        } catch (\Exception $e) {
            // handle failure
            $response = array(
                'status' => '1',
            );
        }
        return $response;
    }

// bulk email
    public
    function sendbulkemail(Request $request)
    {
        $input = $request::all();
        //  dd($input);
        $template = Emails_template::find($input['template_id']);

        // get ids
        $ids = explode(',', $input['ids']);
        foreach ($ids as $id) {

            $unit = Offering::find($id);
            if (isset($unit)) {
                //   try {
                if (isset($unit->coordinator->id)) {
                    $email = new UnitSetupEmail($unit, $template);
                    // dispatch to the job queue
                    dispatch(new SendSetupEmail($email, (isset($input['testing']) ? Auth::user()->id : $unit->coordinator->id), Auth::user()->id, $template, $id));
                    // mail to unit coordinator
                    //  Mail::to('alandow@une.edu.au')->send(new UnitSetupEmail($unit, $template), $unit, $template);
                    // log success
                    $log = new Emails_log();
                    $log->email_id = $input['template_id'];
                    $log->instance_id = $id;
                    $log->sent_to_id = $unit->coordinator->id;
                    $log->sent_by_id = Auth::user()->id;
                    $log->context = 'offeringsetup';
                    $log->fulltext = $email->getfulltext();
                    $log->save();
                    $response = array(
                        'status' => '0',
                    );
                }
            }

            //
        }
        return $response;
    }

    public
    function getemail($id, $emailid)
    {
        return Emails_log::find($emailid);
    }


    public
    function bulkarchive(Request $request)
    {
        $input = $request::all();
        // get ids

        if (strlen($input['ids']) > 0) {
            $ids = explode(',', $input['ids']);
            $units = Offering::whereIn('id', $ids)->update(['archived_at' => Carbon::now()]);;
            //dd($units);
            //$units->update(['archived_at' => Carbon::now()]);
            //   try {
            //$units->update(['archived_at', Carbon::now()]);
        }

        $response = array(
            'status' => '0',
        );
        return $response;
    }

    // search for select2 controls
    public function searchforselect2(Request $request)
    {
        $input = $request::all();
        $search = $input['q']['term'];
        //  dd($search);
        $returnStr = [];
        $offerings = Offering::whereHas('unit', function ($query) use ($search) {
            $query->where('unit_code', 'LIKE', "%{$search}%");
            //})->toSql();
        })->get();
        foreach ($offerings as $offering) {
            $returnStr[] = ['id' => $offering->id, 'text' => "{$offering->unit->unit_code} {$offering->teaching_period->teaching_period_lookup->teaching_period} {$offering->teaching_period['year']} (Coordinator: {$offering->coordinator['name']})"];
        }
        return \json_encode(['results' => $returnStr]);
        //dd($offerings);
    }

    private
    function unique_multidim_array($array, $key)
    {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }


    /**
 * Experimental export of CSV for Kantu browser automation of site creation
 * @param Request $request
 */
    public function exportCSVForKantu(Request $request){

        // disable the debugbar
        \Debugbar::disable();

        $input = $request::all();

        $teachingperiodfilter = [];

        if (isset($input['teachingperiod'])) {
            session(['teachingperiod' => $input['teachingperiod']]);
            UserPreferences::set('offerings_teachingperiod', $input['teachingperiod']);
        }

        if (UserPreferences::has('offerings_teachingperiod')) {
            session(['teachingperiod' => UserPreferences::get('offerings_teachingperiod')]);
        }
        if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {

            $teachingperiodfilter = session('teachingperiod');
        }
        $schoolfilter = [];
        if (isset($input['school'])) {
            session(['schoolid' => $input['school']]);

            UserPreferences::set('offerings_school', $input['school']);
        } else {
            if (UserPreferences::has('offerings_school')) {
                session(['schoolid' => UserPreferences::get('offerings_school')]);
            }
        }

        if (session()->has('schoolid') && session('schoolid') > 0) {
            $schoolfilter = session('schoolid');
        }

        if (isset($input['show_others'])) {
            //dd(session('show_others'));
            session(['show_others' => $input['show_others']]);
            //dd(session('schoolid'));
            UserPreferences::set('offerings_show_others', $input['show_others']);
        } else {
            if (UserPreferences::has('offerings_show_others')) {
                session(['show_others' => UserPreferences::get('offerings_show_others')]);
            }
        }

        $offerings_query = Sortable_offering::whereNull('is_template')->whereNull('archived_at')->doesntHave('units_combined');
        if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {
            $offerings_query->whereIn('teaching_period_id', $teachingperiodfilter);
        }

        if (session()->has('schoolid') && count(session('schoolid')) > 0) {
            $offerings_query->whereIn('school_id', $schoolfilter);
        }



        if (!(isset(session('show_others')[1]) && (session('show_others')[1]=='1'))) {
            if(Auth::user()->roles->contains( "text" , "Coordinator")){
                $offerings_query->unitsforcoordinatororeso([Auth::user()->id]);
            }else {
                $offerings_query->unitsforeso([Auth::user()->id]);
            }
        }

        if(Auth::user()->roles->contains( "text" , "Coordinator")){
            $offerings_query->unitsforcoordinatororeso([Auth::user()->id]);
        }

        $offerings = $offerings_query->get();


        // the CSV headers
        $headers = array('unit codes');
        $outputcsvarray = [$headers];

        foreach ($offerings as $offering){
            $combined_units = [];
            foreach($offering->combined_units as $combined_unit) {
                array_push($combined_units, $combined_unit->unit['unit_code']);
            }
            $outputcsvarray[] = [implode(',', array_merge([$offering['unit_code']],$combined_units))];
        }
        //  dd($outputcsvarray);

        $out = fopen('php://output', 'w');
        header("Content-Type:application/csv");
        header("Content-Disposition:attachment;filename=output.csv");
        foreach ($outputcsvarray as $row) {
            fputcsv($out, $row);
        }
        fclose($out);
        return;
    }

    /**
     * Experimental batch update of Moodle sites after Kantu browser automation of site creation
     * @param Request $request
     */
    public function batchMoodleSiteUpdate(Request $request){

        // disable the debugbar
        \Debugbar::disable();

        $input = $request::all();

        $teachingperiodfilter = [];

        if (isset($input['teachingperiod'])) {
            session(['teachingperiod' => $input['teachingperiod']]);
            UserPreferences::set('offerings_teachingperiod', $input['teachingperiod']);
        }

        if (UserPreferences::has('offerings_teachingperiod')) {
            session(['teachingperiod' => UserPreferences::get('offerings_teachingperiod')]);
        }
        if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {

            $teachingperiodfilter = session('teachingperiod');
        }
        $schoolfilter = [];
        if (isset($input['school'])) {
            session(['schoolid' => $input['school']]);

            UserPreferences::set('offerings_school', $input['school']);
        } else {
            if (UserPreferences::has('offerings_school')) {
                session(['schoolid' => UserPreferences::get('offerings_school')]);
            }
        }

        if (session()->has('schoolid') && session('schoolid') > 0) {
            $schoolfilter = session('schoolid');
        }

        if (isset($input['show_others'])) {
            //dd(session('show_others'));
            session(['show_others' => $input['show_others']]);
            //dd(session('schoolid'));
            UserPreferences::set('offerings_show_others', $input['show_others']);
        } else {
            if (UserPreferences::has('offerings_show_others')) {
                session(['show_others' => UserPreferences::get('offerings_show_others')]);
            }
        }

        $offerings_query = Sortable_offering::whereNull('is_template')->whereNull('archived_at')->doesntHave('units_combined');
        if (session()->has('teachingperiod') && count(session('teachingperiod')) > 0) {
            $offerings_query->whereIn('teaching_period_id', $teachingperiodfilter);
        }

        if (session()->has('schoolid') && count(session('schoolid')) > 0) {
            $offerings_query->whereIn('school_id', $schoolfilter);
        }



        if (!(isset(session('show_others')[1]) && (session('show_others')[1]=='1'))) {
            if(Auth::user()->roles->contains( "text" , "Coordinator")){
                $offerings_query->unitsforcoordinatororeso([Auth::user()->id]);
            }else {
                $offerings_query->unitsforeso([Auth::user()->id]);
            }
        }

        if(Auth::user()->roles->contains( "text" , "Coordinator")){
            $offerings_query->unitsforcoordinatororeso([Auth::user()->id]);
        }

        $offerings = $offerings_query->get();


        foreach ($offerings as $offering){
            $this->detectmoodleurl($offering->id);
        }

        return;
    }


}
