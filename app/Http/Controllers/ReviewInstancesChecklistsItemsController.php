<?php

namespace App\Http\Controllers;

use App\Emails_template;
use App\Jobs\SendReviewEmail;
use App\Jobs\SendSetupEmail;
use App\Mail\UnitReviewEmail;
use App\Mail\UnitSetupEmail;
use App\Review_instances;
use App\Review_instances_checklist_items;

use App\Offering;
use Illuminate\Support\Facades\Auth;
use Mews\Purifier\Facades\Purifier;
use Request;

class ReviewInstancesChecklistsItemsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id)
    {
        return Review_instances_checklist_items::find($id);
    }

    public function create(Request $request)
    {
        $input = $request::all();

        if (isset($input['template_text'])) {
            $input['coordinator_text'] = $input['template_text'];
            $input['peer_reviewer_text'] = $input['template_text'];
            $input['ld_text'] = $input['template_text'];
        }
        $newentry = \App\Review_instances_checklist_items::create($input);
        $response = array(
            'status' => '0',
            'id' => $newentry->id,
            'msg' => 'Item created successfully',

        );
        return $response;
    }

    public function store(Request $request)
    {
        $input = $request::all();
        // get the highest order,
        $highestorder = \App\Review_instances_checklist_items::where('review_instances_id', $input['review_instances_id'])->max('order');
        // make this entry the last on the list
        $input['order'] = $highestorder + 1;
        // perform entry


        if (isset($input['template_text']) && (strlen($input['template_text']) > 0)) {
            // do a little bit of formatting and sanitizing on text. Because we're allowing rich text we need to do this...
            // sanitise
            Purifier::clean($input['template_text']);
            // add a 'note' class to tables
            $doc = new \DOMDocument();
            $doc->loadHTML($input['template_text']);
            $tables = $doc->getElementsByTagName('table');
            foreach ($tables as $table) {
                $table->setAttribute('class', 'note');
            }
            $input['template_text'] = $doc->saveHTML();

            if (Review_instances::find($input['review_instances_id'])->is_template == 'true') {
                $input['template_text'] = preg_replace('~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i', '', $input['template_text']);
                //    $input['coordinator_text'] = $input['template_text'];
//                $input['peer_reviewer_text'] = $input['template_text'];
//                $input['ld_text'] = $input['template_text'];
            } else {
                if ((stripos($input['template_text'], '{cauc_description}') > 0)
                    || (stripos($input['template_text'], '{cauc_description_table}') > 0)
                    || (stripos($input['template_text'], '{cauc_learning_outcomes_comparison_table}') > 0)
                    || (stripos($input['template_text'], '{cauc_assessments_table}') > 0)) {
                    $item = \App\Review_instances_checklist_items::findOrNew($input['id']);
                    // get the JSON object of the unit information
                    $url = "https://my.une.edu.au/courses/{$item->review_instance->reviewed_unit_instance->teaching_period['year']}/units/{$item->review_instance->reviewed_unit_instance->unit->unit_code}.json";
                    try {
                        $str = file_get_contents($url);
                        $json = json_decode($str, true);
                    } catch (\Exception $err) {
                        // @TODO handle this better
                        dd($err);
                    }
                    // search and replace to save typing
                    $input['template_text'] = preg_replace('~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i', '', $input['template_text']);
                    $input['template_text'] = self::search_replace_cauc($item->review_instance->reviewed_unit_instance, $json, $input['template_text']);
                    $input['coordinator_text'] = self::search_replace_cauc($item->review_instance->reviewed_unit_instance, $json, $input['template_text']);
                    $input['peer_reviewer_text'] = self::search_replace_cauc($item->review_instance->reviewed_unit_instance, $json, $input['template_text']);
                    $input['ld_text'] = self::search_replace_cauc($item->review_instance->reviewed_unit_instance, $json, $input['template_text']);
                }
//
            }

        }

        // emails
        if (!isset($input['trigger_email'])) {
            $input['trigger_email'] = null;
            $input['trigger_email_id'] = null;
        }

        $newentry = Review_instances_checklist_items::create($input);

        // return response
        $response = array(
            'status' => '0',
            'id' => $newentry->id,
            'msg' => 'Item created successfully',

        );
        return $response;
    }

    /**
     * Updates the record
     * @param $id
     * @param PatientRequest $request
     * @return $this
     */
    public
    function update(Request $request)
    {
        $input = $request::all();
        $item = \App\Review_instances_checklist_items::findOrNew($input['id']);
        if (isset($input['description'])) {
            $item->description = $input['description'];
        }

        // workout what the textfield is
        $textfield = '';
        if (isset($input['coordinator_text']) && (strlen($input['coordinator_text']) > 0)) {
            $textfield = 'coordinator_text';
        }
        if (isset($input['peer_reviewer_text']) && (strlen($input['peer_reviewer_text']) > 0)) {
            $textfield = 'peer_reviewer_text';
        }
        if (isset($input['ld_text']) && (strlen($input['ld_text']) > 0)) {
            $textfield = 'ld_text';
        }
        if (isset($input['template_text']) && (strlen($input['template_text']) > 0)) {
            $textfield = 'template_text';
        }

        // emails
//        if (!isset($input['trigger_email'])) {
//            $input['trigger_email'] = null;
//            $input['trigger_email_id'] = null;
//        }

        // if it's not template
        if ((strlen($textfield)>0)&&($textfield != 'template_text')) {
            // little labour-saving hack: search and replace keywords with CAUC data
            // get the JSON object of the unit information
            $url = "https://my.une.edu.au/courses/{$item->review_instance->reviewed_offering->teaching_period['year']}/units/{$item->review_instance->reviewed_offering->unit->unit_code}.json";
            try {
                $str = file_get_contents($url);
                $json = json_decode($str, true);
            } catch (\Exception $err) {
                // @TODO handle this better
                dd($err);
            }
            // do a little bit of formatting and sanitizing on text. Because we're allowing rich text we need to do this...
            // sanitise
            Purifier::clean($input[$textfield]);
            // add a 'note' class to tables
            $doc = new \DOMDocument();
            $doc->loadHTML($input[$textfield]);
            $tables = $doc->getElementsByTagName('table');
            foreach ($tables as $table) {
                $table->setAttribute('class', 'note');
            }
            $input[$textfield] = $doc->saveHTML();
            $input[$textfield] = preg_replace('~<(?:!DOCTYPE|/?(?:html|head|body))[^>]*>\s*~i', '', $input[$textfield]);
            // search and replace to save typing
            $input[$textfield] = self::search_replace_cauc($item->review_instance->reviewed_offering, $json, $input[$textfield]);

        }

        // if the status has changed,
        if(isset($input['coordinator_status'])||isset($input['peer_reviewer_status'])){
            // if there is an email to be sent, do it now
            if($item->trigger_email == 1) {
                $template = Emails_template::find($item->trigger_email_id);
                $review = $item->review_instance;
                try {
                    // mail to unit coordinator
                    $email = new UnitReviewEmail($review, $item, $template,  Auth::user()->id);
                    //dd($email->getfulltext());
                    // dispatch to the job queue
                    dispatch(new SendReviewEmail($email, $review->ld->id, Auth::user()->id, $template, $review->id, $item->id));
                    // log success
                    $emailstatus = '0';

                } catch (\Exception $e) {
                    // handle failure
                    dd($e->getMessage());
                    $emailstatus = '1';
                }
            }
        }

        $status = strval($item->update($input));
        $response = array(
            'status' => $status,
        );
        return $response;
    }

    public
    function reorder(Request $request)
    {
        $input = $request::all();
        foreach (json_decode($input['order']) as $item) {
            $checklistitem = Review_instances_checklist_items::find($item->id);
            $checklistitem->order = $item->order;
            $checklistitem->save();
        }
        return array(
            'status' => 0,
        );
    }

    public
    function ajaxstore(Request $request)
    {

        $input = $request::all();
        $status = strval(\App\Review_instances_checklist_items::create($input)->id);
        $response = array(
            'status' => $status,
        );
        return $response;

    }

    public
    function ajaxupdate(Request $request)
    {
        $input = $request::all();
        $review = \App\Review_instances_checklist_items::findOrNew($input['id']);
        $status = strval($review->update($input));
        $response = array(
            'status' => $status,
        );
        return $response;
        return array(
            'status' => $status,
        );
    }


    public
    function destroy(Request $request)
    {
        $input = $request::all();
        //dd($input);
        $status = strval(\App\Review_instances_checklist_items::destroy($input['id']));
        $response = array(
            'status' => $status,
        );
        return $response;
    }

    private function search_replace_cauc(Offering $instance, $json, $inputStr)
    {

        // search and replace to save typing
        // description
        $inputStr = str_replace("{cauc_description}", $json['description'], $inputStr);

        // CAUC description table
        if (stripos($inputStr, '{cauc_description_table}') > 0) {
            $returnStr = "
<table class=\"note\" border=\"2\" cellspacing=\"2\" cellpadding=\"2\">
<tbody>
<tr>
<td><strong>CAUC ({$instance->teaching_period['year']})</strong></td>
<td><strong>Study Guide ({$instance->teaching_period['year']})</strong></td>
<td><strong>Comments</strong></td>
</tr><tr><td>{$json['description']}</td><td><div><strong>External/Internal</strong></div></td><td>&nbsp;</td></tr>
</tbody>
</table>";
            $inputStr = str_replace(["{cauc_description_table}"], [$returnStr], $inputStr);
        }

        // Learning outcomes comparison table
        if (stripos($inputStr, '{cauc_learning_outcomes_comparison_table}') > 0) {
            $returnStr = "<table class=\"note\" border=\"2\" cellspacing=\"2\" cellpadding=\"2\">
<tbody>
<tr>
<td><strong>CAUC ({$instance->teaching_period['year']})</strong></td>
<td><strong>Study Guide ({$instance->teaching_period['year']})</strong></td>
<td><strong>Comments</strong></td>
</tr>";
            foreach ($json['learningOutcomes'] as $learningOutcome) {
                $returnStr .= "<tr><td><p class=\"default\">{$learningOutcome['description']}</p></td>&nbsp;<td><td>&nbsp;</td></tr>";

            }
            $returnStr .= "</tbody></table>";

            $inputStr = str_replace(["{cauc_learning_outcomes_comparison_table}"], [$returnStr], $inputStr);
        }

        // Learning outcomes topics table
        if (stripos($inputStr, '{cauc_learning_outcomes_topics_table}') > 0) {
            $returnStr = "<table class=\"note\" border=\"2\" cellspacing=\"2\" cellpadding=\"2\">
<tbody>
<tr>
<td><strong>Learning outcomes ({$instance->teaching_period['year']})</strong></td>
<td><strong>Topics</strong></td>
</tr>";
            foreach ($json['learningOutcomes'] as $learningOutcome) {
                $returnStr .= "<tr><td><p class=\"default\"><p class=\"default\">{$learningOutcome['description']}</p></td>&nbsp;<td></tr>";

            }
            $returnStr .= "</tbody></table>";

            $inputStr = str_replace(["{cauc_learning_outcomes_topics_table}"], [$returnStr], $inputStr);
        }

        // Assessment comparison table
        if (stripos($inputStr, '{cauc_assessments_table}') > 0) {
            $returnStr = "<table class=\"note\" border=\"2\" cellspacing=\"2\" cellpadding=\"2\">
<tbody>
<tr>
<td><strong>Summative Assessment task in CAUC</strong></td>
<td><strong>Summative Assessment task in Study guide/Moodle page</strong></td>
<td><strong>Assessment Criteria provided (Please tick (√)where applicable)</strong></td>
</tr>";
            foreach ($json['assessments'] as $assessment) {
                $returnStr .= "<tr><td><p class=\"default\"><strong>{$assessment['title']} ({$assessment['weightString']}%)</strong></p>
<p class=\"default\">Compulsory: " . ($assessment['compulsory'] ? 'Yes' : 'No') . "</p>
<p class=\"default\">Word count: {$assessment['wordCount']}</p>
<p class=\"default\">Notes: {$assessment['notes']}</p></td>&nbsp;<td></td>&nbsp;<td></tr>";
            }
            $returnStr .= "</tbody></table>";

            $inputStr = str_replace(["{cauc_assessments_table}"], [$returnStr], $inputStr);
        }

        // unit relationship table
        if (stripos($inputStr, '{cauc_unit_relationship_table}') > 0) {

            $returnStr = "<table class=\"note\" style=\"border-color: black; width: 799px;\" border=\"2\" cellspacing=\"2\" cellpadding=\"2\">
<tbody>
<tr>
<td style=\"border-color: black; width: 299px;\" colspan=\"2\">
<p>&nbsp;</p>
</td>
<td style=\"width: 496px;\">
<p>Clearly justified and appropriate to the majors and courses that the unit contributes to (Please tick (&radic;) where it meets it. Provide explanation where it does not)</p>
</td>
</tr>
<tr>
<td style=\"width: 171.1px;\">
<p>Pre-requisites</p>
</td>
<td style=\"width: 127.9px;\">" . (strlen($json['prerequisites']) > 0 ? $json['prerequisites'] : 'None') . "</td>
<td style=\"width: 496px;\">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style=\"width: 171.1px;\">
<p>Co-requisite:</p>
</td>
<td style=\"width: 127.9px;\">" . (strlen($json['corequisites']) > 0 ? $json['corequisites'] : 'None') . "</td>
<td style=\"width: 496px;\">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style=\"width: 171.1px;\">
<p>Restrictions</p>
</td>
<td style=\"width: 127.9px;\">
<p>" . (strlen($json['restrictions']) > 0 ? $json['restrictions'] : 'None') . "</p>
</td>
<td style=\"width: 496px;\">
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>";
            $inputStr = str_replace(["{cauc_unit_relationship_table}"], [$returnStr], $inputStr);
        }

        return $inputStr;
    }

}
