<?php

namespace App\Http\Controllers;

use App\Course;
use App\Course_plan;
use App\Course_plan_year;
use App\Course_plan_year_units;
use App\Discipline;
use App\Field_of_study;
use App\Major;
use App\Offering;
use App\School;
use App\Teaching_period;
use App\Teaching_period_lookup;
use App\Unit;
use App\User;
use Illuminate\Http\Request;
use Sunra\PhpSimple\HtmlDomParser;

class ScrapeController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(Request $request = null)
    {
        return;
    }

    public function show($id)
    {
        return;
    }



    // maintenance.
    // Update courses and units relationship table from a directory
    // this takes ages to run- we only need to do it once a year or so.
    public function updateCourseUnitsTableFromDirectory($directory)
    {

        $i = 0;
        // the courses folder URL
        $folder = 'courseplans' . DIRECTORY_SEPARATOR . $directory;

// iterate through folder for folders
        // each folder is named with the course code.
        $dir = new \DirectoryIterator($folder);
        foreach ($dir as $fileinfo) {
            if (!$fileinfo->isDot()) {
                if ($fileinfo->isDir()) {
                    $i++;
//                    if ($i >5) {
//                        return;
//                    }
                    // get course id here. Each folder is named with the course code.
                    if ((Course::where('course_code', $dir->getFilename())->count() > 0)) {
                        $courseid = Course::where('course_code', $dir->getFilename())->first()->id;
                    }

                    // get core units here
                    // Programs of study pages are ordinary files that start with 'program-of-study'
                    // Majors and minors are in folders 'major' and 'minor' respectively.

                    $coursefolder = new \DirectoryIterator("$folder\/{$fileinfo->getFilename()}");
                    foreach ($coursefolder as $coursefileinfo) {

                        if (!$coursefileinfo->isDot()) {
                            $coreUnitsArr = [];
                            //    print('<br/>Inspecting file:' . "$folder/{$fileinfo->getFilename()}/" . $coursefileinfo->getFilename() . '<br/>');
                            // Programs of study pages are ordinary files that start with 'program-of-study'
                            // UPDATE! But only sometimes. But it should always have 'program-of-study' in the name
                            if (stripos($coursefileinfo->getFilename(), 'program-of-study') !== false) {
                                print('<hr/>Processing:' . $coursefileinfo->getPathInfo()->getRealPath() . "//{$coursefileinfo->getFilename()}");
                                print ('<br/>');
                                $handle = fopen($coursefileinfo->getPathInfo()->getRealPath() . "//{$coursefileinfo->getFilename()}", "rb");
                                $contents = stream_get_contents($handle);
                                fclose($handle);
                                // load the contents into a (simple) parser
                                $doc = HtmlDomParser::str_get_html($contents);
                                // find the first table. It should contain the listing of units
                                $tables = $doc->find("table");
                                // Find the rows in the first table
                                $rows = $tables[0]->find('tr');
                                $mode = '';
                                // loop through the rows
                                foreach ($rows as $row) {
                                    $cells = $row->find('td');
//                                   // are the following rows core, listed or prescribed?
                                    // this is inelegant, but it works...
                                    if (count($cells) > 0) {
                                        if (stripos($row->plaintext, 'Core Units') !== false) {
                                            $mode = 'core';
                                        }
                                        if (strpos($row->plaintext, 'Listed Units') !== false) {
                                            $mode = 'listed';
                                        }
                                        if (strpos($row->plaintext, 'Prescribed Units') !== false) {
                                            $mode = 'prescribed';
                                        }
                                        if (strpos($row->plaintext, 'Majors') !== false) {
                                            $mode = 'major';
                                        }
                                        if (strpos($row->plaintext, 'Minors') !== false) {
                                            $mode = 'minor';
                                        }
                                    }
                                    // Now, look to see if there's a unit code
                                    if (($mode == 'core') || ($mode == 'listed') || ($mode == 'prescribed')) {
                                        // Because we're only reading the first row, we need to filter out all the rows that aren't courses.
                                        // course are always 2-5 upper case letters followed by numbers
                                        preg_match_all('/\b[A-Z]+[0-9]+\b/', $row->plaintext, $unitresultsarr);

                                        foreach ($unitresultsarr[0] as $item) {
                                            //   print_r($item);
                                            print('Processing unit ' . $item . ' as ' . $mode . '<br/>');
                                            // check that the unit exists in the table. Make a note if it doesn't
                                            if (!(Unit::where('unit_code', '=', $item)->exists())) {
                                                print("<br/><strong>Unit $item not in database!</strong><br/>");
                                            }
                                            // check that the unit exists in the unit table
                                            // get the level
                                            preg_match('/[0-9]/', $item, $results);
                                            // print_r($results);

                                            $unit = Unit::updateOrCreate(['unit_code' => $item],
                                                [
                                                    'level' => $results[0]
                                                ]);
                                            // associate the unit with a course (if it isn't already)
//                                            //@todo use sync for this.
//
//                                            $coreUnitsArr[$unit->id]  = ['relationship' => $mode];
                                            if (!$unit->courses()->where('course_id', '=', $courseid)->exists()) {
                                                $unit->courses()->attach($courseid, ['relationship' => $mode]);
                                            }
                                        }
                                    }
                                }
                            }
                            // perform the course/units sync
                            //    dd(Course::find($courseid));

                            //  Course::find($courseid)->units()->sync($coreUnitsArr);

                            // majors and minors

                            if (($coursefileinfo->getFilename() == 'majors') || ($coursefileinfo->getFilename() == 'minors')) {
                                // loop through the files
                                $field_of_study_folder = new \DirectoryIterator("{$coursefileinfo->getPathInfo()->getRealPath()}\/{$coursefileinfo->getFilename()}");
                                foreach ($field_of_study_folder as $field_of_study_file) {
                                    // make a littel sync array
                                    $fields_of_study_unit_array = [];

                                    if (!$field_of_study_file->isDot()) {
                                        $handle = fopen($field_of_study_file->getPathInfo()->getRealPath() . "\\{$field_of_study_file->getFilename()}", "r");
                                        $contents = stream_get_contents($handle);
                                        fclose($handle);
                                        // load the contents into a (simple) parser
                                        $doc = HtmlDomParser::str_get_html($contents);
                                        $headers = $doc->find("h3");
                                        // sometimes there's weird files in there
                                        if (isset($headers[0])) {
                                            // create/update field of study
                                            $field_of_study = Field_of_study::updateOrCreate(['label' => $headers[0]->plaintext, 'course_id' => $courseid],
                                                ['major_or_minor' => (($coursefileinfo->getFilename() == 'majors') ? 'major' : 'minor'),
                                                    'url' => "{$fileinfo->getFilename()}/{$coursefileinfo->getFilename()}/{$field_of_study_file->getFilename()}"]);
                                            print("Updated field of study {$field_of_study->label} with course ID {$field_of_study->course_id} URL {$field_of_study->url} as {$field_of_study->major_or_minor}<br/>");

                                            $tables = $doc->find("table");
                                            // Find the rows in the first table
                                            $rows = $tables[0]->find('tr');
                                            $mode = '';
                                            // loop through the rows
                                            foreach ($rows as $row) {
                                                $cells = $row->find('td');
//                                   // are the following rows core, listed or prescribed?
                                                // this is inelegant, but it works...
                                                if (count($cells) > 0) {
                                                    if (stripos($row->plaintext, 'Core Units') !== false) {
                                                        $mode = 'core';
                                                    }
                                                    if (strpos($row->plaintext, 'Listed Units') !== false) {
                                                        $mode = 'listed';
                                                    }
                                                    if (strpos($row->plaintext, 'Prescribed Units') !== false) {
                                                        $mode = 'prescribed';
                                                    }
                                                    if (strpos($row->plaintext, 'Majors') !== false) {
                                                        $mode = 'major';
                                                    }
                                                    if (strpos($row->plaintext, 'Minors') !== false) {
                                                        $mode = 'minor';
                                                    }
                                                }
                                                // Now, look to see if there's a unit code
                                                if (($mode == 'core') || ($mode == 'listed') || ($mode == 'prescribed')) {
                                                    // Because we're only reading the first row, we need to filter out all the rows that aren't courses.
                                                    // course are always 2-5 upper case letters followed by numbers
                                                    preg_match_all('/\b[A-Z]+[0-9]+\b/', $row->plaintext, $unitresultsarr);

                                                    foreach ($unitresultsarr[0] as $item) {
                                                        //   print_r($item);
                                                        //print('Processing unit ' . $item . ' as ' . $mode . '<br/>');

                                                        // check that the unit exists in the unit table
                                                        if (!(Unit::where('unit_code', '=', $item)->exists())) {
                                                            print("<br/><strong>Unit $item not in database!</strong><br/>");
                                                        }

                                                        // get the level
                                                        preg_match('/[0-9]/', $item, $results);
                                                        // print_r($results);
                                                        $unit = Unit::updateOrCreate(['unit_code' => $item],
                                                            [
                                                                'level' => $results[0]
                                                            ]);
                                                        // associate the unit with a field of study(if it isn't already)
                                                        //@todo use sync for this.
                                                        //   $fields_of_study_unit_array[$unit->id]  = ['relationship' => $mode,'course_id' => $courseid];
                                                        if (!$unit->fields_of_study()->where('field_of_study_id', '=', $field_of_study->id)->exists()) {
                                                            $unit->fields_of_study()->attach($field_of_study->id, ['relationship' => $mode, 'course_id' => $field_of_study->course_id]);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        // perform unit field_of_study sync
                                        // Field_of_study::find($field_of_study->id)->units()->sync($fields_of_study_unit_array);
                                    }
                                }
                            }


                        }

                    }


                }
            }
        }
        return;
    }



    // maintenance.
    // Update course plans tables, from a directory of files
    // this takes ages to run!
    public function updateCoursePlansTablesFromDirectory($directory, $published_year)
    {

        $i = 0;
        // the courses folder URL
        $folder = 'courseplans' . DIRECTORY_SEPARATOR . $directory;

// iterate through folder for folders
        $dir = new \DirectoryIterator($folder);

        foreach ($dir as $fileinfo) {
            if (!$fileinfo->isDot()) {
                if ($fileinfo->isDir()) {

                    // get course id here
                    if ((Course::where('course_code', $dir->getFilename())->count() > 0)) {
                        $courseid = Course::where('course_code', $dir->getFilename())->first()->id;

                        $coursefolder = new \DirectoryIterator("$folder" . DIRECTORY_SEPARATOR . "{$fileinfo->getFilename()}");
                        foreach ($coursefolder as $coursefileinfo) {
                            if (!$coursefileinfo->isDot()) {

// get course plans here
                                if ((substr($coursefileinfo->getFilename(), 0, 2) === "t1") || (substr($coursefileinfo->getFilename(), 0, 2) === "t2") || (substr($coursefileinfo->getFilename(), 0, 2) === "t3")) {
                                    $commencingtrimester = strtoupper(substr($coursefileinfo->getFilename(), 0, 2));
//dd($trimester);
                                    print('<hr/>opening:' . $coursefileinfo->getPathInfo()->getRealPath() . "//{$coursefileinfo->getFilename()}");
                                    print ('<br/>');

                                    $handle = fopen($coursefileinfo->getPathInfo()->getRealPath() . "//{$coursefileinfo->getFilename()}", "rb");
                                    $contents = stream_get_contents($handle);
                                    fclose($handle);
                                    // load the contents into a (simple) parser
                                    $doc = HtmlDomParser::str_get_html($contents);

                                    // get the field of study (or dummy) name.
                                    $filenameparts1 = explode('.', $coursefileinfo->getFilename());
                                    $filenameparts2 = explode('-', $filenameparts1[0]);
                                    array_shift($filenameparts2);
                                    $fieldofstudy = join(' ', $filenameparts2);

                                    // test to see if it's already in the system. Make a dummy field of study to associate the program of study if it's not
                                    $fieldofstudyid = -1;
                                    if (Field_of_study::whereRaw('label = ?', [$fieldofstudy])->count() < 1) {
                                        $fieldofstudyid = Field_of_study::create(['course_id' => $courseid, 'label' => ucwords($fieldofstudy), 'dummy_for_course_plans' => 'true'])->id;
                                    } else {
                                        $fieldofstudyid = Field_of_study::whereRaw('label = ?', [$fieldofstudy])->first()->id;
                                    }


                                    // get all H3's. These are section titles
                                    $headings = $doc->find("h3");
                                    //dd($headings);
                                    foreach ($headings as $heading) {
                                        // var_dump($heading->plaintext);
                                        // find the ones that says 'Full-time' or 'Part-time'
                                        if ((strpos($heading->plaintext, 'Full-Time') !== false) || (strpos($heading->plaintext, 'Part-Time') !== false)) {
                                            $load = (strpos($heading->plaintext, 'Full-Time') !== false) ? 'full' : 'part';
                                            print ("Found a $load plan<br/>");
                                            $nextsibling = $heading->next_sibling();

                                            //build the course plan root entry
                                            $courseplan = Course_plan::updateOrCreate(['url' => $coursefileinfo->getFilename(),
                                                'course_id' => $courseid,
                                                'published_year' => $published_year,
                                                'load' => $load,
                                                'commencing_teaching_period_lookup_id' => Teaching_period_lookup::where('teaching_period', $commencingtrimester)->first()->id,
                                                'field_of_study_id' => $fieldofstudyid]);

                                            // loop through the table rows
                                            while ($nextsibling = $nextsibling->next_sibling()) {
                                                //  var_dump($nextsibling->tag);

                                                if ($nextsibling->tag == 'table') {

                                                    print("Found a $load table!<br/>");

                                                    // parse the table here
                                                    $rows = $nextsibling->find('tr');
                                                    //dd(count($rows));
                                                    // there are sometimes an orphaned, ghost table. We're looking at something with at least a couple rows
                                                    // see t1-early-childhood-education.html for an example of this
                                                    if (count($rows) > 2) {

                                                        $oldprogramyear = 0;
                                                        $programyear = 0;
                                                        // the rowspan for a year

                                                        // how many blank cells with this rowspan are there?
                                                        $preceedingcells = 0;
                                                        foreach ($rows as $row) {
                                                            $cells = $row->find('td');
                                                            //  print("<br/>There are " . count($cells) . " cells in this row <br/>");
                                                            // does the first cell contain a single number? If so, it's a program year.

                                                            if (count($cells) > 0) {

                                                                //print("0th cell says:{$cells[0]->plaintext}<br/>");

                                                                // regexp for a single number *without* words around it
                                                                preg_match('/(?<!\S)\d{0,2}$/', $cells[0]->plaintext, $results);
                                                                // if there's a result, update year
                                                                $programyear = isset($results[0]) ? $results[0] : $programyear;
                                                                //Has there been a new program year just then set?
                                                                $newprogramyear = ($oldprogramyear != $programyear);
                                                                $oldprogramyear = $programyear;
                                                                print($newprogramyear ? '<br/>New year:' . $programyear . '<br/><br/>' : '');
                                                                if ($newprogramyear) {
                                                                    $i = 1;
                                                                    $preceedingcells = 0;
                                                                    // how many of the first cells in this row have rowspans? There's *so* got to be a better way of doing this
                                                                    foreach ($cells as $cell) {
                                                                        if (isset($cell->rowspan)) {
                                                                            $preceedingcells++;
                                                                        } else {
                                                                            break(1);
                                                                        }
                                                                    }
                                                                    //$preceedingcells = count($row->find('td[rowspan]'));
                                                                    // print("We have $preceedingcells cells with rowspans in this row <br/>");
                                                                    $courseplanyearid = Course_plan_year::updateOrCreate(['course_plan_id' => $courseplan->id,
                                                                        'year_of_study' => $programyear])->id;
                                                                } else {
                                                                    $i = $preceedingcells - 1;
                                                                }
                                                            }
                                                            //$i = 0;
                                                            foreach ($cells as $cell) {

                                                                if (count($cells) > 0) {

                                                                    // UPDATE we also need to check that it's not a notes cell
                                                                    if (!$cell->class == 'notes-text') {

//                                                                    // get an offset. If there is a year set in this row, the 1th cell is T1, if not it's the 0th. Years are set in rows of 4 columns (maybe?)
                                                                        if ($newprogramyear) {
                                                                            $trimester = $i - 1;
                                                                            print("Looking at trimester $trimester in year $programyear<br/>");
                                                                            //  $programyearrowspancounter++;
                                                                        } else {
                                                                            $trimester = $i + 1;
                                                                            // print("<br/>Looking at trimester $trimester <br/>");

                                                                        }

//                                                                    // horrid little hack to account for a 0 trimester that turns up
                                                                        if (($trimester > 0) && ($trimester < 4)) {

                                                                            // if we're in the first row
                                                                            // if ($programyearrowspancounter == 1) {
                                                                            $trimesterlookupid = Teaching_period_lookup::where('teaching_period', 'T' . $trimester)->first()->id;
//                                                                        // test to see if the cell contains a unit code
                                                                            //     print("<br/>Inspecting:{$cell->plaintext} <br/>");
                                                                            // Unit codes have uppercase letters followed by numbers
                                                                            preg_match_all('/\b[A-Z]+[0-9]+\b/', $cell->plaintext, $unitresultsarr);
//                                                                        // loop through results
                                                                            foreach ($unitresultsarr[0] as $item) {
                                                                                // filter out anything that fits the pattern, but is smaller (e.g. T1, T2)- see ADTPF//t1-secondary-education-english.html for an example of this
                                                                                if (strlen($item) > 4) {

                                                                                    print('Adding ' . (count($unitresultsarr[0]) > 1 ? 'optional' : 'mandatory') . ' unit ' . $item . ' to year ' . $programyear . ' trimester ' . $trimester . '<br/>');

                                                                                    if (Unit::where('unit_code', $item)->count() > 0) {

                                                                                        $unitid = Unit::where('unit_code', $item)->first()->id;
                                                                                        // print('Adding unit ' . $item . ' to year ' . $year . ' trimester ' . $trimester);
                                                                                        //print ('<br/>');
                                                                                        Course_plan_year_units::updateOrCreate(['course_plan_year_id' => $courseplanyearid,
                                                                                            'teaching_period_lookup_id' => $trimesterlookupid,
                                                                                            'unit_id' => $unitid,
                                                                                            'mandatory' => (count($unitresultsarr[0]) > 1 ? null : 'true')]);
                                                                                    } else {
                                                                                        //TODO dump into an error log file here
                                                                                        print('<br/><strong>No matching unit found with the code ' . $item . '</strong> for course ' . $dir->getFilename() . ' for field of study ' . $fieldofstudy . ' year ' . $programyear . ' trimester ' . $trimester . '<br/>');
                                                                                    }
                                                                                }
//}
                                                                            }
                                                                        }

                                                                    }
                                                                    $i++;
                                                                }
                                                            }

                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        }


                                    }
                                    //dd($headings);
                                    //  return;
                                    // }
                                }
                                //   print('<br/>');
                                // return;
                            }
                        }

                    } else {
                        print('<br/><strong>No matching course found with the code ' . $dir->getFilename() . '</strong><br/>');
                    }
                }
            }
        }
        return;

    }

    // check that units have an online component
    public function updateUnitOfferingFromCauc($year)
    {
        $units = Unit::all();
        $testcount = 0;
        $count = 0;
        $failcount = 0;
        foreach ($units as $unit) {
            //if ($testcount < 5) {
            //  print("Updating {$year} {$unit->unit_code} from <br/>");
            // Opportunisticlally update the level
//            preg_match('/[0-9]/', $unit, $results);
//            $unit->save(['level' => $results[0]]);
            // get the JSON for the unit from CAUC
            $url = "https://my.une.edu.au/courses/{$year}/units/{$unit->unit_code}.json";
            print("Updating {$year} {$unit->unit_code} from $url<br/>");
            try {
                $str = file_get_contents($url);
                $json = json_decode($str, true);
                //dd($json['offerings'][0]['studyMode']['name']);
                // get the offerings
                foreach ($json['offerings'] as $offering) {
                    //dd(Teaching_period::where(['year' => $year, 'teaching_period_lookup_id' => Teaching_period_lookup::where('cauc_label', '=', $offering['teachingPeriod']['name'])->first()->id])->first()->id);
                    // build the online parameters
                    $onlineparams = [];
                    if ($offering['studyMode']['name'] == 'ON') {
                        $onlineparams['oncampus'] = 'true';
                    }
                    if ($offering['studyMode']['name'] == 'OL') {
                        $onlineparams['online'] = 'true';

                    }
//                        $onlineparams['online'] = 'true';
//                        $onlineparams['oncampus'] = 'true';
                    //dd($onlineparams);
                    // create (or update) the offering.
                    $offeringinstance = Offering::updateOrCreate(['unit_id' => $unit->id,
                        'teaching_period_id' => Teaching_period::where(['year' => $year, 'teaching_period_lookup_id' => Teaching_period_lookup::where('cauc_label', '=', $offering['teachingPeriod']['name'])->first()->id])->first()->id
                    ],
                        $onlineparams);

                    //  dd($offeringinstance);
                }
                $count++;
            } catch (\Exception $err) {
                // @TODO handle this better
                //  dd($err);
                $failcount++;
                print("Could not get CAUC data for {$year} {$unit->unit_code} because {$err->getMessage()} at line {$err->getLine()}<br/>");

            }
            // }
            //$testcount++;
        }
        print("<br/>Total success: $count");
        print("<br/>Total fail: $failcount");
    }


}
