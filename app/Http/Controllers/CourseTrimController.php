<?php

namespace App\Http\Controllers;

use App\Course;
use App\Course_trim;
use App\Field_of_study;
use Illuminate\Database\Eloquent\Collection;
use App\Fieldofstudy;
use App\Unit;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CourseTrimController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return;
    }

    public function show($id)
    {
        return;
    }


    public function update(Request $request)
    {
        return;
    }

    public
    function destroy(\Illuminate\Http\Request $request)
    {
        $input = $request->all();

        return array(
            'status' => (\App\Course_trim::destroy($input['id']) ? '0' : '1')
        );
    }


}
