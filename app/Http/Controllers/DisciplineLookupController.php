<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DisciplineLookupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return null;
    }

    public function search(Request $request)
    {
        $input = $request->all();

        $results = \App\Discipline_lookup::where('description',  'like', "%{$input['searchstr']}%");

        return $results->id;
    }

    public function create(Request $request)
    {
        $input = $request->all();
        $newentry = \App\Discipline_lookup::create($input);
        return $newentry->id;
    }

    /**
     * Updates the record
     * @param $id
     * @param Request $request
     * @return $this
     */
    public function update(Request  $request)
    {

        $input = $request->all();
        $contact = \App\Discipline_lookup::findOrNew($input['id']);

        $status = strval($contact->update($input));
        //dd($input);
        $response = array(
            'status' => $status,
        );
        return $response;
    }


    public function store(Request $request)
    {
        $input = $request->all();
        return \App\Discipline_lookup::create($input)->id;
    }

    public function show($id)
    {
        return \App\Discipline_lookup::findOrFail($id);
    }


    public function destroy(\Illuminate\Http\Request $request)
    {
        $input = $request->all();
        return \App\Discipline_lookup::destroy($input['id']);
    }
}
