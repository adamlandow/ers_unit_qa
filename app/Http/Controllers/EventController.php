<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Controllers\Auth\AuthController;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class EventController extends Controller
{

    //
    public function index()
    {
        $events = Event::all();
        return null;
    }

    // list the events
    public function listEvents(Request $request)
    {

    }

    // return data about a given event
    public function show($id)
    {
        if (Gate::denies('is_admin')) {
            return 0;
        }
        return \App\Event::findOrFail($id);
    }

    public function ajaxstore(Request $request)
    {
        $input = $request->all();
        $input['duration'] = ($input['end_timestamp'] / 1000) - ($input['start_timestamp'] / 1000);
        $input['start_timestamp'] = Carbon::createFromTimestamp($input['start_timestamp'] / 1000, 'Australia/Sydney');
        $event = Event::create($input);

        $response = array(
            'status' => $event->id,
        );
        return $response;
    }

    /**
     * @param Requests\UserRequest $request
     * @return array
     */
    public function ajaxupdate(Request $request)
    {
        $input = $request->all();
        // fix the timestamp coming in from the calendar

        $id = $input['id'];
        $starttime = $input['start_timestamp'] / 1000;
        $input['duration'] = ($input['end_timestamp'] / 1000) - $starttime;
        $input['start_timestamp'] = Carbon::createFromTimestamp($starttime, 'Australia/Sydney');
        //dd($input);
        $event = Event::findOrFail($id);
        unset($input['id']);
        $status = $event->update($input);
        $response = array(
            'status' => $status,
        );
        return $response;
    }

    public function ajaxdestroy(\Illuminate\Http\Request $request)
    {

        $input = $request->all();
        //dd($input);
        $response = array(
            'status' => \App\Event::destroy($input['id']),
        );
        return $response;
    }


}
