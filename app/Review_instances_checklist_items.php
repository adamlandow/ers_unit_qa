<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review_instances_checklist_items extends Model
{
    //
    use SoftDeletes;

    protected $table = 'review_instance_checklists_items';

    protected $fillable = [
        'review_instances_id', 'description', 'text', 'order', 'coordinator_status', 'peer_reviewer_status', 'ld_status', 'template_text', 'coordinator_text', 'peer_reviewer_text', 'ld_text', 'heading',  'trigger_email', 'trigger_email_id', 'email_to_id'
    ];

    public function review_instance()
    {
        return $this->belongsTo('App\Review_instances', 'review_instances_id', 'id');
    }

    public function trigger_email_template()
    {
        return $this->belongsTo('App\Emails_template', 'trigger_email_id', 'id');
    }

    public function email_to()
    {
        return $this->belongsTo('App\User', 'email_to_id', 'id');
    }

}
