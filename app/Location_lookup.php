<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location_lookup extends Model
{
    //
    // define the database table that this model refers to
    use SoftDeletes;

    protected $table = "location_lookup";

    protected $fillable = [
        'text','url',
    ];
}
