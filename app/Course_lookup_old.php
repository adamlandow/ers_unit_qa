<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Course_lookup extends Model
{
    use SoftDeletes;
    use Sortable;

    protected $table = 'course_lookup';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text', 'discipline_id', 'trim_reference',
    ];

    /**
     * The attributes that are sortable.
     *
     * @var array
     */
    protected $sortable = ['text', 'discipline_id'];

    public function discipline()
    {
        return $this->belongsTo('App\Discipline_lookup', 'discipline_id', 'id');
    }

    public function units()
    {
        return $this->belongsToMany('App\Units', 'units_course_lookup');
    }
}
