<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offering_media extends Model
{
    use SoftDeletes;

        /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    // the table this model refers to. I think I'll be explicit with this by default.
    protected $table = 'offering_media';

    //the fillable fields in the patients database. Protect against malicious code
    protected $fillable = [
         'name', 'type','size', 'label', 'description', 'path',
    ];

    /**
     * A media record is owned by a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offering(){
        return $this->belongsTo('App\Offering', 'offering_id', 'id');
    }

}
