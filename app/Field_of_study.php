<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Field_of_study extends Model
{
    protected $table = 'field_of_study';
    use SoftDeletes;

    protected $fillable = [
        'course_id', 'label', 'description', 'url', 'trim_reference','trim_container', 'major_or_minor'
    ];

    public function units()
    {
        return $this->belongsToMany('App\Unit', 'unit_course_field_of_study')->withPivot('course_id', 'relationship');
    }

    public function trim()
    {
        return $this->hasMany('App\Field_of_study_trim', 'field_of_study_id');
    }

}
