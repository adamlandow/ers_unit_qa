<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Olx_media extends Model
{
    use SoftDeletes;

    protected $table = 'olx_media';

    //the fillable fields in the patients database. Protect against malicious code
    protected $fillable = [
       'olx_id', 'name', 'type','size', 'label', 'description', 'path',
    ];

    /**
     * A media record is owned by a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offering(){
        return $this->belongsTo('App\Olx', 'olx_id', 'id');
    }
}
