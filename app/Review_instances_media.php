<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review_instances_media extends Model
{
    use SoftDeletes;

        /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    // the table this model refers to. I think I'll be explicit with this by default.
    protected $table = 'review_instances_media';

    //the fillable fields in the patients database. Protect against malicious code
    protected $fillable = [
        'review_instances_id', 'name', 'type','size', 'label', 'description', 'path',
    ];

    /**
     * A media record is owned by a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function review_instance(){
        return $this->belongsTo('App\Review_instances', 'review_instances_id', 'id');
    }

}
