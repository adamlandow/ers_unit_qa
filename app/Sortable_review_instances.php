<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Sortable_review_instances extends Model
{

    use Sortable;

    protected $table = 'sortable_review_list';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    /**
     * The attributes that are sortable.
     *
     * @var array
     */
    protected $sortable = [  'reviewed_unit_instance_id', 'start_date', 'end_date', 'is_template', 'live', 'ratio'];

//    public function unit()
//    {
//        return $this->belongsTo('App\Units', 'units_id', 'id');
//    }

    public function reviewed_offering()
    {
        return $this->belongsTo('App\Offering', 'reviewed_offering_id', 'id');
    }



    public function getStartDateAttribute($value)
    {

        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('j/m/Y');

    }



    public function getEndDateAttribute($value)
    {

        return Carbon::createFromFormat('Y-m-d H:i:s', $value)->format('j/m/Y');

    }

    public function peer_reviewer()
    {
        return $this->belongsTo('App\User', 'peer_reviewer_id', 'id');
    }

    public function ld()
    {
        return $this->belongsTo('App\User', 'ld_id', 'id');
    }


    public function override_coordinator()
    {
        return $this->belongsTo('App\User', 'override_coordinator_id', 'id');
    }

    // filter- a way of finding coordinators
    public function scopeCoordinators($query, $coordinatorid)
    {
        $query
            ->where(function ($query) use ($coordinatorid) {
                $query->whereHas('reviewed_offering', function ($q) use ($coordinatorid) {
                    // $q->whereHas('conditions_lookup', function ($q2) use ($params) {
                    $q->whereIn('coordinator_id', $coordinatorid);
                    //});
                });
            });
        return $query;
    }

    // filter- a way of finding any reviews associated with the currently logged on user
    public function scopeReviewsforuser($query, $userid)
    {

        $query
            // coordinators
            ->where(function ($query) use ($userid) {

                $query->whereHas('reviewed_offering', function ($q) use ($userid) {
                    // $q->whereHas('conditions_lookup', function ($q2) use ($params) {

                    $q->whereIn('coordinator_id', $userid);
                    //});
                });
            })
            //override coordinators
            ->orWhere(function ($query) use ($userid) {
                $query->where('override_coordinator_id', $userid);
            })
            // peer reviewers
            ->orWhere(function ($query) use ($userid) {
                $query->where('peer_reviewer_id', $userid);
            })
            // learning designers
            ->orWhere(function ($query) use ($userid) {
                $query->where('ld_id', $userid);
            });
        return $query;
    }



}
