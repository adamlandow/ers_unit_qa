<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Review_instances_outcomes extends Model
{

    protected $table = 'review_instances_outcomes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'review_instances_id',
        'unit_code',
        'unit_title',
        'combined_units',
        'credit_points',
        'description',
        'prerequisites',
        'corequisites',
        'offerings',
        'coordinators',
        'new_unit_code',
        'new_unit_title',
        'new_combined_units',
        'new_credit_points',
        'new_description',
        'new_prerequisites',
        'new_corequisites',
        'new_offerings',
        'new_coordinators',
        'learning_outcomes',
        'assessment_tasks'
    ];


    public function review_instances()
    {
        return $this->belongsTo('App\Review_instances', 'review_instances_id', 'id');
    }

    public function review_instances_outcomes_learning_outcomes()
    {
        return $this->hasMany('App\Review_instances_outcomes_learning_outcomes', 'review_instances_outcomes_id', 'id');
    }

    public function review_instances_outcomes_assessment_tasks()
    {
        return $this->hasMany('App\Review_instances_outcomes_assessment_tasks', 'review_instances_outcomes_id', 'id');
    }

}
