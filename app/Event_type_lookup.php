<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event_type_lookup extends Model
{
    //
    // define the database table that this model refers to
    use SoftDeletes;

    protected $table = "event_type_lookup";

    protected $fillable = [
        'text', 'color', 'alarm_prior_secs',
    ];
}
