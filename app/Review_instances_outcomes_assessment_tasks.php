<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Review_instances_outcomes_assessment_tasks extends Model
{

    protected $table = 'review_instances_outcomes_assessment_tasks';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'review_instances_outcomes_id',
        'must_complete',
        'component',
        'exam_length',
        'weight',
        'mode',
        'word_count',
        'notes',
        'relates_to_lo',
    ];





    public function review_instances_outcomes()
    {
        return $this->belongsTo('App\Review_instances_outcomes', 'review_instances_outcomes_id', 'id');
    }



}
