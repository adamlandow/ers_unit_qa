<?php

namespace App\Jobs;

use App\Emails_log;
use App\Mail\UnitSetupEmail;
use App\Offering;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class SendSetupEmail implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $template;
    protected $unitid;
    protected $to_id;
    protected $from_id;
    protected $testing;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UnitSetupEmail $email, $to_id, $from_id, $template, $unitid)
    {
        $this->email = $email;
        $this->template = $template;
        $this->unitid = $unitid;
        // send to back to sender if dev
        $this->to_id = (\config('app.env') == 'dev') ? $from_id : $to_id;
        $this->from_id = $from_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Mail::to(User::find($this->to_id)->email)->send($this->email);
        $log = new Emails_log();
        $log->context = 'unitsetup';
        $log->email_id = $this->template['id'];
        $log->instance_id = $this->unitid;
        $log->sent_to_id = $this->to_id;
        $log->sent_by_id = $this->from_id;

        $log->fulltext = $this->email->getfulltext();
        $log->save();
    }
}
