<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;


class Offering extends Model
{
    use SoftDeletes;
    use Sortable;

    protected $fillable = [
    'unit_id',
        'coordinator_id',
        'eso_id',
        'teaching_period_id',
        'archived_at',
        'moodle_url',
        'is_template',
        'label',
        'follow_up',
        'follow_up_text',
        'cauc_id',
        'status',
        'complete_percent',
        'has_olx',
        'is_accredited'
];

    protected $table = 'offering';

    /**
     * The attributes that are sortable.
     *
     * @var array
     */
    protected $sortable = ['unit_id', 'coordinator_id', 'eso_id', 'teaching_period_id',  'archived_at', 'follow_up', 'status'];

    public function coordinator()
    {
        return $this->belongsTo('App\User', 'coordinator_id', 'id');
    }



    public function eso()
    {
        return $this->belongsTo('App\User', 'eso_id', 'id');
        // for later: return $this->belongsToMany('App\User', 'offering_eso');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id', 'id');
    }

    public function reviewed_instance()
    {
        return $this->hasMany('App\Review_instances', 'reviewed_unit_instance_id', 'id');
    }

    public function teaching_period()
    {
        return $this->belongsTo('App\Teaching_period', 'teaching_period_id', 'id');
    }

    public function teachers()
    {
        return $this->belongsToMany('App\User', 'offering_teachers')->withPivot('role');
    }


    // Tracking combined units: many to many relationship with myself
    // Based on https://github.com/laravel/framework/issues/441
    public function combined_units(){

        return $this->belongsToMany('App\Offering', 'offering_offering', 'unit_id', 'combined_unit_id');
    }

    public function units_combined(){

        return $this->belongsToMany('App\Offering', 'offering_offering', 'combined_unit_id', 'unit_id');
    }


    public function checklist()
    {
        return $this->hasMany('App\Offering_checklist_items', 'offering_id', 'id')->orderBy('order');
    }

    public function media()
    {
        return $this->hasMany('App\Offering_media', 'offering_id', 'id');
    }

    public function getChecklistcompleteAttribute()
    {
        return $this->checklist()->get()->where('heading', '<>', '1')->where('status', '1')->count();
    }

    public function events(){
        return $this->hasMany('App\Event', 'offering_id', 'id');
    }

    public function GetEmailLogAttribute(){
        return Emails_log::where(['instance_id'=> $this->id, 'context'=>'offeringsetup'])->get();
    }

    // is this currently under review?
    public function GetUnderReviewAttribute(){
        return Review_instances::where('reviewed_offering_id', $this->id)->exists();
    }

    public function GetLastReviewedAtAttribute(){
        return Review_instances::where('reviewed_offering_id', $this->id)->exists();
    }


    // filter- a way of finding any units with the currently logged on user as coordinator
    public function scopeUnitsforuser($query, $userid)
    {
        $query
            // coordinators
            ->where(function ($query) use ($userid) {
                $query->where('coordinator_id', $userid);
            });

        return $query;
    }

    public function scopeUnitsforeso($query, $userid)
    {
        $query
            // coordinators
            ->where(function ($query) use ($userid) {
                $query->where('eso_id', $userid);
            });

        return $query;
    }

    public function scopeUnitsforcoordinatororeso($query, $userid)
    {
        // dd($userid);
        $query
            // coordinators
            ->where(function ($query) use ($userid) {
                $query->where('eso_id', $userid);

            })
            ->orWhere(function ($query) use ($userid) {
                $query->where('coordinator_id', $userid);

            });
        return $query;
    }

    // filter- a way of finding any units with the currently logged on user as coordinator
    public function scopeExcludeTemplates($query)
    {
        $query
            // is a template
            ->where(function ($query)  {
                $query->where('is_template', '<>', 'true');
            });

        return $query;
    }

    // filter- a way of finding any units with the currently logged on user as coordinator
    public function scopeHasUnits($query)
    {
        $query
            // is a template
            ->where(function ($query)  {
                $query->where('is_template', '<>', 'true');
            });

        return $query;
    }

    // filter- a way of finding any units with a given school
    public function scopeHasSchool($query, $schoolid)
    {
        $query
            ->where(function ($query) use ($schoolid) {
                $query->whereHas('unit', function ($q) use ($schoolid) {
                    $q->whereHas('discipline', function ($q2) use ($schoolid) {
                        $q2->where('school_id', '=', $schoolid);
                    });
                });
            });
       // dd($query->toSql());
        return $query;
    }

    // filter- a way of finding any units with a given school


    // filter- a way of finding any units with the currently logged on user as coordinator
//    public function scopeExcludeCombinedUnits($query)
//    {
//        $query
//            // is a combined unit
//            ->where(function ($query)  {
//                $query->doesntHave('units_combined', function ($q){
//                    $q->whereNotIn('combined_unit_id', $this->id);
//                });
//            });
//
//        return $query;
//    }
}
