<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field_of_study_trim extends Model
{
    // // the table this model refers to.
    protected $table = 'field_of_study_trim';

    protected $fillable = [
        'reference', 'field_of_study_id', 'version'
    ];

    public function field_of_study(){
        return $this->belongsTo('App\Field_of_study');
    }
}
