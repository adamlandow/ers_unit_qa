<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discipline extends Model
{
    protected $table = 'discipline';

    protected $fillable = [
        'code',  'description', 'school_id'
    ];

    public function units()
    {
        return $this->hasMany('App\Unit', 'discipline_id', 'id');
    }

    public function school()
    {
        return $this->hasOne('App\School', 'id', 'school_id');

    }

}
